<?php

class Menu extends EloquentAdmin {
    
    protected $hidden = array();
    protected $guarded = array('id');
    
    public static $rules = array(
        'name' => 'required|min:3|max:255',
        'key' => 'required|min:3|max:255|unique:menus,key',
        'depth' => 'integer|max:99',
    );
    
    public function categories()
    {
        return $this->hasMany('Category');
    }
    
    public function categoriesList()
    {
        $categories = $this->categories()->with(array('articles' => function($q){
            $q->active()->orderBy('created_at');
        }))->active()->whereHas('articles', function($q) {
            $q->active();
        })->get(array('id', 'name', 'annotation', 'slug'));;
        
        
        return $categories;
    }
    
    public static function scopeOfKey($query, $key)
    {
        return $query->where('key', '=', $key);
    }
    
    public static function scopeMainCategories($query, $key)
    {
        return $query->ofKey($key)->first()
                ->categories()->where('parent_id', '=', 0)->active()
                ->orderBy('sort', 'ASC');
    }
    
    public static function getMenu($key, $status = -1)
    {
        return Cache::rememberForever('menu_' . $key . '_' . $status, function() use($key, $status) {
            if($status >= 0) {
                return Menu::with(array('categories' => function($query) use($key, $status){
                    $query->where('status', $status);
                }))->ofKey($key)->first();
            }
            return Menu::with('categories')->ofKey($key)->first();
        });
    }
    
    public function clearMenuCache()
    {
        Cache::forget('menu_' . $this->key);
        Cache::forget('menu_' . $this->key . '_-1');
        Cache::forget('menu_' . $this->key . '_0');
        Cache::forget('menu_' . $this->key . '_1');
    }
    
    
    public static function getAll($params = array())
    {
        $data =  self::leftJoin('categories', 'menus.id', '=', 'categories.menu_id')
                ->groupBy('menus.id')
                ->get(array('menus.id', 'menus.name', 'menus.key', 'menus.depth', 
                    'menus.updated_at', DB::raw('count(categories.id) as count') ));
        
        return $data;
    }
    
}