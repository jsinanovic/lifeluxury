<?php

class Module extends EloquentAdmin {

    protected $guarded = array('id');
    public $timestamps = false;
    public static $rules = array(
        'name' => 'required|min:2|max:255',
        'model' => 'required|min:2|max:255',
        'uri' => 'required|min:2|max:255',
        'access_level' => 'integer',
        'migration' => '',
        'status' => 'integer',
    );

    public static function getAll($params = array()) {
        return self::get(array(
            'id', 'name', 'model', 'uri', 'access_level', 'status'
        ));
    }

    public function getDefaultRoutes() {
        if (!$this->status)
            return;
        
        if (class_exists($this->model)) {
            Route::model($this->model, $this->model);

            Route::get($this->uri . '/edit/{' . $this->model . '}', $this->name . 'Controller@getEdit');

            if($this->name == 'Categories') {
                Route::post($this->uri . '/edit/{' . $this->model . '}', $this->name . 'Controller@postEdit');
            } else {
                Route::post($this->uri . '/edit/{' . $this->model . '}', array(
                    'before' => 'csrf',
                    'uses' => $this->name . 'Controller@postEdit')
                );
            }
            
        } else {
            //echo 'Routing class of model does not exists: ';
            //dd($this->model, $this);
        }

        if (class_exists($this->name . 'Controller')) {
            Route::controller($this->uri, $this->name . 'Controller');
        } else {
            //echo 'Routing class of controller does not exists: ';
            //dd($this->name . 'Controller', $this);
        }
    }

}
