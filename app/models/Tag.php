<?php

class Tag extends EloquentAdmin {
    
    //protected $hidden = array('type');
    protected $guarded = array('id');
    public $timestamps = false;
    public static $isSlug = true;
    public static $rules = array(
        'name' => 'required|min:2|max:255',
        'slug' => 'required|min:2|max:255',
        'type' => 'required|min:2|max:20',
        'count' => 'integer'
    );
    
    public static function scopeOfType($query, $type)
    {
        return $query->where('type', '=', $type);
    }
    
    public static function popular($count)
    {
        return self::orderBy('count', 'DESC')
                ->orderBy('name', 'ASC')
                ->take($count)
                ->get();
    }
    
    public static function getUniqueSlug($title, $data)
    {
        $title = strtolower(trim($title));
        if (empty($title))
            return '';

        $slug = Slugify::slugify($title);
        
        if (static::where('slug', '=', $slug)->where('type', '=', $data['type'])->count() > 0) {
            
            $entry = static::where('slug', 'LIKE', $slug . '-%')
                    ->where('type', '=', $data['type'])
                    ->orderBy('slug', 'DESC')
                    ->first();

            if ($entry) {
                $arr = explode('-', $entry->slug);
                $max = (int) last($arr);
                $slug = $slug . '-' . ($max + 1);
            } else {
                $slug = $slug . '-1';
            }
            $slug = static::getUniqueSlug($slug, $data);
        }

        return $slug;
    }
    
    public static function add($name, $type)
    {
        $slug = Slugify::slugify($name);
        $tag = Tag::where('slug', '=', $slug)->where('type', '=', $type)->first();
        if(!$tag) {
            $tag = Tag::create(Tag::modifyData(array(
                'name' => $name,
                'type' => $type,
            )));
        }
        
        return $tag;
    }
    
    public function articles()
    {
        return $this->belongsToMany('Article');
    }
    
}