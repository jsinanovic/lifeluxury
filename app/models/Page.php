<?php

class Page extends EloquentAdmin {
    
    use \Dimsav\Translatable\Translatable;
    
    public $translatedAttributes = array('name', 'subtitle', 'content', 'title', 'keywords', 'description');
    
    //protected $hidden = array('type');
    protected $guarded = array('id');
    protected $with = array('translations');
    
    public static $isSlug = true;
    public static $isSort = true;
    public static $isAssign = true;
    public static $uploadFolder = 'pages';
    public static $rules = array(
        'name' => 'required|min:3|max:255',
        'slug' => 'required|min:3|max:255|unique:pages',
        'key' => 'required|min:3|max:50',
        'subtitle' => 'min:3|max:500',
        'content' => '',
        'title' => 'min:3|max:255',
        'keywords' => 'min:3|max:500',
        'description' => 'min:3|max:500',
        'type' => '',
        'status' => '',
        'sort' => '',
    );
    
    public static function scopeActive($query)
    {
        return $query->where('status', '=', 1);
    }
    
    public static function getAll($params = array())
    {
        return self::get(array(
            'id', 'name', 'slug', 'created_at', 'updated_at', 'sort', 'status',
        ));
    }
    
    public function category()
    {
        return $this->hasOne('Category');
    }
    
    
    protected static function getPagesKeys()
    {
        $keys = array();
        $values = Setting::values('pages-keys');
        foreach($values as $value)
            $keys[$value] = $value;
        
        return $keys;
    }
    
    protected static function getActivePageByKey($key)
    {
        return Cache::rememberForever('active_page_key_' . $key, function() use ($key) {
            return Page::with('translations')->where('key', '=', $key)->active()->first();
        });
    }
    
    public function clearEntryCache($event) {
        parent::clearEntryCache($event);
        Cache::forget('active_page_key_' . $this->key);
    }
    
}