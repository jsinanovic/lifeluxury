<?php

use Carbon\Carbon;

class Work extends EloquentAdmin {
    
    use \Dimsav\Translatable\Translatable;
    
    public $translatedAttributes = array('name', 'annotation', 'content', 'features', 'title', 'keywords', 'description');
    
    protected $guarded = array('id');
    public $timestamps = true;
    protected $with = array('translations');
    
    public static $isSlug = true;
    public static $isAssign = true;
    public static $hasComments = true;
    public static $imageField = 'image';
    public static $uploadFolder = 'works';
    public static $imageSizes = array(
        'original' => array(
            'width' => 600, 
            'height' => 'auto'
        ),
        'thumb' => array(
            'width' => 320,
            'height' => 280
        ),
        'icon' => array(
            'width' => 50,
            'height' => 50
        )
    );
    //public static $tagType = 'works';
    public static $rules = array(
        'name' => 'required|min:3|max:255',
        'slug' => 'required|min:3|max:255|unique:works',
        'image' => 'image|max:5120',
        'annotation' => 'min:3|max:500',
        'content' => '',
        'features' => '',
        'count_likes' => 'integer',
        'count_views' => 'integer',
        'status' => 'integer',
        'title' => 'min:3|max:255',
        'keywords' => 'min:3|max:500',
        'description' => 'min:3|max:500',
    );
    
    
    public static function getAll($params = array())
    {
        return self::get(array(
            'id', 'image', 'name', 'slug', 'created_at', 'updated_at', 'image', 'status',
        ));
    }
    
    public function getCreatedAtAttribute($value)
    {
        $c = Carbon::parse($value);
        return $c->day . ' ' . self::$months[$c->month - 1] . ' ' . $c->year;
    }
    
    
    public function getImage($type = 'original')
    {
        $image = parent::getImage($type);
        if(!$image && $type == 'thumb') {
            $image = URL::to('front/images/no_image.jpg');
        } 
        
        return $image;
    }
    
    public function shares()
    {
        return $this->morphMany('Share', 'shareable');
    }
    
    
    public function comments()
    {
        return $this->morphMany('Comment', 'commentable');
    }
    
}