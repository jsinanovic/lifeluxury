<?php

class ArticleTranslation extends Eloquent {
    
    public $timestamps = false;
    protected $fillable = ['name', 'annotation', 'content', 'title', 'keywords', 'description'];
    
}