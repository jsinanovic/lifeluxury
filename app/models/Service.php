<?php

use Carbon\Carbon;

class Service extends EloquentAdmin {

    use \Dimsav\Translatable\Translatable;

    public $translatedAttributes = array('name', 'annotation', 'content', 'features', 'title', 'keywords', 'description');
    protected $guarded = array('id');
    public $timestamps = true;
    protected $with = array('translations');
    public static $isSlug = true;
    public static $isAssign = true;
    public static $hasComments = true;
    public static $imageField = 'image';
    public static $uploadFolder = 'services';
    public static $imageSizes = array(
        'original' => array(
            'width' => 1680,
            'height' => 'auto'
        ),
        'icon' => array(
            'width' => 100,
            'height' => 100
        )
    );
    //public static $tagType = 'works';
    public static $rules = array(
        'name' => 'required|min:3|max:255',
        'slug' => 'required|min:3|max:255|unique:services',
        'image' => 'image|max:5120',
        'annotation' => 'min:3|max:500',
        'content' => '',
        'features' => '',
        'count_likes' => 'integer',
        'count_views' => 'integer',
        'status' => 'integer',
        'title' => 'min:3|max:255',
        'keywords' => 'min:3|max:500',
        'description' => 'min:3|max:500',
    );

    public static function getAll($params = array())
    {
        return self::get(array(
                    'id', 'image', 'name', 'slug', 'created_at', 'updated_at', 'image', 'status',
        ));
    }

    public function getCreatedAtAttribute($value)
    {
        $c = Carbon::parse($value);
        return $c->day . ' ' . self::$months[$c->month - 1] . ' ' . $c->year;
    }

    /*public function getImage($type = 'original')
    {
        $image = parent::getImage($type);
        if (!$image && $type == 'thumb') {
            $image = URL::to('front/images/no_image.jpg');
        }

        return $image;
    }*/

    public function getFeatures()
    {
        return json_decode($this->features, true);
    }

    public function getSlides()
    {
        $slides = array();
        $slide = array();
        $features = $this->getFeatures();
        if($features) {
            foreach ($features as $key => $feature) {
                $slide[] = $feature;
                if (($key+1) % 6 == 0 && $key > 0) {
                    $slides[] = $slide;
                    $slide = array();
                }
            }
            if (count($slide) > 0) {
                $slides[] = $slide;
            }
        }
        
        return $slides;
    }

}
