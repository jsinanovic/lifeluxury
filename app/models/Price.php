<?php

class Price extends EloquentAdmin {
    
    use \Dimsav\Translatable\Translatable;
    
    public $translatedAttributes = array('name', 'annotation', 'content');
    
    public $timestamps = false;
    protected $guarded = array('id');
    protected $with = array('translations');
    
    public static $isSort = true;
    public static $rules = array(
        'name' => 'required|min:3|max:255',
        'category_id' => 'integer',
        'price' => 'numeric|min:0',
        'percent' => 'integer',
        'minimal_time' => 'integer|min:0|max:9999',
        'maximal_time' => 'integer|min:0|max:9999',
        //'annotation' => 'min:3|max:500',
        //'content' => '',
        'sort' => '',
    );
    
    
    public function category()
    {
        return $this->belongsTo('Category');
    }
    
    public function getTime()
    {
        return $this->minimal_time == $this->maximal_time? 
                $this->minimal_time : $this->minimal_time . '-' . $this->maximal_time;
    }
    
    public function getPriceAttribute($value)
    {
        return (int)$value;
    }
    
}