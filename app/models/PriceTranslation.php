<?php

class PriceTranslation extends EloquentAdmin {
    
    public $timestamps = false;
    protected $fillable = ['name', 'annotation', 'content'];
    
}