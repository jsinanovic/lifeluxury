<?php

class Media extends EloquentAdmin 
{
    
    protected $table = 'media';
    protected $guarded = array('id');
    public $timestamps = false;
    
    public static $rules = array(
        'name' => 'required|max:36',
        'model' => 'required|min:3|max:20',
        'entry_id' => 'integer'
    );
    
    public static function scopeOfModel($query, $model)
    {
        return $query->where('model', '=', $model);
    }
    
    public static function scopeOfEntry($query, $model, $entryId)
    {
        return $query->where('model', '=', $model)->where('entry_id', '=', $entryId);
    }
    
}