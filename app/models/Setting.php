<?php

class Setting extends EloquentAdmin {
    
    //protected $hidden = array('type');
    protected $guarded = array('id');
    public $timestamps = false;
    public static $isSlug = true;
    public static $rules = array(
        'name'          => 'required|min:3|max:255',
        'slug'          => 'required|min:3|max:255|unique:settings',
        'description'   => 'min:3|max:500',
        'module'        => 'max:50',
        'type'          => 'max:50',
        'content'       => '',
        'values'        => '',
    );
    
    public static function getByKey($key){
        return Setting::ofKey($key)->first();
    }
    
    public static function scopeOfKey($query, $key)
    {
        return $query->where('slug', '=', $key);
    }
    
    public static function value($key)
    {
        return Setting::ofKey($key)->pluck('content');
    }
    
    public static function values($key)
    {
        $valuesString = Setting::ofKey($key)->pluck('values');
        $values = explode("\n", $valuesString);
        return array_map('trim', $values);
    }
    
    public static function keyValues($key)
    {
        $values = self::values($key);
        $result = array();
        foreach ($values as $val)
            $result[$val] = $val;
        
        return $result;
    }
    
    public static function getAllSettings()
    {        
        return Cache::rememberForever('all_settings_list', function(){
            $settings = Setting::lists('content', 'slug');
            if(isset($settings['social-links']))
                $settings['social-links'] = explode("\n", $settings['social-links']);
            
            return $settings;
        });
    }
    
    protected static function clearGeneralCache($event = '') {
        parent::clearGeneralCache($event);
        Cache::forget('all_settings_list');
    }
    
}