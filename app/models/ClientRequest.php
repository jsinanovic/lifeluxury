<?php

use Carbon\Carbon;

class ClientRequest extends EloquentAdmin {
    
    protected $guarded = array('id');
    public $timestamps = true;
    public static $rules = array(
        'type' => 'required|min:1|max:50',
        'user_name' => 'required|min:1|max:255',
        'user_surname' => 'required|min:1|max:255',
        'user_email' => 'required|email|min:2|max:255',
        'user_phone' => 'required|min:2|max:50',
        'user_website' => 'required|min:2|max:255',
        'subject' => 'required',
        'message' => 'required|min:10',
        'details' => '',
        'status' => 'integer',
        'call_time' => 'integer',
        'service' => '',
        'description' => 'required|min:10|max:2000',
        'budget' => 'required'
    );
    public static $types = array('contacts' => 'Contact Form', 'customercare' => 'Customer Care', 'callback' => 'Call Back', 'quote' => 'Quote');//, 'requests' => 'Work Request');
    
    public static function getAll($params = array())
    {
        $fields = array('id', 'user_name', 'user_email', 'subject', 'created_at', 'status', 'updated_at');
        $type = isset($params['type'])? $params['type'] : false;
        if($type) {
            $data = self::where('type', '=', $type)->get($fields);
        } else {
            $data = self::get($fields);
        }
        
        return $data;
    }
    
    public static function getNewCount($type = false)
    {
        if($type) {
            return self::where('type', '=', $type)
                ->where('status', '=', 0)
                ->count('id');
        } else {
            return self::where('status', '=', 0)->count('id');
        }
    }
    
    public static function getCounters()
    {
        $result['new'] = 0;//self::getNewCount();
        foreach (self::$types as $key => $value) {
            $result['new_' . $key] = self::getNewCount($key);
            $result['new'] += $result['new_' . $key];
        }
        return $result;
    }
    
    public static function getAllCached($params = array())
    {
        $type = isset($params['type'])? $params['type'] : false;
        $modelClass = get_called_class();
        return $modelClass::getAll($params);
        /*
        return Cache::rememberForever($modelClass . '_all_' . $type, function() use($modelClass, $params)
        {
            return $modelClass::getAll($params);
        });*/
    }


    public function getCreatedAtAttribute($value)
    {
        $c = Carbon::parse($value);
        return $c->day . ' ' . self::$months[$c->month - 1] . ' ' . $c->year 
                . ' ' . $c->hour . ':' . $c->minute;
    }
    
    public function getUpdatedAtAttribute($value)
    {
        $c = Carbon::parse($value);
        return $c->day . ' ' . self::$months[$c->month - 1] . ' ' . $c->year 
                . ' ' . $c->hour . ':' . $c->minute;
    }
    
    public static function getRouteTypesFilter()
    {
        $types = array_keys(self::$types);
        return implode('|', $types);
    }
    
}