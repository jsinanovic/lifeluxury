<?php

class WorkTranslation extends EloquentAdmin {
    
    public $timestamps = false;
    protected $fillable = ['name', 'annotation', 'content', 'features', 'title', 'keywords', 'description'];
    
}