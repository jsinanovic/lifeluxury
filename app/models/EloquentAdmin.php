<?php

use \Symfony\Component\HttpFoundation\File\UploadedFile;

class EloquentAdmin extends Eloquent {

    /*
     * All editable fields should be represented in this array. Otherwise this
     * data will be deleted during validation
     */
    public static $rules = array();
    
    public static $isSlug = false;
    public static $isSort = false;
    public static $isAssign = false;
    public static $isCategories = false;
    public static $isSubcategories = false;
    public static $isSimilar = false;
    public static $hasGallery = false;
    public static $hasComments = false;
    public static $imageField = null;
    public static $imageFieldsExtra = array();
    public static $uploadFolder = 'default';
    public static $imageSizes = array(
        'original' => array(
            'width' => '960',
            'height' => 'auto'
        )
    );
    public static $nameField = 'name';
    public static $tagType = false;
    public static $months = array('January', 'February', 'March', 'April', 'May', 
        'June', 'Jule', 'August', 'September', 'October', 'November', 'December');

    /**
     * Validate data for current model. If $doModifyData is true, data would be modified
     * @param array $data - input fields
     * @param boolean $doModifyData - changes input data if it true
     * @param boolean $doRemoveOtherData - if true, it removes data that wasn't in model 
     * @return array - array('valid' => true, 'errors' => null, 'modifiedData' => $data);
     */
    public static function validate($data, $doModifyData = true, $doRemoveOtherData = true)
    {
        $result = array('valid' => true, 'errors' => null, 'modifiedData' => $data);
        
        // object for getting non-static attributes
        $obj = new static();
        if(isset($obj->translatedAttributes)) {
            $processedData = static::prepareTranslation($result['modifiedData'], $obj->translatedAttributes);
            $result['modifiedData'] = $processedData['processed'];
            $result['translation'] = $processedData['translation'];
        }
        
        if ($doModifyData) {
            $result['modifiedData'] = static::modifyData($result['modifiedData']);
        }
        
        if ($doRemoveOtherData) {
           $result['modifiedData'] = static::removeOtherData($result['modifiedData']);
        }
        
        $v = Validator::make($result['modifiedData'], static::$rules);
        if ($v->fails()) {
            $result['valid'] = false;
            $result['validator'] = $v;
        }
        
        if(isset($result['modifiedData'][static::$imageField]) 
                && !$result['modifiedData'][static::$imageField]->isValid()) {
            $result['valid'] = false;
            $v->errors()->add('Wrong file MIME type', 
                    'Uploaded file has not correct MIME type. Check your file please.');
            $result['validator'] = $v;
        }
        
        return $result;
    }
    
    protected static function prepareTranslation($data, $translatedAttributes = array())
    {
        $defaultLocale = 'en';
        $result = [ 'processed' => [], 'translation' => [] ];
        
        foreach ($data as $key => $value) {
            if (in_array($key, $translatedAttributes)) {
                $result['processed'][$key] = isset($value[$defaultLocale]) ? $value[$defaultLocale] : '';
                foreach ($value as $locale => $translateValue) {
                    if ($locale != $defaultLocale) {
                        $result['translation'][$locale][$key] = $translateValue;
                    }
                }
            } else {
                $result['processed'][$key] = $value;
            }
        }

        return $result;
    }

    /**
     * Removes all unnecessary data from input according to rules array
     * @param array $data
     * @return array - modified data
     */
    public static function removeOtherData($data)
    {
        foreach ($data as $key => $value) {
            if(!array_key_exists($key, static::$rules)) {
                unset($data[$key]);
            }   
        }
        
        return $data;
    }

    /**
     * Modify input data according to some model rules. For example generate slug 
     * or sort attribute
     * @param type $data - input data
     * @return array - modified data
     */
    protected static function modifyData($data)
    {
        if (static::$isSlug) {
            $slug = (isset($data['slug']) && !empty($data['slug'])) ?
                    $data['slug'] : $data[static::$nameField];
            $data['slug'] = static::getUniqueSlug($slug, $data);
        }
        if (static::$isSort) {
            if(!isset($data['sort']) || (int)$data['sort'] <= 0)
                $data['sort'] = static::max('sort') + 1;
        }
        if(static::$imageField && isset($data[static::$imageField])) {
            if(!is_object($data[static::$imageField])) {
                unset($data[static::$imageField]);
            }
        }
        if(isset(static::$rules['status']) && !isset($data['status']))
            $data['status'] = 0;
        
        return $data;
    }
    
    /**
     * get full path to the upload folder
     * @return string
     */
    public static function getUploadDir()
    {
        $dir = public_path('uploads') . '/' . static::$uploadFolder . '/';
        if(!is_dir($dir))
            mkdir ($dir, 0755);
        return $dir;
    }
    
    protected static function processImage(UploadedFile $file, $entry)
    {
        $filename = static::uploadImage($file, static::$imageSizes, $entry->id);
        if(!$filename)
            $filename = '';
        $entry->{static::$imageField} = $filename;
        $entry->save();
    }
    
    /**
     * Function resizes image and uploaded it
     * @param UploadedFile $file
     * @param type $sizes - array('folder_name' => array('width' => 'int|auto', 'height' => 'int|auto'))
     * @param string $filename - name of uploaded file without extension. If it is false, original name will be used
     * @return boolean or string - returns uploaded filename if success. Otherwise it returns false
     */
    protected static function uploadImage(UploadedFile $file, $sizes, $filename = false)
    {
        if(!($file && $file->isValid()))
            return false;
        
        ini_set('memory_limit', '256M');
        $filename = $filename? $filename : Str::slug($file->getClientOriginalName());
        $ext = '.' . $file->getClientOriginalExtension();
        $serverFilename = 'undefined';
        $keys = array_keys($sizes);
        
        foreach ($sizes as $name => $size) {
            $image = Image::make($file);
            if($size['width'] == 'auto' && $size['height'] == 'auto') {
                // without resizing
            } elseif($size['width'] == 'auto') {
                $image->heighten($size['height']);
            } elseif($size['height'] == 'auto') {
                $image->widen($size['width']);
            } else {
                $image->fit($size['width'], $size['height'], function($constraint){}, 'top');
            }
            $dir = static::getUploadDir() . $name . '/';
            if(!is_dir($dir))
                mkdir ($dir, 0755);
            
            //$serverFilename = $filename;
            
            
            if($keys[0] == $name) {
                for($k = 0; is_file($dir . $serverFilename . $ext); ++$k) {
                    $serverFilename = $filename . '_' . $k;
                }
            }
            $image->save($dir . $serverFilename . $ext);
        }
        return $serverFilename . $ext;
    }
    
    
    public function removeImage($imageField = false)
    {
        if(!$imageField)
            $imageField = static::$imageField;
        
        foreach (static::$imageSizes as $type => $size) {
            $file = static::getUploadDir() . $type . '/' . $this->getOriginal($imageField);
            if(is_file($file)) {
                unlink ($file);
            }
        }
    }
    
    public static function assignImage(UploadedFile $file, $model, $entryId = 0)
    {
        if(static::$isAssign) {
            $dir = static::getUploadDir() . 'content' . '/';
            if(!is_dir($dir))
                    mkdir ($dir, 0755);
            $filename = md5($file->getClientOriginalName() . time() . rand(0, 100)) 
                    . '.' . $file->getClientOriginalExtension();
            Image::make($file)->save($dir . $filename);
            $assign = Media::create(array(
                'name' => $filename,
                'model' => $model,
                'entry_id' => $entryId
            ));
            return $assign;
        }
        
        return false;
    }
    
    public function removeAssign()
    {
        if(static::$isAssign) {
            $model = get_called_class();
            $media = Media::where('model', '=', $model)->where('entry_id', '=', $this->id);
            foreach ($media as $entry) {
                $file = static::getUploadDir() . 'content/' . $entry->name;
                if(is_file($file))
                    unlink ($file);
            }
            $media->delete();
        }
    }
    
    
    public function bindAssignedImages($assignedIds, $searchFields = array('content'))
    {
        if(static::$isAssign) {
            Media::whereIn('id', $assignedIds)->update(array('entry_id' => $this->id));
            foreach ($searchFields as $fieldName) {
                if(isset($this->{$fieldName})) {
                    // find all src in the content and 
                    // compare them with all assignments of the entry
                    // delete not used assignments
                }
            }
        }
    }

    /**
     * Generate unique slug by not empty string for current model
     * @param type $title
     * @return string - unique slug
     */
    public static function getUniqueSlug($title, $data)
    {
        $title = trim($title);
        if (empty($title))
            return '';

        $slug = Slugify::slugify($title);
        if (static::where('slug', '=', $slug)->count() > 0) {
            $entry = static::where('slug', 'LIKE', $slug . '-%')->orderBy('slug', 'DESC')->first();

            if ($entry) {
                $arr = explode('-', $entry->slug);
                $max = (int) last($arr);
                $slug = $slug . '-' . ($max + 1);
            } else {
                $slug = $slug . '-1';
            }
            $slug = static::getUniqueSlug($slug, $data);
        }

        return $slug;
    }

    /**
     * Makes rules for JS validation using current model validation rules
     * @return string - json rules data
     */
    public static function getJsonValidationRules()
    {
        $result = array();
        foreach (static::$rules as $name => $row) {
            $result[$name] = array();
            $rules = explode('|', trim($row));
            if (count($rules) > 0) {
                foreach ($rules as $rule) {
                    if ($rule == 'required') {
                        $result[$name]['required'] = true;
                    } elseif (strpos($rule, 'min:') !== FALSE) {
                        $arr = explode(':', $rule);
                        $result[$name]['minlength'] = last($arr);
                    } elseif (strpos($rule, 'max:') !== FALSE) {
                        $arr = explode(':', $rule);
                        $result[$name]['maxlength'] = last($arr);
                    } elseif($rule == 'email') {
                        $result[$name]['email'] = true;
                    }
                }
            }
            if (empty($result['name']))
                unset($result['name']);
        }

        $result = static::modifyJsonValidationRules($result);
        return json_encode($result);
    }
    
    public static function getJsonValidationRulesBy($rules)
    {
        $result = array();
        foreach ($rules as $name => $row) {
            $result[$name] = array();
            $rules = explode('|', trim($row));
            if (count($rules) > 0) {
                foreach ($rules as $rule) {
                    if ($rule == 'required') {
                        $result[$name]['required'] = true;
                    } elseif (strpos($rule, 'min:') !== FALSE) {
                        $arr = explode(':', $rule);
                        $result[$name]['minlength'] = last($arr);
                    } elseif (strpos($rule, 'max:') !== FALSE) {
                        $arr = explode(':', $rule);
                        $result[$name]['maxlength'] = last($arr);
                    } elseif($rule == 'email') {
                        $result[$name]['email'] = true;
                    }
                }
            }
            if (empty($result['name']))
                unset($result['name']);
        }

        $result = static::modifyJsonValidationRules($result);
        return json_encode($result);
    }

    /**
     * Modified JS rules that generates getJsonValidationRules() function
     * for example SLUG field should be required on backend, however user can
     * leave this field empty and field will be generated.
     * 
     * @param array $rules
     * @return array - modified rules
     */
    protected static function modifyJsonValidationRules($rules)
    {
        if (isset($rules['slug']['required'])) {
            unset($rules['slug']['required']);
        }
        return $rules;
    }
    
    public static function getImageFolder($type = 'original')
    {
        $url = URL::to('uploads/' . static::$uploadFolder) . '/';
            $url .= $type . '/';
        return $url;
    }
    
    public function getImage($type = 'original', $fieldName = false)
    {
        if(!$fieldName)
            $fieldName = static::$imageField;
        
        if($this->{$fieldName})
            return static::getImageFolder($type) . $this->{$fieldName};
        else
            return false;
    }
    
    public static function getAll($params = array())
    {
        return static::all();
    }
    
    public static function getAllCached($params = array())
    {
        $modelClass = get_called_class();
        return Cache::rememberForever($modelClass . '_all', function() use ($modelClass, $params)
        {
            return $modelClass::getAll($params);
        });
    }
    
    protected static function clearGeneralCache($event)
    {
        $modelClass = get_called_class();
        Cache::forget($modelClass . '_all');
    }
    
    public function clearEntryCache($event)
    {
        
    }
    
    public static function scopeActive($query)
    {
        return $query->where('status', '=', 1);
    }
    
    public static function scopeDisactive($query)
    {
        return $query->where('status', '=', 0);
    }
    
    public static function scopeSlug($query, $slug)
    {
        return $query->where('slug', '=', $slug);
    }
    
    public function getShares()
    {
        $shares = array('total' => 0);
        foreach (Share::$networks as $network)
            $shares[$network] = 0;
        
        if(!$this->shares)
            return $shares;
        
        $total = 0;
        foreach ($this->shares as $share) {
            $shares[$share->network] = $share->count;
            $total += $share->count;
        }
        $shares['total'] = $total;
        return $shares;
    }
    
    public function addShare($network)
    {
        $share = $this->shares()->where('network', '=', $network)->first();
        if(!$share) {
            $share = Share::create (array('network' => $network));
            $this->shares()->save($share);
        }
        $share->increment('count');
        return $share;
    }
    
    public function addComment($content, $parentId, $userId, $status = 0)
    {
        $comment = new Comment(array(
            'content' => $content,
            'parent_id' => ($parentId > 0? $parentId : null),
            'user_id' => $userId,
            'status' => $status
        ));
        
        $this->comments()->save($comment);
        
        if(isset($this->comments_count)) {
            $this->comments_count = $this->comments()->count();
            $this->save();
        }
        
        return $comment;
    }
    
    public function getComments($start = 0, $limit = 5)
    {
        return  $this->comments()
                ->with('user')
                ->where('status', '=', 1)
                ->where('parent_id', '=', null)
                ->orderBy('created_at', 'DESC')
                ->skip($start)
                ->take($limit)
                ->get();
    }
    
    public function prepareAjaxEntryTranslate()
    {
        $entry = array();
        $locales = array('en', 'ru');
        $translatedAttributes = isset($this->translatedAttributes) ? $this->translatedAttributes : array();
        foreach ($this->attributes as $key => $attr) {
            if (in_array($key, $translatedAttributes)) {
                foreach ($locales as $locale) {
                    $entry[$key . '['.$locale.']'] = $this->translateOrNew($locale)->{$key};
                }
            } else {
                $entry[$key] = $attr;
            }
        }
        
        return $entry;
    }
    
    
}
