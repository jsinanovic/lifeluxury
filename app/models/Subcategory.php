<?php

class Subcategory extends EloquentAdmin {

    use \Dimsav\Translatable\Translatable;

    public $translatedAttributes = array('name', 'content', 'title', 'keywords', 'description');

//    protected $hidden = array('key', 'deep');
    protected $guarded = array('id');
//    protected $touches = array('menu');
//    public static $isSlug = true;
//    public static $isSort = true;
    public $timestamps = false;
    protected $with = array('translations');

    public static $rules = array(
        'name' => 'required|min:3|max:255',
        'category_id' => 'integer',
//        'slug' => 'required|min:3|max:255|unique:categories',
//        'url' => 'max:255',
//        'parent_id' => 'integer',
//        'menu_id' => 'integer',
//        'page_id' => 'integer',
//        'annotation' => 'min:3|max:500',
        'content' => '',
        'sort' => '',
        'status' => '',
        'title' => 'min:3|max:255',
        'keywords' => 'min:3|max:500',
        'description' => 'min:3|max:500',
    );

    public function subcategories()
    {
        return $this->hasMany('Subcategory');
    }

    public function category()
    {
        return $this->belongsTo('Category');
    }

    public function articles()
    {
        return $this->belongsToMany('Article');
    }


    public function getUrl()
    {
        $localeUri = (App::getLocale() == 'en')? '' : 'ru';
        $url = '';
        if($this->url)
            $url = $localeUri . $this->url;
        else
            $url = $this->page? $localeUri . '/page/' . $this->page->slug : $localeUri . '/' .$this->slug;
        return URL::to($url);
    }

    public static function boot()
    {
        parent::boot();

        // Attach event handler, on deleting of the category
        Category::deleting(function($category)
        {
            //$category->prices()->delete();
        });
    }

}