<?php

class Photo extends EloquentAdmin {
    
    public $timestamps = false;
    protected $guarded = array('id');
    public static $rules = array(
        'name' => 'required|min:3|max:255',
        'imageable_id' => '',
        'imageable_type' => '',
    );
    
    public function imageable()
    {
        return $this->morphTo();
    }
    
    public function remove()
    {
        $modelClass = $this->imageable_type;
        if($modelClass::$gallerySizes) {
            foreach($modelClass::$gallerySizes as $name => $size) {
                $file = $modelClass::getUploadDir() .  $name . '/' . $this->name;
                if(is_file($file))
                    unlink ($file);
            }
        }
    }
    
    public function getImage($type = 'gallery_original') {
        return $this->getPhotoFolder($type) . $this->name;
    }
    
    public function getPhotoFolder($type = 'gallery_original') {
        $modelClass = $this->imageable_type;
        $url = URL::to('uploads/' . $modelClass::$uploadFolder) . '/';
            $url .= $type . '/';
        return $url;
    }
    
}