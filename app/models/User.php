<?php


use Illuminate\Auth\UserTrait;
use Illuminate\Auth\UserInterface;
use Illuminate\Auth\Reminders\RemindableTrait;
use Illuminate\Auth\Reminders\RemindableInterface;
use Mmanos\Social\SocialTrait;

class User extends EloquentAdmin implements UserInterface, RemindableInterface{
    
    use UserTrait, RemindableTrait, SocialTrait;
    
    protected $hidden = array('password', 'remember_token');
    protected $guarded = array('');
    
    public function getAvatar()
    {
        
        if($this->image) {
            return $this->image;
        }
        /*if ($this->hasProvider('facebook')) {
            $fid = $this->provider('facebook')->provider_id;
            return 'https://graph.facebook.com/' . $fid . '/picture?width=70&height=70&redirect=false';
            
        }*/

        return URL::to('front/images/ava.png');
    }

}
