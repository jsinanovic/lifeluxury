<?php

class CategoryTranslation extends EloquentAdmin {
    
    public $timestamps = false;
    protected $fillable = ['name', 'annotation', 'content', 'title', 'keywords', 'description'];
    
}