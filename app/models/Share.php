<?php

class Share extends EloquentAdmin {
    
    public $timestamps = false;
    protected $guarded = array('id');
    public static $rules = array(
        'network' => 'required|min:1|max:255',
        'count' => 'integer',
        'shareable_id' => 'integer',
        'shareable_type' => '',
    );
    public static $networks = array('twitter', 'facebook', 'gplus', 'linkedin', 'pinterest');
    
    public function shareable()
    {
        return $this->morphTo();
    }
    
}