<?php

class ProductTranslation extends EloquentAdmin {
    
    public $timestamps = false;
    protected $fillable = ['name', 'annotation', 'content', 'features', 'prices', 'title', 'keywords', 'description'];
    
}