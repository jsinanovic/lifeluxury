<?php

use Carbon\Carbon;

class Article extends EloquentAdmin {
    
    use \Dimsav\Translatable\Translatable;
    
    public $translatedAttributes = array('name', 'annotation', 'content', 'title', 'keywords', 'description', 'authors');
    
    protected $guarded = array('id');
    public $timestamps = true;
    protected $with = array('translations');
    
    public static $isSlug = true;
    public static $isAssign = true;
    public static $isCategories = true;
    public static $isSubcategories = true;
    public static $isSimilar = false;
    public static $hasComments = true;
    public static $imageField = 'image';
    public static $imageFieldsExtra = array('articles_bg', 'contents_bg');
    public static $imageSizes = array(
        'original' => array(
            'width' => 'auto', 
            'height' => 'auto'
        ),
        'thumb' => array(
            'width' => 600,
            'height' => 500
        ),
        'minithumb' => array(
            'width' => 280,
            'height' => 183
        ),
        'small' => array(
            'width' => 80,
            'height' => 60,
        ),
        'icon' => array(
            'width' => 50,
            'height' => 50
        )
    );
    public static $uploadFolder = 'articles';
    public static $tagType = 'articles';
    public static $rules = array(
        'name' => 'required|min:3|max:255',
        'slug' => 'required|min:3|max:255|unique:articles',
        'image' => 'image|max:10240',
        'articles_bg' => 'image|max:10240',
        'contents_bg' => 'image|max:10240',
        'template' => 'required',
        'content' => '',
        'annotation' => 'min:3|max:500',
        'title' => 'min:3|max:255',
        'keywords' => 'min:3|max:500',
        'description' => 'min:3|max:500',
        'status' => '',
        'authors' => '',
        'is_cover' => '',
        'created_at' => ''
    );
    
    public static $perPageHome = 5;
    
    public function tags()
    {
        return $this->belongsToMany('Tag');
    }
    
    public function categories()
    {
        return $this->belongsToMany('Category');
    }

    public function subcategories()
    {
        return $this->belongsToMany('Subcategory');
    }
    
    public function similar()
    {
        return Article::active()->where('id', '!=', $this->id)->orderBy('comments_count', 'DESC')->take(4)->get();
        //return $this->belongsToMany('Article', 'article_article', 'article_id', 'similar_id');
    }
    
    
    public static function getRecentPosts($count)
    {
        return Cache::rememberForever('article_recent_posts', function() use ($count){
            return Article::with('translations')->active()->orderBy('created_at', 'DESC')->take($count)->get();
        });
    }
    
    public static function getRecentPostsPaginate($count)
    {
        /*$page = Input::get('page', 1);
        return Cache::rememberForever('article_recent_posts_paginate_' . $count . '_' . $page, function() use ($count){
            
        });*/
        
        return Article::active()
                    ->with(array('categories', 'translations'))
                    ->orderBy('created_at', 'DESC')
                    ->paginate($count);
    }
    
    public static function getAll($params = array())
    {
        return self::get(array(
            'id', 'image', 'name', 'slug', 'created_at', 'updated_at', 'image', 'status',
        ));
    }
    
    public function getCreatedAtAttribute($value)
    {
        $c = Carbon::parse($value);
        return $c->day . ' ' . self::$months[$c->month - 1] . ' ' . $c->year;
    }
    
    public static function getIssues()
    {
        $issues = array();
        $rows = DB::select("SELECT 
            DISTINCT YEAR(created_at) as year, 
            CASE WHEN MONTH(created_at) IN (12, 1, 2) THEN 'winter'
            WHEN MONTH(created_at) IN (3, 4, 5) THEN 'spring'
            WHEN MONTH(created_at) IN (6, 7, 8) THEN 'summer'
            ELSE 'autumn' END AS season,
            MONTH(created_at) as month
            FROM articles
            ORDER BY created_at");
        
        foreach ($rows as $row) {
            if($row->month == 12) {
                ++$row->year;
            }
            if(!isset($issues[$row->year]) || !in_array($row->season, $issues[$row->year])) {
                $issues[$row->year][] = $row->season;
            }
        }
        return $issues;
    }
    
    public static function getCurrentSeason()
    {
        return self::getSeason(date('n'));
    }
    
    public static function getSeason($month) {
        switch ($month) {
            case '3':
            case '4':
            case '5':
                return 'spring';
            case '6':
            case '7':
            case '8':
                return 'summer';
            case '9':
            case '10':
            case '11':
                return 'autumn';
            case '12':
            case '1':
            case '2':
                return 'winter';
            default: 
                return false;
        }
    }
    
    public static function getPrevSeason($season)
    {
        switch ($season) {
            case 'spring':
                return 'winter';
            case 'winter':
                return 'autumn';
            case 'autumn':
                return 'summer';
            case 'summer':
                return 'spring';
        }
    }

    public static function getSeasonMonths($season)
    {
        $months = array();
        switch ($season) {
            case 'spring':
                $months = array('3', '4', '5');
                break;
            case 'summer':
                $months = array('6', '7', '8');
                break;
            case 'autumn':
                $months = array('9', '10', '11');
                break;
            case 'winter':
                $months = array('12', '1', '2');
                break;
        }
        
        return $months;
    }
    
    public function scopeIssue($query, $year, $season)
    {
        $months = self::getSeasonMonths($season);
        if($season == 'winter') {
            $query->whereRaw("((YEAR(created_at) = ? AND MONTH(created_at) IN ('1', '2')) OR (YEAR(created_at) = ? AND MONTH(created_at) = '12'))", array($year, $year - 1));
        } else {
            $query->whereRaw('YEAR(created_at) = ?', array($year));
            $query->whereRaw('MONTH(created_at) IN (?,?,?)', $months);
        }
        
        return $query;
    }
    
    public function scopeCover($query)
    {
        return $query->where('is_cover', 1);
    }
    
    
    public static function popular($count)
    {
        Cache::rememberForever('article_popular_posts', function() use($count) {
            return Article::with('translations')->active()
                    ->orderBy('comments_count', 'DESC')->take($count)->get();
        });
    }
    
    /*public function getImage($type = 'original')
    {
        $image = parent::getImage($type);
        if(!$image) {
            if($type == 'thumb')
                $image = URL::to('front/images/no-picture-yet.jpg');
            elseif($type == 'small')
                $image = URL::to('front/images/no_image.jpg');
        } 
        return $image;
    }*/
    
    public function shares()
    {
        return $this->morphMany('Share', 'shareable');
    }
    
    public function comments()
    {
        return $this->morphMany('Comment', 'commentable');
    }
    
    public static function getCountPages($perPage)
    {
        $count = static::count();
        return ($count % $perPage == 0)? $count / $perPage : $count / $perPage + 1;
    }
    
    protected static function clearGeneralCache($event) {
        parent::clearGeneralCache($event);
        Cache::forget('article_recent_posts');
        Cache::forget('article_popular_posts');
        
        /*$countPages = self::getCountPages(self::$perPageHome);
        for($k = 0; $k <= $countPages; ++$k) {
            Cache::forget('article_recent_posts_paginate_' . self::$perPageHome . '_' . $page);
        }*/
    }
    
    public function season()
    {
        $c = Carbon::parse($this->created_at);
        return self::getSeason($c->month);
    }
    
    public function year()
    {
        $c = Carbon::parse($this->created_at);
        return $c->year;
    }
    
    public function getTemplates()
    {
        $itemsCount = 0;
        switch($this->template){
            case 1:
            case 2:
            case 3:
            case 9:
            case 13:
            case 16:
                $itemsCount = 5;break;
            case 4:
            case 5:
                $itemsCount = 4;break;
            case 6:
            case 7:
            case 8:
                $itemsCount = 3;break;
            case 17:    
                $itemsCount = 6;break;
            case 10:
                $itemsCount = 9;break;
            case 11:
                $itemsCount = 1;break;
            case 12:
            case 18:
                $itemsCount = 7;break;
            case 14:
                $itemsCount = 8;break;
            case 15:
                $itemsCount = 2;break;
        }
        
        $templates = array();
        for($k = 1; $k <= $itemsCount; ++$k) {
            $templates[] = View::make('front.magazine.templates.t' . $this->template . '.item' . $k)->render();
        }
        
        return $templates;
        
    }
    
    
}