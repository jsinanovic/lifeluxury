<?php

use Carbon\Carbon;

class Product extends EloquentAdmin {
    
    use \Dimsav\Translatable\Translatable;
    
    public $translatedAttributes = array('name', 'annotation', 'content', 'features', 'prices', 'title', 'keywords', 'description');
    
    //protected $hidden = array('type');
    protected $guarded = array('id');
    public $timestamps = true;
    protected $with = array('translations');
    
    public static $isSlug = true;
    public static $isSort = true;
    public static $isAssign = true;
    public static $isCategories = true;
    public static $hasGallery = true;
    public static $hasComments = true;
    public static $imageField = 'image';
    public static $imageSizes = array(
        'original' => array(
            'width' => 600, 
            'height' => 'auto'
        ),
        'thumb' => array(
            'width' => 280,
            'height' => 220
        ),
        'icon' => array(
            'width' => 50,
            'height' => 50
        )
    );
    public static $gallerySizes = array(
        'gallery_original' => array(
            'width' => 784,
            'height' => 440,
        ),
        'gallery_thumb' => array(
            'width' => 100,
            'height' => 100,
        ),
    );
    public static $uploadFolder = 'products';
    public static $rules = array(
        'name' => 'required|min:3|max:255',
        'slug' => 'required|min:3|max:255|unique:products',
        'image' => 'image|max:5120',
        'price' => 'numeric|min:0',
        'url' => 'min:3|max:255',
        'demo_url' => 'min:3|max:255',
        'annotation' => 'min:3|max:500',
        'content' => '',
        'features' => '',
        'prices' => '',
        
        'status' => '',
        'sort' => '',
        
        'title' => 'min:3|max:255',
        'keywords' => 'min:3|max:500',
        'description' => 'min:3|max:500',
    );
    
    public function categories()
    {
        return $this->belongsToMany('Category');
    }
    
    
    public static function getAll($params = array())
    {
        return self::get(array(
            'id', 'image', 'name', 'slug', 'price', 'url', 'id', 'status',
        ));
    }
    
    public function getCreatedAtAttribute($value)
    {
        $c = Carbon::parse($value);
        return $c->day . ' ' . self::$months[$c->month - 1] . ' ' . $c->year;
    }
    
    public function getPriceAttribute($value)
    {
        return (int)$value;
    }
    
    public function categoriesStr()
    {
        $cats = array();
        foreach ($this->categories as $cat)
            $cats[] = $cat->name;
        
        if(count($cats) > 0)
            return implode (', ', $cats);
    }
    
    public static function popular($count)
    {
        return Product::active()->take($count)->get(array('id', 'name', 'slug', 'image', 'price', 'url'));
    }
    
    public function getImage($type = 'original')
    {
        $image = parent::getImage($type);
        if(!$image && $type == 'thumb') {
            $image = URL::to('front/images/no_image.jpg');
        } 
        
        return $image;
    }
    
    public function photos()
    {
        return $this->morphMany('Photo', 'imageable');
    }
    
    public function shares()
    {
        return $this->morphMany('Share', 'shareable');
    }
    
    public function comments()
    {
        return $this->morphMany('Comment', 'commentable');
    }
    
}
