<?php

use Baum\Node;
use Carbon\Carbon;

class Comment extends Node {
    
    protected $guarded = array('id', 'lft', 'rgt', 'depth');
    
    public static $rules = array(
        'content' => 'required|min:1|max:500',
        'user_id' => 'integer',
        'parent_id' => 'integer',
        'status' => 'integer',
    );
    
    public function commentable()
    {
        return $this->morphTo();
    }
    
    public function user()
    {
        return $this->belongsTo('User');
    }
    
    public function scopeActive($query)
    {
        return $this->where('status', '=', 1);
    }
    
    public function scopeRoots($query)
    {
        return $this->where('parent_id', '=', null);
    }
    
    public function getCreatedAtAttribute($value)
    {
        $c = Carbon::parse($value);
        return $c->day . '.' . $c->month - 1 . '.' . $c->year . ' at ' . $c->hour . ':' . $c->minute;
    }
    
}