<?php

class PageTranslation extends Eloquent {
    
    public $timestamps = false;
    protected $fillable = ['name', 'subtitle', 'content', 'title', 'keywords', 'description'];
    
}