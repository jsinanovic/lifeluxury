<?php

class Subscriber extends EloquentAdmin {
    
    protected $guarded = array();
    protected $primaryKey = 'id';
    public static $rules = array(
        'name' => 'required|min:1|max:100',
        'surname' => 'required|min:1|max:100',
        'email' => 'required|min:3|max:100',
        'country' => 'required',
        'status' => '',
        'token',
    );
    
    public static function getAll($params = array())
    {
        $data = self::get(array(
            'email', 'name', 'surname', 'country', 'created_at', 'status', 'id',
        ));
        
        $countries = Setting::values('countries');
        foreach ($data as $row) {
            $row->country = isset($countries[$row->country])? $countries[$row->country] : 'undefined';
        }
        
        return $data;
    }
    
}
