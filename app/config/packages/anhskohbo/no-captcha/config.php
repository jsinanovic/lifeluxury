<?php

return array(

	'secret'  => getenv('NOCAPTCHA_SECRET') ?: '6LdQ4AkTAAAAAGrP0TNP9Jy2Cs31XKgsRMVXibbg',
	'sitekey' => getenv('NOCAPTCHA_SITEKEY') ?: '6LdQ4AkTAAAAAGw4BgKUZcvbJMFO9-dRArB0OCeV',

	'lang'    => app()->getLocale(),

);
