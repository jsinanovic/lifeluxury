<?php
//$value = ($entry && $entry->translate($locale))? $entry->translate($locale)->{$key} : '';
//$key = $key . '[' . $locale . ']';
//dd($value);
?>
<div data-key="{{$key}}" data-prices="true">
    {{ Form::hidden($key, $value, array('class'=>'form-control')) }}
    @foreach($field['values'] as $lk => $license)
    <h1 class="text-center">{{$license}}</h1>
    <div class="row prices-license license-{{$lk}}" data-key="{{$lk}}">
        @for($k = 0; $k < 4; ++$k)
        <div class="col-md-3 prices-item">

            <div class="panel panel-success">
                <div class="panel-body panel-body-pricing">
                    <h2><input class="form-control prices-name" type="text" placeholder="Name"></h2>
                    @for($p = 0; $p < 4; ++$p)
                    <p><input class="form-control prices-detail" type="text" placeholder="Details Item"></p>
                    @endfor
                    <h1><input class="form-control prices-value" type="text" placeholder="Price $"></h1>
                    <label>Selected: <input class="prices-active" type="radio" name="active-{{$lk}}" placeholder="Name"></label>
                </div>

                <div class="panel-footer">
                    <p><input class="form-control prices-link" type="text" placeholder="URL to product"></p>
                    <p><input class="form-control prices-button" type="text" placeholder="Button Text"></p>
                </div>
            </div>

        </div>
        @endfor
    </div>
    @endforeach
</div>