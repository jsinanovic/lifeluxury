<!DOCTYPE html>
<html lang="en" class="body-full-height">
    <head>        
        <!-- META SECTION -->
        <title>Admin Panel</title>            
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <meta http-equiv="X-UA-Compatible" content="IE=edge" />
        <meta name="viewport" content="width=device-width, initial-scale=1" />
        
        <link rel="icon" href="favicon.ico" type="image/x-icon" />
        <!-- END META SECTION -->
        
        <!-- CSS INCLUDE -->        
        {{HTML::style('css/theme-default.css')}}
        <!-- EOF CSS INCLUDE -->                          
    </head>
    <body>
        
        <div class="login-container">
        
            <div class="login-box animated fadeInDown">
                <a href="{{URL::to('/admin')}}" class="login-logo"></a>
                <div class="login-body">
                    <div class="login-title"><strong>Welcome</strong>, Please login</div>
                    {{ Form::open(array('url'=>'admin/login', 'class'=>'form-horizontal', 'method' => 'POST')) }}
                        <div class="form-group">
                            <div class="col-md-12">
                                {{ Form::text('email', null, array('class'=>'form-control', 'placeholder'=>'Email Address')) }}
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-md-12">
                                {{ Form::password('password', array('class'=>'form-control', 'placeholder'=>'Password')) }}
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-md-6">
                                <a href="{{URL::to('remind')}}" class="btn btn-link btn-block">Forgot your password?</a>
                            </div>
                            <div class="col-md-6">
                                {{ Form::submit('Login', array('class'=>'btn btn-info btn-block', 'value' => 'Log In'))}}
                            </div>
                        </div>
                    {{ Form::close() }}
                </div>
                <div class="login-footer">
                    <div class="pull-left">
                        &copy; {{date('Y')}} Guru Team
                    </div>
                    <div class="pull-right">
                        <a href="http://guru-team.com/about-us" target="_blank">About</a> |
                        <!--<a href="#" target="_blank">Privacy</a> |-->
                        <a href="http://guru-team.com/contact/" target="_blank">Contact Us</a>
                    </div>
                </div>
            </div>
            
        </div>
        
    </body>
</html>






