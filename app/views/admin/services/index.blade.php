@extends('admin.layouts.main')

@section('breadcrumb')
<li><a href="{{URL::to($moduleUri)}}">{{ ucfirst($moduleName) }}</a></li>
@stop

@section('content')
<div class="row">
    <div class="col-md-12">
        <div class="panel panel-success">
            <div class="panel-heading">                                
                <h3 class="panel-title">{{$pageName}}</h3>
                <ul class="panel-controls">
                    <li><a href="#" class="panel-refresh"><span class="fa fa-refresh"></span></a></li>
                </ul>                                
            </div>
            <div class="panel-body">
                <div class="table-responsive">
                    <table class="table admin-datatable table-hover">
                        <thead>
                            <tr>
                                <th>ID</th>
                                <th>Thumb</th>
                                <th>Name</th>
                                <th>Slug</th>
                                <th>Created</th>
                                <th>Last Update</th>
                                <th>Status</th>
                                <th>Actions</th>
                            </tr>
                        </thead>
                        <tbody></tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
@stop

@section('js')
{{HTML::script('js/plugins/icheck/icheck.min.js');}}
{{HTML::script('js/plugins/mcustomscrollbar/jquery.mCustomScrollbar.min.js');}}
{{HTML::script('js/plugins/datatables/jquery.dataTables.min.js');}}
<script>
    var iconsUri = '{{Service::getImageFolder("icon")}}';
    var moduleUri = '{{ URL::to($moduleUri) }}';
    var panel = $('.panel');
    var table = $(".admin-datatable").on( 'processing.dt', function ( e, settings, processing ) {
        panel_refresh(panel);
    }).dataTable({
        processing : true,
        ajax : moduleUri + '/all-json',
        stateSave : true,
        "columnDefs": [ {
            "targets": 1,
            sClass : 'text-center',
            "orderable": false,
            "data": null,
            "render": function ( data, type, row ) {
                var img = 'No image';
                if(data[1])
                    img = '<img src="'+ iconsUri + data[1]+'?'+new Date(data[5]).getTime()+'" alt="" width="50" height="50">';
                return img;
            },
        }, {
            "targets": 4,
            sClass : 'text-center'
        }, {
            "targets": 5,
            sClass : 'text-center'
        }, {
            "targets": 6,
            sClass : 'text-center',
        },  {
            sClass : 'text-center',
            "orderable": false,
            "targets": -2,
            "data": null,
            "render": function ( data, type, row ) {
                return '<label class="switch change-status">'
                     + '<input type="checkbox" '+ ((data[6])?'checked="checked"' : '')
                     + 'value="0"><span onclick="changeStatus($(this).closest(\'.switch\'), '+data[0]+');return false;"></span></label>';
            },
        }, {
            sClass : 'text-center',
            "orderable": false,
            "targets": -1,
            "data": null,
            "render": function ( data, type, row ) {
                return '<div class="btn-group"><a href="' + moduleUri + '/edit/' + data[0] + '" '  
                        + 'class="btn btn-primary btn-condensed"><i class="fa fa-pencil"></i></a> '
                        + '<button class="btn btn-danger btn-condensed" onclick="removeEntry($(this).closest(\'tr\'), '+data[0]+', onRowDeleted)"><i class="fa fa-trash-o"></i></button></div>';
            },
        } ],
        
    });
    
    function onRowDeleted($row) {
        table.api().row($row).remove().draw();
    }
    
    $(".panel-refresh").on("click",function(){
        table.api().ajax.reload();
    });
    $(".admin-datatable").on('page.dt',function () {
        onresize(100);
    });
</script>
@stop