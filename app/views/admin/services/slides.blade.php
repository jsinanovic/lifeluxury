<div class="panel panel-colorful slides">
    {{ Form::hidden($key, $entry? $entry->{$key} : '') }}
    <div class="panel-heading">
        <h3 class="panel-title">{{{$field['label']}}}</h3>
        <ul class="panel-controls">                                                                        
            <li><a href="javascript: void(0);" class="slide-add"><span class="fa fa-plus"></span></a></li>
        </ul>
    </div>
    <div class="panel-body">
        <div class="item">
            <div class="row">
                <div class="col-md-3 col-first">
                    <div class="text-block">
                        <textarea name="" id="" cols="30" rows="10" class="summernote">
                            <h3>WEB DESIGN & DEVELOPMENT</h3>
                            <p>Gregor then turned to look out the window at the dull weather. Drops of rain could be heard hitting the pane, which made him feel quite sad. "How about if I sleep a little bit longer and forget all this nonsense"</p>
                            <p>He thought, but that was something he was unable to do because he was used to sleeping on his right, and in his present state couldn't get into that position. However hard he threw himself onto his right, he always rolled back to where he was.</p>
                        </textarea>
                    </div>
                </div>
                <div class="col-md-9 col-last">
                    <div class="col-md-6 large-one">
                        <div class="image-block" style="background-image: url(front/img/creative_item.jpg)"></div>
                    </div>
                    <div class="col-md-6 large-two">
                        <div class="image-block" style="background-image: url(front/img/creative_item.jpg)"></div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-9 col-first">
                    <div class="col-md-6 large-one">
                        <div class="image-block" style="background-image: url(front/img/creative_item.jpg)"></div>
                    </div>
                    <div class="col-md-6 large-two">
                        <div class="image-block" style="background-image: url(front/img/creative_item.jpg)"></div>
                    </div>
                </div>
                <div class="col-md-3 col-last">
                    <div class="text-block">
                        <h3>WEB DESIGN & DEVELOPMENT</h3>
                        <p>Gregor then turned to look out the window at the dull weather. Drops of rain could be heard hitting the pane, which made him feel quite sad. "How about if I sleep a little bit longer and forget all this nonsense"</p>
                        <p>He thought, but that was something he was unable to do because he was used to sleeping on his right, and in his present state couldn't get into that position. However hard he threw himself onto his right, he always rolled back to where he was.</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>