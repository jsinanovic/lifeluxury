@extends('admin.layouts.form')

@section('form')
{{ Form::model($entry, array('url' => $formAction, 'class'=>'form-horizontal form-for-' . Str::lower($modelName), 
                    'method' => 'POST', 'id' => 'jvalidate', 'role' => 'form', 'files'=> true) ) }}
               
{{ Form::hidden('assigned[]') }}

<div class="panel panel-info">
    <div class="panel-heading">
        <h3 class="panel-title">{{$title}}</h3>
    </div>

    <div class="panel-body">

        @if(Session::get('success'))
        <div class="form-group">
            <div class="col-md-2 col-xs-12"></div>
            <div class="col-md-7 col-xs-12">  
                <div class="alert alert-success" role="alert">
                    <button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">×</span><span class="sr-only">Close</span></button>
                    <strong>Well done!</strong> You successfully saved this {{$modelName}}.
                </div>
            </div>
        </div>
        @endif

        @if(Session::get('errors'))
        <div class="form-group">
            <div class="col-md-2 col-xs-12"></div>
            <div class="col-md-7 col-xs-12">  
                <div class="alert alert-danger" role="alert">
                    <button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">×</span><span class="sr-only">Close</span></button>
                    <strong>Oh snap!</strong> Change a few things up and try submitting again.<br>
                    @foreach(Session::get('errors')->all() as $key => $error)
                    {{$key + 1}}. {{$error}}<br>
                    @endforeach
                </div>
            </div>
        </div>
        @endif

        @foreach($formFields as $key => $field)
            <?php $isTranslated = $translatedAttributes && in_array($key, $translatedAttributes); ?>
        
            @if($isTranslated)
                @foreach($locales as $locale)
                    <div class="form-group translate locale-{{$locale}}">
                        <label class="col-md-2 col-xs-12 control-label">{{{ $field['label'] }}} ({{$locale}})</label>
                        <div class="col-md-7 col-xs-11">
                            @include('admin.layouts.smartfield_translated')
                        </div>

                        @if($isTranslated)
                        <div class="col-md-1 col-xs-1">
                            <span class="flag flag-{{$locale}}"></span>
                        </div>
                        @endif
                    </div>
                @endforeach
            @else
                <div class="form-group">
                    <label class="col-md-2 col-xs-12 control-label">{{{ $field['label'] }}}</label>
                    <div class="col-md-7 col-xs-11">
                        @include('admin.layouts.smartfield')
                    </div>
                </div>
            @endif
        
        
        @endforeach

        <div class="form-group">
            <label class="col-md-2 col-xs-12 control-label" for="active">Continue Editing</label>
            <div class="col-md-8 col-xs-12">                                                                                                                                        
                <label class="check">{{Form::checkbox('continue', 1, true, array('class' => 'icheckbox'));}} Do you want to continue to edit page after saving?</label>
            </div>
        </div>

    </div>
    <div class="panel-footer">
        <a href="{{URL::to($moduleUri)}}" class="btn btn-default pull-left">Cancel</a>
        <button class="btn btn-primary pull-right">Save</button>
    </div>
</div>
{{ Form::close() }}
@stop
