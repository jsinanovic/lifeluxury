@extends('admin.layouts.main')

@section('breadcrumb')
<li><a href="{{URL::to($moduleUri)}}">{{ ucfirst($moduleName) }}</a></li>
<li><a href="{{URL::to($formAction)}}">{{ $title }}</a></li>
@stop

@section('content-title')
<!--
<div class="page-title">                    
    <h2><span class="fa fa-arrow-circle-o-left"></span> Example Title</h2>
</div>           
-->
@stop


@section('content')
<div class="row">
    <div class="col-md-12">
        @yield('form')
    </div>                   
</div>
@stop


@section('js')
{{HTML::script('js/plugins/icheck/icheck.min.js')}}
{{HTML::script('js/plugins/mcustomscrollbar/jquery.mCustomScrollbar.min.js')}}
{{HTML::script('js/plugins/summernote/summernote.js')}}
{{HTML::script('js/plugins/summernote/summernote-ext-video.js')}}

{{HTML::script('js/plugins/ckeditor/ckeditor.js')}}
{{HTML::script('js/plugins/ckeditor/adapters/jquery.js')}}

{{HTML::script('js/plugins/bootstrap/bootstrap-datepicker.js')}}
{{HTML::script('js/plugins/bootstrap/bootstrap-file-input.js')}}
{{HTML::script('js/plugins/bootstrap/bootstrap-select.js')}}
{{HTML::script('js/plugins/tagsinput/jquery.tagsinput.min.js')}}
{{HTML::script('js/plugins/jquery-validation/jquery.validate.js')}}
{{HTML::script('js/plugins/fileinput/fileinput.min.js')}}
{{HTML::script('js/plugins/tagsinput/jquery.tagsinput.min.js')}}
{{HTML::script('js/plugins/dropzone/dropzone.min.js')}}

<script>
    var rules = {{ $formRules }};
    var jvalidate = $("#jvalidate").validate({rules : rules});
    var moduleUri = '{{URL::to($moduleUri)}}';
    var entryId = {{$entry? $entry->id : 0}};
    var previewFolder = '{{$entry? $entry->getImageFolder() : 0}}';
</script>

{{HTML::script('js/form.js')}}
@stop
