<?php 
    $value = ($entry && $entry->translate($locale))? $entry->translate($locale)->{$key} : '';
    $key = $key . '[' . $locale . ']';
    $extraClass = isset($field['class'])? ' ' . $field['class'] : '';
?>
@if($field['type'] == 'text')
{{ Form::text($key, $value, array('class'=>'form-control')) }}

@elseif($field['type'] == 'date')
{{ Form::text($key, $entry? $entry->getOriginal('created_at') : '', array('class'=>'datepicker form-control' . $extraClass)) }}

@elseif($field['type'] == 'textarea')
{{ Form::textarea($key, $value, array('class'=>'form-control', 'rows' => 5, 'cols' => '200')) }}

@elseif($field['type'] == 'html')
{{ Form::textarea($key, $value, array('class'=>'summernote' . $extraClass)) }}

@elseif($field['type'] == 'advancedHtml')
{{ Form::textarea($key, $value, array('class'=>'advanced-html' . $extraClass)) }}

@elseif($field['type'] == 'features')
<div class="panel panel-colorful features {{$extraClass}}">
    {{ Form::hidden($key, $value) }}
    <div class="panel-heading">
        <h3 class="panel-title">{{{$field['label']}}}</h3>
        <ul class="panel-controls">                                                                        
            <li><a href="javascript: void(0);" class="feature-add"><span class="fa fa-plus"></span></a></li>
        </ul>
    </div>
    <div class="sortable panel-body list-group list-group-contacts"></div>
</div>

@elseif($field['type'] == 'custom')
    @include($baseName . '.' .$moduleName . '.' .$field['template'])
@endif

@if( isset($field['help']) && !empty($field['help']) && $field['type'] != 'checkbox')
<span class="help-block">{{ $field['help'] }}</span>
@endif