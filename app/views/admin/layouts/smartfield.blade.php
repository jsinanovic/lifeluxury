<?php $extraClass = isset($field['class'])? ' ' . $field['class'] : ''; ?>

@if($field['type'] == 'text')
{{ Form::text($key, null, array('class'=>'form-control' . $extraClass)) }}

@elseif($field['type'] == 'date')
{{ Form::text($key, $entry? $entry->getOriginal('created_at') : null, array('class'=>'datepicker form-control' . $extraClass)) }}

@elseif($field['type'] == 'money')
<div class="input-group">
    <span class="input-group-addon">$</span>
    {{ Form::number($key, null, array('class'=>'form-control')) }}
    <span class="input-group-addon">.00</span>
</div>

@elseif($field['type'] == 'percent')
<div class="input-group">
    {{ Form::number($key, null, array('class'=>'form-control')) }}
    <span class="input-group-addon">%</span>
</div>

@elseif($field['type'] == 'number')
{{ Form::number($key, null, array('class'=>'form-control' . $extraClass)) }}

@elseif($field['type'] == 'textarea')
<?php $a = Input::get($field['type'], null); ?>
{{ Form::textarea($key, (isset($field['default']) && !$entry && empty($a) )? $field['default'] : null, array('class'=>'form-control' . $extraClass, 'rows' => 5, 'cols' => '200')) }}

@elseif($field['type'] == 'html')
{{ Form::textarea($key, null, array('class'=>'summernote' . $extraClass)) }}

@elseif($field['type'] == 'advancedHtml')
{{ Form::textarea($key, null, array('class'=>'advanced-html' . $extraClass)) }}

@elseif($field['type'] == 'checkbox')
<label class="check">
    {{Form::checkbox($key, $field['value'], $field['default'], array('class' => 'icheckbox', 'id' => 'form-' . $key));}} 
    @if(isset($field['help']))
        {{ $field['help'] }}
    @endif
</label>

@elseif($field['type'] == 'select')
{{ Form::select($key, $field['values'], isset($field['value'])? $field['value'] : null, array('class' => 'form-control select' . $extraClass)) }} 

@elseif($field['type'] == 'multiselect')
{{ Form::select($key . '[]', $field['values'], null, array('class' => 'form-control select' . $extraClass, 'multiple')) }} 

@elseif($field['type'] == 'file')
<div class="file-control">
{{ Form::file($key, array('class' => 'file' . $extraClass, 'data-preview-file-type' => 'any')) }}
{{ Form::hidden($key . '_name', $entry? $entry->{$key} : '', array('class' => 'file-name')) }}
</div>

@elseif($field['type'] == 'gallery')
@if($entry)
<div class="dropzone" data-url="{{URL::to($moduleUri . 'upload-gallery-item/' . $entry->id)}}"></div>
<ul class="dropzone-files" style="display: none;">
    @foreach($entry->photos as $photo)
    <li data-url="{{$photo->getImage('gallery_thumb')}}" data-id="{{$photo->id}}" data-size="{{$photo->size}}">{{$photo->name}}</li>
    @endforeach
</ul>
@else
<h5>You will be able to add <b>{{{$field['label']}}}</b> after entry creation.</h5>
@endif


@elseif($field['type'] == 'tags')
{{ Form::text($key, null, array('class' => 'tagsinput', 'placeholder' => '')) }}

@elseif($field['type'] == 'features')
<div class="panel panel-colorful features {{$extraClass}}">
    {{ Form::hidden($key, $entry? $entry->{$key} : '') }}
    <div class="panel-heading">
        <h3 class="panel-title">{{{$field['label']}}}</h3>
        <ul class="panel-controls">                                                                        
            <li><a href="javascript: void(0);" class="feature-add"><span class="fa fa-plus"></span></a></li>
        </ul>
    </div>
    <div class="sortable panel-body list-group list-group-contacts"></div>
</div>

@elseif($field['type'] == 'custom')
    @include($baseName . '.' .$moduleName . '.' .$field['template'])

@endif

@if( isset($field['help']) && !empty($field['help']) && $field['type'] != 'checkbox')
<span class="help-block">{{ $field['help'] }}</span>
@endif