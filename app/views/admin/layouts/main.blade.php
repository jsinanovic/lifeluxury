<!DOCTYPE html>
<html lang="en">
    <head>        
        <!-- META SECTION -->
        <title>Admin Panel</title>            
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <meta http-equiv="X-UA-Compatible" content="IE=edge" />
        <meta name="viewport" content="width=device-width, initial-scale=1" />
        
        <link rel="icon" href="favicon.ico" type="image/x-icon" />
        <!-- END META SECTION -->
        
        <!-- CSS INCLUDE -->        
        {{HTML::style('css/theme-default.css')}}
        {{HTML::style('css/admin.css')}}
        @yield('css')
        <!-- EOF CSS INCLUDE -->                                       
    </head>
    <body data-base="{{URL::to('/')}}">
        <!-- START PAGE CONTAINER -->
        <div class="page-container">
            
            @include('admin.layouts.sidebar')
            
            <!-- PAGE CONTENT -->
            <div class="page-content">
                
                @include('admin.layouts.navbar')                     

                <!-- START BREADCRUMB -->
                <ul class="breadcrumb">
                    <li><a href="{{URL::to('admin')}}">Dashboard</a></li>
                    @yield('breadcrumb')
                </ul>
                <!-- END BREADCRUMB -->         
                
                @yield('content-title')
                
                <!-- PAGE CONTENT WRAPPER -->
                <div class="page-content-wrap">
                    @yield('content')
                </div>
                <!-- END PAGE CONTENT WRAPPER -->                                
            </div>            
            <!-- END PAGE CONTENT -->
        </div>
        <!-- END PAGE CONTAINER -->

        <!-- MESSAGE BOX-->
        <div class="message-box animated fadeIn" data-sound="alert" id="mb-signout">
            <div class="mb-container">
                <div class="mb-middle">
                    <div class="mb-title"><span class="fa fa-sign-out"></span> Log <strong>Out</strong> ?</div>
                    <div class="mb-content">
                        <p>Are you sure you want to log out?</p>                    
                        <p>Press No if youwant to continue work. Press Yes to logout current user.</p>
                    </div>
                    <div class="mb-footer">
                        <div class="pull-right">
                            {{ HTML::link('admin/logout', 'Yes', array('class' => 'btn btn-success btn-lg')) }}
                            <button class="btn btn-default btn-lg mb-control-close">No</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="message-box animated fadeIn" data-sound="alert" id="mb-commingsoon">
            <div class="mb-container">
                <div class="mb-middle">
                    <div class="mb-title"><span class="fa fa-sign-out"></span> Coming <strong>Soon</strong> activate page ?</div>
                    <div class="mb-content">
                        <p>Are you sure you want to disable page and activate coming soon page?</p>
                        <p>Press No if you want to continue work. Press Yes to  activate coming soon page.</p>
                    </div>
                    <div class="mb-footer">
                        <div class="pull-right">
                            {{ HTML::link('admin/commingsoon', 'Yes', array('class' => 'btn btn-success btn-lg')) }}
                            <button class="btn btn-default btn-lg mb-control-close">No</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="message-box animated fadeIn" data-sound="alert" id="mb-commingsoonEnable">
            <div class="mb-container">
                <div class="mb-middle">
                    <div class="mb-title"><span class="fa fa-sign-out"></span> Coming <strong>Soon</strong> disable page ?</div>
                    <div class="mb-content">
                        <p>Are you sure you want to disable coming soon page?</p>
                        <p>Press No if you want to continue work. Press Yes to disable coming soon page.</p>
                    </div>
                    <div class="mb-footer">
                        <div class="pull-right">
                            {{ HTML::link('admin/commingsoonEnable', 'Yes', array('class' => 'btn btn-success btn-lg')) }}
                            <button class="btn btn-default btn-lg mb-control-close">No</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- END MESSAGE BOX-->

        <!-- START PRELOADS -->
        <audio id="audio-alert" src="{{asset('/')}}audio/alert.mp3" preload="auto"></audio>
        <audio id="audio-fail" src="{{asset('/')}}audio/fail.mp3" preload="auto"></audio>
        <!-- END PRELOADS -->       
        
        <div class="message-box message-box-danger animated fadeIn" data-sound="fail" id="msg-alert">
            <div class="mb-container">
                <div class="mb-middle">
                    <div class="mb-title"><span class="fa fa-times"></span> DANGER!</div>
                    <div class="mb-content">
                        <p></p>                    
                    </div>
                    <div class="mb-footer">
                        <button class="btn btn-default btn-lg pull-right mb-control-close">Close</button>
                    </div>
                </div>
            </div>
        </div>
        
    <!-- START SCRIPTS -->
        <!-- START PLUGINS -->
        {{HTML::script('js/plugins/jquery/jquery.min.js')}}
        {{HTML::script('js/plugins/jquery/jquery-ui.min.js')}}
        {{HTML::script('js/plugins/bootstrap/bootstrap.min.js')}}
        <!-- END PLUGINS -->
        
        <!-- START TEMPLATE -->
        <!--<script type="text/javascript" src="js/settings.js"></script>-->
        {{HTML::script('js/plugins.js')}}
        {{HTML::script('js/actions.js')}}
        {{HTML::script('js/admin.js')}}
        <!-- END TEMPLATE -->

        <!-- START THIS PAGE PLUGINS-->    
        @yield('js')
        <!-- END THIS PAGE PLUGINS-->    
        
    <!-- END SCRIPTS -->         
    </body>
</html>






