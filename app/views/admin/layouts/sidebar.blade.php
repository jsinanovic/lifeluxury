<!-- START PAGE SIDEBAR -->
<div class="page-sidebar">
    <!-- START X-NAVIGATION -->
    <ul class="x-navigation">
        <li class="xn-logo">
            <a href="{{URL::to('/')}}">{{{ Setting::value('site-name')}}}</a>
            <a href="#" class="x-navigation-control"></a>
        </li>
        <!--<li class="xn-profile">
            <a href="#" class="profile-mini">
                <img src="{{Auth::user()->getAvatar()}}" alt="{{Auth::user()->first_name}} {{Auth::user()->last_name}}"/>
            </a>
            <div class="profile">
                <div class="profile-image">
                    <img src="{{Auth::user()->getAvatar()}}" alt="{{Auth::user()->first_name}} {{Auth::user()->last_name}}"/>
                </div>
                <div class="profile-data">
                    <div class="profile-data-name">{{Auth::user()->first_name}} {{Auth::user()->last_name}}</div>
                    <div class="profile-data-title">Administrator</div>
                </div>
                <div class="profile-controls">
                    <a href="#" onclick="alert('Cooming soon')" class="profile-control-left"><span class="fa fa-info"></span></a>
                    <a href="{{URL::to('admin/client-requests/')}}" class="profile-control-right"><span class="fa fa-envelope"></span></a>
                </div>
            </div>                                                                        
        </li>-->
        
        <!--<li class="xn-title">Main</li>-->
        <!--<li class="{{ ($moduleName == 'admin')? 'active' : '' }}">
            <a href="{{URL::to('admin')}}"><span class="fa fa-desktop"></span> <span class="xn-text">Dashboard</span>
            </a>                        
        </li>-->
        
        
        <li class="xn-openable {{ ($moduleName == 'pages')? 'active' : '' }}">
            <a href="#"><span class="fa fa-file-text"></span> <span class="xn-text">Pages</span></a>
            <ul>
                <li><a href="{{URL::to('admin/pages')}}"><span class="fa fa-copy"></span> List of Pages</a></li>
                <li><a href="{{URL::to('admin/pages/create')}}"><span class="fa fa-plus"></span> Add a new page</a></li>                       
            </ul>
        </li>
        
        
        <li class="xn-openable {{ ($moduleName == 'menus')? 'active' : '' }}">
            <a href="#"><span class="fa fa-sitemap"></span> <span class="xn-text">Menus</span></a>
            <ul>
                <li><a href="{{URL::to('admin/menus')}}"><span class="fa fa-list-ul"></span> List of Menus</a></li>
                @if(count($menus) <= 5)
                    @foreach($menus as $menu)
                        <li><a href="{{URL::to('admin/menus/view/' . $menu->id)}}"><span class="fa fa-list-alt"></span> {{{ $menu->name }}}</a></li>
                    @endforeach
                @endif
                <li><a href="{{URL::to('admin/menus/create')}}"><span class="fa fa-plus"></span> Add a new menu</a></li>                       
            </ul>
        </li>
        
        
        <li class="xn-openable {{ ($moduleName == 'articles')? 'active' : '' }}">
            <a href="#"><span class="fa fa-pencil"></span> <span class="xn-text">Magazine</span></a>
            <ul>
                <li><a href="{{URL::to('admin/articles')}}"><span class="fa fa-copy"></span> List of Articles</a></li>
                <li><a href="{{URL::to('admin/articles/create')}}"><span class="fa fa-plus"></span> Add a new Article</a></li>
                @if(isset($menus['blog']))
                <li><a href="{{URL::to('admin/menus/view/' . $menus['blog']->id)}}"><span class="fa fa-list-alt"></span> Edit menu</a></li>
                @endif
            </ul>
        </li>
        
        <li class="xn-openable {{ ($moduleName == 'services')? 'active' : '' }}">
            <a href="#"><span class="fa fa-briefcase"></span> <span class="xn-text">Services</span></a>
            <ul>                                    
                <li><a href="{{URL::to('admin/services')}}"><span class="fa fa-copy"></span> List of services</a></li>
                <li><a href="{{URL::to('admin/services/create')}}"><span class="fa fa-plus"></span> Add a new Service</a></li>
            </ul>
        </li>
        
        <!--<li class="xn-openable {{ ($moduleName == 'products')? 'active' : '' }}">
            <a href="#"><span class="fa fa-cart-plus"></span> <span class="xn-text">Shop</span></a>
            <ul>                                    
                <li><a href="{{URL::to('admin/products')}}"><span class="fa fa-copy"></span> List of Products</a></li>
                <li><a href="{{URL::to('admin/products/create')}}"><span class="fa fa-plus"></span> Add a new Product</a></li>
                @if(isset($menus['shop']))
                <li><a href="{{URL::to('admin/menus/view/' . $menus['shop']->id)}}"><span class="fa fa-list-alt"></span> Edit menu</a></li>
                @endif
            </ul>
        </li>
        
        <li class="xn-openable {{ ($moduleName == 'works')? 'active' : '' }}">
            <a href="#"><span class="fa fa-briefcase"></span> <span class="xn-text">Portfolio</span></a>
            <ul>                                    
                <li><a href="{{URL::to('admin/works')}}"><span class="fa fa-copy"></span> List of works</a></li>
                <li><a href="{{URL::to('admin/works/create')}}"><span class="fa fa-plus"></span> Add a new Work</a></li>
            </ul>
        </li>
        
        <li class="xn-openable {{ ($moduleName == 'prices')? 'active' : '' }}">
            <a href="#"><span class="fa fa-dollar"></span> <span class="xn-text">Prices</span></a>
            <ul>
                <li><a href="{{URL::to('admin/prices')}}"><span class="fa fa-copy"></span> List of prices</a></li>
                <li><a href="{{URL::to('admin/prices/create')}}"><span class="fa fa-plus"></span> Add a new price</a></li>
            </ul>
        </li>
        -->
        
        <li class="xn-openable {{ ($moduleName == 'client-requests')? 'active' : '' }}">
            <a href="#">
                <span class="fa fa-user-plus"></span>
                <span class="xn-text">Clients Requests</span>
                @if($requestsCounter['new'])
                <div class="informer informer-warning">+{{$requestsCounter['new']}}</div>
                @endif
            </a>
            <ul>
                @foreach(ClientRequest::$types as $type => $name)
                <li>
                    <a href="{{URL::to('admin/client-requests/' . $type)}}">
                        <span class="fa fa-envelope"></span> {{{$name}}} 
                        @if($requestsCounter['new_' . $type])
                        <div class="informer informer-warning">+{{$requestsCounter['new_' . $type]}}</div>
                        @endif
                    </a>
                </li>
                @endforeach
            </ul>
        </li>
        
        <li class="xn-openable {{ ($moduleName == 'subscribers')? 'active' : '' }}">
            <a href="#"><span class="fa fa-user-plus"></span> <span class="xn-text">Subscribers</span></a>
            <ul>                                    
                <li>
                    <a href="{{URL::to('admin/subscribers')}}">
                        <span class="fa fa-copy"></span> List of Subscribers
                        @if($newSubscribers > 0)
                            <div class="informer informer-warning">+{{$newSubscribers}}</div>
                        @endif
                    </a>
                </li>
                <li><a href="{{URL::to('admin/subscribers/create')}}"><span class="fa fa-plus"></span> Add a new Subscriber</a></li>
                <li><a href="{{URL::to('admin/subscribers/message')}}"><span class="fa fa-mail-reply-all"></span> Send message</a></li>
            </ul>
        </li>
        
        <li class="xn-openable {{ ($moduleName == 'settings')? 'active' : '' }}">
            <a href="#"><span class="fa fa-gears"></span> <span class="xn-text">Settings</span></a>
            <ul>
                <li><a href="{{URL::to('admin/settings')}}"><span class="fa fa-gear"></span> General Settings</a></li>
                <li><a href="{{URL::to('admin/settings/create')}}"><span class="fa fa-plus"></span> Add a new setting</a></li>                       
            </ul>
        </li>
        
        @if(Auth::user()->membership_id == 2)
        <li class="xn-openable {{ ($moduleName == 'modules')? 'active' : '' }}">
            <a href="#"><span class="fa fa-database"></span> <span class="xn-text">Modules</span></a>
            <ul>
                <li><a href="{{URL::to('admin/modules')}}"><span class="fa fa-code"></span> All Modules</a></li>
                <li><a href="{{URL::to('admin/modules/create')}}"><span class="fa fa-code-fork"></span> Add a new module</a></li>                       
            </ul>
        </li>
        <li>
            <a href="javascript:void(0);" class="clear-cache"><span class="fa fa-refresh"></span> <span class="xn-text">Clear cache</span></a>
        </li>
        @endif
        
        
    </ul>
    <!-- END X-NAVIGATION -->
</div>
<!-- END PAGE SIDEBAR -->