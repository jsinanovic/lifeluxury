<tr data-id="{{$price->id}}">
    <td class="services">{{{$price->name}}}</td>
    <td class="time text-center">{{$num > 0? '+ ': ''}}{{$price->getTime()}} days</td>
    <td class="cost text-center">
        @if($price->price > 0)
        {{{$price->price}}}$
        @elseif($price->percent > 0)
        +{{{$price->percent}}}%
        @endif
    </td>
    <td class="actions text-right">
        <a href="{{URL::to('admin/prices/edit/' . $price->id)}}" class="btn btn-default btn-rounded btn-condensed btn-sm"><span class="fa fa-pencil"></span></a>
        <button class="btn btn-danger btn-rounded btn-condensed btn-sm" onclick="removeEntry($(this).closest('tr'), {{$price->id}})"><span class="fa fa-times"></span></button>
    </td>
</tr>