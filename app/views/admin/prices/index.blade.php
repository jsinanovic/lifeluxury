@extends('admin.layouts.main')

@section('breadcrumb')
<li><a href="{{URL::to($moduleUri)}}">{{ ucfirst($moduleName) }}</a></li>
@stop

@section('content')
<div class="row">
    <div class="col-md-12">
        <div class="panel panel-default tabs">
            <ul class="nav nav-tabs">
                @foreach($categories as $key => $category)
                <li class="{{$key==0? 'active' : ''}}">
                    <a href="#{{{$category->slug}}}" data-toggle="tab" aria-expanded="{{$key==0? 'true' : 'false'}}">{{{$category->name}}}</a>
                </li>
                @endforeach
                <li>
                    <a href="{{$menuLink}}">&nbsp;<b>+</b>&nbsp;</a>
                </li>
            </ul>
            <div class="panel-body tab-content">
                @foreach($categories as $key => $category)
                <div class="tab-pane {{$key==0? 'active' : ''}}" id="{{$category->slug}}">
                    @if($category->content)
                    <p>{{$category->content}}</p>
                    @endif
                    
                    <table class="table prices-table sortable-table">
                        @foreach($category->pricesList() as $num => $price)
                            @include('admin.prices.item', array('price' => $price, 'num' => $num) )
                        @endforeach
                        <tr class="active"><td colspan="4"><a href="{{URL::to('admin/prices/create/' . $category->id)}}" class=""><b>+ Add Price</b></a></td></tr>
                    </table>

                    @foreach($category->categories()->active()->get() as $subcategory)
                    <h2 class="lower">
                        {{{$subcategory->name}}} 
                        <a href="{{$menuLink}}" class="btn btn-default btn-rounded btn-condensed btn-sm"><span class="fa fa-pencil"></span></a>
                    </h2>
                    <table class="table prices-table sortable-table">
                        @foreach($subcategory->pricesList() as $num => $price)
                            @include('admin.prices.item', array('price' => $price, 'num' => $num) )
                        @endforeach
                        <tr class="active"><td colspan="4"><a href="{{URL::to('admin/prices/create/' . $subcategory->id)}}"><b>+ Add Price</b></a></td></tr>
                    </table>
                    @endforeach
                    
                    <a href="{{$menuLink}}" class="btn-block btn btn-primary"><b>+ Add Subcategory</b></a>
                    
                </div>
                @endforeach                    
            </div>
        </div>
    </div>
</div>
@stop


@section('css')
<style>
    td.services{
        width: 50%;
    }
    td.time{
        width: 20%;
    }
    td.cost{
        width: 20%;
    }
    td.actions{
        width: 10%;
    }
    .sortable-table tr{
        background: #fff;
        width: 100%;
    }
</style>
@stop

@section('js')
{{HTML::script('js/plugins/icheck/icheck.min.js');}}
{{HTML::script('js/plugins/mcustomscrollbar/jquery.mCustomScrollbar.min.js');}}
{{HTML::script('js/plugins/datatables/jquery.dataTables.min.js');}}
<script>
    var moduleUri = '{{ URL::to($moduleUri) }}';
    var panel = $('.panel');
</script>
@stop