@extends('admin.layouts.main')

@section('breadcrumb')
<li><a href="{{URL::to($moduleUri)}}">{{ ucfirst($moduleName) }}</a></li>
@stop

@section('content')
<div class="row">
    <div class="col-md-12">

        <div class="panel panel-success">
            <div class="panel-heading">
                <div class="pull-left">
                    <h3 class="panel-title">{{{$entry->user_name}}} {{{$entry->user_surname}}} <small>({{{$entry->user_email}}})</small></h3>
                </div>
                <div class="pull-right">
                    <button class="btn {{$entry->status == 2? 'btn-success' : 'btn-default'}} change-status"><span class="fa fa-check"></span></button>
                    <button class="btn btn-danger delete-entry"><span class="fa fa-trash-o"></span></button>                                    
                </div>
            </div>
            <div class="panel-body">
                <h3>{{{$entry->subject}}} <small class="pull-right text-muted"><span class="fa fa-clock-o"></span> {{{$entry->created_at}}}</small></h3>
                
                <div class="panel-info">
                    @if($entry->type == 'contacts')
                    <div class="panel-field"><b>Message:</b> {{$entry->message}}</div>
                    
                    @elseif($entry->type == 'callback')
                    <div class="panel-field"><b>Phone number:</b> {{$entry->user_phone}}</div>
                    <div class="panel-field"><b>Call time:</b> 
                        @if($entry->call_time==1)
                            Morning
                        @elseif($entry->call_time==2)
                            Afternoon
                        @else
                            All time
                        @endif
                    </div>
                    
                    @elseif($entry->type == 'quote')
                    <div class="panel-field"><b>Website:</b> {{$entry->user_website}}</div>
                    <div class="panel-field"><b>Services:</b> {{$entry->service}}</div>
                    <div class="panel-field"><b>Description:</b> {{$entry->description}}</div>
                    <div class="panel-field"><b>Budget:</b> {{$entry->budget}}</div>
                    @endif
                </div>
                
                {{{$entry->message}}}

                <!--<div class="form-group push-up-20">
                    <label>Quick Reply</label>
                    <textarea class="form-control summernote_lite" rows="3" placeholder="Click to get editor"></textarea>
                </div>-->
            </div>
            <!--
            <div class="panel-body panel-body-table">
                <h6>Attachments</h6>
                <table class="table table-bordered table-striped">
                    <tr>
                        <th width="50">type</th><th>name</th><th width="100">size</th>
                    </tr>
                    <tr>
                        <td><span class="label label-primary">HTML</span></td><td><a href="#">index.html</a></td><td>54 Kb</td>
                    </tr>
                    <tr>
                        <td><span class="label label-success">CSS</span></td><td><a href="#">stylesheet.css</a></td><td>15 Kb</td>
                    </tr>                                    
                    <tr>
                        <td><span class="label label-danger">JS</span></td><td><a href="#">actions.js</a></td><td>3 Kb</td>
                    </tr>                                    
                </table>
            </div>-->
            <div class="panel-footer">
                <a href="{{URL::to($moduleUri . $entry->type)}}" class="btn btn-default pull-left back">Back</a>
                <!--<button class="btn btn-primary pull-right"><span class="fa fa-mail-reply"></span> Post Reply</button>-->
            </div>
        </div>
    </div>
</div>
@stop

@section('js')
<script>
    var moduleUri = '{{ URL::to($moduleUri) }}';
            var back = '{{URL::to($moduleUri . $entry->type)}}';
    var id = {{$entry->id}};
            $(document).ready(function(){
    $('.change-status').click(function(){
    var $button = $(this);
            $button.button('loading');
            changeStatus($button, id, function(){
            $button.button('reset');
                    $button.toggleClass('btn-default').toggleClass('btn-success');
            });
    });
        
            $('.delete-entry').click(function(){
    var $button = $(this);
            $button.button('loading');
            removeEntry($button, id, function(){
            $button.button('reset');
                    window.location.href = back;
            });
    });
    });
</script>
@stop