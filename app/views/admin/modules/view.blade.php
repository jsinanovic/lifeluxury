@extends('admin.layouts.main')

@section('breadcrumb')
<li><a href="{{URL::to($moduleUri)}}">{{ ucfirst($moduleName) }}</a></li>
<li>{{$module->name}}</li>
@stop

@section('content')
<div class="row">
    <div class="col-md-12">
        <div class="panel panel-success">
            <div class="panel-heading">                                
                <h3 class="panel-title">Module {{$module->name}}</h3>                         
            </div>
            <div class="panel-body">
                <div class="col-md-6">
                    <form method="POST" action="{{URL::to('admin/modules/generate-migration/' . $module->id)}}" class="form">
                        <div class="form-group">
                            <label> Create Table Migration: </label>
                            <textarea class="form-control" name="migration" placeholder="Enter fields string">{{$module->migration}}</textarea>
                            <p class="help-block">Something like this: "name:string(255):unique, content:text:default('Test'):nullable"</p>
                        </div>
                        <button type="submit" class="btn btn-primary" >Submit</button>
                    </form>
                    
                    <a class='btn btn-primary' href="{{URL::to('admin/modules/generate-controller/' . $module->id);}}">Generate Controller</a>
                    <a class='btn btn-primary' href="{{URL::to('admin/modules/generate-model/' . $module->id);}}">Generate Model</a>
                </div>
                
            </div>
        </div>
    </div>
</div>
@stop