@extends('admin.layouts.main')

@section('breadcrumb')
<li><a href="{{URL::to($moduleUri)}}">{{ ucfirst($moduleName) }}</a></li>
@stop

@section('content')
<div class="row">
    <div class="col-md-12">
        <!-- START DEFAULT DATATABLE -->
        <div class="panel panel-success">
            <div class="panel-heading">                                
                <h3 class="panel-title">{{$pageName}}</h3>
                <ul class="panel-controls">
                    <li><a href="#" class="panel-refresh"><span class="fa fa-refresh"></span></a></li>
                </ul>                                
            </div>
            <div class="panel-body">
                <div class="table-responsive">
                    <table class="table admin-datatable table-hover">
                        <thead>
                            <tr>
                                <th>Email</th>
                                <th>Name</th>
                                <th>Surname</th>
                                <th>Country</th>
                                <th>Created</th>
                                <th>Subscription status</th>
                                <th>Actions</th>
                            </tr>
                        </thead>
                        <tbody></tbody>
                    </table>
                </div>
            </div>
        </div>
        <!-- END DEFAULT DATATABLE -->
    </div>
</div>
@stop

@section('js')
{{HTML::script('js/plugins/icheck/icheck.min.js');}}
{{HTML::script('js/plugins/mcustomscrollbar/jquery.mCustomScrollbar.min.js');}}
{{HTML::script('js/plugins/datatables/jquery.dataTables.min.js');}}
<script>
    var moduleUri = '{{ URL::to($moduleUri) }}';
    
    var panel = $('.panel');
    var table = $(".admin-datatable").on( 'processing.dt', function ( e, settings, processing ) {
        panel_refresh(panel);
    }).dataTable({
        processing : true,
        ajax : moduleUri + '/all-json', //?type=' + type,
        stateSave : true,
        "columnDefs": [ {
            "targets": 3,
            sClass : 'text-center'
        },{
            sClass : 'text-center',
            "orderable": false,
            "targets": -2,
            "data": null,
            "render": function ( data, type, row ) {
                return '<label class="switch change-status">'
                     + '<input type="checkbox" '+ (parseInt(data[data.length - 2])?'checked="checked"' : '')
                     + 'value="0"><span onclick="changeStatus($(this).closest(\'.switch\'), '+data[data.length - 1]+');return false;"></span></label>';
            },
        }, {
            sClass : 'text-center',
            "orderable": false,
            "targets": -1,
            "data": null,
            "render": function ( data, type, row ) {
                return '<div class="btn-group"><a href="' + moduleUri + '/edit/' + data[data.length - 1] + '" '  
                        + 'class="btn btn-primary btn-condensed"><i class="fa fa-pencil"></i></a> '
                        + '<button class="btn btn-danger btn-condensed" onclick="removeEntry($(this).closest(\'tr\'), '+data[data.length - 1]+', onRowDeleted)"><i class="fa fa-trash-o"></i></button></div>';
            },
        } ],
        
    });
    
    function onRowDeleted($row) {
        table.api().row($row).remove().draw();
    }
    
    $(".panel-refresh").on("click",function(){
        table.api().ajax.reload();
    });
    $(".admin-datatable").on('page.dt',function () {
        onresize(100);
    });
</script>

<style>
    .table .btn.btn-primary{
        color: #e4e4e4;
    }
</style>
@stop