<!DOCTYPE html>
<html lang="en" class="body-full-height">
    <head>        
        <!-- META SECTION -->
        <title>Password Recovery | {{$settings['site-name']}}</title>    
        <meta name="description" content="Cres Cor Reps - personal cabinet and dashboard for Cres Cor Representatives">
        <meta name="keywords" content="Cres Cor Reps, Reps, Representatives, Cres Cor, CresCor, Cres Cor Inc., Mobile Food Service, Mobile, Food Service, Mobile FoodService, FoodService">
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <meta http-equiv="X-UA-Compatible" content="IE=edge" />
        <meta name="viewport" content="width=device-width, initial-scale=1" />
        
        <link rel="apple-touch-icon" sizes="57x57" href="{{asset('favicons/apple-touch-icon-57x57.png')}}">
        <link rel="apple-touch-icon" sizes="60x60" href="{{asset('favicons/apple-touch-icon-60x60.png')}}">
        <link rel="apple-touch-icon" sizes="72x72" href="{{asset('favicons/apple-touch-icon-72x72.png')}}">
        <link rel="apple-touch-icon" sizes="76x76" href="{{asset('favicons/apple-touch-icon-76x76.png')}}">
        <link rel="apple-touch-icon" sizes="114x114" href="{{asset('favicons/apple-touch-icon-114x114.png')}}">
        <link rel="apple-touch-icon" sizes="120x120" href="{{asset('favicons/apple-touch-icon-120x120.png')}}">
        <link rel="apple-touch-icon" sizes="144x144" href="{{asset('favicons/apple-touch-icon-144x144.png')}}">
        <link rel="apple-touch-icon" sizes="152x152" href="{{asset('favicons/apple-touch-icon-152x152.png')}}">
        <link rel="apple-touch-icon" sizes="180x180" href="{{asset('favicons/apple-touch-icon-180x180.png')}}">
        <link rel="icon" type="image/png" href="{{asset('favicons/favicon-32x32.png')}}" sizes="32x32">
        <link rel="icon" type="image/png" href="{{asset('favicons/android-chrome-192x192.png')}}" sizes="192x192">
        <link rel="icon" type="image/png" href="{{asset('favicons/favicon-96x96.png')}}" sizes="96x96">
        <link rel="icon" type="image/png" href="{{asset('favicons/favicon-16x16.png')}}" sizes="16x16">
        <link rel="manifest" href="{{asset('favicons/manifest.json')}}">
        <meta name="msapplication-TileColor" content="#ffffff">
        <meta name="msapplication-TileImage" content="{{asset('favicons/mstile-144x144.png')}}">
        <meta name="theme-color" content="#ffffff">
        <!-- END META SECTION -->

        <!-- CSS INCLUDE -->        
        {{HTML::style('css/theme-default.css')}}
        {{HTML::style('css/admin.css')}}
        <style>
            label.error{font-size: 12px;font-weight: bold;}
            .g-recaptcha > div > div {width: 100% !important;text-align: center;}
        </style>
        <!-- EOF CSS INCLUDE -->                          
    </head>
    <body>

        <div class="login-container">

            <div class="login-box animated fadeInDown">
                <a href="{{URL::to('/')}}" class="login-logo" style="margin-bottom: 15px;"></a>
                <div class="login-body">
                    <div class="login-title">
                        <strong>Password Reset</strong>
                    </div>
                    

                    {{ Form::open(array('url'=> 'password/reset', 'class'=>'form-horizontal remind-form', 'method' => 'POST')) }}
                        <input type="hidden" name="token" value="{{ $token }}">

                    @if(Session::get('error'))
                    <div class="form-group">
                        <div class="alert alert-danger" role="alert">
                            <button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">×</span><span class="sr-only">Close</span></button>
                            <strong>{{Session::get('error')}}</strong>
                        </div>
                    </div>
                    @endif

                    @if(Session::get('status'))
                    <div class="form-group">
                        <div class="alert alert-success" role="alert">
                            <button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">×</span><span class="sr-only">Close</span></button>
                            <strong>{{Session::get('status')}}</strong>
                        </div>
                    </div>
                    @endif

                    <div class="form-group">
                        {{Form::text('email', null, array('class'=>'form-control', 'placeholder'=>'E-mail', 'id' => 'email'))}}
                    </div>
                    
                    <div class="form-group">
                        <input type="password" name="password" class="form-control" placeholder="New Password">
                    </div>
                    <div class="form-group">
                        <input type="password" name="password_confirmation" class="form-control" placeholder="Password Confirmation">
                    </div>
                    

                    <div class="form-group">
                            {{ Form::submit('Reset Password', array('class'=>'btn btn-info btn-block'))}}
                    </div>
                    {{ Form::close() }}
                </div>

                <div class="login-footer">
                    <div class="pull-left">
                        &copy; {{date('Y')}} Cres Cor
                    </div>
                    <div class="pull-right">
                        <a href="http://www.crescor.com/about/story" target="_blank">About</a> |
                        <a href="{{URL::to('/faq')}}">FAQ</a> |
                        <a href="http://www.crescor.com/about/contact" target="_blank">Contact Us</a>
                    </div>
                </div>
            </div>

        </div>


        {{HTML::script('js/plugins/jquery/jquery.min.js')}}
        {{HTML::script('js/plugins/bootstrap/bootstrap.min.js')}}
        {{HTML::script('js/plugins/jquery-validation/jquery.validate.js')}}
        <script>
            $(document).ready(function () {
                $('.remind-form').validate({
                    rules: {
                        email: {
                            required: true,
                            email: true,
                        }
                    },
                })
            });
        </script>
    </body>
</html>






