@extends('admin.layouts.main')


@section('content')
<!-- START WIDGETS -->                    
<div class="row">
    <div class="col-md-3">

        <!-- START WIDGET MESSAGES -->
        <div class="widget widget-default widget-item-icon" onclick="location.href = '{{URL::to('admin/client-requests')}}';">
            <div class="widget-item-left">
                <span class="fa fa-envelope"></span>
            </div>                             
            <div class="widget-data">
                <div class="widget-int num-count">{{ClientRequest::getNewCount()}}</div>
                <div class="widget-title">New messages</div>
                <div class="widget-subtitle">In Clients Requests</div>
            </div>      
            <div class="widget-controls">                                
                <a href="#" class="widget-control-right widget-remove" data-toggle="tooltip" data-placement="top" title="Remove Widget"><span class="fa fa-times"></span></a>
            </div>
        </div>
        <!-- END WIDGET MESSAGES -->

    </div>
    <div class="col-md-3">

        <!-- START WIDGET REGISTRED -->
        <div class="widget widget-default widget-item-icon">
            <div class="widget-item-left">
                <span class="fa fa-user"></span>
            </div>
            <div class="widget-data">
                <div class="widget-int num-count">{{Subscriber::count()}}</div>
                <div class="widget-title">Subscribers</div>
                <div class="widget-subtitle">On your website</div>
            </div>
            <div class="widget-controls">                                
                <a href="#" class="widget-control-right widget-remove" data-toggle="tooltip" data-placement="top" title="Remove Widget"><span class="fa fa-times"></span></a>
            </div>                            
        </div>                            
        <!-- END WIDGET REGISTRED -->

    </div>
    
    <div class="col-md-3">

        <!-- START WIDGET REGISTRED -->
        <div class="widget widget-default widget-item-icon" onclick="location.href = '{{URL::to('admin/pages')}}';">
            <div class="widget-item-left">
                <span class="fa fa-file-text"></span>
            </div>
            <div class="widget-data">
                <div class="widget-int num-count">{{Page::count()}}</div>
                <div class="widget-title">Pages</div>
                <div class="widget-subtitle">On the Site</div>
            </div>
            <div class="widget-controls">                                
                <a href="#" class="widget-control-right widget-remove" data-toggle="tooltip" data-placement="top" title="Remove Widget"><span class="fa fa-times"></span></a>
            </div>                            
        </div>                            
        <!-- END WIDGET REGISTRED -->

    </div>
    
    <div class="col-md-3">

        <!-- START WIDGET REGISTRED -->
        <div class="widget widget-default widget-item-icon" onclick="location.href = '{{URL::to('admin/articles')}}';">
            <div class="widget-item-left">
                <span class="fa fa-pencil"></span>
            </div>
            <div class="widget-data">
                <div class="widget-int num-count">{{Article::count()}}</div>
                <div class="widget-title">Articles</div>
                <div class="widget-subtitle">In the Blog</div>
            </div>
            <div class="widget-controls">                                
                <a href="#" class="widget-control-right widget-remove" data-toggle="tooltip" data-placement="top" title="Remove Widget"><span class="fa fa-times"></span></a>
            </div>                            
        </div>                            
        <!-- END WIDGET REGISTRED -->

    </div>
    
    <div class="col-md-3">

        <!-- START WIDGET REGISTRED -->
        <div class="widget widget-default widget-item-icon" onclick="location.href = '{{URL::to('admin/products')}}';">
            <div class="widget-item-left">
                <span class="fa fa-cart-arrow-down"></span>
            </div>
            <div class="widget-data">
                <div class="widget-int num-count">{{Product::count()}}</div>
                <div class="widget-title">Products</div>
                <div class="widget-subtitle">In your Shop</div>
            </div>
            <div class="widget-controls">                                
                <a href="#" class="widget-control-right widget-remove" data-toggle="tooltip" data-placement="top" title="Remove Widget"><span class="fa fa-times"></span></a>
            </div>                            
        </div>                            
        <!-- END WIDGET REGISTRED -->

    </div>
    
    <div class="col-md-3">

        <!-- START WIDGET REGISTRED -->
        <div class="widget widget-default widget-item-icon" onclick="location.href = '{{URL::to('admin/works')}}';">
            <div class="widget-item-left">
                <span class="fa fa-briefcase"></span>
            </div>
            <div class="widget-data">
                <div class="widget-int num-count">{{Work::count()}}</div>
                <div class="widget-title">Works</div>
                <div class="widget-subtitle">In your Portfolio</div>
            </div>
            <div class="widget-controls">                                
                <a href="#" class="widget-control-right widget-remove" data-toggle="tooltip" data-placement="top" title="Remove Widget"><span class="fa fa-times"></span></a>
            </div>                            
        </div>

    </div>
    
    <div class="col-md-3">

        <div class="widget widget-default widget-item-icon" onclick="location.href = '{{URL::to('admin/prices')}}';">
            <div class="widget-item-left">
                <span class="fa fa-dollar"></span>
            </div>
            <div class="widget-data">
                <div class="widget-int num-count">{{Price::count()}}</div>
                <div class="widget-title">Prices</div>
                <div class="widget-subtitle">In your Prices Table</div>
            </div>
            <div class="widget-controls">                                
                <a href="#" class="widget-control-right widget-remove" data-toggle="tooltip" data-placement="top" title="Remove Widget"><span class="fa fa-times"></span></a>
            </div>                            
        </div>

    </div>
    
    <div class="col-md-3">

        <div class="widget widget-default widget-item-icon" onclick="location.href = '{{URL::to('admin/settings')}}';">
            <div class="widget-item-left">
                <span class="fa fa-gears"></span>
            </div>
            <div class="widget-data">
                <div class="widget-int num-count">{{Setting::count()}}</div>
                <div class="widget-title">Settings</div>
                <div class="widget-subtitle">In your Admin Panel</div>
            </div>
            <div class="widget-controls">                                
                <a href="#" class="widget-control-right widget-remove" data-toggle="tooltip" data-placement="top" title="Remove Widget"><span class="fa fa-times"></span></a>
            </div>                            
        </div>

    </div>
    
    
</div>
<!-- END WIDGETS -->                    
<div class="row">
    <div class="col-md-6">
        <h2>Common actions</h2>
        <a href="{{URL::to('admin/pages/create')}}" class="btn btn-primary">Add Page</a>
        <a href="{{URL::to('admin/articles/create')}}" class="btn btn-primary">Add Article</a>
        <a href="{{URL::to('admin/products/create')}}" class="btn btn-primary">Add Product</a>
        <a href="{{URL::to('admin/works/create')}}" class="btn btn-primary">Add Work</a>
        <a href="{{URL::to('admin/prices/create')}}" class="btn btn-primary">Add Price</a>
        <a href="{{URL::to('admin/settings/create')}}" class="btn btn-primary">Add Setting</a>
    </div>
    <div class="col-md-6">
        <h2>Menus</h2>
        @foreach($menus as $menu)
            <a href="{{URL::to('admin/menus/view/' . $menu->id)}}" class="btn btn-primary">{{{ $menu->name }}}</a>
        @endforeach
    </div>
</div>

<div class="row">

</div>

@stop

@section('js')
<!--
{{HTML::script('js/plugins/icheck/icheck.min.js')}}
{{HTML::script('js/plugins/mcustomscrollbar/jquery.mCustomScrollbar.min.js')}}
{{HTML::script('js/plugins/scrolltotop/scrolltopcontrol.js')}}

{{HTML::script('js/plugins/morris/raphael-min.js')}}
{{HTML::script('js/plugins/morris/morris.min.js')}}
{{HTML::script('js/plugins/rickshaw/d3.v3.js')}}
{{HTML::script('js/plugins/rickshaw/rickshaw.min.js')}}
{{HTML::script('js/plugins/jvectormap/jquery-jvectormap-1.2.2.min.js')}}
{{HTML::script('js/plugins/jvectormap/jquery-jvectormap-world-mill-en.js')}}
{{HTML::script('js/plugins/bootstrap/bootstrap-datepicker.js')}}
{{HTML::script('js/plugins/owl/owl.carousel.min.js')}}       

{{HTML::script('js/plugins/moment.min.js')}}
{{HTML::script('js/plugins/daterangepicker/daterangepicker.js')}}

{{HTML::script('js/demo_dashboard.js')}}
-->
@stop