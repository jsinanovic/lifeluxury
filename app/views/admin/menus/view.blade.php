@extends('admin.layouts.main')

@section('breadcrumb')
<li><a href="{{URL::to($moduleUri)}}">{{ ucfirst($moduleName) }}</a></li>
<li><a href="{{URL::to($moduleUri) . 'view/' . $entry->id}}">{{ $title }}</a></li>
@stop

@section('content-title')
<div class="page-title">                    
    <h2>
        <a href="{{URL::to($moduleUri)}}"><span class="fa fa-arrow-circle-o-left"></span></a> 
        {{{$entry->name}}}  {{$modelName}}   
    </h2>
    <div class="form-group pull-right">
        <a href="{{URL::to($moduleUri)}}/edit/{{$entry->id}}" class="btn btn-primary"><i class="fa fa-pencil"></i> Edit Menu</a>
        <button class="btn btn-danger" onclick="removeMenu({{$entry->id}})"><i class="fa fa-trash-o"></i> Remove Menu</button>
    </div>
</div>
@if(Session::get('success'))
<div class="form-group">
    <div class="col-md-12 col-xs-12"></div>
    <div class="col-md-12 col-xs-12">  
        <div class="alert alert-success" role="alert">
            <button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">×</span><span class="sr-only">Close</span></button>
            <strong>Well done!</strong> You successfully saved this {{$modelName}}.
        </div>
    </div>
</div>
@endif
@stop

@section('content')

<div class="row">
    
    <div class="col-md-12">
        <div class="panel panel-info">
            <div class="panel-heading">
                <h3 class="panel-title">Categories List</h3>
                <ul class="panel-controls">
                    <li><a href="#" class="panel-collapse"><span class="fa fa-angle-down"></span></a></li>
                    <li><a href="#" class="refresh-categories"><span class="fa fa-refresh"></span></a></li>
                    <li><a href="#" class="panel-fullscreen"><span class="fa fa-expand"></span></a></li>
                </ul>                                
            </div>
            <div class="panel-body">
                <div class="row">
                    <div class="col-md-6">
                        <div class="dd nestable">
                            <ol class="dd-list categories-list">
                                @include('admin.menus.item', array('categories' => $categories))
                            </ol>
                        </div>                                            
                    </div>
                </div>
            </div>                      
        </div>
    </div>
    
    <div class="col-md-6">
        @include('admin.menus.form', array('formType' => 'create'))
    </div>
    
    <div class="col-md-6">
        @include('admin.menus.form', array('formType' => 'edit'))
    </div>
</div>
@stop

@section('js')
{{HTML::script('js/plugins/nestable/jquery.nestable.js');}}
{{HTML::script('js/plugins/icheck/icheck.min.js')}}
{{HTML::script('js/plugins/mcustomscrollbar/jquery.mCustomScrollbar.min.js')}}
{{HTML::script('js/plugins/summernote/summernote.js')}}
{{HTML::script('js/plugins/bootstrap/bootstrap-datepicker.js')}}
{{HTML::script('js/plugins/bootstrap/bootstrap-file-input.js')}}
{{HTML::script('js/plugins/bootstrap/bootstrap-select.js')}}
{{HTML::script('js/plugins/tagsinput/jquery.tagsinput.min.js')}}
{{HTML::script('js/plugins/jquery-validation/jquery.validate.js')}}

<script>
    var moduleUri = '{{ URL::to($moduleUri) }}';
    var baseUri = '{{ URL::to($baseName) }}';
    var menu_id = {{ $entry->id }};
    var depth = {{ $entry->depth }};
    var rules = {{ $formRules }};
    var jvalidate = $("#jvalidate").validate({
        rules : rules,
    });
    var jvalidate2 = $("#jvalidate2").validate({
        rules : rules,
    });
    
    var operations_buffer = {};
    var prev_sort = 0;
    var next_sort = 0;
    var parent_id = 0;
    var menu = $('.nestable').nestable({
        maxDepth : depth,
        onStart  : function(item) {
            operations_buffer = {};
            prev_sort = item.prev().data('sort') || 0;
            next_sort = item.next().data('sort') || 0;
            parent_id = item.parents('.dd-item').data('id') || 0;
        },
        onDrop   : function(item) {
            var new_prev_sort = item.prev().data('sort') || 0;
            var new_next_sort = item.next().data('sort') || 0;
            var new_parent_id = item.parents('.dd-item').data('id') || 0;
            var item_sort = item.data('sort');
            var item_id = item.data('id');
            
            if(prev_sort != new_prev_sort || next_sort != new_next_sort) {
                if(new_next_sort == 0 || new_prev_sort - new_next_sort > 1) {
                    operations_buffer[item_id] = {
                        sort : new_prev_sort + 1
                    };
                    item.data('sort', new_prev_sort + 1);
                } else if(new_prev_sort == 0 && new_next_sort > 1) {
                    operations_buffer[item_id] = {
                        sort : new_next_sort - 1
                    };
                    item.data('sort', new_next_sort - 1);
                } else {
                    item.data('sort', new_next_sort);
                    item.nextAll().each(function(){
                        $(this).data('sort', $(this).data('sort')+1 );
                    });
                    operations_buffer[item_id] = {
                        sort : new_next_sort,
                        move : true
                    };
                }
            }
            
            if(parent_id != new_parent_id && !!operations_buffer[item_id]) {
                operations_buffer[item_id].parent_id = new_parent_id;
            }
            
            if(!$.isEmptyObject(operations_buffer))
                $.post(baseUri + '/categories/sort', {
                    operations  : operations_buffer,
                    menu_id     : menu_id,
                }, function(res){
                    if(res.status == 1) {
                        //alert('Done');
                    } else {
                        msg("Fail. Data is not updated!!!");
                    }
                }, 'JSON').fail(function() {
                    msg("Server error. Route contains errors.");
                });
        }
    });
    
    $('[name="page_id"]').change(function(){
        if($(this).val() != 0) {
            $(this).closest('form').find('.page-hide').hide(500);
        } else {
            $(this).closest('form').find('.page-hide').show(500);
        }
    });
    
    var $success = $('.add-category-success');
    var timer = null;
    $('.add-category-form').submit(function(){
        var $form = $(this);
        if(!$form.valid())
            return false;
        
        var $panel = $(this).find('.panel');
        panel_refresh($panel);
        var data = $form.serializeObject();
        data['menu_id'] = menu_id;
        data['parent_id'] = 0;
        data['content'] = [];
        data['content']['en'] = $form.find('[name="content[en]"]').code();
        data['content']['ru'] = $form.find('[name="content[ru]"]').code();
        $.post(baseUri + '/categories/create', data, function(res){
            if(res.status == 1) {
                $form.find('input[type="text"]').val('').removeClass('valid');
                $form.find('textarea').val('').removeClass('valid');
                $form.find('.page-select').val(0).selectpicker('refresh').trigger('change');
                jvalidate.resetForm();
                $form.find('.summernote').code('');
                $('.categories-list').append(res.item);
                $success.show(400);
                if(timer)
                    clearTimeout(timer);
                timer = setTimeout(function(){$success.hide(600)}, 5000);
            } else {
                msg("Server error.");
            }
            panel_refresh($panel);
        }).fail(function(){
            panel_refresh($panel);
            msg("Server error.");
        });
        return false;
    });
    
    
    var $success2 = $('.edit-category-success');
    var timer2 = null;
    $('.edit-category-form').submit(function(){
        var $form = $(this);
        if(!$form.valid())
            return false;
        
        var id = $form.data('id');
        var $panel = $(this).find('.panel');
        panel_refresh($panel);
        var data = $form.serializeObject();
        //data['content'] = $form.find('.summernote').code();
        data['content[en]'] = $form.find('[name="content[en]"]').code();
        data['content[ru]'] = $form.find('[name="content[ru]"]').code();
        $.post(baseUri + '/categories/edit/' + id, data, function(res){
            if(res.status == 1) {
                $form.find('input[type="text"]').val('').removeClass('valid');
                $form.find('textarea').val('').removeClass('valid');
                $form.find('.page-select').val(0).selectpicker('refresh').trigger('change');
                jvalidate2.resetForm();
                $form.find('.summernote').code('');
                $('.categories-list li[data-id="'+id+'"]').fadeOut(500, function(){
                    $(this).replaceWith(res.item).fadeIn(500);
                });
            } else {
                msg("Server error.");
            }
            panel_refresh($panel);
            closeEditForm();
            
        }).fail(function(){
            panel_refresh($panel);
            msg("Server error.");
        });
        return false;
    });
    
    
    function removeMenu(id) {
        if(confirm("Are you sure?")) {
            $.post(moduleUri + '/remove', {id:id}, function(res){
                if(res.status) {
                    window.location.href = moduleUri;
                }
            }).fail(function(){
                msg('Server error');
            });
        }
    }
    
    function closeEditForm()
    {
        var $editForm = $('.edit-category-form');
        $editForm.hide(500);
        if($('.add-category-form .panel').hasClass('panel-toggled'))
                panel_collapse($('.add-category-form .panel'));
        return false;
    }
    
    $('.categories-list').on('click', '.item-edit', function(res){
        if(!$('.add-category-form .panel').hasClass('panel-toggled'))
            panel_collapse($('.add-category-form .panel'));
        
        var id = $(this).closest('li').data('id');
        var $editForm = $('.edit-category-form');
        $editForm.data('id', id);
        $editForm.show(500, function() {
            panel_refresh($editForm.find('.panel'));
            $.get(baseUri + '/categories/entry/' + id, function(res) {
                if(res.status) {
                    $fields = $editForm.find('[name]');
                    $fields.each(function(){
                        var name = $(this).prop('name');
                        $(this).val(res.entry[name]);
                        if($(this).hasClass('page-select')) {
                            if(!res.entry[name])
                                $(this).val(0);
                            $(this).change();
                        } else if($(this).hasClass('icheckbox')) {
                            $(this).val(1);
                            if(res.entry[name] == 1) {
                                $(this).prop('checked', 'checked');
                                $(this).parent().addClass('checked');
                            } else {
                                $(this).removeProp('checked');
                                $(this).parent().removeClass('checked');
                            }
                        } else if($(this).hasClass('summernote')) {
                            $(this).code(res.entry[name]);
                        }
                    });
                    
                } else {
                    msg('Category not found. Refresh page ant try again.');
                }
                panel_refresh($editForm.find('.panel'));
            }, 'JSON').fail(function(){
                msg('Server error');
                panel_refresh($editForm.find('.panel'));
            });
        });
        
    });
    
    $('.categories-list').on('click', '.item-remove', function(){
        $row = $(this).closest('li');
        removeEntry($row, $row.data('id'), function(){}, baseUri + '/categories');
    });
    
    $('.categories-list').on('click', '.item-status', function(){
        $switch = $(this);
        $row = $switch.closest('li');
        changeStatus($switch, $row.data('id'), function(){}, baseUri + '/categories');
    });
    
    $('.refresh-categories').click(function(){
        var $panel = $(this).closest('.panel');
        panel_refresh($panel);
        $.get(moduleUri + '/menu-view/' + menu_id, function(res){
            if(res.status != 1) {msg('Server error!')}
            else {
                $('.categories-list').html(res.menu);
            }
            panel_refresh($panel);
        }).fail(function(){
            msg('Server error!');
            panel_refresh($panel);});
        return false;
    });
    initSummernote($('.summernote'), 'minimal');
</script>
@stop