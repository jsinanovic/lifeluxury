@extends('admin.layouts.main')

@section('breadcrumb')
<li><a href="{{URL::to($moduleUri)}}">{{ ucfirst($moduleName) }}</a></li>
@stop

@section('content')
<div class="row">
    <div class="col-md-12">
        <!-- START DEFAULT DATATABLE -->
        <div class="panel panel-success">
            <div class="panel-heading">                                
                <h3 class="panel-title">{{$pageName}}</h3>
                <ul class="panel-controls">
                    <li><a href="#" class="panel-refresh"><span class="fa fa-refresh"></span></a></li>
                </ul>                                
            </div>
            <div class="panel-body">
                <div class="table-responsive">
                    <table class="table admin-datatable table-hover">
                        <thead>
                            <tr>
                                <th>ID</th>
                                <th>Name</th>
                                <th>Key</th>
                                <th class="text-center">Depth</th>
                                <th class="text-center">Last Update</th>
                                <th class="text-center">Categories Count</th>
                                <th>Actions</th>
                            </tr>
                        </thead>
                        <tbody></tbody>
                    </table>
                </div>
            </div>
        </div>
        <!-- END DEFAULT DATATABLE -->
    </div>
</div>
@stop

@section('js')
{{HTML::script('js/plugins/icheck/icheck.min.js');}}
{{HTML::script('js/plugins/mcustomscrollbar/jquery.mCustomScrollbar.min.js');}}
{{HTML::script('js/plugins/datatables/jquery.dataTables.min.js');}}
<script>
    var moduleUri = '{{ URL::to($moduleUri) }}';
    var panel = $('.panel');
    var table = $(".admin-datatable").on( 'processing.dt', function ( e, settings, processing ) {
        panel_refresh(panel);
    }).dataTable({
        processing : true,
        ajax : moduleUri + '/all-json',
        stateSave : true,
        "columnDefs": [ {
            "targets": 1,
            "data": null,
            "render": function ( data, type, row ) {
                return '<a href="' + moduleUri + '/view/' + data[0] + '">' + data[1] + '</a>';
            },
        }, {
            "targets": 3,
            sClass : 'text-center'
        }, {
            "targets": 4,
            sClass : 'text-center'
        }, {
            "targets": 5,
            sClass : 'text-center',
            data : null,
            "render": function ( data, type, row ) {
                var sClass = data[5] > 0? 'badge-success' : 'badge-warning';
                return '<span class="badge '+sClass+'">'+data[5]+'</span>';
            },
        },  {
            "orderable": false,
            "targets": -1,
            sClass : 'text-center',
            "data": null,
            "render": function ( data, type, row ) {
                return '<div class="btn-group">'
                + '<a href="' + moduleUri + '/view/' + data[0] + '" '  
                + 'class="btn btn-primary btn-condensed"><i class="fa fa-eye"></i></a> '
                + '<a href="' + moduleUri + '/edit/' + data[0] + '" '  
                + 'class="btn btn-primary btn-condensed"><i class="fa fa-pencil"></i></a> '
                + '<button class="btn btn-danger btn-condensed" onclick="removeEntry($(this).closest(\'tr\'), '+data[0]+')"><i class="fa fa-trash-o"></i></button></div>';
            },
        } ],
        
    });
    
    
    function removeEntry($row, id) {
        if(confirm("Are you sure?")) {
            $.post(moduleUri + '/remove', {id:id}, function(res){
                if(res.status) {
                    $row.fadeOut(300, function(){
                        table.api().row($row).remove().draw();
                    });
                }
            });
        }
    }
    
    $(".panel-refresh").on("click",function(){
        table.api().ajax.reload();
    });
    $(".admin-datatable").on('page.dt',function () {
        onresize(100);
    });
</script>
@stop