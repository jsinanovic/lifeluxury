@foreach($categories as $category)
<li class="dd-item dd3-item" data-id="{{$category->id}}" data-sort="{{$category->sort}}">
    @if(count($category->categories) > 0 && isset($isAddButtons))
    <button data-action="collapse" type="button">Collapse</button>
    <button data-action="expand" type="button" style="display: none;">Expand</button>
    @endif
    <div class="dd-handle dd3-handle">Drag</div>
    <div class="dd3-content">
        <div class="item-name pull-left">{{{$category->name}}}</div>
        <div class="item-control pull-right">
            <span class="badge badge-primary item-edit"><i class="fa fa-pencil"></i></span>
            <span class="badge badge-danger item-remove"><i class="fa fa-trash-o"></i></span>
            <label class="switch switch-small item-status">
                <input type="checkbox" {{$category->status? 'checked' : ''}} value="1">
                <span></span>
            </label>
        </div>
    </div>
    @if(count($category->categories) > 0)
    <ol class="dd-list">
        @include('admin.menus.item', array('categories' => $category->categories))
    </ol>
    @endif
</li>
@endforeach