<form action="" method="POST" id="{{$formType == 'create'? 'jvalidate' : 'jvalidate2'}}" class="form-horizontal {{$formType == 'create'? 'add-category-form' : 'edit-category-form'}}" role="form">
    <div class="panel {{$formType == 'create'? 'panel-success' : 'panel-warning'}}">
        <div class="panel-heading">
            <h3 class="panel-title">{{$formType == 'create'? 'Add new' : 'Edit'}} Category</h3>
            <ul class="panel-controls">
                <li><a href="#" class="panel-collapse"><span class="fa fa-angle-down"></span></a></li>
                <li><a href="#" class="panel-fullscreen"><span class="fa fa-expand"></span></a></li>
                @if($formType == 'edit')
                <li><a href="#" onclick="return closeEditForm();"><span class="fa fa-times"></span></a></li>
                @endif
            </ul>                                
        </div>
        <div class="panel-body form-group-separated" style="border-top: 0px;">
            <div class="form-group add-category-success" style="display: none;">
                <div class="col-md-12 col-xs-12">  
                    <div class="alert alert-success" role="alert">
                        <button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">×</span><span class="sr-only">Close</span></button>
                        <strong>Wow!</strong> Your Category has been saved successfully.
                    </div>
                </div>
            </div>
            
            @foreach($categoryFields as $key => $field)
                <?php $isTranslated = $translatedAttributes && in_array($key, $translatedAttributes); ?>
        
                @if($isTranslated)
                    @foreach($locales as $locale)
                        <div class="form-group translate locale-{{$locale}} {{isset($field['pagehide'])? 'page-hide' : ''}}">
                            <label class="col-md-3 col-xs-12 control-label">{{{ $field['label'] }}} ({{$locale}})</label>
                            <div class="col-md-8 col-xs-11">
                                @include('admin.layouts.smartfield_translated', array('entry' => false))
                            </div>

                            @if($isTranslated)
                            <div class="col-md-1 col-xs-1">
                                <span class="flag flag-{{$locale}}"></span>
                            </div>
                            @endif
                        </div>
                    @endforeach
                @else
                    <div class="form-group {{isset($field['pagehide'])? 'page-hide' : ''}}">
                        <label class="col-md-3 col-xs-12 control-label">{{{ $field['label'] }}}</label>
                        <div class="col-md-8 col-xs-11">
                            @include('admin.layouts.smartfield', array('entry' => false))
                        </div>
                    </div>
                @endif


            @endforeach
            
            
<!--
            @foreach($locales as $locale)
            <div class="form-group  translate locale-{{$locale}}">
                <label class="col-md-3 col-xs-12 control-label">Name <span class="flag flag-{{$locale}}"></span></label>
                <div class="col-md-8 col-xs-12">                                          
                    {{ Form::text('name' , null, array('class'=>'form-control')) }}
                </div>
            </div>
            @endforeach
            
            <div class="form-group">
                <label class="col-md-3 col-xs-12 control-label">Slug</label>
                <div class="col-md-8 col-xs-12">                                  
                    {{ Form::text('slug', null, array('class'=>'form-control')) }}
                    <span class="help-block">Leave a blank for autogenerating</span>
                </div>
            </div>
            <div class="form-group">
                <label class="col-md-3 col-xs-12 control-label">URL</label>
                <div class="col-md-8 col-xs-12">                                  
                    {{ Form::text('url', null, array('class'=>'form-control')) }}
                    <span class="help-block">URL to another page (it will override default link)</span>
                </div>
            </div>
            <div class="form-group">
                <label class="col-md-3 col-xs-12 control-label">Page</label>
                <div class="col-md-8 col-xs-12">
                    {{ Form::select('page_id', $pages, 0, array('class' => 'form-control select page-select')) }} 
                    <span class="help-block">You can connect static page to menu element.</span>
                </div>
            </div>
            <div class="form-group page-hide">
                <label class="col-md-3 col-xs-12 control-label">Annotation</label>
                <div class="col-md-8 col-xs-12">                                            
                    {{ Form::textarea('annotation', null, array('class'=>'form-control', 'rows' => 2, 'cols' => '200')) }}
                    <span class="help-block">Short category description (shows on special pages)</span>
                </div>
            </div>
            <div class="form-group page-hide">
                <label class="col-md-3 col-xs-12 control-label">Content</label>
                <div class="col-md-8 col-xs-12">                    
                    {{ Form::textarea('content', null, array('class'=>'form-control summernote', 'rows' => 2, 'cols' => '200')) }}
                    <span class="help-block">Category content (shows on special pages)</span>
                </div>
            </div>
            <div class="form-group page-hide">
                <label class="col-md-3 col-xs-12 control-label">META Title</label>
                <div class="col-md-8 col-xs-12">                                            
                    {{ Form::text('title', null, array('class'=>'form-control')) }}                                        
                </div>
            </div>
            <div class="form-group page-hide">
                <label class="col-md-3 col-xs-12 control-label">META Keywords</label>
                <div class="col-md-8 col-xs-12">                                            
                    {{ Form::textarea('keywords', null, array('class'=>'form-control', 'rows' => 2, 'cols' => '200')) }}                        
                </div>
            </div>
            <div class="form-group page-hide">
                <label class="col-md-3 col-xs-12 control-label">META Description</label>
                <div class="col-md-8 col-xs-12">                                            
                    {{ Form::textarea('description', null, array('class'=>'form-control', 'rows' => 2, 'cols' => '200')) }}                        
                </div>
            </div>

            <div class="form-group">
                <label class="col-md-3 col-xs-12 control-label" for="active">Active</label>
                <div class="col-md-8 col-xs-12">                                                                                                                                        
                    <label class="check">{{Form::checkbox('status', 1, true, array('class' => 'icheckbox'));}} Do you want to make this category active?</label>
                </div>
            </div>
-->
        </div>
        <div class="panel-footer">
            @if($formType == 'edit')
                <button class="btn btn-default pull-left" onclick="return closeEditForm();">Cancel</button>
            @endif
            <input type="submit" class="btn btn-primary pull-right add-category" value="Save">
        </div>
    </div>
</form>