@extends('admin.layouts.main')

@section('breadcrumb')
<li><a href="{{URL::to($moduleUri)}}">{{ ucfirst($moduleName) }}</a></li>
@stop

@section('content')
<div class="row">
    <div class="col-md-12">
        <!-- START DEFAULT DATATABLE -->
        <div class="panel panel-success">
            <div class="panel-heading">                                
                <h3 class="panel-title">{{$pageName}}</h3>
                <ul class="panel-controls">
                    <li><a href="#" class="panel-refresh"><span class="fa fa-refresh"></span></a></li>
                </ul>                                
            </div>
            <div class="panel-body">
                <div class="table-responsive">
                    <table class="table admin-datatable table-hover">
                        <thead>
                            <tr>
                                <th>ID</th>
                                <th>Name</th>
                                <th>Key</th>
                                <th>Description</th>
                                <th>Module</th>
                                <th>Type</th>
                                <th>Value</th>
                                <th>Actions</th>
                            </tr>
                        </thead>
                        <tbody></tbody>
                    </table>
                </div>
            </div>
        </div>
        <!-- END DEFAULT DATATABLE -->
    </div>
</div>
@stop

@section('js')
{{HTML::script('js/plugins/icheck/icheck.min.js');}}
{{HTML::script('js/plugins/mcustomscrollbar/jquery.mCustomScrollbar.min.js');}}
{{HTML::script('js/plugins/datatables/jquery.dataTables.min.js');}}
<script>
    var moduleUri = '{{ URL::to($moduleUri) }}';
    var panel = $('.panel');
    var table = $(".admin-datatable").on( 'processing.dt', function ( e, settings, processing ) {
        panel_refresh(panel);
    }).dataTable({
        processing : true,
        ajax : moduleUri + '/all-json',
        stateSave : true,
        "columnDefs": [ {
            "targets": 3,
            sClass : '',
        }, {
            "targets": 4,
            sClass : 'text-center',
        }, {
            "targets": 5,
            sClass : 'text-center',
            "visible": false,
        },  {
            sClass : 'text-center settings-value',
            "orderable": true,
            "targets": -2,
        }, {
            sClass : 'text-center',
            "orderable": false,
            "targets": -1,
            "data": null,
            "render": function ( data, type, row ) {
                return '<div class="btn-group"><a href="' + moduleUri + '/edit/' + data[0] + '" '  
                        + 'class="btn btn-primary btn-condensed"><i class="fa fa-pencil"></i></a> '
                        + '<button class="btn btn-danger btn-condensed" onclick="removeEntry($(this).closest(\'tr\'), '+data[0]+', onRowDeleted)"><i class="fa fa-trash-o"></i></button></div>';
            },
        } ],
        
    });
    
    function onRowDeleted($row) {
        table.api().row($row).remove().draw();
    }
    
    $(".panel-refresh").on("click",function(){
        table.api().ajax.reload();
    });

    $(".panel-commingsoon").on("click",function(){

    });

    $(".admin-datatable").on('page.dt',function () {
        onresize(100);
    });
</script>

<style>
    .dataTable.table > tbody > tr > td:nth-child(4){
        width: 200px;
    }
    .dataTable.table{
        font-size: 13px;
    }
    .dataTables_wrapper{
        overflow: hidden;
    }
</style>

@stop