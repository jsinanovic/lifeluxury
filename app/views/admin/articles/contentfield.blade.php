@if($entry)
<a href="{{URL::to('admin/articles/edit-content/' . $entry->slug)}}" class="btn btn-primary" target="_blank">Edit Content</a>
@else
<strong>You will be able to edit the content after article saving</strong>
@endif