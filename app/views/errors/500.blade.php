@extends('errors.layout')

@section('title')
    Server error 500
@stop

@section('code')
    500
@stop

@section('message')
    Seems like server doesn't know what to answer you...
@stop