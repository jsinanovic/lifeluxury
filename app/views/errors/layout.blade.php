<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <title>@yield('title')</title>
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        {{HTML::style('front/plugins/bootstrap-3.3.2-dist/css/bootstrap.min.css')}}
        {{HTML::style('front/css/styles.css')}}

    </head>
    <body>
        <section class="error">
            <header>
                <a href="/" class="logo"></a>
            </header>
            <div class="error-description">
                <table>
                    <tr>
                        <td>
                            <div class="error-description-content">
                                <div class='error-number'>@yield('code')</div>
                                <p>@yield('message')</p>
                                <a href="javascript:void(0);" onclick="window.history.back()" class="btn btn-back">GO BACK</a>
                                <a href="{{URL::to('/')}}" class="btn btn-home">GO HOME</a>
                            </div>
                        </td>
                    </tr>
                </table>

            </div>
            <div class="copyright">© Pandora, 2013. All rights reserved. Done by Rondesign</div>
        </section>
    </body>
</html>