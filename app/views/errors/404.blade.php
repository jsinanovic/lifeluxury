@extends('front.layouts.main')

@section('header')
    @include('front.partials.header')
@stop

@section('content')


        <!-- END SITE NAVIGATION -->
@include('front.partials.sidebar-left')
        <!-- START POPUP -->
@include('front.partials.sidebar-right')
        <!-- END POPUP -->

    <section class="section-full section-top" id="error404">
        <div class="container error404-content">
            <div class="row">
                <div class="col-md-12">
                    <div class="error404-title">
                        404
                    </div>
                    <div class="error404-desc">
                        Oops, the page your are looking for can’t be found
                    </div>

                    <div class="go-back-content" onclick="history.back()">
                        <div class="go-back-btn">
                            <a href="#" class="section-btn-next-back section-btn-next-down-back">
                                <span class="section-next-icon-back fa fa-chevron-left" aria-hidden="true"></span>
                            </a>
                            {{--<span class="section-next-icon fa fa-chevron-left" aria-hidden="true"></span>--}}
                            {{--<span class="round-button-rotated"></span>--}}
                        </div>
                        <div class="go-back-btn-text">
                        <span>
                        <a href="/" class="" style="margin-left: -35px;">
                            Go Back
                        </a>
                        </span>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div style="clear: both;"></div>
        <div class="footer-transparent">
            @include('front.partials.footer-home')
        </div>
    </section>

@stop