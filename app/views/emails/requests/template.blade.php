<!DOCTYPE html>
<html lang="en-US">
    <head>
        <meta charset="utf-8">
    </head>
    <body>
        @yield('content')
        
        @if(isset($recipient->token))
        <br><br>
        <a href="http://lifeandluxury.com/unsubscribe/{{urlencode($recipient->token)}}">Unsubscribe</a>
        @endif
    </body>
</html>
