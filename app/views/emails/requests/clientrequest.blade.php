<!DOCTYPE html>
<html lang="en-US">
    <head>
        <meta charset="utf-8">
    </head>
    <body>
        <h2>{{{$request->subject}}}</h2>
        <div>From: <b>{{{$request->user_name}}} ({{{$request->user_email}}})</b></div>
        <br>
        
        @if($request->type == 'requests')
            <h3>Client's comment:</h3>
            <div>{{$request->message['message']}}</div><br>
            
            <h3>Details</h3><br>
            <?php $cellStyle = 'border: 1px solid #e4e4e4; vertical-align: middle; padding: 6px 16px; '; ?>
            <table style="border-collapse: collapse; border-spacing: 0;">
                <thead>
                    <tr>
                        <td style="{{$cellStyle}}">Name</td>
                        <td style="{{$cellStyle}}">Time</td>
                        <td style="{{$cellStyle}}">Price</td>
                        <td style="{{$cellStyle}}">Qty</td>
                    </tr>
                </thead>
                <tbody>
                    @if(isset($request->message['extra']))
                        @foreach($request->message['extra'] as $priceRow)
                            <?php $price = Price::find($priceRow['id']); ?>
                            @if($price)
                                <tr>
                                    <td style="{{$cellStyle}}">{{{$price->name}}}</td>
                                    <td style="{{$cellStyle}}">{{$price->getTime()}} days</td>
                                    <td style="{{$cellStyle}}">
                                        @if($price->price > 0)
                                            {{{$price->price}}}$
                                        @elseif($price->percent > 0)
                                            +{{{$price->percent}}}%
                                        @endif
                                    </td>
                                    <td style="{{$cellStyle}}">
                                        @if($price->price > 0)
                                            {{{$priceRow['qty']}}}
                                        @elseif($price->percent > 0)
                                            Yes
                                        @endif    
                                    </td>
                                </tr>
                            @endif
                        @endforeach
                    @endif
                </tbody>
            </table>
            
        @else
            <h3>Message: </h3>
            <div>{{$request->message}}</div>
        @endif
        <br>
        <div><i>Created at: {{{$request->created_at}}}</i></div>
    </body>
</html>
