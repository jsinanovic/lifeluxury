@extends('front.layouts.main')

@section('meta')
    <meta property="fb:app_id" content="{{Config::get('laravel-social::providers')['facebook']['client_id']}}" />
    <meta property="og:title" content="{{{$service->name}}}" />
    <meta property="og:site_name" content="Life&Luxury"/>
    <meta property="og:url" content="{{URL::to($localeUri . 'creative/' . $service->slug)}}" />
    @if($service->image)
    <meta property="og:image" content="{{$service->getImage()}}" />
    @endif
    <meta property="og:description" content="{{{$service->annotation}}}" />
    <meta property="og:type" content="article" />
    <meta property="og:locale" content="en_US" />

    <meta property="article:author" content="https://www.facebook.com/lifeandluxurycollection" />
    <meta property="article:publisher" content="https://www.facebook.com/lifeandluxurycollection" />
@stop

@section('content')
<section class="intro">
    <div class="banner" style="background-image: url({{$service->getImage()}})"></div>

    <div class="container">
        <div class="col-md-3">
            <div class="quote-block">
                <button class="btn btn-default">Get a free quote today</button>
                <ul class="social-list">
                    @foreach($settings['social-links'] as $key => $link)
                    <li><a href="{{$link}}" target="_blank"><i class="fa fa-{{ isset($socialOrder[$key])? $socialOrder[$key] : 'question'}}"></i></a></li>
                    @endforeach
                </ul>
            </div>

            <div class="instagram-block">
                <div class="title">Instagram</div>
                <a href="{{{$settings['instagram']}}}" class="btn btn-primary" target="_blank">Discover</a>
            </div>
        </div>

        <div class="col-md-9">
            <div class="intro-desc">
                <div class="content">
                    <h1>{{{$service->name}}}</h1>
                    <div class="service-desc">{{$service->content}}</div>
                    <!--
                    <p>One morning, when Gregor Samsa woke from troubled dreams, he found himself transformed in his bed into a horrible vermin. He lay on his armour-like back, and if he lifted his head a little he could see his brown belly, slightly domed and divided by arches into stiff sections. The bedding was hardly able to cover it and seemed ready to slide off any moment.</p>
                    <p>His many legs, pitifully thin compared with the size of the rest of him, waved about helplessly as he looked. "What's happened to me? " he thought. It wasn't a dream. His room, a proper human room although a little too small, lay peacefully between its four familiar walls.</p>-->
                </div>
                <!--<img src="/front/img/creative2.jpg" alt="" />-->
            </div>
        </div>
    </div>

    <a href="javascript:void(0);" class="discover">
        Discover
        <div class="fa fa-angle-double-down"></div>
    </a>
</section>

<section class="articles">
    <div class="container">
        <div class="col-md-12">
            <nav class="categories">
                <ul class="categories-list">
                    @foreach($services as $item)
                    <li><a href="{{URL::to('creative/' . $item->slug)}}">{{$item->name}}</a></li>
                    @endforeach
                    <!-- <li><a href="">advertising</a></li>
                     <li><a href="">brand identity</a></li>
                     <li><a href="">graphic design</a></li>
                     <li><a href="">website design & development</a></li>
                     <li><a href="">motion graphics</a></li>
                     <li class="last"><a href="">music production</a></li>
                     <li class="search"><i class="fa fa-search"></i></li>-->
                </ul>
            </nav>


            <?php $slides = $service->getSlides(); ?>
            @if($slides && !empty($slides))
            <div class="slider creative-slider">

                <div id="myCarousel" class="carousel slide">
                    <!-- Carousel items -->
                    <div class="carousel-inner">
                        @foreach($slides as $key => $item)
                        <div class="item {{$key == 0? 'active' : ''}}">
                            <div class="row">
                                @if(isset($item[0]))
                                <div class="col-md-3 col-first">
                                    <div class="slider-block loading text-block" data-content="{{{$item[0]['content']}}}" data-name="{{{$item[0]['name']}}}"></div>
                                </div>
                                @endif

                                @if(isset($item[1]))
                                <div class="col-md-9 col-last">
                                    <div class="col-md-6 large-one">
                                        <div class="slider-block loading image-block" data-content="{{{$item[1]['content']}}}" data-name="{{{$item[1]['name']}}}"></div>
                                    </div>
                                    @if(isset($item[2]))
                                    <div class="col-md-6 large-two">
                                        <div class="slider-block loading image-block" data-content="{{{$item[2]['content']}}}" data-name="{{{$item[2]['name']}}}"></div>
                                    </div>
                                    @endif
                                </div>
                                @endif
                            </div>
                            @if(isset($item[3]))
                            <div class="row">
                                <div class="col-md-9 col-first">
                                    <div class="col-md-6 large-one">
                                        <div class="slider-block loading image-block"  data-content="{{{$item[3]['content']}}}" data-name="{{{$item[3]['name']}}}"></div>
                                    </div>
                                    @if(isset($item[4]))
                                    <div class="col-md-6 large-two">
                                        <div class="slider-block loading image-block" data-content="{{{$item[4]['content']}}}" data-name="{{{$item[4]['name']}}}"></div>
                                    </div>
                                    @endif
                                </div>
                                @if(isset($item[5]))
                                <div class="col-md-3 col-first">
                                    <div class="slider-block loading text-block" data-content="{{{$item[5]['content']}}}" data-name="{{{$item[5]['name']}}}"></div>
                                </div>
                                @endif
                            </div>
                            @endif
                        </div>
                        @endforeach
                    </div>
                    <!-- Carousel nav -->
                    <a class="carousel-control left" href="#myCarousel" data-slide="prev"><span>&lsaquo;</span></a>
                    <a class="carousel-control right" href="#myCarousel" data-slide="next"><span>&rsaquo;</span></a>
                </div>


                <div class="modal fade" id="slider-modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                    <div class="modal-dialog" role="document">
                        <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                <h4 class="modal-title" id="myModalLabel"></h4>
                            </div>
                            <div class="modal-body">

                            </div>
                        </div>
                    </div>
                </div>

                @section('js')
                @parent
                <script>
                    $(document).ready(function () {
                        var $blocks = $('.slider-block');
                        var $modal = $('#slider-modal');
                        $(window).load(function(){
                            setTimeout(function () {
                                $blocks.each(function () {
                                    var $block = $(this);
                                    var $layer = $('<div class="slider-block-layer">');
                                    var $temp = $('<div>').css('display', 'none').html($block.data('content'));
                                    var $media = $temp.find('img, iframe');
                                    if($media.length > 0) {
                                        $media.load(function(){
                                            $block.removeClass('loading').html($temp.html()).prepend($layer)
                                                .data('content', '').removeAttr('data-content');
                                            $temp.remove();
                                        });
                                        $temp.appendTo('body');
                                    } else {
                                        $block.removeClass('loading').html($block.data('content')).prepend($layer)
                                    }

                                });
                            }, 100);
                        });

                        $blocks.on('click', function (e) {
                            var $block = $(this);
                            if ($block.hasClass('loading'))
                                return false;

                            $modal.find('.modal-title').text($block.data('name'));
                            var $newBlock = $block.clone();
                            $newBlock.find('.slider-block-layer').remove();
                            $modal.find('.modal-body').html($newBlock.html());
                            $modal.modal('show');
                        });

                        $modal.on('hidden.bs.modal', function(){
                            $modal.find('.modal-body').html('');
                        });
                    });
                </script>
                @stop
            </div>
            @endif

        </div>
    </div>





</section>

@if(!empty($randomArticles))
<section class="random-articles section-magazine">
    <div class="top-title">
        Creative Articles
        <div>By Life&Luxury Magazine</div>
    </div>

    <div class="container">
        @if(isset($randomArticles[0]))
        <a href="{{URL::to('magazine/article/' . $randomArticles[0]->slug)}}" class="col-md-6 col-first">
            <div class="article-big" style="background-image: url({{$randomArticles[0]->getImage('thumb')}})">
                <div class="article-info">
                    <div class="date">{{$randomArticles[0]->created_at}}</div>
                    <div class="title">{{{$randomArticles[0]->name}}}</div>
                    <div class="annotation">{{{$randomArticles[0]->annotation}}}</div>
                </div>
            </div>
        </a>
        @endif

        @if(isset($randomArticles[1]))
        <div class="col-md-6 col-last">
            <a href="{{URL::to('magazine/article/' . $randomArticles[1]->slug)}}" class="article-small">
                <div class="col-sm-6 for-img" style="background-image: url({{$randomArticles[1]->getImage('thumb')}})"></div>
                <div class="col-sm-6  col-xs-12">
                    <div class="article-info arrow-box-left">
                        <div class="title">{{{$randomArticles[1]->name}}}</div>
                        <div class="annotation">{{{$randomArticles[1]->annotation}}}</div>
                    </div>
                </div>
            </a>

            @if(isset($randomArticles[2]))
            <a href="{{URL::to('magazine/article/' . $randomArticles[2]->slug)}}" class="article-small">
                <div class="col-sm-6  col-xs-12" style="z-index: 10;">
                    <div class="article-info arrow-box-right">
                        <div class="title">{{{$randomArticles[2]->name}}}</div>
                        <div class="annotation">{{{$randomArticles[2]->annotation}}}</div>
                    </div>
                </div>
                <div class="col-sm-6 for-img" style="background-image: url({{$randomArticles[2]->getImage('thumb')}})"></div>
            </a>
            @endif
        </div>
        @endif


    </div>


</section>
@endif

<!--
<div class="get-connected">
    <div class="top-title">Get connected</div>

    <ul class="social-list">
        <li><a href=""><i class="fa fa-facebook"></i></a></li>
        <li><a href=""><i class="fa fa-twitter"></i></a></li>
        <li><a href=""><i class="fa fa-vimeo-square"></i></a></li>
        <li><a href=""><i class="fa fa-facebook"></i></a></li>
        <li><a href=""><i class="fa fa-twitter"></i></a></li>
        <li><a href=""><i class="fa fa-vimeo-square"></i></a></li>
    </ul>

</div>
-->

@include('front.layouts.footer')

@stop
