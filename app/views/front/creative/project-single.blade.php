@extends('front.layouts.home')

@section('header')
@include('front.partials.header')
@stop

@section('content')

        <!-- END SITE NAVIGATION -->
@include('front.partials.sidebar-left')
        <!-- START POPUP -->
@include('front.partials.sidebar-right')
        <!-- END POPUP -->>

    <section class="section-top-general" id="home" style="background-image: url('front/img/home/banner_background.jpg');">
        <div class="container vertical-center-general">
            <div class="row" style="width: 100%;">
                <div class="">
                    <div class="col-md-12 welcome-main-title text-center">
                        {{ $services[0]->name  }}

                        <div class="" style=" margin-top:20px;width: 90px;height:76px; float: left;background-image: url('front/img/creative/rectanglea.png'); z-index: 500">
                            <i class="fa fa-angle-left" style="margin-left: 20px; font-size: 50px;" aria-hidden="true"></i>
                        </div>
                        <div class="" style=" margin-top:20px;width: 250px;height:76px; float: right;background-image: url('front/img/creative/rectangle2.png'); z-index: 500;
                          font-size: 24px; color: white;">
                            {{ $services[1]->name  }}  <i class="fa fa-angle-right" style="margin-left: 20px;" aria-hidden="true"></i>
                        </div>
                    </div>



                </div>
            </div>
            <div class="">
                <div>
                    <a href="javascript:void(0);" class="learn-more">
                        DISCOVER MORE
                        <div class="round-button"></div>
                    </a>
                </div>
            </div>
        </div>
    </section>

    <section class="" id="servicepromise-content" style="margin-top: 100px; margin-bottom: 100px; background-image: url('front/img/creative/sarajevo.png');">
        <div class="container vertical-center-general"  style="height: 100%; background-image: url('front/img/creative/sarajevo.png');">

        </div>
    </section>

    @include('front.partials.advertisment-area')
    <section class="" id="newsletter-area" style="position:relative;">
        @include('front.partials.newsletter')
    </section>

    <section id="footer" style="position:relative;">
        @include('front.partials.footer')
    </section>

@stop


