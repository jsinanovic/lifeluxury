@extends('front.layouts.home')

@section('header')
@include('front.partials.header')
@stop

@section('content')

        <!-- END SITE NAVIGATION -->
@include('front.partials.sidebar-left')
        <!-- START POPUP -->
@include('front.partials.sidebar-right')
        <!-- END POPUP -->

<main class="main">

    <!-- START BANNER SECTION -->
    <section class="section" style="background-image: url('front/img/home/banner_background.jpg');">
        <div class="container-wide"  style="width: 100%; height: 100%">
            @include('front.partials.projects-slider')
        </div>
    </section>
    <!-- END BANNER SECTION -->

    <!-- START FOOTER -->
    @include('front.partials.footer-home')
    <!-- END FOOTER -->

</main>
@stop


