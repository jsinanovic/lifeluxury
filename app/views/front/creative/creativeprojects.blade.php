@extends('front.layouts.main')


@section('header')
@include('front.partials.header')
@stop

@section('content')
        <!-- END SITE NAVIGATION -->
@include('front.partials.sidebar-left')
        <!-- START POPUP -->
@include('front.partials.sidebar-right')
        <!-- END POPUP -->

<section class="section-full section-top" id="home">
    <div class="container vertical-center">
        <div class="row">
            <div class="">
                <div class="col-md-12 welcome-main-title text-center">
                    {{ $service->name  }}
                </div>
                <div class="col-md-3 category-articles-back" onclick="history.back()">
                    <i class="fa fa-angle-left" style="margin-left: 5px;" aria-hidden="true"></i>
                </div>
                <div class="col-md-6 welcome-main-desc text-center">
                 Visual presentation is a highly important assets of any project.
                    At Life Luxury Creative we enjoy unique design creations and all our ideas are filled with passion and love.
                </div>
                <div class="col-md-3 category-articles-next">
                        <span>
                            <a href="/creativProjects/{{$nextCategory->slug}} " style="font: inherit;">
                                <span> {{$nextCategory->name}}</span>
                                <span><i class="fa fa-angle-right" style="margin-left: 5px;" aria-hidden="true"></i></span>
                            </a>
                        </span>

                </div>
            </div>
            <a href="javascript:void(0);" class="learn-more">
                DISCOVER MORE
                <div class="round-button"></div>
            </a>
        </div>
    </div>
</section>

<section class="" id="category-articles" style="padding-bottom: 40px; background-color:#eeede8;">
    <div id="exTab1" class="container-fluid" style="padding-left: 0px; padding-right: 0px;">
        <ul  class="nav nav-pills" style="text-align: center; padding-top: 10px; padding-bottom: 10px; background-color: #ffffff;">
            <li class="active">
                <a  href="#1a" data-toggle="tab">FLAYERS & BROCHURES</a>
            </li>
            <li>
                <a class="tab-slash" href="" style="cursor: default;">/</a>
            </li>
            <li><a href="#2a" data-toggle="tab">MEDIA KITS</a>
            </li>
            <li>
                <a class="tab-slash" href="" style="cursor: default;">/</a>
            </li>
            <li><a href="#3a" data-toggle="tab">SOCIAL MEDIA</a>
            </li>
            <li>
                <a class="tab-slash" href="" style="cursor: default;">/</a>
            </li>
            <li><a href="#4a" data-toggle="tab">PROMOTIAL MATERIALS</a>
            </li>
        </ul>

        <div class="container tab-content" style="z-index: 505;padding-top: 60px; padding-bottom: 60px; ">
            <div style="width: 400px; height: 300px; float: left; color: green;">
                 <div class="firstimage" >
                     <img src="/front/img/creative/creative2.png" style="cursor: hand !important;  cursor:hand !important; width: 471px; height: 275px;z-index: 505; float: left;"/>
                     <div class="popup-item-link-creativ1" style="width: 471px; height: 275px;">
                        <a  href="/" class="popup-item-link-creativ"  style="background-image:url(/front/img/article/add.png);">
                            <p href="/" class="popup-item-link-creativ"  style="width: 400px; top:60% !important;left: -10% !important; font: 700 28px Butler Medium !important;">AtoZ Company</p>
                            <p href="/" class="popup-item-link-creativ"  style="width: 400px; top:85% !important;left: -10% !important;font: 700 12px OpenSans !important;font-weight: bold;">SUBHEADING</p>
                        </a>
                     </div>
                 </div>
                 <div class="secoundimage">
                     <img src="/front/img/creative/creative3.png" style="cursor: hand !important;  cursor:hand !important; width: 471px; height: 275px;z-index: 505; float: left;"/>
                     <div class="popup-item-link-creativ1" style="width: 471px; height: 275px;">
                         <a  href="/" class="popup-item-link-creativ"  style="background-image:url(/front/img/article/add.png);">
                             <p href="/" class="popup-item-link-creativ"  style="width: 400px; top:60% !important;left: -10% !important;  font: 700 28px Butler Medium !important;">Main Headline</p>
                             <p href="/" class="popup-item-link-creativ"  style="width: 400px; top:85% !important;left: -10% !important;  font: 700 12px OpenSans !important;font-weight: bold;">Sub Headline</p>
                         </a>
                     </div>
            </div>
            </div>
            <div class="thirdimage1" style="background-image:url(/front/img/creative/creative1.png); width: 670px;height: 550px;z-index: 501; float: right"></div>
            <div class="fourth-image"  style="width: 670px; height: 550px; float: left;">
                <img class="image" src="/front/img/creative/creative4.png" style=" width: 670px;height: 550px;z-index: 501; float: left;"/>
                <a  href="/" class="popup-item-link-creativ"  style="top:30%; left:53% ;background-image:url(/front/img/article/add.png);">
                    <p href="/" class="popup-item-link-creativ"  style="width:400px; top:60% !important;left: -10% !important; text-align: center; font: 700 28px Butler Medium !important;">Main Headline</p>
                    <p href="/" class="popup-item-link-creativ"  style="width:400px;top:85% !important; left: -10% !important; text-align: center; font: 700 12px OpenSans !important;font-weight: bold;">Sub Headline</p>
                </a>
            </div>

            <div  style="width: 400px; height: 300px; float: left; color: green;">
                <div class="fifthimage">
                    <img src="/front/img/creative/creative5.png" style="cursor: hand !important;  cursor:hand !important; width: 471px; height: 275px;z-index: 505; float: left;"/>
                    <div class="popup-item-link-creativ1" style="width: 471px; height: 275px; ">
                        <a  href="/" class="popup-item-link-creativ"  style="background-image:url(/front/img/article/add.png);">
                            <p href="/" class="popup-item-link-creativ"  style="width: 400px; top:60% !important;left: -10% !important;  font: 700 28px Butler Medium !important;">Main Headline</p>
                            <p href="/" class="popup-item-link-creativ"  style="width: 400px; top:85% !important;left: -10% !important;  font: 700 12px OpenSans !important;font-weight: bold;">Sub Headline</p>
                        </a>
                    </div>
                </div>
                <div class="siximage">
                    <img src="/front/img/creative/creative6.png" style="cursor: hand !important;  cursor:hand !important; width: 471px; height: 275px;z-index: 505; float: left;"/>
                    <div class="popup-item-link-creativ1" style="width: 471px; height: 275px; ">
                        <a  href="/" class="popup-item-link-creativ"  style="background-image:url(/front/img/article/add.png);">
                            <p href="/" class="popup-item-link-creativ"  style="width: 400px; top:60% !important;left: -10% !important;  font: 700 28px Butler Medium !important;">Main Headline</p>
                            <p href="/" class="popup-item-link-creativ"  style="width: 400px; top:85% !important;left: -10% !important;  font: 700 12px OpenSans !important;font-weight: bold;">Sub Headline</p>
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>


@include('front.partials.advertisment-area')

<section class="" id="newsletter-area" style="position:relative;">

    @include('front.partials.newsletter')

</section>

<section id="footer" style="position:relative;">

    @include('front.partials.footer')

</section>

@stop


@section('js')

    <script>
        $(document).ready(function (ev) {
            $('#custom_carousel').on('slide.bs.carousel', function (evt) {
                $('#custom_carousel .controls li.active').removeClass('active');
                $('#custom_carousel .controls li:eq(' + $(evt.relatedTarget).index() + ')').addClass('active');
            });

            $('#join-newsletter-btn').click(function() {
                displayNewsletterPopup();
            });

            $('#newsletter-close-btn').click(function() {
                hideNewsletterPopup();
            });

            function hideNewsletterPopup() {
                $('#newsletter-popup-container').hide();
                $('#newsletter-content').show();
            }
            function displayNewsletterPopup() {
                $('#newsletter-content').hide();
                $('#newsletter-popup-container').show();
            }
        });
    </script>

@stop
