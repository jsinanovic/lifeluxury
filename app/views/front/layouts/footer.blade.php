<footer>
    <div class="static-links">
        <div class="container">
            <div class="row">

                <div class="col-sm-5  col-sm-offset-1 col-xs-6">
                    <div class="title">Social connect</div>
                    <div class="social-links">
                        @foreach($settings['social-links'] as $key => $link)
                        <a href="{{$link}}" target="_blank"><i class="fa fa-{{ isset($socialOrder[$key])? $socialOrder[$key] : 'question'}}"></i></a>
                        @endforeach
                    </div>
                    <p>a life&luxury group member</p>
                </div>

                <div class="col-sm-4  col-sm-offset-1  col-xs-5 col-xs-offset-1 static-pages">
                    <div class="title">Client login</div>
                    <div class="row">
                        <div class="col-sm-6">
                            @foreach($footerMenu->categories as $key => $item)
                                <!-- {{{$item->url}}}-->
                                <a href="{{URL::to('page/' . $item->slug)}}" >{{{$item->name}}}</a>
                                @if($key == round(count($footerMenu->categories) / 2) - 1)
                                </div><div class="col-sm-5 col-sm-offset-1">
                                @endif
                            @endforeach
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>

    <div class="line">
        <div class="round-button to-top"></div>
    </div>
</footer>
