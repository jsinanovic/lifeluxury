<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta charset="utf-8">
        <!-- TITLE -->
        <title>{{{$metaTitle}}}</title>
        <meta name="keywords" content="{{{$metaKeywords}}}"/>
        <meta name="description" content="{{{$metaDescription}}}"/>
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        @yield('meta')

        <link rel="shortcut icon" href="{{asset('/favicon.ico')}}" type="image/x-icon">
        <link rel="icon" href="{{asset('/favicon.ico')}}" type="image/x-icon">

        {{HTML::style('front/plugins/bootstrap-3.3.2-dist/css/bootstrap.min.css', array('async'))}}
        {{HTML::style('front/plugins/bootstrap-3.3.2-dist/css/bootstrap-theme.min.css', array('async'))}}
        {{HTML::style('front/plugins/font-awesome/css/font-awesome.min.css', array('async'))}}
        {{HTML::style('front/plugins/jqueryformstyler-master/jquery.formstyler.css', array('async'))}}
        {{HTML::style('front/libs/fullpage.js/dist/jquery.fullpage.min.css', array('async'))}}
        {{HTML::style('front/libs/swiper/dist/css/swiper.min.css', array('async'))}}

        {{HTML::style('front/libs/font-awesome/css/font-awesome.min.css', array('async'))}}

        <!-- FONTS -->
        <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700,800" rel="stylesheet">
        <link href="https://fonts.googleapis.com/css?family=Montserrat:400,700" rel="stylesheet">

        {{--<!-- CSS LIBS -->--}}
        {{--<link href="front/libs/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet" type="text/css"/>--}}
        {{--<link href="front/libs/fullpage.js/dist/jquery.fullpage.min.css" rel="stylesheet" type="text/css"/>--}}
        {{--<link href="front/libs/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css"/>--}}
        {{--<link href="front/libs/swiper/dist/css/swiper.min.css" rel="stylesheet" type="text/css"/>--}}

        <!-- CSS -->

        {{HTML::style('front/css/stylesHome.css', array('async'))}}


        @yield('css')
        @yield('css-plugins')

        @if(isset($settings['services-connection']))
            {{$settings['services-connection']}}
        @endif
    </head>
        <body class="mz-loading locale-{{App::getLocale()}}">
        <div class="wrapper">

            <!-- START SITE HEADER -->
            <header class="header">
                @yield('header')
            </header>

        @yield('content')
            @include('front.magazine.loader')

            <script>
                var baseUrl = '{{URL::to("/")}}';
                var subscribeFormRules = {{$subscribeFormRules}};
                var contactFormRules = {{$contactFormRules}};
                var isHomePage = {{$pageType == 'home'? 1 : 0}};
            </script>


{{--    {{HTML::script('front/libs/jquery/dist/jquery.min.js')}}--}}
{{--    {{HTML::script('front/libs/bootstrap/dist/js/bootstrap.min.js')}}--}}

        {{HTML::script('front/plugins/jquery/jquery-1.11.2.min.js')}}
        {{HTML::script('front/plugins/bootstrap-3.3.2-dist/js/bootstrap.min.js')}}

        {{HTML::script('front/libs/fullpage.js/dist/jquery.fullpage.min.js')}}
        {{HTML::script('front/libs/swiper/dist/js/swiper.jquery.min.js')}}
        {{HTML::script('front/js/app.js')}}

        {{HTML::script('front/plugins/jqueryformstyler-master/jquery.formstyler.min.js')}}
        {{HTML::script('js/plugins/jquery-validation/jquery.validate.js')}}

        @yield('js-plugins')

        {{HTML::script('front/js/scripts.js')}}
        {{HTML::script('front/js/mz-loader.js')}}

        @yield('js')


</div>
    </body>
</html>