@extends('front.layouts.main')

@section('header')
    @include('front.partials.header')
@stop

@section('content')
     <!-- END SITE NAVIGATION -->
    @include('front.partials.sidebar-left')
     <!-- START POPUP -->
   @include('front.partials.sidebar-right')
    <section class="section-top-general" id="home">
        <div class="container vertical-center-general">
            <div class="row" style="width: 100%;">
                <div class="">
                    <div class="col-md-12 welcome-main-title text-center">
                        Privacy Policy
                    </div>
                    {{--<div class="col-md-6 col-md-offset-3 welcome-main-desc text-center">--}}
                    {{--Welcome to the world of life and luxury, the leading luxury group offering the finest collection--}}
                    {{--of services covering an essential facet in the luxury market.--}}
                    {{--</div>--}}
                </div>
            </div>
            <div class="section-footer">
                <a href="#" class="section-btn-next section-btn-next-down">
                    <span class="section-next-name">Discover More</span>
                    <span class="section-next-icon fa fa-chevron-down" aria-hidden="true"></span>
                </a>
            </div>
            {{--<div class="">--}}
                {{--<div>--}}
                    {{--<a href="javascript:void(0);" class="learn-more">--}}
                        {{--DISCOVER MORE--}}
                        {{--<div class="round-button"></div>--}}
                    {{--</a>--}}
                {{--</div>--}}
            {{--</div>--}}
        </div>

    </section>

    <section class="" id="tandc-content" style="padding-top: 100px; margin-bottom: 100px;">
        <div class="container">
            <div class="row">
                <div class="term-item">
                    <div class="col-md-12 tandc-title">
                        1. Our Policy
                    </div>
                    <div class="col-md-12 tandc-desc">
                        One morning, when Gregor Samsa woke from troubled dreams, he found himself transformed in his bed into a horrible vermin. He lay on his armour-like back, and if he lifted his head a little he could see his brown belly, slightly domed and divided by arches into stiff sections. The bedding was hardly able to cover it and seemed ready to slide off any moment. His many legs, pitifully thin compared with the size of the rest of him, waved about helplessly as he looked.
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="term-item">
                    <div class="col-md-12 tandc-title">
                        2. Our Policy
                    </div>
                    <div class="col-md-12 tandc-desc">
                        One morning, when Gregor Samsa woke from troubled dreams, he found himself transformed in his bed into a horrible vermin. He lay on his armour-like back, and if he lifted his head a little he could see his brown belly, slightly domed and divided by arches into stiff sections. The bedding was hardly able to cover it and seemed ready to slide off any moment. His many legs, pitifully thin compared with the size of the rest of him, waved about helplessly as he looked.
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="term-item">
                    <div class="col-md-12 tandc-title">
                        3. COur Policy
                    </div>
                    <div class="col-md-12 tandc-desc">
                        One morning, when Gregor Samsa woke from troubled dreams, he found himself transformed in his bed into a horrible vermin. He lay on his armour-like back, and if he lifted his head a little he could see his brown belly, slightly domed and divided by arches into stiff sections. The bedding was hardly able to cover it and seemed ready to slide off any moment. His many legs, pitifully thin compared with the size of the rest of him, waved about helplessly as he looked.
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="term-item">
                    <div class="col-md-12 tandc-title">
                        4. Our Policy
                    </div>
                    <div class="col-md-12 tandc-desc">
                        One morning, when Gregor Samsa woke from troubled dreams, he found himself transformed in his bed into a horrible vermin. He lay on his armour-like back, and if he lifted his head a little he could see his brown belly, slightly domed and divided by arches into stiff sections. The bedding was hardly able to cover it and seemed ready to slide off any moment. His many legs, pitifully thin compared with the size of the rest of him, waved about helplessly as he looked.
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="term-item">
                    <div class="col-md-12 tandc-title">
                        5. Our Policy
                    </div>
                    <div class="col-md-12 tandc-desc">
                        One morning, when Gregor Samsa woke from troubled dreams, he found himself transformed in his bed into a horrible vermin. He lay on his armour-like back, and if he lifted his head a little he could see his brown belly, slightly domed and divided by arches into stiff sections. The bedding was hardly able to cover it and seemed ready to slide off any moment. His many legs, pitifully thin compared with the size of the rest of him, waved about helplessly as he looked.
                    </div>
                </div>
            </div>
        </div>

    </section>


    @include('front.partials.advertisment-area')

    <section class="" id="newsletter-area" style="position:relative;">

        @include('front.partials.newsletter')

    </section>

    <section id="footer" style="position:relative;">

        @include('front.partials.footer')

    </section>

@stop

@section('js')

    <script>
        $(document).ready(function (ev) {
            $('#custom_carousel').on('slide.bs.carousel', function (evt) {
                $('#custom_carousel .controls li.active').removeClass('active');
                $('#custom_carousel .controls li:eq(' + $(evt.relatedTarget).index() + ')').addClass('active');
            });

            $('#join-newsletter-btn').click(function() {
                displayNewsletterPopup();
            });

            $('#newsletter-close-btn').click(function() {
                hideNewsletterPopup();
            });

            function hideNewsletterPopup() {
                $('#newsletter-popup-container').hide();
                $('#newsletter-content').show();
            }
            function displayNewsletterPopup() {
                $('#newsletter-content').hide();
                $('#newsletter-popup-container').show();
            }
        });
    </script>

@stop