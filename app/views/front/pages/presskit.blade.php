<!DOCTYPE html>

<html>

<head>
    <meta charset="utf-8">
    <title>Press Kit</title>
    {{HTML::style('front/css/press-kit.css', array('async'))}}

            <!-- Font awesome -->
    <script src="https://use.fontawesome.com/3fbfe264e4.js"></script>
</head>

<body>
<section id="header">
    <!-- Brand identity -->
    <div class="full-width">
        <div class="inner-box full-width">
            <div class="inner-text-box welcome-msg full-width">
                <div class="txt">Welcome to the world of Life&Luxury,<br>
                    your finest collection of services.</div>
            </div>

            <div class="inner-text-box central-text first-page full-width">
                <div class="header-logo">
                    <img class="full-width" src="/front/img/presskit/life-luxury-logo.png">
                </div><br>
                <div class="press-kit">
                    <img class="full-width" src="/front/img/presskit/press-kit.png">
                </div><br>
                <div class="header-line">
                    <div class="vertical-line"></div>
                </div><br>
                <div class="header-button">
                    <a href="#header-content"><img src="/front/img/presskit/header-button-BI.png"></a>
                </div>
            </div>

            <img class="header-img full-width" src="/front/img/presskit/header-circles-BI.png">
        </div>
        <img class="bg-image full-width" src="/front/img/presskit/header-BI.png">
    </div>
    <div class="full-width move-up-250 paddingTop20 absolute" id="header-content">
        <div class="contents-box full-width">
            <div class="row">
                <div class='col-xs-3 col-md-5'>
                    <div class="row">
                        <div class='col-xs-9 col-xs-offset-3 col-md-5 col-md-offset-6  contents'>
                            <img class="contents-img" src="/front/img/presskit/contents.png">
                        </div>
                    </div>
                </div>
                <div class='col-xs-9 col-md-7 contents-link'>
                    <div class="row">
                        <div class='col-xs-5'>
                            <div class="content-title">
                                <div class="number">01</div>
                                <a href="#warm-welcome"><div class="title">A warm welcome</div></a>
                            </div>
                            <div class="clearfix"></div>
                        </div>
                        <div class='col-xs-5'>
                            <div class="content-title">
                                <div class="number">03</div>
                                <a href="#our-vision"><div class="title">Our vision</div></a>
                            </div>
                            <div class="clearfix"></div>
                        </div>
                    </div>
                    <div class="clearfix"></div>
                    <div class="row margintop40">
                        <div class='col-xs-5'>
                            <div class="content-title">
                                <div class="number">02</div>
                                <a href="#about-us"><div class="title">About us</div></a>
                            </div>
                            <div class="clearfix"></div>
                        </div>
                        <div class='col-xs-5'>
                            <div class="content-title">
                                <div class="number">04</div>
                                <a href="#the-collection"><div class="title">The collection</div></a>
                            </div>
                            <div class="clearfix"></div>
                        </div>
                    </div>
                </div>
                <div class="clearfix"></div>
            </div>
            <div class="clearfix"></div>
        </div>
    </div>

</section>

<section id="section_1" class="relative">
    <!-- Verbal identity -->
    <div id="warm-welcome" class="section-link"></div>
    <div class="full-width relative">
        <div class="inner-box full-width">
            <div class="inner-text-box central-text full-width section-page">
                <div class="warm-welcome-title">
                    <img class="img"  src="/front/img/presskit/warm-welcome.png">
                </div>
                <div class="number">01</div>
            </div>

            <img class="header-img full-width" src="/front/img/presskit/circles.png">
        </div>
        <img class="bg-image full-width"  src="/front/img/presskit/purple-bg.png">
    </div>

    <!-- Title -->
    <div class="full-width relative" style="margin-top:-12%;">
        <div class="inner-box full-width">
            <div class="inner-text-box full-width relative">
                <div class="warm-welcome-title-text">
                    <img src="/front/img/presskit/warm-welcome-title.png">
                </div>
            </div>
        </div>
        <img class="bg-image full-width"  src="/front/img/presskit/warm-welcome-title-image.png">
    </div>

    <!-- Text -->
    <div class="full-width relative" style="margin-top:-5%;">
        <div class="inner-box full-width">
            <div class="inner-text-box full-width relative">
                <!-- sm-3 -->
                <div class="text-box relative col-xs-offset-5 col-md-offset-6">
                    <div class="hidden-xs hidden-sm hidden-box"></div>
                    <div class="text warm-welcome-text">
                        <div class="big-letter letter-l relative">
                            Life & Luxury: two concepts united by one passion. They are<br>
                            reflected throughout the services and products that we offer.<br>
                            Our appreciation of aesthetics is embossed in our sophisticated<br>
                            design. Our work ethics lie in our exceptional standards, and we do<br>
                            not take the ordinary for an answer.<br><br>

                            Luxury is more than a silent sparkling chandelier. Luxury has a soul,<br>
                            and this soul should blend together beautifully with lifestyle.<br>
                            We extend our warmest welcome to you. Together, we shall thrive<br>
                            and excel in the essence of elegance.<br>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <img class="bg-image full-width"  src="/front/img/presskit/warm-welcome-background.png">
    </div>

</section>

<section id="section_2" class="relative">
    <div id="about-us" class="section-link"></div>
    <div class="full-width relative" style="margin-top:-5%;">
        <div class="inner-box full-width">
            <div class="inner-text-box central-text full-width section-page" style="top:30%;">
                <div class="about-us-title">
                    <img class="img" src="/front/img/presskit/about-us.png">
                </div>
                <div class="number">02</div>
            </div>

            <img class="header-img full-width" src="/front/img/presskit/circles.png">
        </div>
        <img class="bg-image full-width"  src="/front/img/presskit/purple-bg-reverse.png">
    </div>

    <!-- Title -->
    <div class="full-width relative" style="margin-top:-8%;">
        <div class="inner-box full-width">
            <div class="inner-text-box full-width relative">
                <div class="about-us-title-text">
                    <img src="/front/img/presskit/about-us-title.png">
                </div>
            </div>
        </div>
        <img class="bg-image full-width"  src="/front/img/presskit/about-us-title-image.png">
    </div>

    <!-- Text -->
    <div class="service-info-image relative" style="margin-top:-35%;">
        <div class="inner-box full-width">
            <div class="inner-text-box full-width relative">
                <!-- sm-3 -->
                <div class="text-box relative col-xs-offset-5 col-md-offset-6">
                    <div class="hidden-xs hidden-sm hidden-box"></div>
                    <div class="text about-us-text">
                        <div class="big-letter letter-w relative">
                            Welcome to the world of Life & Luxury, the leading luxury<br>
                            group offering the finest collection of services covering<br>
                            an essential facet in the luxury market.<br><br>

                            Our award winning services include Advertising, Creative Design<br>
                            Services, Events Management, Publishing Services, Music as well as<br>
                            Video Production Services.<br><br>

                            With our coverage in the Central as well as Eastern Europe, represe-<br>
                            ntatives speaking over 6 languages, Life & Luxury connects global<br>
                            and local, providing each of our clients with an unparalleled service<br>
                            whatever the location.<br><br>

                            Our Unique Guest Relation Service is available 24 hours a day, 365<br>
                            days a year, by sharing specialist knowledge and expertise.<br><br>

                            Our brand is a promise - our personnel, a finest quality service and<br>
                            a passion for our work, will make your experience truly exclusive!
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <img class="bg-image full-width"  src="/front/img/presskit/about-us-background.png">
    </div>

    <div class="clearfix"></div>
</section>

<section id="section_3" class="relative">
    <div id="our-vision" class="section-link"></div>
    <!-- Our vision -->
    <div class="full-width relative" style="margin-top:-7%;">
        <div class="inner-box full-width">
            <div class="inner-text-box central-text full-width section-page" style="top:30%;">
                <div class="our-vision-title">
                    <img class="img"  src="/front/img/presskit/our-vision.png">
                </div>
                <div class="number">03</div>
            </div>

            <img class="header-img full-width" src="/front/img/presskit/circles.png">
        </div>
        <img class="bg-image full-width"  src="/front/img/presskit/purple-bg.png">
    </div>

    <!-- Title -->
    <div class="full-width relative" style="margin-top:-8%;">
        <div class="inner-box full-width">
            <div class="inner-text-box full-width relative">
                <div class="our-vision-title-text">
                    <img src="/front/img/presskit/our-vision-title.png">
                </div>
            </div>
        </div>
        <img class="bg-image full-width"  src="/front/img/presskit/our-vision-title-image.png">
    </div>

    <!-- Text -->
    <div class="service-info-image relative" style="margin-top:-35%;">
        <div class="inner-box full-width">
            <div class="inner-text-box full-width relative">
                <!-- sm-3 -->
                <div class="text-box relative col-xs-offset-5 col-md-offset-6">
                    <div class="hidden-xs hidden-sm hidden-box height200"></div>
                    <div class="text our-vision-text">
                        <div class="big-letter letter-w relative">
                            We have created a unique collection of services which are<br>
                            just as unique as our customers. All services are carefully<br>
                            laid out to fulfil even the most demanding desires.<br><br>

                            Our work is like your journey, it is never complete until it reaches<br>
                            perfection. Swiss Quality combined with our Award Winning Service<br>
                            makes the whole process an Unforgettable Experience for each of<br>
                            our customers whether first time or returning.<br><br>

                            Always Innovating, Always Challenging, and Always Pushing our<br>
                            Boundaries. All customers are just as unique as their dreams<br>
                            and most important of all, our knowledge is always at your fingertips.<br><br>

                            Whether it's Management, Production or any other service, our<br>
                            client orientated team will return your request with an outstanding<br>
                            service.
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <img class="bg-image full-width"  src="/front/img/presskit/our-vision-background.png">
    </div>
    <div class="clearfix"></div>
</section>

<section id="section_4" class="relative">
    <div id="the-collection" class="section-link"></div>
    <div class="full-width relative" style="margin-top: -8%;">
        <div class="inner-box full-width">
            <div class="inner-text-box central-text full-width section-page connect" style="top:30%;">
                <div class="the-collection-title">
                    <img class="img" src="/front/img/presskit/the-collection.png">
                </div>
                <div class="number">04</div>
            </div>

            <img class="header-img full-width" src="/front/img/presskit/circles.png">
        </div>
        <img class="bg-image full-width"  src="/front/img/presskit/purple-bg-reverse.png">
    </div>

    <!-- Creative -->
    <div class="full-width ll-creative relative" style="margin-top:-8%;">
        <div class="inner-box full-width">
            <div class="inner-text-box full-width relative">
                <div class="text-box row">
                    <div class="col-sm-6">
                        <div class="ll-creative-image">
                            <img src="/front/img/presskit/ll-creative-ipad.png" class="img">
                        </div>
                    </div>
                    <div class="col-sm-6">
                        <div class="hidden-xs hidden-sm hidden-box height30"></div>
                        <div class="service-info-logo title to-left creative-title">
                            <img src="/front/img/presskit/l-l-creative-title.png">
                        </div>
                        <div class="service-info-text">
                            A dolor sit amet, consectetuer adipiscing elit  commodo ligula eget<br>
                            dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis<br>
                            parturient montes, nascetur ridiculus mus.
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <img class="bg-image full-width"  src="/front/img/presskit/l-l-creative.png">
    </div>

    <!-- Rendez vous -->
    <div class="full-width ll-renedez-vous relative" style="margin-top:-8%;">
        <div class="inner-box full-width">
            <div class="inner-text-box full-width relative">
                <div class="text-box row">
                    <div class="col-sm-6">
                        <div class="pull-right">
                            <div class="hidden-xs hidden-sm hidden-box height30"></div>
                            <div class="service-info-logo title rendez-vous-title">
                                <img src="/front/img/presskit/l-l-rendez-vous-title.png">
                            </div>
                            <div class="service-info-text">
                                A dolor sit amet, consectetuer adipiscing elit  commodo ligula eget<br>
                                dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis<br>
                                parturient montes, nascetur ridiculus mus.
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-6">
                        <div class="ll-rendez-vous-image">
                            <img src="/front/img/presskit/ll-rendez-vous-image.png" class="img">
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <img class="bg-image full-width"  src="/front/img/presskit/l-l-rendez-vous.png">
    </div>

    <!-- Magazine -->
    <div class="full-width ll-magazine relative" style="margin-top: -13%; z-index: 10;">
        <div class="inner-box full-width">
            <div class="inner-text-box full-width relative">
                <div class="text-box row">
                    <div class="col-sm-6">
                    </div>
                    <div class="col-sm-6">
                        <div class="hidden-xs hidden-sm hidden-box height30"></div>
                        <div class="service-info-logo title to-left magazine-title">
                            <img src="/front/img/presskit/l-l-magazine-title.png">
                        </div>
                        <div class="service-info-text">
                            A dolor sit amet, consectetuer adipiscing elit  commodo ligula eget<br>
                            dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis<br>
                            parturient montes, nascetur ridiculus mus.
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <img class="bg-image full-width"  src="/front/img/presskit/l-l-magazine.png">
    </div>

    <!-- Newsletter -->
    <div class="full-width newsletter-signup relative" style="margin-top:-19%; z-index: 20;">
        <div class="ll-magazine-image absolute">
            <img src="/front/img/presskit/ll-magazine-image.png" class="img">
        </div>
        <div class="inner-box full-width">
            <div class="inner-text-box full-width relative">
                <div class="text-box row">
                    <div class="col-sm-6">
                        <div class="newsletter-info">
                            <div class="newsletter-info-inner">
                                <div class="hidden-xs hidden-sm hidden-box"></div>
                                <div class="service-info-logo title">
                                    <img src="/front/img/presskit/newsletter-signup-title.png">
                                </div>
                                <div class="service-info-text">
                                    A dolor sit amet, consectetuer adipiscing elit  commodo<br>
                                    ligula eget dolor. Aenean massa. Cum sociis natoque<br>
                                    penatibus et magnis dis parturient montes, nascetur<br>
                                    ridiculus mus.
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-6">
                        <div class="newsletter-form">
                            <div class="newsletter-form-inner">
                                <div class="hidden-xs hidden-sm hidden-box height30 "></div>
                                <form>
                                    <div class="form-group">
                                        <label>First name</label>
                                        <input type="text" name="">
                                    </div>
                                    <div class="form-group">
                                        <label>Surname</label>
                                        <input type="text" name="">
                                    </div>
                                    <div class="form-group">
                                        <label>Email</label>
                                        <input type="text" name="">
                                    </div>
                                    <div class="form-group">
                                        <label>Country</label>
                                        <input type="text" name="">
                                    </div>
                                    <button type="submit" class="btn btn-default pull-right">Signup</button>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <img class="bg-image full-width"  src="/front/img/presskit/newsletter-bg-image.png">
    </div>

    <!-- Get in touch -->

    <div class="get-in-touch full-width relative" style="margin-top: -15%; z-index: 40;">
        <div class="inner-box full-width">
            <!-- Header -->
            <div class="service-info relative">
                <div class="service-info-logo title" id="get-in-touch">
                    <img src="/front/img/presskit/get-in-touch.png">
                </div>
                <div class="service-info-title">
                    Contact us
                </div>
            </div>
            <!-- Central text with ellipse BG -->
            <div class="central-content relative">
                <div class="inner-text-box contact-links full-width">
                    <div class="message-icons">
                        <div class="to-center">
                            <div class="chat-with-us">
                                <div class="img">
                                    <a href="#">
                                        <img src="/front/img/presskit/chat.png">
                                    </a>
                                </div>
                                <div class="chat-text">Chat with us</div>
                            </div>
                            <div class="contact-us">
                                <div class="img">
                                    <a href="#">
                                        <img src="/front/img/presskit/contact.png">
                                    </a>
                                </div>
                                <div class="contact-text">Contact us</div>
                            </div>
                        </div>
                    </div>
                    <div class="social-media">
                        <div class="to-center">
                            <div class="col-sm-3 icon">
                                <a href="https://www.facebook.com/lifeandluxurycollection"><img src="/front/img/presskit/facebook.png"></a>
                            </div>
                            <div class="col-sm-3 icon">
                                <a href="https://twitter.com/LifenLuxury"><img src="/front/img/presskit/twitter.png"></a>
                            </div>
                            <div class="col-sm-3 icon">
                                <a href="https://www.youtube.com/channel/UCePsdWq9xg-EN9E7DGaAuHg"><img src="/front/img/presskit/youtube.png"></a>
                            </div>
                            <div class="col-sm-3 icon">
                                <a href="https://www.instagram.com/your_finest_collection"><img src="/front/img/presskit/instagram.png"></a>
                            </div>
                        </div>
                    </div>
                    <div class="lifeluxury-link">
                        <a href="http://www.lifeandluxury.com/">www.lifeandluxury.com</a>
                    </div>
                </div>
                <!-- Background -->
                <div class="ellipse-contact-img full-width">
                    <img src="/front/img/presskit/ellipse-contact.png">
                </div>
            </div>
            <!-- Footer -->
            <div class="service-info-text footer">
                G.S. LIFE AND LUXURY CYPRUS LTD. All rights reserved. LIFE AND LUXURY, the<br>
                LIFE AND LUXURY logo, and all other LIFE AND LUXURY marks contained here in are trademarks<br>of G.S. LIFE AND LUXURY CYPRUS LTD
            </div>
        </div>
        <img class="bg-image full-width"  src="/front/img/presskit/get-in-touch-image.png">
    </div>
</section>

</body>


<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
</html>

