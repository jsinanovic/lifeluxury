@extends('front.layouts.main')

@section('header')
@include('front.partials.header')
@stop

@section('content')
        <!-- END SITE NAVIGATION -->
@include('front.partials.sidebar-left')
        <!-- START POPUP -->
@include('front.partials.sidebar-right')
    <section class="section-top-general" id="home">
        <div class="container vertical-center-general">
            <div class="row" style="width: 100%;">
                <div class="">
                    <div class="col-md-12 welcome-main-title text-center">
                        Service Promise
                    </div>
                </div>
            </div>
            <div class="section-footer">
                <a href="#" class="section-btn-next section-btn-next-down">
                    <span class="section-next-name">Discover More</span>
                    <span class="section-next-icon fa fa-chevron-down" aria-hidden="true"></span>
                </a>
            </div>
            {{--<div class="">--}}
                {{--<div>--}}
                    {{--<a href="javascript:void(0);" class="learn-more">--}}
                        {{--DISCOVER MORE--}}
                        {{--<div class="round-button"></div>--}}
                    {{--</a>--}}
                {{--</div>--}}
            {{--</div>--}}
        </div>
    </section>


    <section class="" id="servicepromise-content" style="padding-top: 100px; margin-bottom: 100px;">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="row">
                        <div class="col-md-6">
                            <div class="term-item">
                                <div class="col-md-12 tandc-title">
                                    Service Promise
                                </div>
                                <div class="col-md-12 tandc-desc">
                                    Any anticipated completion date provided by the agency is subject to options chosen and client co-operation in provision of information, resource (logos, images etc.) and approvalLife & Luxury will do its very best to ensure that agreed timelines but please be aware that circumstances, amends requested, or additional requirements may potentially result in delays.
                                    <br><br>
                                    Time lines provided are estimated but the agency will not be held liable if the project over-runs due to delays caused by the client passing information or approval, or any <b>third party issues</b>.
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="">
                                <img class="img-responsive" src="/img/backgrounds/servicepromise.png">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>


    @include('front.partials.advertisment-area')

    <section class="" id="newsletter-area" style="position:relative;">

        @include('front.partials.newsletter')

    </section>

    <section id="footer" style="position:relative;">

        @include('front.partials.footer')

    </section>

@stop

@section('js')

    <script>
        $(document).ready(function (ev) {
            $('#custom_carousel').on('slide.bs.carousel', function (evt) {
                $('#custom_carousel .controls li.active').removeClass('active');
                $('#custom_carousel .controls li:eq(' + $(evt.relatedTarget).index() + ')').addClass('active');
            });

            $('#join-newsletter-btn').click(function() {
                displayNewsletterPopup();
            });

            $('#newsletter-close-btn').click(function() {
                hideNewsletterPopup();
            });

            function hideNewsletterPopup() {
                $('#newsletter-popup-container').hide();
                $('#newsletter-content').show();
            }
            function displayNewsletterPopup() {
                $('#newsletter-content').hide();
                $('#newsletter-popup-container').show();
            }
        });
    </script>

@stop