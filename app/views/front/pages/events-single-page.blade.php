@extends('front.layouts.main')

@section('header')
@include('front.partials.header')
@stop

@section('content')
        <!-- END SITE NAVIGATION -->
@include('front.partials.sidebar-left')
        <!-- START POPUP -->
@include('front.partials.sidebar-right')
    <section class="section-top-general" id="home">
        <div class="container vertical-center-general">
            <div class="row" style="width: 100%;">
                <div class="">
                    <div class="col-md-12 welcome-main-title text-center">
                        Event Name
                    </div>
                </div>
            </div>
            <div class="section-footer">
                <a href="#" class="section-btn-next section-btn-next-down">
                    <span class="section-next-name">Discover More</span>
                    <span class="section-next-icon fa fa-chevron-down" aria-hidden="true"></span>
                </a>
            </div>
        </div>
    </section>

    <section class="" id="tandc-content" style="padding-top: 100px; margin-bottom: 100px;">
        <div class="container">
             <div class="event-image">
                 <img src="/front/img/event/single-events-1.png" alt="" />
             </div>
            <div style="width: 100%; margin-top: 40px;">
                <div class="event-text" style="width: 65%; float: left;">
                    <h3 style=" border-bottom: 0.5px solid #aaa;">London Club</h3>
                     <p style=" padding-top: 10px;">Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor.
                         Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes nascetur ridiculus mus.
                         Donec quam felis, ultricies nec, pellentesque eu,ascetur ridiculus mus. Donec quam felis, ultricies nec,
                         pellentesque eu, Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor.<br/><br/>
                         Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor.
                         Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes nascetur ridiculus mus.
                         Donec quam felis, ultricies nec, pellentesque eu,ascetur ridiculus mus. Donec quam felis, ultriciev
                     </p>
                    <div id="join-newsletter-btn" class="newsletter-join-btn" style="background-color: #2b2d39 !important;">
                        <span>I'm Going  <i class="fa fa-angle-right" style="margin-left: 20px;" aria-hidden="true"></i></span>
                    </div>
                </div>
                <div class="event-informations" style="float:right; width: 28%;  ">
                    <h3 style="border-bottom: 0.5px solid #aaa;">Informations</h3>
                    <li style=" padding-top: 10px;" >City: <a>test</a></li> <br/>
                    <li>Location: <a>test</a></li><br/>
                    <li>Date: <a>test</a></li><br/>
                    <li>Dress Code: <a>test</a></li><br/>
                    <li>Reservations: <a>test</a></li><br/>
                    <li style="font-weight: 700;"href="#">BUY TICKET <i class="fa fa-angle-right" style="margin-left: 20px;" aria-hidden="true"></i></li>
                </div>

                <div class="event-informations-1" style="float:right; width: 28%;  margin-right: 10px;">
                    <h3 style="border-bottom: 0.5px solid #aaa;">Map</h3>
                    <img src="/front/img/event/evet-map.png" alt="" />
                </div>
                <div class="event-image-2" style="float: left; margin-top: 40px;">
                    <img src="/front/img/event/single-events-2.png" alt="" />
                </div>
                <div class="event-image-2" style="float: left; margin-top: 40px;">
                    <img src="/front/img/event/single-events-3.png" alt="" />
                </div>
                <div class="article-image-3" style="float: left; width: 110%; display: block; margin-top: 60px;">
                    <div  class="headLine" style="">Related</div>
                    <div class="article-image-3-1" style="float: left; width: 31%; " >
                        <img style="float: left;" src="/front/img/event/single-events1-1.png" alt="" />
                        <div class="popup-item-link-creativ1" >
                                <p href="/" class="popup-item-link-creativ" style="width: 400px; left: 25% !important; line-height: 5px !important; font: 700 36px Butler  !important;">Event Name</p>
                                <a href="#" class="popup-item-link-creativ" style="width: 400px; left: 25% !important; top:40% !important; font: 700 18px Butler Medium !important;" >Discover More <i class="fa fa-angle-right" style="margin-left: 10px;" aria-hidden="true"></i></a>
                                </a>
                        </div>
                    </div>

                    <div class="article-image-3-2" style="float: left; width: 31%; " >
                        <img style="float: left;" src="/front/img/event/single-events1-2.png" alt="" />
                        <div class="popup-item-link-creativ1" >
                            <p href="/" class="popup-item-link-creativ" style="width: 400px; left: 25% !important; line-height: 5px !important; font: 700 36px Butler  !important;">Event Name</p>
                            <a href="#" class="popup-item-link-creativ" style="width: 400px; left: 25% !important; top:40% !important; font: 700 18px Butler  !important;" >Discover More <i class="fa fa-angle-right" style="margin-left: 10px;" aria-hidden="true"></i></a>
                            </a>
                        </div>
                    </div>

                    <div class="article-image-3-3" style="float: left; width: 31%; " >
                        <img style="float: left;" src="/front/img/event/single-events1-1.png" alt="" />
                        <div class="popup-item-link-creativ1" >
                            <p href="/" class="popup-item-link-creativ" style="width: 400px; left: 25% !important; line-height: 5px !important; font: 700 36px Butler  !important;">Event Name</p>
                            <a href="#" class="popup-item-link-creativ" style="width: 400px; left: 25% !important; top:40% !important; font: 700 18px Butler  !important;" >Discover More <i class="fa fa-angle-right" style="margin-left: 10px;" aria-hidden="true"></i></a>
                            </a>
                        </div>
                    </div>
                </div>
            </div>

            <div  id="join-newsletter-btn" class="newsletter-join-btn"  style="border : none !important;margin-left: 45%;float: left; text-align: center;">
                <span><i class="fa fa-angle-left" aria-hidden="true" style="margin-right: 20px;"></i>GO BACK</span>
            </div>
        </div>
    </section>


@include('front.partials.advertisment-area')

<section class="" id="newsletter-area" style="position:relative;">

    @include('front.partials.newsletter')

</section>

<section id="footer" style="position:relative;">

    @include('front.partials.footer')

</section>
@stop

@section('js')

    <script>
        $(document).ready(function (ev) {
            $('#custom_carousel').on('slide.bs.carousel', function (evt) {
                $('#custom_carousel .controls li.active').removeClass('active');
                $('#custom_carousel .controls li:eq(' + $(evt.relatedTarget).index() + ')').addClass('active');
            });

            $('#join-newsletter-btn').click(function() {
                displayNewsletterPopup();
            });

            $('#newsletter-close-btn').click(function() {
                hideNewsletterPopup();
            });

            function hideNewsletterPopup() {
                $('#newsletter-popup-container').hide();
                $('#newsletter-content').show();
            }
            function displayNewsletterPopup() {
                $('#newsletter-content').hide();
                $('#newsletter-popup-container').show();
            }
        });
    </script>

@stop