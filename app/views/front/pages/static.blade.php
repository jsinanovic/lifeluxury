@extends('front.layouts.main')

@section('content')
<section class="banner">
    <div class="banner-header">
        <h1>{{{$page->name}}}</h1>  
    </div>
</section>

<section class="section-page">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="content">{{$page->content}}</div>
            </div>
        </div>
    </div>
</section>

@include('front.layouts.contacts')
@include('front.layouts.footer')
@stop