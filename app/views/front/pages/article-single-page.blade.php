@extends('front.layouts.main')

@section('header')
@include('front.partials.header')
@stop

@section('content')
        <!-- END SITE NAVIGATION -->
@include('front.partials.sidebar-left')
        <!-- START POPUP -->
@include('front.partials.sidebar-right')
    <section class="section-top-general" id="home">
        <div class="container vertical-center-general">
            <div class="row" style="width: 100%;">
                <div class="">
                    <div class="col-md-12 welcome-main-title text-center">
                       Effective Forms Advertising
                    </div>
                </div>
            </div>
            <div class="section-footer">
                <a href="#" class="section-btn-next section-btn-next-down">
                    <span class="section-next-name">Discover More</span>
                    <span class="section-next-icon fa fa-chevron-down" aria-hidden="true"></span>
                </a>
            </div>
        </div>
    </section>

    <section class="" id="tandc-content" style="padding-top: 100px; margin-bottom: 100px;">
        <div class="container">
            <div class="article-text" style="width: 80%; float: right;">
                <p>
                    <firstletter>A</firstletter>
                    rt-Work shot with Ringuet over several collections, always aiming to capture the beauty and
                    simplicity of the garments. The shoots were often focussed on capturing the balance between light and dark andboutique versus haute couture.<br/><br/>
                    AW also drew on years of experience within the garment industry to provide vivid, experimental graphics across a
                    number of print seasons, along with providing business development and consultation services.<br/><br/>
                    Each season was wrapped up with the creation of limited release look books, showcasing campaign and
                    sales imagery. Our work with Ringuet is a great example of our multi-disciplinary skills; a fusion of product, print and photography.
                </p>
            </div>
            <div class="article-informations1" style="float:left; width: 20%">
               <p>Photography<br/>
                   Product<br/>
                   Styling<br/>
                   Studio
               </p>
            </div>
            <div class="article-image-2" style="padding-top: 0px;">
                <img src="/front/img/article/article-photo1.png" alt="" />
            </div>

            <div class="article-text-2" style="margin-top: 50px;width: 80%; float: left;">
                <p>
                    One morning, when Gregor Samsa woke from troubled dreams, he found himself transformed in his bed into a horrible vermin.
                    He lay on his armour-like back, and if he lifted his head a little he could see his brown belly, slightly domed and divided
                    by arches into stiff sections. The bedding was hardly able to cover it and seemed ready to slide off any moment. His many legs,
                    pitifully thin compared with the size of the rest of him,<br/><br/><br/>
                    Waved about helplessly as he looked. "What's happened to me?"
                    he thought. It wasn't a dream. His room, a proper human room although a little too small, lay peacefully between
                    its four familiar walls. A
                </p>
            </div>
            <div class="article-informations2" style="float:right; margin-top: 50px; width: 20%;">
                <p>“A wonderful serenity  has taken possession  of my sweet mornings of spring."<br/><br/>
                    <a style="color: #e5e6df !important; font-size: 14px;">-Bil Gates</a></p>
            </div>

            <div class="article-photos" style="width: 100%;argin-top: 50px;">
                <div class="photo1"  style="width: 45%; float:left;">
                    <img src="/front/img/article/article-single-2.png" alt="" />
                    <img src="/front/img/article/article-single-3.png" alt="" />
                </div>
                <div class="photo2"  style="width: 55%; float:right;">
                    <img src="/front/img/article/article-single.png" alt="" />
                </div>
            </div>

            <div class="article-text-3" style="width: 75%; float: right; margin-top: 50px;">
                <p>
                    One morning, when Gregor Samsa woke from troubled dreams, he found himself transformed in his bed into a horrible vermin.
                    He lay on his armour-like back, and if he lifted his head a little he could see his brown belly, slightly domed and divided by arches into stiff sections.
                </p>
            </div>
            <div class="article-informations-3" style="float:left; width: 20%; margin-top:50px;">
                <p>Maggazine <br/> Winter Issue
            </div>
            <div class="article-image-2" style="margin-top: 50px;">
                <img src="/front/img/article/article-single-4.png" alt="" />
                <div class="article-image-2-images">

                    <div class="article-image-3-2" style="float: left; margin-right: 2px;" >
                        <img src="/front/img/article/article-photo-1.png" alt="" />
                        <a  href="/" class="popup-item-link-creativ"  style="left:50% !important; background-image:url(/front/img/article/plusonhovr.png); "></a>
                    </div>

                    <div class="article-image-3-2" style="float: left;  margin-right: 2px;" >
                        <img src="/front/img/article/article-photo-2.png" alt="" />
                        <a  href="/" class="popup-item-link-creativ"  style="left:50% !important; background-image:url(/front/img/article/plusonhovr.png);"></a>
                    </div>

                    <div class="article-image-3-2" style="float: left; margin-right: 2px;" >
                        <img src="/front/img/article/article-photo-3.png" alt="" />
                        <a  href="/" class="popup-item-link-creativ"  style="left:50% !important; background-image:url(/front/img/article/plusonhovr.png);"></a>
                    </div>

                    <div class="article-image-3-2" style="float: left; margin-right: 2px;" >
                        <img src="/front/img/article/article-photo-4.png" alt="" />
                        <a  href="/" class="popup-item-link-creativ"  style="left:50% !important; background-image:url(/front/img/article/plusonhovr.png);"></a>
                    </div>

                    <div class="article-image-3-2" style="float: left; margin-right: 2px;" >
                        <img src="/front/img/article/article-photo-5.png" alt="" />
                        <a  href="/" class="popup-item-link-creativ"  style="left:50% !important; background-image:url(/front/img/article/plusonhovr.png);"></a>
                    </div>

                    <div class="article-image-3-2" style="float: left;" >
                        <img src="/front/img/article/article-photo-2.png" alt="" />
                        <a  href="/" class="popup-item-link-creativ"  style="left:50% !important; background-image:url(/front/img/article/plusonhovr.png);"></a>
                    </div>






                    {{--<img src="/front/img/article/article-photo-1.png" alt="" />--}}
                    {{--<img src="/front/img/article/article-photo-2.png" alt="" />--}}
                    {{--<img src="/front/img/article/article-photo-3.png" alt="" />--}}
                    {{--<img src="/front/img/article/article-photo-4.png" alt="" />--}}
                    {{--<img src="/front/img/article/article-photo-5.png" alt="" />--}}
                    {{--<img src="/front/img/article/article-photo-2.png" alt="" />--}}
                </div>
            </div>
            <div class="article-image-3" style="float: left; width: 110%; display: block; margin-top: 60px;">
                <div  class="headLine" style="margin-bottom: 20px;">Related</div>
                <div class="article-image-3-1" style="float: left; width: 31%; " >
                 <img style="float: left;" src="/front/img/event/single-events1-1.png" alt="" />
                    <div class="popup-item-link-creativ1" >
                        <p href="/" class="popup-item-link-creativ" style="width: 400px; left: 25% !important; line-height: 5px !important; font: 700 36px Butler !important;">Everyday Transformed</p>
                        <a href="#" class="popup-item-link-creativ" style="width: 400px; left: 25% !important; top:55% !important; font: 700 18px Butler !important;" >Discover More <i class="fa fa-angle-right" style="margin-left: 10px;" aria-hidden="true"></i></a>
                    </a>
                 </div>
                </div>


                <div class="article-image-3-2" style="float: left; width: 31%; " >
                    <img style="float: left;" src="/front/img/event/single-events1-2.png" alt="" />
                    <div class="popup-item-link-creativ1" >
                        <p href="/" class="popup-item-link-creativ" style="width: 400px; left: 25% !important; line-height: 5px !important; font: 700 36px Butler !important;">Everyday Transformed</p>
                        <a href="#" class="popup-item-link-creativ" style="width: 400px; left: 25% !important; top:55% !important; font: 700 18px Butler !important;" >Discover More <i class="fa fa-angle-right" style="margin-left: 10px;" aria-hidden="true"></i></a>
                        </a>
                    </div>
                </div>

                <div class="article-image-3-3" style="float: left; width: 31%; " >
                    <img style="float: left;" src="/front/img/event/single-events1-1.png" alt="" />
                    <div class="popup-item-link-creativ1" >
                        <p href="/" class="popup-item-link-creativ" style="width: 400px; left: 25% !important; line-height: 5px !important; font: 700 36px Butler !important;">Everyday Transformed</p>
                        <a href="#" class="popup-item-link-creativ" style="width: 400px; left: 25% !important; top:55% !important; font: 700 18px Butler !important;" >Discover More <i class="fa fa-angle-right" style="margin-left: 10px;" aria-hidden="true"></i></a>
                        </a>
                    </div>
                </div>


                {{--<img src="/front/img/event/single-events1-1.png" alt="" />--}}
                {{--<img src="/front/img/event/single-events1-2.png" alt="" />--}}
                {{--<img src="/front/img/event/single-events1-1.png" alt="" />--}}

            </div>
        </div>
        <div  id="join-newsletter-btn" class="newsletter-join-btn"  style="border : none !important;margin-left: 45%;float: left; text-align: center;">
            <span><i class="fa fa-angle-left" aria-hidden="true" style="margin-right: 20px;"></i>Go back</span>
        </div>
    </section>

        @include('front.partials.advertisment-area')
    <section class="" id="newsletter-area" style="position:relative;">
        @include('front.partials.newsletter')
    </section>

    <section id="footer" style="position:relative;">
        @include('front.partials.footer')
    </section>

@stop

@section('js')

    <script>
        $(document).ready(function (ev) {
            $('#custom_carousel').on('slide.bs.carousel', function (evt) {
                $('#custom_carousel .controls li.active').removeClass('active');
                $('#custom_carousel .controls li:eq(' + $(evt.relatedTarget).index() + ')').addClass('active');
            });

            $('#join-newsletter-btn').click(function() {
                displayNewsletterPopup();
            });

            $('#newsletter-close-btn').click(function() {
                hideNewsletterPopup();
            });

            function hideNewsletterPopup() {
                $('#newsletter-popup-container').hide();
                $('#newsletter-content').show();
            }
            function displayNewsletterPopup() {
                $('#newsletter-content').hide();
                $('#newsletter-popup-container').show();
            }
        });
    </script>

@stop

{{--<style type="text/css">--}}
    {{--.event-text p:first-line { font-weight: bold; }--}}
    {{--.event-text p:first-letter { font-size: 2em; }--}}
{{--</style>--}}