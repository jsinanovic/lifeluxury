@extends('front.layouts.main')
@section('header')
@include('front.partials.header')
@stop

@section('content')

        <!-- END SITE NAVIGATION -->
@include('front.partials.sidebar-left')
        <!-- START POPUP -->
@include('front.partials.sidebar-right')
        <!-- END POPUP -->

    <section class="section-full section-top" id="error404">
        <div class="container error404-content">
            <div class="row" style="width: 100%;">
                <div class="col-md-12 contact-main-title">
                    Global yet local approach
                </div>
                <div class="col-md-12 contact-main-text">
                    <div class="">
                        Life and luxury international
                    </div>
                </div>
                <div class="container">
                    <div class="row">
                        <div class="col-md-4" style="float: left; text-align: left;">
                            <div class="row">
                                <div class="col-md-12 contact-left-item-main">
                                    <i class="fa fa-phone" aria-hidden="true"></i>
                                    <span>Skype</span>
                                </div>
                                <div class="col-md-12 contact-left-item-content">
                                    <span>{{$settings['skype']}}</span>
                                </div>
                            </div>
                            <div class="row" style="margin-top: 20px;">
                                <div class="col-md-12 contact-left-item-main">
                                    <i class="fa fa-envelope" aria-hidden="true"></i>
                                    <span>Email</span>
                                </div>
                                <div class="col-md-12 contact-left-item-content">
                                    <span>{{$settings['contact-email']}}</span>
                                </div>
                            </div>


                            <div class="row" style="margin-top: 20px;">
                                <div class="col-md-12 contact-left-item-main">
                                    <i class="fa fa-comments" aria-hidden="true"></i>
                                    <span>Chat Live</span>
                                    <i class="fa fa-angle-right" style="margin-left: 10px;" aria-hidden="true"></i></span>
                                </div>
                            </div>

                            <div class="row" style="margin-top: 15px;">
                                <div class="col-md-12">
                                     <span class="contact-social-icons">
                                    <a href="https://www.facebook.com/lifeandluxurycollection"><i class="fa fa-facebook" aria-hidden="true"></i></a> &nbsp
                                    <a href="https://twitter.com/LifenLuxury" style="margin-left: 15px;"><i class="fa fa-twitter" aria-hidden="true"></i></a> &nbsp
                                    <a href="https://www.youtube.com/channel/UCePsdWq9xg-EN9E7DGaAuHg" style="margin-left: 15px;"><i class="fa fa-youtube" aria-hidden="true"></i></a> &nbsp
                                    <a href="https://www.instagram.com/your_finest_collection" style="margin-left: 15px;"><i class="fa fa-instagram" aria-hidden="true"></i></a>
                                </span>
                                </div>
                            </div>

                        </div>
                        <div class="col-md-8">

                            <div class="row" style="margin-top: 25px">

                                {{Form::open(array('url' => 'contacts', 'method' => 'post', 'role' => 'form', 'class' => 'contact-form-main', 'data-type' => 'contacts'))}}
                                <div class="">
                                    <div class="">
                                        <div class="row">
                                            <div class="col-md-6 form-group">
                                                <input  type="text" name="user_name" id="user_name" class="form-control contact-form-control"  placeholder="First Name:"  />
                                            </div>
                                            <div class="col-md-6 form-group">
                                                <input  type="text" name="user_surname" id="user_surname" class="form-control contact-form-control"  placeholder="Surname:" />
                                            </div>
                                        </div>

                                        <div class="row">
                                            <div class="col-md-6 form-group">
                                                <input type="email" name="user_email" id="user_email" class="form-control contact-form-control"  placeholder="E-mail:"   />
                                            </div>

                                            <div class="col-md-6 form-group">
                                                {{Form::select('country', Setting::values('countries'), null, array('id' => 'subscribe-country', 'class' => 'form-control','placeholder' => 'Country'))}}
                                                        <!--<input type="text" class="form-control" />-->
                                            </div>
                                        </div>

                                        <div class="row">
                                            <div class="form-group col-md-12">
                                                <textarea name="message" class="form-control contact-form-control" placeholder="Message:" rows="6" ></textarea>
                                            </div>
                                        </div>

                                        <div class="row">
                                            <div class="col-sm-12">
                                                <div class="alert alert-success" role="alert" style="display: none;">
                                                    Success! You message has been sent
                                                </div>
                                                <div class="alert alert-danger" role="alert" style="display: none;">
                                                    <div class="alert-text"></div>
                                                </div>
                                            </div>
                                        </div>

                                    </div>
                                    <div style="float: left;">
                                        <input type="submit" class="btn btn-primary contact-form-btn" value="Contact us">
                                    </div>
                                </div>
                                {{Form::close()}}

                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div style="clear: both;"></div>

        <div class="footer-transparent">
            @include('front.partials.footer')
        </div>
    </section>
@stop