<!DOCTYPE html>

<html>

<head>
    <meta charset="utf-8">
    <title>Brand identity</title>
    {{--<link rel="stylesheet" type="text/css" href="brand-identity.css">--}}
    {{HTML::style('front/css/brand-identity.css', array('async'))}}

    <!-- Font awesome -->
    <script src="https://use.fontawesome.com/3fbfe264e4.js"></script>
    {{HTML::script('front/js/scripts.js')}}
</head>

<body>
<section id="header">
    <!-- Brand identity -->
    <div class="full-width">
        <div class="inner-box full-width">
            <div class="inner-text-box welcome-msg full-width">
                <div class="txt">Welcome to the world of Life&Luxury,<br>
                    your finest collection of services.</div>
            </div>

            <div class="inner-text-box central-text first-page full-width">
                <div class="header-logo">
                    <img class="full-width" src="/front/img/brandidentity/life-luxury-logo.png">
                </div><br>
                <div class="brand-identity">
                    <img class="full-width" src="/front/img/brandidentity/brand-identity.png">
                </div><br>
                <div class="header-line">
                    <div class="vertical-line"></div>
                </div><br>
                {{--<div class="header-button">--}}

                <div class="section-footer3">
                    <a href="#" class="section-btn-next3 section-btn-next-down3">
                        <img src="/front/img/brandidentity/header-button-BI.png">
                    </a>
                </div>

                {{--<div class="section-footer">--}}
                    {{--<a href="#" class="section-btn-next section-btn-next-down">--}}
                        {{--<img src="/front/img/brandidentity/header-button-BI.png">--}}
                    {{--</a>--}}
                {{--</div>--}}
                    {{--<a href="#header-content">--}}
                        {{--<img src="/front/img/brandidentity/header-button-BI.png">--}}
                    {{--</a>--}}
                {{--</div>--}}
            </div>

            <img class="header-img full-width" src="/front/img/brandidentity/header-circles-BI.png">
        </div>
        <img class="bg-image full-width" src="/front/img/brandidentity/header-BI.png">
    </div>
    <div class="full-width move-up-250 paddingTop20 absolute" id="header-content">
        <div class="contents-box full-width">
            <div class="row">
                <div class='col-xs-3 col-md-5'>
                    <div class="row">
                        <div class='col-xs-9 col-xs-offset-3 col-md-5 col-md-offset-6  contents'>
                            <img class="contents-img" src="/front/img/brandidentity/contents.png">
                        </div>
                    </div>
                </div>
                <div class='col-xs-9 col-md-7 contents-link'>
                    <div class="row">
                        <div class='col-xs-5'>
                            <div class="content-title">
                                <div class="number">01</div>
                                <div class="title">Verbal Identity</div>
                            </div>
                            <div class="clearfix"></div>
                            <div class="content-navigation">
                                <a href="#intro"> Introduction </a>
                                <a href="#our-story"> Our Story </a>
                                <a href="#our-values"> Our Values </a>
                            </div>
                        </div>
                        <div class='col-xs-5'>
                            <div class="content-title">
                                <div class="number">03</div>
                                <div class="title">Application</div>
                            </div>
                            <div class="clearfix"></div>
                            <div class="content-navigation">
                                <a href="#collateral"> Collateral </a>
                                <a href="#vehicles"> Vehicles </a>
                                <a href="#digital"> Digital </a>
                                <a href="#publication"> Publication </a>
                                <a href="#environmental"> Environmental </a>
                            </div>
                        </div>
                    </div>
                    <div class="clearfix"></div>
                    <div class="row margintop30">
                        <div class='col-xs-5'>
                            <div class="content-title">
                                <div class="number">02</div>
                                <div class="title">Visual Identity</div>
                            </div>
                            <div class="clearfix"></div>
                            <div class="content-navigation"  style="z-index: 500">
                                <a href="#logo"> Logo </a>
                                <a href="#color-pallete"> Color Palette </a>
                                <a href="#typography"> Typography </a>
                                <a href="#typeface"> Typeface </a>
                            </div>
                        </div>
                        <div class='col-xs-5'>
                            <div class="content-title">
                                <div class="number">04</div>
                                <div class="title">Connect</div>
                            </div>
                            <div class="clearfix"></div>
                            <div class="content-navigation">
                                <a href="#get-in-touch"> Get in touch </a>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="clearfix"></div>
            </div>
            <div class="clearfix"></div>
        </div>
    </div>

</section>

<section id="section_1" class="relative">
    <!-- Verbal identity -->
    <div class="full-width relative">
        <div class="inner-box full-width">
            <div class="inner-text-box central-text full-width section-page">
                <div class="verbal-identity-title">
                    <img class="img"  src="/front/img/brandidentity/verbal-BI.png">
                </div>
                <div class="identity">
                    <img class="img" src="/front/img/brandidentity/identity-BI.png">
                </div>
                <div class="number">01</div>
            </div>

            <img class="header-img full-width" src="/front/img/brandidentity/circles.png">
        </div>
        <img class="bg-image full-width"  src="/front/img/brandidentity/purple-bg.png">
    </div>
    <div class="verbal-identity-text" id="intro">
        At Life&Luxury, our identity is our soul. Distinctive and reflective, we treat each<br> brand that
        we create the way we treat ourselves: with passion and vision.<br>
        For us, a brand is not as simple as a catchy title nor a fancy logo. It is, however,<br>
        a hand-carved cover that embodies a beautiful story worth telling.
    </div>

    <!-- Our story -->
    <div class="full-width relative">
        <div class="inner-box full-width">
            <div class="inner-text-box full-width relative" id="our-story">
                <div class="title">
                    <img src="/front/img/brandidentity/our-story-title.png">
                </div>
                <!-- sm-3 -->
                <div class="text-box relative col-xs-offset-5 col-md-offset-7">
                    <div class="hidden-xs hidden-sm hidden-box"></div>
                    <div class="text">
                        Enriched by diverse backgrounds and experience,<br>
                        we are a team of passionate and dedicated members<br>
                        who strive to achieve excellence in everything we do. Having<br>
                        met in pristine Switzerland, the country is also the birth-<br>
                        place of our mutual idea: to bring joys to our clients through<br>
                        an exclusive collection of luxury services.
                        <br><br>
                        This brilliant collection is inspired by our years of<br>
                        international experience and unified by the Swiss high<br>
                        standards of refined quality. Every business request is an<br>
                        exciting adventure. For us, the sky is not the limit. It is our<br>
                        creativity of high calibre that shall lead us to infinity and<br>
                        beyond.
                    </div>
                </div>
            </div>
        </div>
        <img class="bg-image full-width"  src="/front/img/brandidentity/our-story.png">
    </div>

    <!-- Our values -->
    <div class="full-width relative" style="margin-top:-12%;">
        <div class="inner-box full-width">
            <div class="inner-text-box full-width relative" id="our-values">
                <div class="title">
                    <img src="/front/img/brandidentity/our-values-title.png">
                </div>
                <!-- sm-3 -->
                <div class="text-box relative our-story">
                    <div class="hidden-xs hidden-sm hidden-box"></div>
                    <div class="text">
                        As straightforward and honest as our own name,<br>
                        Life&Luxury places simplicity and elegance at the<br>
                        core of our business. We aspire to deliver services that are<br>
                        unsurpassed. Our creative dimensions are vast, and the<br>
                        renowned Swiss efficiency moulds our ambitions.<br><br>

                        Satisfying our customers are not a goal. It is our philosophy.<br>
                        Establishing and maintaining meaningful relationships is<br>
                        not a business strategy. It is our way of life. At Life&Luxury,<br>
                        leading a purposeful business has been, is, and will always<br>
                        be the heart of our priorities.<br>
                    </div>
                </div>
            </div>
        </div>
        <img class="bg-image full-width"  src="/front/img/brandidentity/our-values.png">
    </div>
</section>

<section id="section_2" class="relative">
    <!-- Visual identity -->
    <div class="full-width relative" style="margin-top:-8%;">
        <div class="inner-box full-width">
            <div class="inner-text-box central-text full-width section-page" style="top:30%;">
                <div class="visual-identity-title">
                    <img class="img" src="/front/img/brandidentity/visual.png">
                </div>
                <div class="identity">
                    <img class="img" src="/front/img/brandidentity/identity-BI.png">
                </div>
                <div class="number">02</div>
            </div>

            <img class="header-img full-width" src="/front/img/brandidentity/circles.png">
        </div>
        <img class="bg-image full-width"  src="/front/img/brandidentity/purple-bg.png">
    </div>

    <div class="service-info-image visual-identity relative" style="margin-top:-15%;">
        <div class="inner-box full-width relative">
            <div class="inner-text-box full-width full-height relative" id="logo">
                <!-- Logo -->
                <div class="service-info-logo logo-title">
                    <img class="img" src="/front/img/brandidentity/logo-section2.png">
                </div>
                <div class="main-logo">
                    <img class="img" src="/front/img/brandidentity/life-luxury-logo.png">
                </div>
                <div class="service-info-text about-logo">
                    The first and foremost ambassador of our brand is our logo. It speaks to others<br>
                    about our values. It allows our customers to evaluate us even before the first<br>
                    meeting. Our logo, therefore, is the non-verbal version of ourselves: Elegant, simple,<br>
                    efficient, and transparent.
                </div>
                <!-- Color pallete -->
                <div class="service-info-logo color-pallete" id="color-pallete">
                    <img class="img" src="/front/img/brandidentity/color-pallete.png">
                </div>
                <div class="pallete-container">
                    <div class="col-sm-4 color">
                        <div class="color-image">
                            <a>
                            <img class="img" src="/front/img/brandidentity/first-color.png">
                            </a>
                        </div>
                        <div class="color-name">
                            Anthracite Blue
                        </div>
                        <div class="color-details">
                            HEX : #2b2d39 <br>
                            R: 43 G: 45 B: 57 <br>
                            C: 78 M:71 Y:53 K:56 <br>
                        </div>
                    </div>
                    <div class="col-sm-4 color">
                        <div class="color-image">
                            <a>
                            <img class="img" src="/front/img/brandidentity/second-color.png">
                          </a>
                        </div>
                        <div class="color-name">
                            Champagne Gold
                        </div>
                        <div class="color-details">
                            HEX : #cdcfc1<br>
                            R: 205 G: 207 B: 193<br>
                            C: 20 M:13 Y:23 K:0<br>
                        </div>
                    </div>
                    <div class="col-sm-4 color">
                        <div class="color-image">
                            <a>
                            <img class="img" src="/front/img/brandidentity/third-color.png">
                            </a>
                        </div>
                        <div class="color-name">
                            Diamond Silver
                        </div>
                        <div class="color-details">
                            HEX : #eeede8<br>
                            R: 238 G: 237 B: 232<br>
                            C: 6 M:4 Y:7 K:0<br>
                        </div>
                    </div>
                    <div class="clearfix"></div>
                </div>
                <!-- Typography -->
                <div class="service-info-logo" id="typography">
                    <img class="img" src="/front/img/brandidentity/Typography.png">
                </div>
                <div class="typography">
                    <div class="typography-title">
                        Scriptina
                    </div>
                    <div class="typography-content">
                        ABCDEFGHIJKLMNOPQRSTUVWXYZ<br>
                        abcdefghjklmnopqrstuvwxyz<br>
                        1234567890
                    </div>
                </div>
                <!-- Typeface -->
                <div class="service-info-logo" id="typeface">
                    <img class="img" src="/front/img/brandidentity/typeface.png">
                </div>
                <div class="typeface">
                    <div class="col-sm-6 typeface-primary">
                        <div class="typeface-title">
                            Primary typeface
                        </div>
                        <div class="typeface-content">
                            ABCDEFGHIJKLMNOPQRS<br>
                            TYUVWXYZabcdefghijklm<br>
                            nopqrstyuwxyz<br>
                            1234567890
                        </div>
                    </div>
                    <div class="col-sm-6 typeface-secondary">
                        <div class="typeface-title">
                            Secondary typeface
                        </div>
                        <div class="typeface-content">
                            ABCDEFGHIJKLMNOPQRS<br>
                            TYUVWXYZabcdefghijklm<br>
                            nopqrstyuwxyz<br>
                            1234567890
                        </div>
                    </div>
                    <div class="clearfix"></div>
                </div>
            </div>
        </div>
    </div>

    <div class="clearfix"></div>
</section>

<section id="section_3" class="relative">
    <!-- Application -->
    <div class="full-width relative">
        <div class="inner-box full-width">
            <div class="inner-text-box central-text full-width section-page" style="top:30%;">
                <div class="application-title">
                    <img class="img" src="/front/img/brandidentity/application-BI.png">
                </div>
                <div class="number">03</div>
            </div>

            <img class="header-img full-width" src="/front/img/brandidentity/circles.png">
        </div>
        <img class="bg-image full-width"  src="/front/img/brandidentity/purple-bg.png">
    </div>

    <!-- Collateral stationery -->
    <div class="collateral-stationery service-info paddingTop20" id="collateral">
        <div class="service-info-logo title">
            <img src="/front/img/brandidentity/collateral.png">
        </div>
        <div class="service-info-title">
            Stationery
        </div>
        <div class="service-info-text">
            Our envelope and note pad are not merely an envelope and a note pad. We instil the<br>
            spirit of our business into each item that carries the mark of our brand. For us, a pen<br>
            means as much as an ambassador, which successfully communicates the charm of<br>
            our identity.
        </div>
        <div class="service-info-image">
            <img class="full-width" src="/front/img/brandidentity/collateral-stationery-image.png">
        </div>
    </div>

    <!-- Collateral vehicles -->
    <div class="collateral-vehicles service-info paddingTop20" id="vehicles">
        <div class="service-info-logo title">
            <img src="/front/img/brandidentity/collateral.png">
        </div>
        <div class="service-info-title">
            Vehicles
        </div>
        <div class="service-info-text">
            Throughout our products, our design language runs as consistently as the<br>
            mechanism of the latest all-star vehicle model. Speaking of which, the image often<br>
            associated with luxury cars are also reflected in our design concepts:<br>
            luxury, seamless and modern.
        </div>
        <div class="service-info-image">
            <img class="full-width" src="/front/img/brandidentity/collateral-vehicles-image.png">
        </div>
    </div>

    <!-- Digital video production -->
    <div class="digital-video-production service-info paddingTop20" id="digital">
        <div class="service-info-logo title">
            <img src="/front/img/brandidentity/digital.png">
        </div>
        <div class="service-info-title">
            Video Production
        </div>
        <div class="service-info-text">
            Whether it is a short advertisement or a medium-length promotional video, we<br>
            complete every single project with the highest work ethics. Whether it is our first<br>
            video or our thirtieth, we believe that each of them is a masterpiece<br>
            worth retelling.
        </div>
        <div class="service-info-image">
            <img class="full-width" src="/front/img/brandidentity/digital-video-production-image.png">
        </div>
    </div>

    <!-- Digital website -->
    <div class="digital-website service-info">
        <div class="service-info-logo title">
            <img src="/front/img/brandidentity/digital.png">
        </div>
        <div class="service-info-title">
            Website
        </div>
        <div class="service-info-text">
            In this era, every footsteps on the World Wide Web are immediately and permanent-<br>
            ly recorded in history. That is exactly how we see our website: A golden page in our<br> business saga that retains our milestones for generations to witness. It is also our<br>
            portal through which the world can reach us, and we the stars.
        </div>
        <div class="service-info-image">
            <img class="full-width" src="/front/img/brandidentity/digital-website-image.png">
        </div>
    </div>

    <!-- Publications online magazine -->
    <div class="publications-online-magazine service-info paddingTop20" id="publication">
        <div class="service-info-logo title">
            <img src="/front/img/brandidentity/publications.png">
        </div>
        <div class="service-info-title">
            Online Magazine
        </div>
        <div class="service-info-text">
            Packed with innovative concepts and fabulous trends, Life&Luxury Online Magazine is your to-go<br>
            guide of business ideas and luxury lifestyles. Carefully wrapped with enchanting narratives and<br>
            decorated with appealing visuals, Life&Luxury Online Magazine is a treat to read. Simple yet rich,<br>
            the quarterly-published Online Magazine covers events, vogues, and tips that would ensure<br>
            three months full of styles; that is, until the next issue is out.
        </div>
        <div class="service-info-image">
            <img class="full-width" src="/front/img/brandidentity/publications-online-magazine-image.png">
        </div>
    </div>

    <!-- Publications online magazine -->
    <div class="environmental-advertising service-info paddingTop20" id="environmental">
        <div class="service-info-logo title">
            <img src="/front/img/brandidentity/environmental.png">
        </div>
        <div class="service-info-title">
            Advertising
        </div>
        <div class="service-info-text">
            We have carefully integrated our design language flow throughout all our<br>
            advertisements. This creates a sense of visual connectivity and resemblance<br>
            with our brand no matter the location.
        </div>
        <!-- TV Advertising -->
        <div class="service-info-image tv-advertising relative">
            <div class="inner-box full-width">
                <div class="inner-text-box full-width relative">
                    <div class="text-box">
                        <div class="service-info-logo title to-left">
                            <img src="/front/img/brandidentity/tv-advertising.png">
                        </div>
                        <div class="service-info-text">
                            In a world flooded with advertisements in all shapes<br>
                            and sizes, creative excellence is our remedy to the<br>
                            already saturated quality of TV advertisements.<br>
                            Concise and powerful, our scripts aim straight for the<br>
                            human emotions to ensure the most long-lasting<br>
                            effects, and that our stories will shine above an ocean<br>
                            of information.
                        </div>
                    </div>
                </div>
            </div>
            <img class="bg-image full-width"  src="/front/img/brandidentity/tv-advertising-image.png">
        </div>
        <!-- Newsletter -->
        <div class="service-info-image newsletter relative">
            <div class="inner-box full-width">
                <div class="inner-text-box full-width relative">
                    <div class="text-box">
                        <div class="service-info-logo title to-left">
                            <img src="/front/img/brandidentity/newsletter.png">
                        </div>
                        <div class="service-info-text">
                            Like any other branding tools that we offer, our<br>
                            newsletters go beyond the status of a soulless<br>
                            instrument. Carefully designed with our identity in<br>
                            mind, our newsletters are a pleasure to receive, and a<br>
                            reason to look forward to every day, week, or month,<br>
                            depending on how eager our customers wish to engage<br>
                            with us.
                        </div>
                    </div>
                </div>
            </div>
            <img class="bg-image full-width"  src="/front/img/brandidentity/newsletter-image.png">
        </div>
        <!-- Email Signature -->
        <div class="service-info-image email-signature relative">
            <div class="inner-box full-width">
                <div class="inner-text-box full-width relative">
                    <div class="text-box">
                        <div class="service-info-logo title to-left">
                            <img src="/front/img/brandidentity/email-signature.png">
                        </div>
                        <div class="service-info-text">
                            A signature is unique. It is our public face. It represents<br>
                            us. An email signature should not be any different. Our<br>
                            keen eyes for details guarantees perfection, and that<br>
                            includes polishing even the few lines at the end of an<br>
                            email. After all, everything that comes from us is<br>
                            our symbol.
                        </div>
                    </div>
                </div>
            </div>
            <img class="bg-image full-width"  src="/front/img/brandidentity/email-signature-image.png">
        </div>
    </div>
</section>

<section id="section_4" class="relative">
    <!-- Connect -->
    <div class="full-width relative" style="margin-top: -8%;">
        <div class="inner-box full-width">
            <div class="inner-text-box central-text full-width section-page connect" style="top:30%;">
                <div class="connect-title">
                    <img class="img" src="/front/img/brandidentity/connect-BI.png">
                </div>
                <div class="number">04</div>
            </div>

            <img class="header-img full-width" src="/front/img/brandidentity/circles.png">
        </div>
        <img class="bg-image full-width"  src="/front/img/brandidentity/purple-bg.png">
    </div>
    <!-- Get in touch -->

    <div class="get-in-touch full-width relative" style="margin-top: -8%;">
        <div class="inner-box full-width">
            <!-- Header -->
            <div class="service-info relative">
                <div class="service-info-logo title" id="get-in-touch">
                    <img src="/front/img/brandidentity/get-in-touch.png">
                </div>
                <div class="service-info-title">
                    Contact us
                </div>
            </div>
            <!-- Central text with ellipse BG -->
            <div class="central-content relative">
                <div class="inner-text-box contact-links full-width">
                    <div class="message-icons">
                        <div class="to-center">
                            <div class="chat-with-us">
                                <div class="img">
                                    <a href="#">
                                    <img src="/front/img/brandidentity/chat.png">
                                    </a>
                                </div>
                                <div class="chat-text">Chat with us</div>
                            </div>
                            <div class="contact-us">
                                <div class="img">
                                    <a href="#">
                                    <img src="/front/img/brandidentity/contact.png">
                                    </a>
                                </div>
                                <div class="contact-text">Contact us</div>
                            </div>
                        </div>
                    </div>
                    <div class="social-media">
                        <div class="to-center">
                            <div class="col-sm-3 icon">
                                <a href="https://www.facebook.com/lifeandluxurycollection"><img src="/front/img/brandidentity/facebook.png"></a>
                            </div>
                            <div class="col-sm-3 icon">
                                <a href="https://twitter.com/LifenLuxury"><img src="/front/img/brandidentity/twitter.png"></a>
                            </div>
                            <div class="col-sm-3 icon">
                                <a href="https://www.youtube.com/channel/UCePsdWq9xg-EN9E7DGaAuHg"><img src="/front/img/brandidentity/youtube.png"></a>
                            </div>
                            <div class="col-sm-3 icon">
                                <a href="https://www.instagram.com/your_finest_collection"><img src="/front/img/brandidentity/instagram.png"></a>
                            </div>
                        </div>
                    </div>
                    <div class="lifeluxury-link">
                        <a href="http://www.lifeandluxury.com/">www.lifeandluxury.com</a>
                    </div>
                </div>
                <!-- Background -->
                <div class="ellipse-contact-img full-width">
                    <img src="/front/img/brandidentity/ellipse-contact.png">
                </div>
            </div>
            <!-- Footer -->
            <div class="service-info-text footer">
                G.S. LIFE AND LUXURY CYPRUS LTD. All rights reserved. LIFE AND LUXURY, the<br>
                LIFE AND LUXURY logo, and all other LIFE AND LUXURY marks contained here in are trademarks<br>of G.S. LIFE AND LUXURY CYPRUS LTD
            </div>
        </div>
        <img class="bg-image full-width"  src="/front/img/brandidentity/get-in-touch-image.png">
    </div>
</section>

</body>


<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
</html>

<script>
    $('.section-footer3 , .section-btn-next-down3').on('click', function () {
        var $section = $('#section_1').length > 0 ? $('#section_1') : $(this).closest('section').next();
        $('html,body').animate({scrollTop: $section.offset().top - 0}, 'slow');
        return false;
    });
</script>