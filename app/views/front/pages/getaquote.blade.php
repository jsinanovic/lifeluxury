@extends('front.layouts.main')

@section('header')
@include('front.partials.header')
@stop

@section('content')
        <!-- END SITE NAVIGATION -->
@include('front.partials.sidebar-left')
        <!-- START POPUP -->
@include('front.partials.sidebar-right')
    <section class="section-top-general" id="home">
        <div class="container vertical-center-general">
            <div class="row" style="width: 100%;">
                <div class="">
                    <div class="col-md-12 welcome-main-title text-center">
                        Get a Quote
                    </div>
                </div>
            </div>
            <div class="section-footer">
                <a href="#" class="section-btn-next section-btn-next-down">
                    <span class="section-next-name">Discover More</span>
                    <span class="section-next-icon fa fa-chevron-down" aria-hidden="true"></span>
                </a>
            </div>
            {{--<div class="">--}}
                {{--<div>--}}
                    {{--<a href="javascript:void(0);" class="learn-more">--}}
                        {{--DISCOVER MORE--}}
                        {{--<div class="round-button"></div>--}}
                    {{--</a>--}}
                {{--</div>--}}
            {{--</div>--}}
        </div>
    </section>

    <section class="" id="getaquote-content" style="padding-top: 100px; margin-bottom: 100px;">
        <div class="container">
            <div class="row">
                <div class="col-md-4" style="float: left; text-align: left;">
                    <div class="row">
                        <div class="col-md-12 gaq-main-title">
                            <span>Get a Quote</span>
                        </div>
                        <div class="col-md-12 gaq-main-text">
                            <span>Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus.

Donec quam felis, ultricies nec, pellentesque eu, pretium quis, sem. Nulla consequat massa quis enim. Donec pede justo, fringilla vel, aliquet nec, vulputate eget, arcu. In enim justo, rhoncus ut, imperdiet a, venenatis vitae, justo.</span>
                        </div>
                    </div>
                    <div class="row" style="margin-top: 20px;">
                    </div>
                    <div class="row" style="margin-top: 15px;">
                        <div class="col-md-12">
                                     <span class="gaq-social-icons">
                                    <a href="https://www.facebook.com/lifeandluxurycollection"><i class="fa fa-facebook" aria-hidden="true"></i></a> &nbsp
                                    <a href="https://twitter.com/LifenLuxury" style="margin-left: 15px;"><i class="fa fa-twitter" aria-hidden="true"></i></a> &nbsp
                                    <a href="https://www.youtube.com/channel/UCePsdWq9xg-EN9E7DGaAuHg" style="margin-left: 15px;"><i class="fa fa-youtube" aria-hidden="true"></i></a> &nbsp
                                    <a href="https://www.instagram.com/your_finest_collection" style="margin-left: 15px;"><i class="fa fa-instagram" aria-hidden="true"></i></a>
                                </span>
                        </div>
                    </div>

                </div>
                <div class="col-md-8">

                    <div class="row">

                        {{Form::open(array('url' => 'contacts', 'method' => 'post', 'role' => 'form', 'class' => 'quote-form', 'data-type' => 'quote'))}}
                        {{--<div class="modal-content">--}}
                        {{--<div class="modal-body">--}}
                        {{--<div class="modal-section">--}}

                        <div class="gaq-main-title">
                            <span>Form</span>
                        </div>

                        <div class="row gaq-form-first-section">
                            <div class="form-group col-sm-6">
                                <label for="">First name</label>
                                <input type="text" name="user_name" class="form-control" />
                            </div>

                            <div class="form-group col-sm-6">
                                <label for="">Surname</label>
                                <input type="text" name="user_surname" class="form-control" />
                            </div>
                        </div>

                        <div class="row gaq-form-first-section">
                            <div class="form-group col-sm-6">
                                <label for="">Email</label>
                                <input type="email" name="user_email" class="form-control" />
                            </div>

                            <div class="form-group col-sm-6">
                                <label for="">Website</label>
                                <input type="text" name="user_website" class="form-control" />
                            </div>
                        </div>
                        {{--</div>--}}

                        {{--<div class="modal-section">--}}
                        <div class="gaq-select-service-title">
                        <h4>PLEASE SELECT DESIRED SERVICE</h4>
                        </div>
                        <div class="row">
                            <div class="form-group col-sm-6 col-md-4">
                                <div class="checkbox">
                                    <label>
                                        <input type="checkbox" value="Advertising" name="service">
                                        ADVERTISING
                                    </label>
                                </div>
                                <div class="checkbox">
                                    <label>
                                        <input type="checkbox" value="Brand Identity" name="service">
                                        BRAND IDENTITY
                                    </label>
                                </div>
                                <div class="checkbox">
                                    <label>
                                        <input type="checkbox" value="Graphics Design" name="service">
                                        GRAPHICS DESIGN
                                    </label>
                                </div>
                            </div>
                            <div class="form-group col-sm-6 col-md-8">
                                <div class="checkbox">
                                    <label>
                                        <input type="checkbox" value="Web Design & Development" name="service">
                                        WEB DESIGN & DEVELOPMENT
                                    </label>
                                </div>
                                <div class="checkbox">
                                    <label>
                                        <input type="checkbox" value="Video Production" name="service">
                                        VIDEO PRODUCTION
                                    </label>
                                </div>
                                <div class="checkbox">
                                    <label>
                                        <input type="checkbox" value="MARKETING" name="service">
                                        MARKETING
                                    </label>
                                </div>
                            </div>
                        </div>
                        {{--</div>--}}

                        {{--<div class="modal-section modal-section-service">--}}
                        <div class="row gaq-form-second-section">
                            <div class="form-group col-sm-12">
                                <label for="">PLEASE PROVIDE US WITH A BRIEF PROJECT DESCRIPTION</label>
                                <textarea name="description" class="form-control" rows="3"></textarea>
                            </div>
                        </div>

                        <div class="row gaq-form-second-section">
                            <div class="form-group col-sm-6">
                                <label for="">PLEASE PROVIDE US WITH AN ESTIMATED BUDGET</label>
                                <input type="text" name="budget" class="form-control" />
                            </div>
                        </div>
                        {{--</div>--}}

                        {{--<div class="modal-section">--}}
                        <div class="row">
                            <div class="col-sm-12">
                                <div class="alert alert-success" role="alert" style="display: none;">
                                    Success! You message has been sent
                                </div>
                                <div class="alert alert-danger" role="alert" style="display: none;">
                                    <div class="alert-text"></div>
                                </div>
                            </div>
                        </div>
                        {{--</div>--}}

                        {{--</div>--}}

                        <div>
                            <button type="submit" class="btn gaq-form-btn">Get a quote<i class="fa fa-angle-right" style="margin-left: 20px;" aria-hidden="true"></i></button>
                        </div>
                        {{--</div>--}}
                        {{Form::close()}}

                    </div>

                </div>
            </div>
        </div>

    </section>


    @include('front.partials.advertisment-area')

    <section class="" id="newsletter-area" style="position:relative;">

        @include('front.partials.newsletter')

    </section>

    <section id="footer" style="position:relative;">

        @include('front.partials.footer')

    </section>

@stop

@section('js')

    <script>
        $(document).ready(function (ev) {
            $('#custom_carousel').on('slide.bs.carousel', function (evt) {
                $('#custom_carousel .controls li.active').removeClass('active');
                $('#custom_carousel .controls li:eq(' + $(evt.relatedTarget).index() + ')').addClass('active');
            });

            $('#join-newsletter-btn').click(function() {
                displayNewsletterPopup();
            });

            $('#newsletter-close-btn').click(function() {
                hideNewsletterPopup();
            });

            function hideNewsletterPopup() {
                $('#newsletter-popup-container').hide();
                $('#newsletter-content').show();
            }
            function displayNewsletterPopup() {
                $('#newsletter-content').hide();
                $('#newsletter-popup-container').show();
            }
        });
    </script>

@stop