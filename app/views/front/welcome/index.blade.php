@extends('front.layouts.main')


@section('header')
    @include('front.partials.header')
@stop

@section('content')
        <!-- END SITE NAVIGATION -->
@include('front.partials.sidebar-left')
        <!-- START POPUP -->
        @include('front.partials.sidebar-right')
        <!-- END POPUP -->

    <section class="section-full section-top" id="home">

        {{--<div class="home-container" style="text-align: center;">--}}
        {{--<div class="home-under-info" style=" text-align: center; margin-bottom: 10px; font-size: 62pt;">Welcome to our Universe</div>--}}
        {{--<div class="home-under-info-2" style=" ">Welcome to our Universe</div>--}}

        {{--<a href="#" class="home-welcome-page">--}}
        {{--WELCOME </a>--}}
        {{--</div>--}}

        {{--<a href="javascript:void(0);" class="learn-more">--}}
        {{--DISCOVER MORE--}}
        {{--<div class="round-button"></div></a>--}}

        <div class="container vertical-center">
            <div class="row">
                <div class="">
                    <div class="col-md-12 welcome-main-title text-center">
                        Welcome to our Universe
                    </div>
                    <div class="col-md-6 col-md-offset-3 welcome-main-desc text-center">
                        Welcome to the world of life and luxury, the leading luxury group offering the finest collection
                        of services covering an essential facet in the luxury market.
                    </div>
                </div>

                <div class="section-footer">
                    <a href="#" class="section-btn-next section-btn-next-down">
                        <span class="section-next-name">Discover More</span>
                        <span class="section-next-icon fa fa-chevron-down" aria-hidden="true"></span>
                    </a>
                </div>

                {{--<a href="javascript:void(0);" class="learn-more">--}}
                    {{--DISCOVER MORE--}}
                    {{--<div class="round-button"></div>--}}
                {{--</a>--}}
            </div>
        </div>

    </section>

    <section id="welcom2" style="height: 80vh; padding-top: 50px; margin-bottom: 40px;">
        <div class="container">
            <div id="custom_carousel" class="carousel slide" data-ride="carousel" data-interval="2500">
                <!-- Wrapper for slides -->
                <div class="carousel-inner">
                    <div class="item active">
                        <div class="container-fluid">
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="welcome-img-tab-wrp">
                                        <img id="welcome_img-1-3" src="front/img/welcome/welcome_slider_1.png" class="img-responsive">
                                    </div>
                                </div>
                                <div class="col-md-6" style="display: flex; align-items: center; height: 400px;">
                                    <div class="row">
                                        <div class="welcome-slider-title col-md-12">THE COLLECTION</div>
                                        <div class="welcome-slider-text col-md-12">Our award winning services include Advertising, Creative Design Services, Events Management, Publishing Services, Music as well as Video Production Services.</div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="item">
                        <div class="container-fluid">
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="welcome-img-tab-wrp">
                                        <img id="welcome_img-1-3" src="front/img/welcome/welcome_slider_2.png" class="img-responsive">
                                    </div>
                                </div>
                                <div class="col-md-6" style="display: flex; align-items: center; height: 400px;">
                                    <div class="row">
                                        <div class="welcome-slider-title col-md-12">CREATIVE VISION</div>
                                        <div class="welcome-slider-text col-md-12">We have created a unique collection of services which are just as unique as our customers. All services are carefully laid out to fulfil even the most demanding desires.</div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="item">
                        <div class="container-fluid">
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="welcome-img-tab-wrp">
                                        <img id="welcome_img-1-3" src="front/img/welcome/welcome_slider_3.png" class="img-responsive">
                                    </div>
                                </div>
                                <div class="col-md-6" style="display: flex; align-items: center; height: 400px;">
                                    <div class="row">
                                        <div class="welcome-slider-title col-md-12">OUR PROMISE</div>
                                        <div class="welcome-slider-text col-md-12">Our brand is a promise - our personnel, a finest quality service and a passion for our work, will make your experience truly exclusive!</div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- End Item -->
                </div>
                <!-- End Carousel Inner -->
                <div class="controls">
                    <ul class="nav nav-welcome-tabs">
                        <li data-target="#custom_carousel" data-slide-to="0" class="active"><a href="#"><span></span></a></li>
                        <li data-target="#custom_carousel" data-slide-to="1"><a href="#"><span></span></a></li>
                        <li data-target="#custom_carousel" data-slide-to="2"><a href="#"><span></span></a></li>
                    </ul>
                </div>
            </div>
            <!-- End Carousel -->
        </div>

    </section>



    <section class="" id="welcome-3" style="padding-top: 150px;">
        <div class="container-fluid nopadding">
            <div class="row-fluid">
                <div class="col-md-6 nopadding">
                    <div class="welcome-3-mainimg-container" style="height: 500px;">
                        <img  src="/img/commingsoon/maingimg.png">
                    </div>
                    <div class="col-md-12 welcome-3-left-content">
                        <row>
                            <div class="col-md-12 col-md-offset-3 welcome-3-left-title nopadding-left">
                                YOUR FINEST<br>COLLECTION
                            </div>
                            <div class="col-md-12 col-md-offset-3 welcome-3-left-btn">
                                <span><a href="/press-kit">Press Kit<i class="fa fa-angle-right" style="margin-left: 40px;" aria-hidden="true"></i></a></span>
                            </div>
                        </row>
                    </div>
                </div>
                <div class="col-md-6 nopadding-left" style="display: flex; align-items: center; height: 500px; background-color: #f6f6f3; border-top: 1px solid #d3d3d3; border-bottom: 1px solid #d3d3d3;">
                    <div class="col-md-10 col-md-offset-1 nopadding-left">
                        <div class="welcome-3-right-content">
                            <div class="col-md-12 nopadding">
                                <div class="col-md-1 nopadding">
                                    <img class="welcome-3-right-img" src="/front/img/welcome/welcome-3-img-1.png">
                                </div>
                                <div class="col-md-10 welcome-right-title">BESPOKE SERVICES</div>

                                <div class="col-md-11 col-md-offset-1 welcome-right-text">
                                    Our Unique Guest Relation Service is available 24 hours a day, 365 days a year, by sharing specialist knowledge and expertise.
                                </div>
                            </div>
                        </div>
                        <div class="welcome-3-right-content">
                            <div class="col-md-12 nopadding">
                                <div class="col-md-1 nopadding">
                                    <img class="welcome-3-right-img" src="/front/img/welcome/welcome-3-img-2.png">

                                </div>
                                <div class="col-md-10 welcome-right-title">ATTENTION TO DETAIL</div>

                                <div class="col-md-11 col-md-offset-1 welcome-right-text">
                                    Our work is never complete until it reaches perfection. Swiss Quality combined with our Award Winning Service makes the whole process an Unforgettable Experience for each of our customers whether first time or returning.
                                </div>
                            </div>
                        </div>
                        <div class="welcome-3-right-content">
                            <div class="col-md-12 nopadding">
                                <div class="col-md-1 nopadding">
                                    <img class="welcome-3-right-img" src="/front/img/welcome/welcome-3-img-3.png">

                                </div>
                                <div class="col-md-10 welcome-right-title">WHY LIFE & LUXURY?</div>

                                <div class="col-md-11 col-md-offset-1 welcome-right-text">
                                    Always Innovating, Always Challenging and Always Pushing our Boundaries. All customers are just as unique as their dreams and most important of all, our knowledge is available to anyone - no matter how small the project.
                                </div>
                            </div>
                        </div>

                    </div>
                    {{--<div class="row">--}}
                    {{--<div class="welcome-slider-title col-md-12">THE COLLECTION</div>--}}
                    {{--<div class="welcome-slider-text col-md-12">Our award winning services include Advertising, Creative Design Services, Events Management, Publishing Services, Music as well as Video Production Services.</div>--}}
                    {{--</div>--}}
                </div>
            </div>
        </div>

    </section>

    <section class="" id="welcome-4" style="margin-top: 100px; margin-bottom: 100px;">
        <div class="container">
            <div class="row">
                <div class="col-md-12 welcome-4-main-title">
                    Discover the collection
                </div>
                <div class="col-md-12 welcome-4-text">
                    We have created a unique collection of services
                </div>
                <div class="row">
                    <div class="col-md-4">
                        <div class="welcome-4-img">
                            <img class="" src="/front/img/welcome/welcome-4-rendez-vous.png">
                        </div>
                        <a href="#">
                        <div class="welcome-4-title">
                            <img class="img1" href="#" src="/front/img/welcome/sh.png" style="margin-right: 15px;">
                            <img class="img2" href="#" src="/front/img/welcome/sh.png" style="margin-right: 15px;">
                            <img class="img3" href="#" src="/front/img/welcome/sh.png"><br/>
                            <span>Rendez Vouz</span>
                        </div>
                        </a>
                    </div>
                    <div class="col-md-4">
                        <div class="welcome-4-img">
                            <img class="" src="/front/img/welcome/welcome-4-life-and-luxury-creative.png">
                        </div>
                        <a href="#">
                        <div class="welcome-4-title">
                            Life & Luxury Creative
                        </div>
                        </a>
                    </div>
                    <div class="col-md-4">
                        <div class="welcome-4-img">
                            <img class="" src="/front/img/welcome/welcome-4-life-and-luxury-magazine.png">
                        </div>
                        <a href="#">
                        <div class="welcome-4-title">
                            Life & Luxury Magazine
                        </div>
                        </a>
                    </div>
                </div>
            </div>
        </div>

    </section>

    <section class="" id="welcome-5-newsletter" style="margin-top: 50px; position:relative;">

        @include('front.partials.newsletter')

    </section>

    <section id="welcome-5-footer" style=" position:relative;">

        @include('front.partials.footer')

    </section>

@stop

@section('js')

    <script>
        $(document).ready(function (ev) {
            $('#custom_carousel').on('slide.bs.carousel', function (evt) {
                $('#custom_carousel .controls li.active').removeClass('active');
                $('#custom_carousel .controls li:eq(' + $(evt.relatedTarget).index() + ')').addClass('active');
            });

            $('#join-newsletter-btn').click(function() {
                displayNewsletterPopup();
            });

            $('#newsletter-close-btn').click(function() {
                hideNewsletterPopup();
            });

            function hideNewsletterPopup() {
                $('#newsletter-popup-container').hide();
                $('#newsletter-content').show();
            }
            function displayNewsletterPopup() {
                $('#newsletter-content').hide();
                $('#newsletter-popup-container').show();
            }
        });
    </script>

@stop