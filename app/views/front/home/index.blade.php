@extends('front.layouts.home')


@section('header')
@include('front.partials.header')
@stop

@section('content')

        <!-- END SITE NAVIGATION -->
@include('front.partials.sidebar-left')
        <!-- START POPUP -->
@include('front.partials.sidebar-right')
        <!-- END POPUP -->
<style>
    .modal-dialog {
        height: 80%;
        width: 81%;
        display: flex;
        align-items: center;
        transform: scale(3);
    }

    .modal-content {
        margin: 0 auto;
    }
</style>
    <!-- START MAIN CONTENT -->
    <main class="main">

        <!-- START BANNER SECTION -->
        <section class="section"  id="welcome" style="background-image: url('front/img/home/banner_background.jpg');">
            <div class="container-wide">
                <div class="section-body section-banner-body">
                    <h1 class="section-header">Everyday Transformed</h1>
                    <div class="section-desc">Welcome to our Universe</div>
                    <div class="section-name" >
                        <a style="color: white;" class="section-link-right link-custom-right" href="/welcome" >Welcome</a>
                    </div>
                    {{--<a href="/welcome" class="section-link link-custom">Welcome</a>--}}
                </div>

                <div class="section-footer test2">
                    <a href="#" class="section-btn-next section-btn-next-down">
                        <span class="section-next-name">Discover The Collection</span>
                        <span class="section-next-icon fa fa-chevron-down" aria-hidden="true"></span>
                    </a>
                </div>
            </div>
        </section>
        <!-- END BANNER SECTION -->

        <!-- START CREATIVE PROJECTS SECTION -->

        <section class="section" id="creative" style="background-image: url('front/img/home/creative_projects_background.jpg')">
            <div class="container-wide">
                <div class="section-body">
                    <h1 class="section-header">Creative Ideas Filled With Passion & Love</h1>
                    <div class="section-desc">Art of Seeing the Invisible</div>
                    <div class="section-name"><a style="color: white;" class="section-link-right link-custom-right"  href="/creativ">Creative Projects</a></div>
                    <a href="/creativ" class="section-link link-custom">Discover More
                        <i class="fa fa-angle-right" style="margin-left: 5px;" aria-hidden="true"></i>
                    </a>
                </div>

                <div class="section-footer test3">
                    <a href="#" class="section-btn-next section-btn-next-down">
                        <span class="section-next-name">Rendez Vous</span>
                        <span class="section-next-icon fa fa-chevron-down" aria-hidden="true"></span>
                    </a>
                </div>
            </div>
        </section>
        <!-- END CREATIVE PROJECTS SECTION -->

        <!-- START ADVERTISING SECTION -->
        <section class="section section-full" id="randvuaz">
            <div class="section-body section-body-advertising">
                <div class="advertising-content">
                    <div class="advertising-item" style="background-image: url('front/img/home/advertising_lifestyle.jpg');">

                            <div class="advesting-link-holder" style="text-decoration: none !important;left: -108px;">
                                <a class="advertising-link 1link-custom-adv">DISCOVER</a>
                            </div>
                        <a href="/randvuaz/category/lifestyle"><h2 class="advertising-header">Lifestyle</h2></a>
                    </div>

                    <div class="advertising-item" style="background-image: url('front/img/home/advertising_placeses.jpg');">
                        <div class="advesting-link-holder" style="text-decoration: none !important ;left: -72px;">
                            <a class="advertising-link 1link-custom-adv">DISCOVER</a>
                        </div>
                        <a href="/randvuaz/category/rendez-vouz"><h2 class="advertising-header">Places</h2></a>
                    </div>

                    <div class="advertising-item" style="background-image: url('front/img/home/advertising_rendez_vous.jpg');">
                        <div class="advesting-link-holder" style="left: -180px;">
                            <a class="advertising-link 1link-custom-adv">DISCOVER</a>
                        </div>
                        <a href="/randvuaz/category/rendez-vouz"><h2 class="advertising-header">Rendez Vous</h2></a>
                    </div>
                </div>
            </div>

            <div class="section-footer test4">
                <a href="#" class="section-btn-next section-btn-next-down">
                    <span class="section-next-name">Magazine</span>
                    <span class="section-next-icon fa fa-chevron-down" aria-hidden="true"></span>
                </a>
            </div>
        </section>
        <!-- START ADVERTISING SECTION -->

        <!-- START MAGAZINE SECTION -->
        <section class="section" id="magazine21" style="background-image: url('front/img/home/magazine_background.jpg');">
            <div class="container-wide">
                <div class="section-body">
                    <h1 class="section-header section-header-short">Immerse Yourself Into a Journey Not a Story</h1>
                    <div class="section-desc">Worlds Most Intriguing Subjects on Business News, Lifestyle, Rendez Vous, Persona & Voyage.</div>
                    <div class="section-name"><a style="color: white;" class="section-link-right link-custom-right"  href="#">Magazine</a></div>
                    <a href="#" class="section-link link-custom">Discover The Magazine
                        <i class="fa fa-angle-right" style="margin-left: 5px;" aria-hidden="true"></i>
                    </a>

                </div>
                <div class="section-date">Featured Issue :<a class="section-date-part" href="/magazine/issue/2016/winter" style="text-decoration: none; height:20px; color: #cdcfc1;">Summer 2016</a>
            </div>

                <div class="section-footer test5">
                    <a href="/contacts" class="section-btn-next section-btn-next-down">
                        <span class="section-next-name">Contact Us</span>
                        <span class="section-next-icon fa fa-chevron-down" aria-hidden="true"></span>
                    </a>
                </div>
            </div>
        </section>
        <!-- END MAGAZINE SECTION -->

        <!-- START CONTACTS SECTION -->
        <section class="section" id="contactus" style="background-image: url('front/img/home/contacts_background.jpg');">
            <div class="container-wide">
                <div class="section-body">
                    <h1 class="section-header">Global yet Local Approach</h1>
                    <div class="section-desc section-desc-contact">Life & Luxury International</div>
                    <div class="section-name"><a style="color: white;" class="section-link-right link-custom-right"  href="/contacts">Contact Us</a></div>
                    <a href="/contacts" class="section-link link-custom">Contact Us
                        <i class="fa fa-angle-right" style="margin-left: 5px;" aria-hidden="true"></i>
                    </a>
                </div>

                <div class="section-footer">
                    <a href="#" class="section-btn-next section-btn-next-up">
                        <span class="section-next-name">Home</span>
                        <span class="section-next-icon fa fa-chevron-up" aria-hidden="true"></span>
                    </a>
                </div>
            </div>
        </section>
        <!-- END CONTACTS SECTION -->
        <div class="go-to-home" style="display: none">Go to home</div>
        <div class="go-to-collection" style="display: none">Go to collection</div>
        <div class="go-to-rendezvous" style="display: none">Go to rendezvous</div>
        <div class="go-to-magazine" style="display: none">Go to magazine</div>
        <div class="go-to-contact" style="display: none">Go to contact</div>

        <!-- START FOOTER -->
        @include('front.partials.footer-home')
        <!-- END FOOTER -->

    </main>
    <!-- END MAIN CONTENT -->

    <!-- START NEWSLETTER MODAL -->
    <div class="newsletter-modal modal fade" id="templateM" tabindex="-1" role="dialog">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="newsletter"  style=" border: 13px solid #cdcfc1 !important;">
                    @include('front.partials.newsletter-home')
                </div>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>

            </div>
        </div>
    </div>
    <!-- END NEWSLETTER MODAL -->
            <!-- START collection  MODAL -->
        <div class="collection-modal modal fade" id="collectiontemplateM" tabindex="-1" role="dialog">
            <div class="modal-dialog-collection" role="document">
                <div class="modal-content-collection">
                    <div class="collection-collection"  style=" ">
                        @include('front.partials.collection-modal')
                    </div>
                </div>
            </div>
        </div>
        <!-- END NEWSLETTER MODAL -->

        <!-- START collection  MODAL -->
        <div class="press-modal modal fade" id="presstemplateM" tabindex="-1" role="dialog">
            <div class="modal-dialog-collection" role="document">
                <div class="modal-content-collection">
                    <div class="collection-collection"  style=" ">
                        @include('front.partials.presskit-modal')
                    </div>
                </div>
            </div>
        </div>
        <!-- END NEWSLETTER MODAL -->




@stop

@section('js-plugins')
    {{HTML::script('/front/plugins/parallax/jquery.parallax.min.js')}}
    {{--<script>--}}
        {{--$(document).ready(function () {--}}
            {{--setTimeout(function(){--}}
                {{--$('.parallax').parallax();--}}
                {{--$('.parallax-img-1').parallax({--}}
                    {{--invertX: true,--}}
                    {{--invertY: false--}}
                {{--});--}}
                {{--$('.parallax-img-2').parallax({--}}
                    {{--invertX: false,--}}
                    {{--invertY: true--}}
                {{--});--}}
                {{--$('.parallax-img-3').parallax({--}}
                    {{--invertX: true,--}}
                    {{--invertY: false--}}
                {{--});--}}
            {{--}, 1);--}}

            {{--$('#circle-join-btn').click( function() {--}}
                {{--activateNewsletterPopup();--}}
            {{--});--}}

            {{--function activateNewsletterPopup() {--}}

            {{--}--}}


            {{--$('#link-custom-newsletter-btn').click( function() {--}}
                {{--var screenSize = $(window).width();--}}
                {{--$('#templateM').modal('show');--}}

            {{--});--}}

            {{--$('#link-custom-collection-btn').click( function() {--}}
                {{--var screenSize = $(window).width();--}}
                {{--$('#templateCollection').modal('show');--}}

            {{--});--}}

            {{--$('#link-custom-press-btn').click( function() {--}}
                {{--var screenSize = $(window).width();--}}
                {{--$('#presstemplateM').modal('show');--}}

            {{--});--}}



            {{--$(".section-button-move-to-1").click(function(e) {--}}
                {{--e.preventDefault();--}}
                {{--debugger--}}
                {{--$(".go-to-home").click();--}}
            {{--});--}}
            {{--$(".section-button-move-to-3").click(function(e) {--}}
                {{--e.preventDefault();--}}
                {{--$(".go-to-collection").click();--}}
            {{--});--}}
            {{--$(".section-button-move-to-4").click(function(e) {--}}
                {{--e.preventDefault();--}}
                {{--$(".go-to-rendezvous").click();--}}
            {{--});--}}

            {{--$(".section-button-move-to-5").click(function(e) {--}}
                {{--e.preventDefault();--}}
                {{--$(".go-to-magazine").click();--}}
            {{--});--}}
            {{--$(".section-button-move-to-6").click(function(e) {--}}
                {{--e.preventDefault();--}}
                {{--$(".go-to-contact").click();--}}
            {{--});--}}

        {{--});--}}
    {{--</script>--}}

    <script>
        $(document).ready(function () {

            setTimeout(function(){
                $('.parallax').parallax();
                $('.parallax-img-1').parallax({
                    invertX: true,
                    invertY: false
                });
                $('.parallax-img-2').parallax({
                    invertX: false,
                    invertY: true
                });
                $('.parallax-img-3').parallax({
                    invertX: true,
                    invertY: false
                });
            }, 1);

            $('#circle-join-btn').click( function() {
                activateNewsletterPopup();
            });

            function activateNewsletterPopup() {

            }

            $('#link-custom-newsletter-btn').click( function() {
                var screenSize = $(window).width();
                hideNewsletterPopup();
                $('#templateM').modal('show');

            });

            $('#link-custom-collection-btn').click( function() {
                var screenSize = $(window).width();
                $('#collectiontemplateM').modal('show');

            });

            $('#custom_carousel').on('slide.bs.carousel', function (evt) {
                $('#custom_carousel .controls li.active').removeClass('active');
                $('#custom_carousel .controls li:eq(' + $(evt.relatedTarget).index() + ')').addClass('active');
            });

            $('#join-newsletter-btn').click(function() {
                displayNewsletterPopup();
            });

            $('#newsletter-popup-join-btn').click(function() {
                displayNewsletterPopup();
                hideNewsletterPopup();
            });

            $('.newsletter-popup-join-btn').click(function() {
                hideNewsletterPopup();
            });

            $('#link-custom-press-btn').click( function() {
            var screenSize = $(window).width();
            $('#presstemplateM').modal('show');

            });
            function hideNewsletterPopup() {
                $('#newsletter-popup-container').hide();
                $('#newsletter-content').show();
            }
            function displayNewsletterPopup() {
                $('#newsletter-content').hide();
                $('#newsletter-popup-container').show();
            }

            $(".section-button-move-to-1").click(function(e) {
            e.preventDefault();

            $(".go-to-home").click();
            });
            $(".section-button-move-to-3").click(function(e) {
            e.preventDefault();
            $(".go-to-collection").click();
            });
            $(".section-button-move-to-4").click(function(e) {
            e.preventDefault();
            $(".go-to-rendezvous").click();
            });

            $(".section-button-move-to-5").click(function(e) {
            e.preventDefault();
            $(".go-to-magazine").click();
            });
            $(".section-button-move-to-6").click(function(e) {
            e.preventDefault();
            $(".go-to-contact").click();
            });
        });
    </script>

@stop

