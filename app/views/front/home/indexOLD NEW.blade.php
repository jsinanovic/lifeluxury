@extends('front.layouts.main')

@section('content')
<section class="section-full section-top" id="home">
    <div class="section-right-text">
        <a style="margin-right:58px; padding: 0px;" href="welcome">WELCOME</a></div>
    {{--<div class="section-right-line" style="">------</div>--}}

    <div class="home-container" style="text-align: center;">
        <div class="home-under-info" style=" text-align: center; margin-bottom: 10px; letter-spacing: 2px;">EveryDay Transformed</div>
        <div class="home-under-info-2" style=" ">Welcome to our Universe</div>
        <a href="#" class="home-welcome-page">
            WELCOME </a>
    </div>

    <a href="javascript:void(0);" class="learn-more">
        DISCOVER THE COLLECTION
         <div class="round-button"></div></a>

</section>

<section class="section-full section-about" id="about">

    <div class="about-new-container" style="text-align: left;">
        <div class="about-under-info" style=" text-align: center; margin-bottom: 10px; letter-spacing: 2px;">Creative Ideas Filled With Passion & Love</div>
        <div class="about-under-info-2" >Art of Seeing Invisible</div>
        <a href="#" class="about-welcome-page">
            DISCOVER MORE </a>
    </div>


    <div class="section-right-text">
        <a  href="#">CREATIVE PROJECTS</a></div>
    <a href="javascript:void(0);" class="learn-more2">
        RENDEZ VOUS
        <div class="round-button"></div></a>

</section>

<section class="section-full section-creative" id="creative">


<div class="slike">
    <img href="" class="liplbe" style="float: left;"/>
    <p id="text1">
        Lifestyle
    </p>

    <img href="" class="liplbe1" style="float: left;"/>
    <a href="#" id="text21">
        Discover
    </a>
        <p id="text2">
           Places
        </p>

    <img href="" class="liplbe2" style="float: left;"/>
    <p id="text3">
        Rendez Vous
    </p>

    <a href="javascript:void(0);" class="learn-more3">
        MAGAZINE
        <div class="round-button"></div></a>
</div>

</section>

<section class="section-full section-link" id="rendezvous">

    <div class="rendezvous-new-container" style="text-align: left;">
        <div class="rendezvous-under-info" style=" text-align: center; margin-bottom: 10px; letter-spacing: 2px;">Immerse Yourself Into a Journey Note a Story</div>
        <div class="rendezvous-under-info-2" >Worlds Most Intriguing Subject on Business News, Lifestyle, Rendez Vous, Persona & Voyage </div>
        <a href="#" class="rendezvous-welcome-page">
            DISCOVER THE MAGAZINE </a>

        <div class="rendezvous-under-info-2" style="margin-top: 5% !important; color:  #cdcfc1; !important; font-size: 14px;">FEATURED ISSUE: SUMMER 2016</div>
    </div>


    <a href="javascript:void(0);" class="learn-more4">
       CONTACT US
    <div class="round-button"></div></a>
</section>


<section class="section-full section-magazine" id="magazine" >


    <div class="magazine-new-container" style="text-align: left;">
        <div class="magazine-under-info" style=" text-align: center; margin-bottom: 10px; letter-spacing: 2px;">Global yet Local Approach</div>
        <div class="magazine-under-info-2" >Life & Luxury International </div>
        <a href="#" class="magazine-welcome-page">
            CONTACT US </a>
    </div>



    <div class="section-right-line" style=""></div>
    <div class="section-right-text" style=" ">
        <a  style="margin-right:57px; padding: 0px;" href="#">CONTACT US</a></div>

    <a href="javascript:void(0);" class="learn-more5">
        Home
        <div class="round-button"></div></a>

    <div class="footer-transparent">
        @include('front.partials.footer')
    </div>
</section>

@stop

@section('js-plugins')
{{HTML::script('/front/plugins/parallax/jquery.parallax.min.js')}}
<script>
    $(document).ready(function () {
        setTimeout(function(){
            $('.parallax').parallax();
            $('.parallax-img-1').parallax({
                invertX: true,
		invertY: false
            });
            $('.parallax-img-2').parallax({
                invertX: false,
		invertY: true
            });
            $('.parallax-img-3').parallax({
                invertX: true,
		invertY: false
            });
        }, 1);
    });
</script>
@stop