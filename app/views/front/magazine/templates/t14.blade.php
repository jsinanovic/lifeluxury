<div class="s s-1">
    <div class="text text-center" data-sr="scale down 40%">
        <h2>Dubai embraces the extraordinary</h2>
        <div class="large">Boasting the world’s tallest building, a man-made island shaped like a palm tree and a 7 star hotel oozing with glamour and gold, Dubai is a glittering monument to Arab enterprise and Western capitalism.</div>
        <p>A city of opportunity rising from the desert sands, it is the perfect location for one of the most important events in the aerospace calendar: The Dubai Airshow.  </p>
        <img src="/front/img/t14/1.jpg" alt="">
        <p>Known for luxury shopping, ultramodern architecture and a lively nightlife scene, </p>
    </div>
</div>

<div class="s s-2">
    <div class="block-table">
        <div class="block-cell">
            <div class="for-img square square-t-r" data-sr="hustle 150px and scale up 20% enter left">
                <img src="http://lifeandluxury.com/uploads/articles/content/3c47b8487d5b4de4b7e0a1338d824599.jpg" data-img="img-1" alt="">
            </div>
        </div>

        <div class="block-cell">
            <div class="text" data-sr="scale down 40%">
                <p>Organised by F&amp;E Aerospace under the patronage of HH Sheikh Mohammed bin Rashid Al Maktoum, the record-breaking biennial show connects aerospace professionals from all areas of the industry. Originally beginning life as Arab Air in 1986, The Dubai Airshow has grown and evolved to facilitate successful trade on a global scale increasing from 200 exhibitors and 25 aircraft in 1989, to 1,100 exhibitors from over 60 countries with over 160 aircraft on display and media representation from every corner of the world in 2015. </p>
            </div>
        </div>
    </div>
</div>


<div class="s s-3">

    <div class="block-table">
        <div class="block-cell">
            <div class="text text-1" data-sr="scale down 40%">
                <b>While traditional markets are in decline, the focus on the Middle East is on the ascent as it continues to see strong growth across all aerospace sectors.</b>
            </div>
        </div>

        <div class="block-cell">
            <div class="for-img square square-b-l" data-sr="hustle 150px and scale up 20% enter right">
                <img src="http://lifeandluxury.com/uploads/articles/content/bb96a8adf97d7d791177b134414ee7e9.jpg" data-img="img-2" alt="">
            </div>
        </div>
    </div>

    <div class="block-table">    
        <div class="block-cell"></div>
        <div class="block-cell">
            <div class="text text-2" data-sr="scale down 40%">
                <p>The Dubai Airshow demonstrates the success of commercial, business and military aviation with exhibitions including the latest air defence innovations, the UAE Space Agency and for the first time in 2015, the 3D Printshow. In addition, the return of the Gulf Aviation Training Event pavilion and the Futures’ Day event continue to broaden the show’s appeal. Returning exhibitors include industry giants such as Boeing and Airbus as well as Bell Helicopters, Alpha Star, Safat and Tawazan. </p>
            </div>
        </div>

    </div>
</div>

<div class="s s-4">
    <div class="block-table">
        <div class="block-cell">
            <div class="text" data-sr="scale down 40%">
                <p>One of the most exciting parts of the show is inevitably the aircraft display. Past displays have included the F-16, V-22 Osprey, F-22 Raptor and the Eurofighter Typhoon. This year, visitors will be treated to the cream of the airspace with a Boeing P-8 military jet, a Qatar Executive G6R0ER and an Emirates Airline Airbus A380 expected on the static display along with aerobatic displays by international teams including the Red Arrows from the UK and Al Fursan from the UAE. </p>
            </div>
        </div>
        <div class="block-cell">
            <div class="for-img square square-t-l" data-sr="hustle 150px and scale up 20% enter top left"><img src="http://lifeandluxury.com/uploads/articles/content/3cd33409096dde7b6b8d51128c3c2c02.jpg" data-img="img-3" alt=""></div>
        </div>
    </div>
</div>

<div class="s s-5">
    <div class="block-table">
        <div class="block-cell">
            <div class="for-img square square-b-l" data-sr="hustle 150px and scale up 20% bottom left"><img src="http://lifeandluxury.com/uploads/articles/content/67f295bf6688696424178fce0cef4afc.jpg" data-img="img-4" alt=""></div>
        </div>
        <div class="block-cell">
            <div class="text" data-sr="scale down 40%">
                <p>Much debate is expected from the presence of the <b>world’s largest passenger airliner</b>
                    the magnificent Airbus A380, with talks surrounding the development of the elusive A380neo. Publicly lobbied by Emirates, the super-transporter is set to be at the forefront of The Dubai Airshow’s discussions
                </p>
            </div>
        </div>
    </div>
</div>

<div class="s s-6">
    <div class="text" data-sr="scale down 40%">
        <p>Also on display will be the  <b>future of flight itself</b>
            the Boeing 777X. Building on the success of the 777 and 787 Dreamliner, the 777X will be the largest and most efficient twin-engine jet in the world, unmatched in comfort and convenience with custom-tailored, enhanced architecture. 
        </p>

    </div>

    <div class="for-img" data-sr="hustle 150px and scale up 20% enter top"><img src="http://lifeandluxury.com/uploads/articles/content/58f4a7c4fab071222aa9688e32ba5f03.jpg" data-img="img-5" alt=""></div>
</div>

<div class="s s-7">
    <div class="block-table">
        <div class="block-cell">
            <div class="for-img square square-t-l"><img data-sr="hustle 150px and scale up 20% enter top left" src="http://lifeandluxury.com/uploads/articles/content/38090e0d4e697b15eb498c5003d3e88e.jpg" data-img="img-6" alt=""></div>
        </div>
        <div class="block-cell">
            <div class="text" data-sr="scale down 40%">
                <p>Other events at the show include the Skyview, a dedicated grandstand seating in an area located directly beside the airshow’s venue, as well as the glitzier <b>invite-only events</b> including a gala dinner and exhibitor party. </p>
            </div>
        </div>
    </div>
</div>

<div class="s s-8">
    <div class="block-table">

        <div class="block-cell">
            <div class="text" data-sr="scale down 40%">
                <p>In an age where society is becoming increasingly superlative in its wants and needs, The Dubai Airshow is a stirring alchemy of past feats of <b>aerospace brilliance</b> and ambitious futuristic vision. Now in its 28th year, the 2015 show will be held from 8 – 12 November at the Dubai World Central.   </p>
            </div>
        </div>
        <div class="block-cell">
            <div class="for-img square square-t-r" data-sr="hustle 150px and scale up 20% enter top right"><img src="http://lifeandluxury.com/uploads/articles/content/7552889897c105dd2810d643b3078f53.jpg" data-img="img-7" alt=""></div>
        </div>

    </div>
</div>

