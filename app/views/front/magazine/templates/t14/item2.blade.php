<div class="s s-2">
    <div class="block-table">
        <div class="block-cell">
            <div class="for-img square square-t-r" data-sr="hustle 150px and scale up 20% enter left">
                <img src="http://lifeandluxury.com/uploads/articles/content/3c47b8487d5b4de4b7e0a1338d824599.jpg" data-img="img-1" alt="">
            </div>
        </div>

        <div class="block-cell">
            <div class="text" data-sr="scale down 40%">
                <p>Organised by F&amp;E Aerospace under the patronage of HH Sheikh Mohammed bin Rashid Al Maktoum, the record-breaking biennial show connects aerospace professionals from all areas of the industry. Originally beginning life as Arab Air in 1986, The Dubai Airshow has grown and evolved to facilitate successful trade on a global scale increasing from 200 exhibitors and 25 aircraft in 1989, to 1,100 exhibitors from over 60 countries with over 160 aircraft on display and media representation from every corner of the world in 2015. </p>
            </div>
        </div>
    </div>
</div>