<div class="s s-6">
    <div class="text" data-sr="scale down 40%">
        <p>Also on display will be the  <b>future of flight itself</b>
            the Boeing 777X. Building on the success of the 777 and 787 Dreamliner, the 777X will be the largest and most efficient twin-engine jet in the world, unmatched in comfort and convenience with custom-tailored, enhanced architecture. 
        </p>

    </div>

    <div class="for-img" data-sr="hustle 150px and scale up 20% enter top"><img src="http://lifeandluxury.com/uploads/articles/content/58f4a7c4fab071222aa9688e32ba5f03.jpg" data-img="img-5" alt=""></div>
</div>