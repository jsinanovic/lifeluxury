<div class="item item-dark">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="for-img content-data-img" data-key="img-6" data-sr=""><img src="http://lifeandluxury.com/uploads/articles/content/58df05fafe99b04ad120c5f97425d79b.png"></div>
            </div>
            <div class="col-md-11 col-md-offset-1 img-p">
                <div data-sr="enter bottom, move 100px, wait 0.5s">
                    <p><img src="http://lifeandluxury.com/uploads/articles/content/357a10a6cfd10dfcfbdf9d84d6cb04e9.png" style="width: 275px; float: left;"><span style="color: rgb(255, 255, 255);">Originated from the same kingdom, Jaguar F-PACE encompasses everything that the brand stands for: beautiful design, precise handling, a supple ride, luxurious interior finishes and cutting-edge technology. Being Jaguar’s first SUV, this all-new, highly efficient five-seat performance crossover is the answer to the overwhelmingly positive response to the C-X17 concept car at the IAA Frankfurt 2013. Sleek profile, upright stance, muscular ridged bonnet, lightweight chassis, and spacious interior combined with exceptional on-road dynamics are few of the many contributions to the style and practicality of Jaguar’s newest hype.</span><br></p>
                </div>
            </div>
        </div>
    </div>
</div>