<div class="container">
    <div class="row">
        <div class="col-md-12">
            <div class="article-view-category" data-sr="">
                Rendez Vouz  |  <span>Life &amp; Luxury Magazine</span>
            </div>
        </div>
    </div>
</div>

<div class="item item-dark">
    <div class="container">
        <div class="row">
            <div class="col-md-7">
                <div class="for-img content-data-img" data-key="img-1" data-sr=""><img src="http://lifeandluxury.com/uploads/articles/content/45a0cbd30cf2624412a22f7bfce7a158.png"></div>
            </div>
            <div class="col-md-5">
                <div data-sr="enter right, move 100px, wait 0.5s">
                    <h2><span style="color: rgb(255, 255, 255);">A prestigious gallery of the future</span></h2>
                    <p><span style="color: rgb(255, 255, 255);">Upon seeing this title, two certain emotions are anticipated: excitement versus indifference. A third reaction is nevertheless the one most expected: curiosity, since you understand the importance and meaning of the world’s largest motor show, not just from the perspectives of a professional or an enthusiast, but of someone who apprehends the four-wheeled invention as an element of lifestyle.</span></p>
                    <p><span style="color: rgb(255, 255, 255);">Scattered across the calendar from the 17th to 27th September on the fairground of Messe Frankfurt are around 400 exhibitions, including: special actions for children and grown-ups, such as test drives to identify “Germany’s best driver at the IAA”, off-road vehicle test track, and “Driver’s License” for kids; a career fair for the automotive sector; various initiatives to promote safety on the road; and presentations of the most innovative mobility trends along with future urban mobility solutions.</span></p>
                </div>
            </div>
        </div>
    </div>
</div>