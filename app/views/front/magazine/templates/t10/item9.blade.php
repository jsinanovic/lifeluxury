<div class="item item-photo-full content-data-bg" data-key="img-9">
    <div class="block-table">
        <div class="block-cell">
            <div data-sr="enter bottom move 100px spin 180deg over 1s wait 0.5s">
                <p><span style="color: rgb(255, 255, 255);">With all that being said, this year will surely be a fun ride for car aficionados, partly on account of the show’s host city as well. Home to European Central Bank and Frankfurt Stock Exchange, Frankfurt constantly plays a vital role in the financial picture of Europe and the world. High-rise buildings surrounding marches of suited bankers and professionals are one such demonstration for that reputation. Its skyline is certainly one rare sight in not just Germany, but also in the European Union, hence the nickname “Germany’s Manhattan”. Yet, what sets it apart from the real Manhattan is the sheer romance created by a harmonious balance between culture, history and nature that embraces the well-lit concrete jungle. Indeed, Frankfurt itself is a grand exhibition with constant displays of business, technology and entertainment.
                        Therefore, again, cars are one essential element of lifestyle. Yet, many others are still hidden somewhere in this multicolour and lively metropolitan, waiting for you to discover and savour with class. 
                    </span></p>
            </div>
        </div>
    </div>
    <img src="http://lifeandluxury.com/uploads/articles/content/243f3e851c0e074b21c3cfd2fef2a802.png">
</div>