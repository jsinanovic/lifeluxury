<div class="item item-light">
    <div class="container">
        <div class="row">
            <div class="col-md-5">
                <div data-sr="enter left, move 100px, wait 0.5s">
                    <p>The other German elite is nowhere far behind in this race towards quintessential luxury. As rumours and spied photos speculating the internet among avid drivers, it appears that Mercedes-Benz S-Class Cabriolet may just be the next beauty queen for star gazers. With respect to the distinctive elegant silhouette of the S-Class Coupe family through sleek design of concave and convex surfaces at the front, the S-Class Cabriolet now features electronically-foldable fabric roof plus a few minor changes at the rear to accommodate this new freedom. Roofless or not, this highly anticipated convertible would continue to grace the roads and its driver.</p><p><img src="http://lifeandluxury.com/uploads/articles/content/f42a9ae4ca176c1c89ea0af9e665ae2a.png" style="width: 375px;"><br></p>
                </div>
            </div>
            <div class="col-md-7">
                <div class="for-img content-data-img" data-key="img-4" data-sr=""><img src="http://lifeandluxury.com/uploads/articles/content/edadb59080e86b2472f4f9e8a6b73bb3.jpg"></div>
            </div>
        </div>
    </div>
</div>