<div data-sr="">
    <div class="item item-bg-center content-data-bg" data-key="img-2">
        <div class="item-cell">
            <div class="container">
                <div class="row">
                    <div class="col-md-8 col-md-offset-2" data-sr="wait 0.3s flip 90deg over 1.5s">
                                                <p>Whether well-organised or spontaneous, a perfect day of leisure and entertainment can be achieved with ease. In this day and age, information are, praise be, only a few clicks or swipes away. 
</p><p>
Running out of ideas for fun and activities are hence becoming a fleeting trace of the past. An upscale cafe, a retro restaurant, a bizarre exhibition or some famous DJ in town, there is simply nothing that the internet is oblivious to. Born and nurtured with such philosophy, LifestyleClubbing chose to become the virtual Good Samaritan, who grants guidance to vogue trends and a chic lifestyle for seekers of all ages. In any case, a concrete example speaks more volume than mere figure of speech. Let’s discover how an otherwise ordinary Saturday could be transformed into one of those celebrity’s desirable reality episodes, minus world tours and on-set filming. Probably.
</p>
                                            </div>
                </div>
            </div>
        </div>
    <img src="http://lifeandluxury.com/uploads/articles/content/c94c76e5e6f4c5ad32e5d7694b7dd3e5.jpg"></div>
</div>