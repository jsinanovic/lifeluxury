<div data-sr="">
    <div class="item item-bg-center content-data-bg" data-key="img-5">
        <div class="item-cell" data-sr="vFactor 0.8 wait 0.3s roll 45deg, spin 45deg, flip 45deg">
            <div class="for-img content-data-img" data-key="img-6"><img src="http://lifeandluxury.com/uploads/articles/content/69b236bd71cda43352a8479427263cc3.jpg"></div>
        </div>
        <div class="item-cell" data-sr="vFactor 0.8 wait 0.6s enter right scale down 20%">
            <p><span style="color: rgb(255, 255, 255);">Refreshed and revitalised, you make your way to Tokio Restau  
                    rant Bar. This decision for a change of palate has not fallen short of expectations. It sure is entertaining to witness skilled chefs literally having an art performance at the Teppanyaki grill table with high-rise flames, rhythmic cutting motions and precise manipulations. Amusing for the eyes and equivalently appetising. 
                </span></p>
        </div>
        <img src="http://lifeandluxury.com/uploads/articles/content/b426a41071f7621d9cf6993e1124b9bb.jpg"></div>
</div>