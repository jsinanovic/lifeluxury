<div class="s">
    <div class="row row-1">
        <div class="col-md-4 col-md-offset-1">
            <div class="text" data-sr="wait 0.5s, ease-in-out 100px, scale down 25%">
                <h2>THE OFFICIAL HOMETOWN Of<span><br>SANTA CLAUS</span></h2>
                <i></i>
                <p>Ho ho ho thus comes<br>Santa Claus! </p> 
            </div>
        </div>
        <div class="col-md-7">
            <div class="img-holder" data-sr="flip -20deg, spin -20deg, hustle 40px">
                <img src="http://lifeandluxury.com/uploads/articles/content/f543709277b6e01630801deaa951238d.jpg" data-img="img-1" alt="">
            </div>
        </div>
    </div>

    <div class="row row-2">
        <div class="col-md-5 col-md-offset-1">
            <div class="img-holder" data-sr="flip -20deg, spin -20deg, hustle 40px">
                <img src="http://lifeandluxury.com/uploads/articles/content/5b3331df337903422d358402050dc5ab.jpg" data-img="img-2" alt="">
            </div>
        </div>
        <div class="col-md-5">
            <div class="text" data-sr="wait 0.5s, ease-in-out 100px, scale down 25%">
                <p>
                    Here in Santa’s home-sweet-home, stay at the Santa’s Suite
                    of the Artic Light Hotel, or have Luxury Action customise
                    your high-end holiday with exclusive Nordic-style experience, 
                    such as a VIP cruise on an icebreaker, nights of a lifetime 
                    in an ice or glass igloo underneath the Northern Lights, 
                    delicious cocktails served at the Ice Bar amidst the frozen sea,
                    and just-for-you firework display in the deep winter sky of Finland. 
                </p>
                <i>Christmas could never be more authentic.</i>
            </div>
        </div>
    </div>
</div>


<div class="s row row-3">
    <div class="col-md-5 col-md-offset-1">
        <div class="text" data-sr="wait 0.5s, ease-in-out 100px, scale down 25%">
            <h2>TRACING TRADITIONS</h2>
            <h3>DRESDEN</h3>
            <i class="define-line"></i>
            <p>
                Standing in front of the dreamy Elbe River and the Frauenkirche
                is one of Germany’s greatest and no doubt oldest Christmas Market.
                That is, Striezelmarkt embodies a great history of this festive 
                season with wooden ornaments, Christmas pyramids, nutcrackers, the
                smoking man, mulled wine and the famous Dresden Stollen. 
            </p>
            <p>
                After having greeted Father Christmas, celebrate the event by
                treating yourself to a gastronomically enriching dinner at either
                the charming Caroussel, the characterful bean &amp; beluga, or the
                contemporary Elements, all of which represent the one-star-Michelin
                pride of the city. Childhood traditions mixed with sensible enjoyment,
                now that what Christmas is all about.
            </p>
        </div>
    </div>
    <div class="col-md-6">
        <div class="img-holder" data-sr="flip -20deg, spin -20deg, hustle 40px">
            <img src="http://lifeandluxury.com/uploads/articles/content/319a4688a23e0a99202a8ee7479fc275.jpg" data-img="img-3" alt="">
        </div>
    </div>
</div>



<div class="s row row-4">
    <div class="col-md-6">
        <div class="img-holder">
            <img src="http://lifeandluxury.com/uploads/articles/content/462038a4f9b75b28f7ac71e23a4d410b.jpg" data-img="img-4" alt="">
        </div>
    </div>
    <div class="col-md-6">
        <div class="text" data-sr="wait 0.5s, ease-in-out 100px, scale down 25%">
            <h2>SKI DOWN WITH STYLE</h2>
            <h3>ST MORITZ</h3>
            <i class="define-line"></i>
            <p>
                For winter sport aficionados, St. Moritz is the place 
                to be. Famous for its world-class elegance, exclusivity
                and cosmopolitan ambiance, St. Moritz has much more to 
                offer than its wondrous ski slopes and perfect powder snow.
            </p>
        </div>
    </div>
</div>


<div class="s">
    <div class="row">
        <div class="col-md-6">
            <div class="text" data-sr="wait 0.5s, ease-in-out 100px, scale down 25%">
                <p>
                    For winter sport aficionados, St. Moritz is the place 
                    to be. Famous for its world-class elegance, exclusivity
                    and cosmopolitan ambiance, St. Moritz has much more to 
                    offer than its wondrous ski slopes and perfect powder snow.
                </p>
            </div>
        </div>
        <div class="col-md-6">
            <div class="img-holder" data-sr="flip -20deg, spin -20deg, hustle 40px">
                <img src="http://lifeandluxury.com/uploads/articles/content/e05cd24bf8816e19604eae559c0b644c.jpg" data-img="img-5" alt="">
            </div>
        </div>
    </div>



    <div class="s row row-6">
        <div class="col-md-6">
            <div class="img-holder" data-sr="flip -20deg, spin -20deg, hustle 40px">
                <img src="http://lifeandluxury.com/uploads/articles/content/585be1e18dfabdb0b74ca9a3956cc2b6.jpg" data-img="img-6" alt="">
            </div>
        </div>
        <div class="col-md-6">
            <div class="text" data-sr="wait 0.5s, ease-in-out 100px, scale down 25%">
                <h2>A “HYGGELIGHT” HOLIDAY</h2>
                <h3>COPENHAGEN</h3>
                <i class="define-line"></i>
                <p>
                    In Danish, “hygge” stands for the creation of a warm 
                    atmosphere and the enjoyment of good things in life 
                    with good people. And that is exactly what you can do
                    in this far north capital. 
                </p>
                <p>
                    From the delightful Christmas in Tivoli Gardens and 
                    the two-star-Michelin trios Restaurant AOC, noma and 
                    Restaurant Genarium to Copenhagen’s Best Wine Bar 2015 
                    Ravnsborg Vinbar and the stylish Royal Copenhagen Christmas 
                    Tables exhibition where ideas for your Christmas table 
                    setting assembled by various artists and celebrities are 
                    found, there are chances aplenty for a truly “hyggeligt” 
                    vacation filled with life’s wonders and shared with your dearest.
                </p>
            </div>
        </div>
    </div>

</div>

<div class="s row">
    <div class="col-md-5 col-md-offset-1">
        <div class="text" data-sr="wait 0.5s, ease-in-out 100px, scale down 25%">
            <h2>MERRY SECOND CHRISTMAS</h2>
            <h3>MOSCOW</h3>
            <i class="define-line"></i>
            <p>
                Celebrations never seize if you know where and when 
                to toast. In this beautiful Orthodox capital, Ded Moroz – 
                Grandfather Frost – and Snegurochka – Snow Maiden – will visit on 7th January.  
            </p>
        </div>
    </div>
    <div class="col-md-6">
        <div class="img-holder" data-sr="flip -20deg, spin -20deg, hustle 40px">
            <img src="http://lifeandluxury.com/uploads/articles/content/b997b6de7fb9689d30df2c34d0950f4f.jpg" data-img="img-7" alt="">
        </div>
    </div>
</div>



<div class="s row">
    <div class="col-md-6">
        <div class="img-holder" data-sr="flip -20deg, spin -20deg, hustle 40px">
            <img src="http://lifeandluxury.com/uploads/articles/content/760686de93bb319312f3ef65daf1ea50.jpg" data-img="img-8" alt="">
        </div>
    </div>
    <div class="col-md-5 col-md-offset-1">
        <div class="text" data-sr="wait 0.5s, ease-in-out 100px, scale down 25%">
            <p>
                Throughout this holiday season, expect a snow-coated
                fairyland decorated with fir trees, lavish ornaments,
                the majestic luxury shopping complex GUM, the colourful
                Red Square and its symbolic St. Basil’s Cathedral.   
            </p>
        </div>
    </div>
</div>



<div class="s row row-9">
    <div class="col-md-5 col-md-offset-1">
        <div class="text" data-sr="wait 0.5s, ease-in-out 100px, scale down 25%">
            <p>
                Don’t forget to keep yourself warm with hot cocoa from
                Bosco Bar while strolling down the elegant fashion paradise
                of Russian Street. Whether it is December or January, winter
                is always beautiful enough to be celebrated.   
            </p>
        </div>
    </div>
    <div class="col-md-6">
        <div class="img-holder" data-sr="flip -20deg, spin -20deg, hustle 40px">
            <img src="http://lifeandluxury.com/uploads/articles/content/134bb133f8a4027ecb8f946166f036ae.jpg" data-img="img-9" alt="">
        </div>
    </div>
</div>

