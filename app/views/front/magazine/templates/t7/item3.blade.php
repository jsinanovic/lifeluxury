<div class="item item-t7 item-t7-img-left">
    <div class="block-table" data-sr="enter left">
        <div class="block-cell img-block content-data-all" data-key="img-3"><img src="http://lifeandluxury.com/uploads/articles/content/83050e6500088c34f41f37ace1982967.jpg"></div>
        <div class="block-cell block-cell-separator"></div>
        <div class="block-cell" style="background-color: #e27a3f" data-sr="hustle 100px enter right">
            <div class="cell-content" data-sr="wait 0.3s enter top opacity 0.2 scale down 20%"><hr><p class="letter">Underneath all the glamour the frivolities of life have been stripped away leaving only a steel core burning through the toxic layers to reveal a strip of pure, unadulterated light. No longer living just for ourselves but fighting on with a sense of renewal and pride, life has adopted a new meaning with the wisdom that nothing should be taken for granted and that luxury is golden.   
                </p>
            </div>
        </div>
    </div>
</div>