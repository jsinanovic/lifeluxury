<div class="item item-t7 item-t7-img-left">
    <div class="block-table" data-sr="enter left">
        <div class="block-cell img-block content-data-all">
            <img src="http://lifeandluxury.com/uploads/articles/content/b65fa8d2048154644132899b31e906e6.jpg">
        </div>
        <div class="block-cell block-cell-separator"></div>

        <div class="block-cell" style="" data-sr="hustle 100px enter right">
            <div class="cell-content" data-sr="wait 0.3s enter top opacity 0.2 scale down 20%"><hr><h2>The Autumn Years</h2>
                <div class="article-view-category">
                    Cover Editorial  |  <span>Life &amp; Luxury Magazine</span>
                </div>
                <p class="letter">
                    The golden age of life is upon us. Here in the 21st century, life and luxury go hand in hand with more successful business and leisure enterprises at our disposal than ever before. Despite the financial crisis that threatens to cripple the European Union, our nations face the challenge with pride and a steely determination that has seen us survive through the decades. 
                </p></div>
        </div>
    </div>
</div>