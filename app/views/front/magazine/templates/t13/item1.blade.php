<div class="item item-1">
    <img src="http://lifeandluxury.com/uploads/articles/content/a2caf43c5703221f1c893f8ec89e15cb.png" data-img="img-1" alt="" width="100%">
    <div class="block-content">
        <div class="text bordered" data-sr="scale up 25% move 500px enter top">
            <p>As winter tightens her icy grasp and the air grows crisp with frost, memories of itsy bitsy bikinis, slinky sarongs and sun-kissed skin seem long forgotten. Yet, the festive months needn’t be bleak. From silk scarves to soft shearling, fabulous faux fur to liquid leather, bundle up in style this winter with the hottest accessories and must have pieces to brighten up your wardrobe. As winter tightens her icy grasp and the air grows crisp with frost, memories of itsy bitsy bikinis, slinky sarongs and sun-kissed skin seem long forgotten. Yet, the festive months needn’t be bleak. From silk scarves to soft shearling, fabulous faux fur to liquid leather, bundle up in style this winter with the hottest accessories and must have pieces to brighten up your wardrobe.</p>
        </div>
    </div>
</div>