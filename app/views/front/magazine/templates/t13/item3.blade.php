<div class="item item-3">
    <img src="http://lifeandluxury.com/uploads/articles/content/de018340a98c88fa4cf6336925de55a1.png" data-img="img-3" alt="" width="100%">
    <div class="block-content">
        <div class="text" data-sr="scale up 25% move 500px enter top">
            <p>Originally a staple item of the male middle-class wardrobe, polo-necks grew in popularity among both sexes when they hit the Hollywood scene, with the likes of Greto Garbo and Audrey Hepburn donning the ‘tight turtleneck’ to emphasise their figures. Comfortable, flattering and elegant, the garment can be teamed with trousers, skirts or leggings and layered to add style. Whether worn casually or formally, the cosy polo/turtleneck is a must have winter piece for any fashionista.</p>
        </div>
        <div style="position: relative;" data-sr="spin -90deg">
            <div class="bordered">
                <img src="http://lifeandluxury.com/uploads/articles/content/6d22539f20bfd6e5f3eae480d16df8fe.jpg" data-img="img-4" alt="">
            </div>
        </div>
    </div>
</div>
