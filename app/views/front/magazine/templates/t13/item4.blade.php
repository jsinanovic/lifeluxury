<div class="item item-4">
    <img src="http://lifeandluxury.com/uploads/articles/content/cba953740a9232e37fb4818376d6d460.png" data-img="img-5" alt="" width="100%">
    <div class="block-content">
        <div class="text bordered" data-sr="scale up 25% move 500px enter top">
            <p>No accessory quite transforms an outfit like the wonderfully versatile scarf. Classy, functional and absolutely essential for winter, the scarf is a favourite of many celebrities, whether worn in the traditional way around the neck, or used as a hair accessory. A year-round hero layer, choose from chunky knits, elegant silks or painterly prints to brighten up simple looks and add warmth and texture.</p>

        </div>
    </div>
</div>