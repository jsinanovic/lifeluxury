<div class="item item-photo-half" data-sr="scale down 50% over 1s">
    <div class="block-table">
        <div class="block-cell img-block content-data-all" data-key="img-3"><img src="http://lifeandluxury.com/uploads/articles/content/1fad6190064030d8d67ca4d966bb68d9.jpg"></div>
        <div class="block-cell">
            <div class="cell-content">
                <p>The hearts of wise adventure seekers and nature lovers would miss a beat upon the enchanting sights of England’s oldest oak forest. Served as the key scenes for Harry Potter and the Deathly Hallows as well as an inspiration for J.R.R Tolkien’s Middle Earth’s forests, Gloucestershire’s Forest of Dean’s mysterious depths combined with its mixture of yellow, brown and burnt orange colours make this destination an ideal escape. Temporary leave behind your filled agendas and social catch-ups for a breath of fresh air and inner peace. Walking, hiking, biking, horse riding, kayaking, fishing and private microlight flying lessons, just to name a few; there is always something for everyone to participate in and above this former royal hunting ground. Allow the Wye River to carry your worries away and who knows, some innovative ideas may come up unexpectedly as you reminisce The Fellowship of the Ring’s journeys while promenading along the forest’s edge towards the charming village of Symonds Yat.</p>
            </div>
        </div>
    </div>
</div>