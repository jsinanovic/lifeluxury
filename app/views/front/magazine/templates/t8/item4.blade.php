<div class="item" data-sr="scale down 20%">
    <div class="item-img-center content-data"><img src="{{URL::to('front/img/add.jpg')}}" /></div>

    <div class="item-img-center item-img-decor" style="margin-bottom: -180px;" data-sr>
        <img src="/front/img/t1/dragon_top.png" alt="" width="100%">
    </div>

    <div class="container" data-sr="enter bottom move 100px">
        <div class="row" style="background: url(/front/img/t1/formula.png) no-repeat center;">
            <div class="col-md-6 text-col">
                <p>
                    The heat of Singapore Grand Prix this year will not concentrate only      
                    on-track with the passion of determined drivers, but also off the track as Bon Jovi rocks the Lion City on the last day of the racing weekend. Together with the legendary rock band from the US are Jimmy Cliff and the home-grown pop group The Sam Willows. Before that, the Jamaican reggae artist and the young Singaporean musicians will also join Pharrell Williams to kick-start the post-race entertainment scene on Friday, leading up to the Saturday upbeats of Maroon 5 (US), Spandau Ballet (UK) and Dirty Loops (Sweden).In the name of comfort and luxury, all those excitements and thrills can be experienced with class through the Hospitality and Executive Packages. Singapore truly deserves its title as the вЂњjewel in the Formula One crownвЂќ as the race are not simply a mere gathering of sports enthusiasts, but rather a social event of the highest standard with refined service and utmost dedication. 
                </p>
            </div>
            <div class="col-md-6 text-col">
                <p>
                    In order to satisfy the sophisticated and diverse palates of their prestigious guests at the Sky Suite, Club Suite and The Green Room during the races, chefs from six five-star hotels вЂ“ namely Mandarin Oriental Singapore, Grand Hyatt Singapore, InterContinental Singapore, Sheraton Towers Singapore, Pan Pacific Singapore and Marina Mandarin Singapore вЂ“ along with apprentices from the At-Sunrise Global Chef Academy have divided into teams and competed against each other to decide the best recipes for the menu. Hospitality guests can rest assured that the roaring engines would be heard loud and clear with an up-close view of their favourite driver flashing by whilst they enjoy the exquisite international cuisines with a glass of Chardonnay chosen from the extensive wine list from either exclusive balconies, terraces, private grandstands or the air-conditioned trackside suite.
                </p>
            </div>
        </div>
    </div>
</div>