<div class="item bg-center-cover content-data content-data-bg" data-key="img-4">
    <div class="container">
        <div class="row">
            <div class="col-md-12" data-sr="vFactor 1 enter left spin 90deg over 1s">
                <p>Being married to one of the greatest shipping magnates carries heavy responsibilities and no other woman could have carried them off as glamorously as Jackie O. Her wedding to Aristotle Onassis was held onboard the stately superyacht вЂChristina OвЂ™, which played host to the creme del la creme of 1950s high society including Marilyn Monroe and was also the palatial setting for the wedding reception of MonacoвЂ™s very own Prince Rainer and Grace Kelly. Yet long before Jackie became Mrs O, she was enthralling the public with her simple approach to clothing and beauty as the First Lady. Popularising the pillbox hat and sleek shifts, Jackie often stepped out in attention-grabbing hue but also understood the power of the monochromatic look. In later years her style evolved to incorporate neck scarves and wide-legged trousers but she continued to make her mark with crisp silhouettes in little black dresses and white elbow gloves. Away from the political scene, Jackie favoured tasteful bathing suits in graphic prints and oversized dark sunglasses. A leading style icon in both The White House and on the high seas, Jackie OвЂ™s fashion favourites endure to this day.</p>
            </div>
        </div>
    </div>
    <img src="http://lifeandluxury.com/uploads/articles/content/44eb7e252713887b589adc280a178bb2.jpg">
</div>