<div class="item" style="padding: 0" data-sr="over 1s">
    <div class="container">
        <div class="row">
            <div class="col-md-5 col-sm-8">
                <h2>RIDING ON THE GOLDEN WAVES OF LIFESTLYE</h2>
            </div>
        </div>
        <div class="row">
            <div class="col-md-4">
                <p style="font-size: 24px;">As the megayachts of Monaco prepare to woo the cream of society, itвЂ™s not just the pearl finishes that will be polished to perfection</p>
            </div>
            <div class="col-md-4">
                <p>The WhoвЂ™s Who of the yachting world will be dazzling Port Hercules with their designer trends in an effort to match the elegance of bygone eras. In an ode to timeless celebrity fashion icons, many of the elite guests will be dressed in head to toe glamour inspired by HollywoodвЂ™s goddesses of the past. </p>
            </div>
            <div class="col-md-4">
                <p>Screen legend Grace Kelly, Princess of Monaco, may have once ruled the high seas of fashion and the memory of her classical looks represents an eternal style inspiration for women all over the world. But she wasnвЂ™t the only icon of the golden age to cruise in couture along the French Riviera. </p>
            </div>
            <div class="col-md-12">
                <div class="for-img">
                    <img src="/front/img/t3/s1.jpg" alt="" width="100%">
                </div>
            </div>
        </div>
    </div>
</div>