<div class="item item-2" style="background-color: #f5f5f5;" data-sr="vFactor 0.4 over 1s">
    <div class="container bg-center-cover content-data content-data-bg" data-key="img-1">
        <div class="row">
            <div class="col-md-5">
                <p><span style="color: rgb(255, 255, 255);">Long before the Monaco Yacht Show, Elizabeth Taylor was already causing a stir on the high seas with her love of luxury boats as well as her opulent taste in exquisite finery. In 1967 Richard Burton presented her with an Edwardian motor yacht named вЂKalizmaвЂ™ and the following year he surprised Elizabeth with a 33.19-carat ring named the Krupp Diamond whilst on board. </span></p>
                <p><span style="color: rgb(255, 255, 255);">Famed for her high-octane taste in jewels, Elizabeth epitomised elegance with the help of feminine tailoring, favouring full skirts and nipped in waists with daring halter necks and lashings of red lipstick. Even on board, Elizabeth was never seen without her decorous diamonds which she teamed with classic, curve-enhancing bathing suits. Exuding sophistication and a royal attitude, which saw her dazzle through the decades, Elizabeth TaylorвЂ™s legendary style still proves an inspiration to many. </span></p>
            </div>
        </div>
        <img src="http://lifeandluxury.com/uploads/articles/content/ec1ea9f5b5be7f7c06ce72abdfd50c57.jpg">
    </div>
</div>