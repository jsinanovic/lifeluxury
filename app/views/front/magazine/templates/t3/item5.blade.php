<div class="item bg-center-cover content-data content-data-bg" data-key="img-5">
    <div class="container">
        <div class="row">
            <div class="col-md-5 col-md-offset-7 big-padding" data-sr="vFactor 1 enter right spin 90deg over 1s">
                <p>Sailing the high seas with grace and poise, the sirens of the golden age continue to entice the modern world of fashion with their timeless style. Whether simplistic and sensual, or sophisticated and sassy, the icons of the past have left a legacy of glamorous garbs to inspire and influence the couture courters of today. </p>
            </div>
        </div>
    </div>
    <img src="http://lifeandluxury.com/uploads/articles/content/67a6cb64371d0cae4da75ad9c6acdd71.jpg">
</div>