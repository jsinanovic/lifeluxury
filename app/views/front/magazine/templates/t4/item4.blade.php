<div class="item item-photo-half" data-sr="scale down 50% over 1s">
    <div class="block-table">
        <div class="block-cell img-block content-data-all" data-key="img-5"><img src="http://lifeandluxury.com/uploads/articles/content/9998660641ccf9d91b5f7bb17300dd96.jpg"></div>
        <div class="block-cell">
            <div class="cell-content">

                <p>
                    Led by Alex Tsipras, the leftist Syriza party stormed to power this year promising to renegotiate the bailout. In a whirl of political strife, the new government called a polarising referendum in July asking the Greek people to vote yes or no to even more austerity measures in return for an extension of bailout funds, demanded mainly by Germany’s chancellor Angela Merkel and the International Monetary Fund. The Greeks responded with overwhelming enthusiasm voting NO by more than 60%. Sadly, the euphoria of the national vote gave way to fatigue ten days later when Tsipras performed an embarrassing yet ultimately inevitable U-turn and signed up to further harsh measures to save the economy. As of yet there are no provisions for departure of the European Union and exiting the single Euro currency would involve a legal minefield that no country has ventured to cross. Despite the political roller coaster, it would seem that Greece do not want to rock the international financial boat. 
                </p>
            </div>
        </div>
    </div>
</div>    