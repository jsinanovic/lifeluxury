<div class="item item-photo-half" data-sr="scale down 50% over 1s">
    <div class="block-table">
        <div class="block-cell img-block content-data">
            <img src="http://lifeandluxury.com/uploads/articles/content/1b44d234291b87e4323f255185feceeb.jpg">
        </div>
        <div class="block-cell">
            <div class="cell-content">
                <div class="article-view-category">
                    Business  |  <span>Life &amp; Luxury Magazine</span>
                </div>

                <p>
                    Greece has given the world much. The founding father of democracy, the country has charmed many a wanderer with its fine food, rich history and enduring traditions. Yet, behind the picture perfect beauty of the cool Mediterranean waters lapping at the sun kissed landscape, an ugly truth lurks in the shadows. In 377 BC, Greece became the first country to default on its debts. Fast-forward 2,392 years and not a lot has changed. 
                </p>
            </div>
        </div>
    </div>
</div>