<div class="item item-photo-half" data-sr="scale down 50% over 1s">
    <div class="block-table">


        <div class="block-cell">
            <div class="cell-content">

                <p>
                    To avoid financial calamity, in May 2010 the International Monetary Fund, European Union and European Commission, collectively known as Troika, granted the Greek government a 110 billion bailout. A second bailout followed but much of the cash went on Greece’s running costs leaving little if any leftover for an economic recovery. On top of this the bailouts came with conditions and harsh austerity measures, with an increase of 29 percent to the yield of Greek government bonds. Through a programme of strict spending cuts and tax reforms, EU leaders pledged to slash the country’s budget deficit, requiring Greece to overhaul its economy and streamline the government. Unable to repay the monstrous debts of around 356 billion, Greece has had to face the consequences with unemployment above 25 per cent and an economy that has shrunk by a quarter in just five years. 
                </p>
            </div>
        </div>

        <div class="block-cell img-block content-data-all" data-key="img-3"><img src="http://lifeandluxury.com/uploads/articles/content/b78bf26f3d5490176b8dafd019e0b4ae.jpg"></div>

    </div>
</div>  