<div class="item item-col-3">
    <div class="container-fluid">
        <div class="row-fluid">
            <div class="col-md-4" data-sr="enter left scale down 20%">
                <h2>VTTV VASILIKO CYPRUS</h2>
                <blockquote>
                    <p>Tanks: 28 </p>
                    <p>Capacity 544,000 m3 </p>
                    <p>Marine Jetty: 1500m </p>
                    <p>Special Features:</p>
                    <p>Road tanker loading facilities.</p>
                </blockquote>
            </div>
            <div class="col-md-4" data-sr="wait 0.5s opacity 0 enter top scale down 20%">
                <div class="article-view-category">
                    Business  |  <span>Life &amp; Luxury Magazine</span>
                </div>
                <p>Attracting major foreign investment into Cyprus in a time of global recession was never going to be easy, so when Netherlands based VTTI B.V., a major player in oil and fuel storage, announced their interest in building a state of the art storage facility with a brand new deep water marine jetty at Vasiliko, many wondered how and when this project would reach fruition. The project itself was not without initial delays and setbacks, but since November 2014 phase1 is now fully operational.</p>
            </div>
            <div class="col-md-4" data-sr="wait 1s  opacity 0 enter right scale down 20%">
                <div class="for-img content-data-img" data-key="img-1">
                    <img src="http://lifeandluxury.com/uploads/articles/content/cbdd0357551a2d10cec2d59e18cdc5b7.jpg">
                </div>
            </div>
        </div>
    </div>
</div>

<div class="item item-col-3">
    <div class="container-fluid">
        <div class="row-fluid">
            <div class="col-md-4" data-sr="enter left scale down 20%">
                <h3>Cyprus a strategic energy hub</h3>
                <p>Does this description make you smile too? - it is of course referring to Cyprus??™ import/export trans-shipment of oil products to international markets and trade with the Eastern Med, Middle East and European markets. </p>
            </div>
            <div class="col-md-4" data-sr="wait 0.5s opacity 0 enter top scale down 20%">
                <p>It is strategic for VTTI V.B., because, ???Until now, Malta was the main turning circle in the Eastern Mediterranean for oil flows to Lebanon, Egypt and Turkey. In addition, significant quantities were in floating storage on vessels in the open sea in the Mediterranean, which could be a major risk for the environment,??? according to CEO Rob Nijst. It appears that storage in Cyprus is not only a commercial advantage but has some environmental benefits too. </p>
                <p>This obviously would apply if there was a serious accident off the shores of Cyprus, which could prove catastrophic for Cyprus??™ beaches and tourism, but onshore storage could also reduce future potential damage to the environment and wildlife, which is most definitively, positive progress.</p>
            </div>
            <div class="col-md-4" data-sr="wait 1s  opacity 0 enter right scale down 20%">
                <div class="for-img content-data-img" data-key="img-2">
                    <img src="http://lifeandluxury.com/uploads/articles/content/19a2643437bed4ab0ec0dab841347bdd.jpg">
                </div>
            </div>
        </div>
    </div>
</div>

<div class="item item-col-3">
    <div class="container-fluid">
        <div class="row-fluid">
            <div class="col-md-4 col-up" data-sr="wait 0.2s vFactor 0.3 enter bottom scale up 40%">
                <h4>GENERATING REVENUE</h4>

                <p style="padding-bottom: 0">As well as creating around 40 full time new jobs in service provision to support terminal and shipping activities, it is estimated that the facility will generate in the region of 10 to 18 million per year via port dues and fees - which is impressive.</p>

                <img class="img-bordered" src="/front/img/t9/event.jpg" alt="">
            </div>
            <div class="col-md-5" data-sr="vFactor 0.3 enter top scale down 30%">
                <div class="for-img content-data-img" data-key="img-3">
                    <img src="http://lifeandluxury.com/uploads/articles/content/3f80650117b3428f66785756d532501a.jpg">
                </div>
            </div>
            <div class="col-md-3" data-sr="wait 0.5s vFactor 0.3 opacity 0 enter right scale down 20%">
                <h4>CREATING BENEFITS</h4>
                <p>Cypriot President Nicos Anastasiades proffered these benefits at the inaugural ceremony, ???There is also the opportunity for storage for Cyprus??™ home fuel markets and the possibility of repatriating stores from outside the island. It is important to say that the cost of storing the national stocks in these facilities will remain at the same level, while, at the same time, an opportunity has been created to repatriate the reserves we kept abroad, securing a strategic advantage in case of a sudden energy crisis.???</p>
                <p>In effect, despite its boost to employment and other local benefits, this new storage opportunity would seem not to automatically translate into a price reduction for domestic users, as many consumers had hoped. Compulsory stocks are currently held abroad in Greece and Malta, as well as locally in Cyprus, at a cost the 17 million per year.</p>
            </div>
        </div>
    </div>
</div>

<div data-sr>
    <div class="item item-success content-data-bg" data-key="img-4">
        <div class="block-table">
            <div class="block-cell" data-sr="wait 0.5s enter left">
                <div class="item-success-text">
                    <h5>A SUCCESSFUL PROJECT?</h5>
                </div>
            </div>
            <div class="block-cell" data-sr="wait 0.5s enter right">
                <div class="item-success-text">
                    <p>Very much so, it would appear. VTT Vasiliko has received all Health Safety and Environmental, and Environmental Impact Assessment certificates required for an oil storage facility of this kind. It is a state of the art facility, which has used modern technology in the construction of its storage tanks, to meet the needs of its potential customers. The tanks are made from a mix of mild steel and, are mild steel coated as well as mild steel insulated; this makes them suitable for holding gasoline, jet fuel, kerosene, naphtha, gasoil, diesel, fuel oil and petrochemicals.</p>
                </div>
            </div>
        </div>
        <img src="http://lifeandluxury.com/uploads/articles/content/4ca406262fc2c657db24e237764ad8d9.jpg" alt="" />
    </div>
</div>

<div class="item item-col-3">
    <div class="container-fluid">
        <div class="row-fluid">
            <div class="col-md-4" data-sr="wait 0.8s enter left scale down 30%">
                <h4>FUTURE DEVELOPMENT</h4>
                <p>It is believed that the new facility will attract more tankers as well as tankers with higher holding capacities. Should recent discoveries of natural gas and hydrocarbon resources come to fruition, there is also the possibility of export. Current discussions taking place about the upgrading of the Suez Canal to allow ships of larger capacities to pass through it, could also affect the landscape of the international energy markets, resulting in new business at Vasiliko. 
                </p><p>
                    Up until recently, 250 trans-shipments per year were taking place in the open sea around Cyprus, but this is expected to change dramatically with the marine jetty now in use. To cope with the projected increased in demand, a phase 2 expansion plan is currently under evaluation, and this would create:
                </p>
                <blockquote>
                    <p>Additional Tanks: 13</p>
                    <p>Additional Capacity: 305,000 m3.</p>
                </blockquote>
            </div>
            <div class="col-md-8" data-sr="wait 0.3s enter top scale up 50%">
                <div class="for-img content-data-img" data-key="img-5">
                    <img src="http://lifeandluxury.com/uploads/articles/content/78305ded184eb9510404ee57ed935cb4.jpg">
                </div>
            </div>
        </div>
    </div>
</div>