<div class="s s-0" data-sr="enter bottom move 100px spin 90deg roll 45deg over 1s wait 0.2s">
    <div class="block-table">
        <div class="block-cell text-cell">
            <div class="text" data-sr="enter top move 100px flip 180deg over 0.5s wait 0.4s">
                <p style="text-transform: uppercase">
                    Wrapping fingers<br>
                    around your cup of morning<br>
                    coffee, you stare nonchalantly at<br> 
                    the calendar. <br>
                </p>

                <p style="padding: 0 5% 30px;">
                    <img src="/front/img/t15/c1.jpg" alt="">
                </p>

                <p>
                    <span class="special">Two months before your</span><br>
                    <span class="special">most favourite holiday of</span><br>
                    <span class="special">the entire year.</span><br>
                </p>
            </div>
        </div>

        <div class="block-cell img-cell"><div class="for-img"><img src="/front/img/t15/1.jpg" alt=""></div></div>
    </div>
</div>