
<div class="item item-1">
    <img src="http://lifeandluxury.com/uploads/articles/content/a2caf43c5703221f1c893f8ec89e15cb.png" data-img="img-1" alt="" width="100%">
    <div class="block-content">
        <div class="text bordered" data-sr="scale up 25% move 500px enter top">
            <p>As winter tightens her icy grasp and the air grows crisp with frost, memories of itsy bitsy bikinis, slinky sarongs and sun-kissed skin seem long forgotten. Yet, the festive months needn’t be bleak. From silk scarves to soft shearling, fabulous faux fur to liquid leather, bundle up in style this winter with the hottest accessories and must have pieces to brighten up your wardrobe. As winter tightens her icy grasp and the air grows crisp with frost, memories of itsy bitsy bikinis, slinky sarongs and sun-kissed skin seem long forgotten. Yet, the festive months needn’t be bleak. From silk scarves to soft shearling, fabulous faux fur to liquid leather, bundle up in style this winter with the hottest accessories and must have pieces to brighten up your wardrobe.</p>
        </div>
    </div>
</div>

<div class="item item-2" data-sr="">
    <div class="for-img" data-sr="scale up 25% move 500px enter top"><img src="http://lifeandluxury.com/uploads/articles/content/519cbbb2d4008b485ecfe73c2f663dc2.png" data-img="img-2" alt=""></div>
</div>

<div class="item item-3">
    <img src="http://lifeandluxury.com/uploads/articles/content/de018340a98c88fa4cf6336925de55a1.png" data-img="img-3" alt="" width="100%">
    <div class="block-content">
        <div class="text" data-sr="scale up 25% move 500px enter top">
            <p>Originally a staple item of the male middle-class wardrobe, polo-necks grew in popularity among both sexes when they hit the Hollywood scene, with the likes of Greto Garbo and Audrey Hepburn donning the ‘tight turtleneck’ to emphasise their figures. Comfortable, flattering and elegant, the garment can be teamed with trousers, skirts or leggings and layered to add style. Whether worn casually or formally, the cosy polo/turtleneck is a must have winter piece for any fashionista.</p>
        </div>
        <div style="position: relative;" data-sr="spin -90deg">
            <div class="bordered">
                <img src="http://lifeandluxury.com/uploads/articles/content/6d22539f20bfd6e5f3eae480d16df8fe.jpg" data-img="img-4" alt="">
            </div>
        </div>
    </div>
</div>

<div class="item item-4">
    <img src="http://lifeandluxury.com/uploads/articles/content/cba953740a9232e37fb4818376d6d460.png" data-img="img-5" alt="" width="100%">
    <div class="block-content">
        <div class="text bordered" data-sr="scale up 25% move 500px enter top">
            <p>No accessory quite transforms an outfit like the wonderfully versatile scarf. Classy, functional and absolutely essential for winter, the scarf is a favourite of many celebrities, whether worn in the traditional way around the neck, or used as a hair accessory. A year-round hero layer, choose from chunky knits, elegant silks or painterly prints to brighten up simple looks and add warmth and texture.</p>

        </div>
    </div>
</div>

<div class="item item-5">
    <img src="http://lifeandluxury.com/uploads/articles/content/31d74d364a5be51ec6162c324d728796.png" data-img="img-6" alt="" width="100%">
    <div class="block-content">
        <div class="text bordered" data-sr="scale up 25% move 500px enter top">
            <p>Add a touch of glamour to that thick heavy coat or drab jumper with a sprinkling of faux fur. First introduced in 1929, the use of faux/fake fur has increased in popularity due to the promotion of animal rights and is supported by fashion design labels such as Ralph Lauren and Chanel. Available in a range of styles and trends, have fun with a set of furry ear muffs or go classic with an elegant stole or glamorous gilet. Warm, soft and oh so cosy, make a statement like Catherine Deneuve and fake it ‘till you make it.</p>
        </div>
    </div>
</div>

<div class="item item-2">

    <div class="for-img" data-sr="scale up 25% move 500px enter top"><img src="http://lifeandluxury.com/uploads/articles/content/52c4e22488f10be6e01b4b754fb22e07.png" data-img="img-7" alt=""></div>
</div>


