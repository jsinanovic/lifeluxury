<div class="item" data-sr="enter left">
    <div class="item-img-right content-data" data-key="img-1"><img src="http://lifeandluxury.com/uploads/articles/content/a1595cd69725bc611056fb740b95ff61.jpg"></div>
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-offset-1 col-md-4 text-col">
                <div class="article-view-category" data-sr="wait 0.3s enter top move">
                    Rendez Vouz  |  <span>Life &amp; Luxury Magazine</span>
                </div>

                <div data-sr="wait 0.5s enter right">
                    <h2>SIGHTS SET ON THE MONACO YACHT SHOW</h2>
                    <p>
                        Home to some of the richest and most influential players on the world 
                        stage, the tiny principality of Monaco on the French Riviera has long served as a haven of opulence for the cream of society. The heady mix of dazzling sunshine, delicious cuisine and decadent lifestyle, make Grace KellyвЂ™s adopted home the perfect site for a luxury rendezvous of the international elite. 
                    </p>
                </div>
            </div>
        </div>
    </div>
</div>