<div data-sr="hustle 50px spin 90deg">
    <div class="item item-bg-left content-data content-data-bg" data-key="img-6">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-offset-8 col-md-3 text-col" data-sr="wait 0.5s enter bottom spin 90deg">
                    <p>Ooozing with glamour and brimming with philanthropic goodness,the Monaco Yacht Show is the epitome of celebrity chic, seducing the rich and famous all over the world with its intoxicating mix of nautical genius and luxury service all set in the iconic setting of the Grimaldi playground. A visit to the show is a must for yachting enthusiasts and business people alike and of course for any of those who believe size does matter.  The 25th anniversary of The Monaco Yacht Show takes place from 23rd to 26th September 2015 at Port Hercules, Monte Carlo, Monaco.</p>
                </div>
            </div>
        </div>
        <img src="http://lifeandluxury.com/uploads/articles/content/7c2eefe1140b77b8d5776a425458ae39.png"></div>
</div>