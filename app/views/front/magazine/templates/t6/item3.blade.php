<div class="item item-doubled" data-sr="vfactor 0.5 enter left">
    <div class="container">
        <div class="item-num " data-sr="wait 0.3s enter top">
            <span>03</span>
        </div>

        <div class="block-table">
            <div class="block-cell" data-sr="wait 0.5s opacity 0.5 spin 90deg">
                <p>In theory, we all want what is best for the planet, unless of course, it causes us undue inconvenience; and there is little doubt that less consumption of products with a higher quality would mean that the environmental impact of consumer consumption would have considerably less long-term damaging effects. But the interesting question is: would consumers of the luxury goods and services, be willing to pay extra for  goods which are already expensive, if it was proved that doing so, was beneficial to the environment? </p>
            </div>
            <div class="block-cell">
                <div class="special-img content-data-bg" data-key="img-5"><img src="http://lifeandluxury.com/uploads/articles/content/eb450edcf903eab2ec385f5577a11e0c.png"></div>
                <p><span style="color: rgb(255, 255, 255);">Obviously for this concept to work there must not be any perceivable drop in the quality of the product; and customers are much more likely to embrace the idea if there is a perceived enhancement of some sort within the new product. Increased sustainability offers that very incentive. If products are seen to be of better quality, higher durability or improved design, then the criteria of ‘sustainable luxury’ is not only ethical and functional, but mostly importantly, it becomes desirable - and desirability is the golden chalice of luxury consumerism.</span></p>
            </div>
        </div>
    </div>
</div>