<div class="item item-doubled" data-sr="vfactor 0.5 enter left">

    <div class="container">
        <div class="item-num " data-sr="wait 0.3s enter top">
            <span>01</span>
            <div class="article-view-category">
                Lifestyle  |  <span>Life &amp; Luxury Magazine</span>
            </div>
        </div>

        <div class="block-table">
            <div class="block-cell" data-sr="wait 0.5s opacity 0.5 spin 90deg">
                <p>The idea of new products being designed to be both luxurious and sustainable maybe seem to be  antithetical in nature and the yet the concept of ‘positive luxury’ is rapidly  becoming the newest marketing strategy in the luxury products industry. So is this new development a real phenomenon or merely a cynical manipulation of the modern market? Perhaps it is an attempt to make the consumers of luxury goods feel less guilty about their conspicuous consumption during the current global economic down turn; or could this new trend actually be more about values, respect and brand image?</p>
            </div>
            <div class="block-cell">
                <div class="special-img content-data-bg" data-key="img-1">
                    <img src="http://lifeandluxury.com/uploads/articles/content/0504ad92f8b324a6de7485510e85c151.png">
                </div>
            </div>
        </div>
    </div>
</div>