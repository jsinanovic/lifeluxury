<div class="item" data-sr="scale down 20%">
    <div class="item-img-left content-data"><img src="{{URL::to('front/img/add.jpg')}}" /></div>
    <div class="container">
        <div class="row">
            <div class="col-md-offset-7 col-md-5 text-col">
                <div class="article-view-category" data-sr>
                    Rendez Vouz | Life &amp; Luxury Magazine
                    <hr />
                </div>

                <div data-sr="vFactor 0.1 wait 0.5s and then enter right and move 100px and scale up 20%">
                    <h2>Heading</h2>
                    <p>Paragraph 1</p>
                    <p>Paragraph 2</p>
                </div>
            </div>
        </div>
    </div>
</div>