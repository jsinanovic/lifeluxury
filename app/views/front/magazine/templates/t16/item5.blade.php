<div class="s s-5">
    <div class="for-img" data-sr="move 100px roll 30deg hustle 200px over 0.5s wait 0.2s">
        <img src="http://lifeandluxury.com/uploads/articles/content/75c08147345d418dfbbc73f52e0f5624.jpg" data-img="img-5" alt="">
    </div>

    <div class="text" data-sr="move 100px roll -30deg hustle 200px over 0.5s wait 0.2s">
        <p>No longer grey and gloomy, the festive months are bursting with colour and the sweet smell of new opportunity mingles with the aromas of roaring log fires and intoxicating mulled wine. <br>Whether pushing the boundaries of science, excelling in the work place, or entertaining high society, let Life &amp; Luxury be your guide this winter and live a life less ordinary by going </p>
        <img src="/front/img/t16/7.jpg" alt="">
    </div>
</div>