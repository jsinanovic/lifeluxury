<div class="s s-3">

    <div class="for-img" data-sr="enter top move 100px over 0.5s wait 0.2s">
        <img src="http://lifeandluxury.com/uploads/articles/content/07b65528a17425474b6fe41732e32fb6.jpg" data-img="img-3" width="100%" alt="">
    </div>

    <div class="cols">
        <div class="half">
            <div class="text" data-sr="enter left move 100px over 0.5s wait 0.4s">
                <p>what we once thought man was capable of, NASA continue to push the boundaries of scientific experimentation with the New Horizons mission to Pluto. The voyage of discovery is sure to unlock further secrets about our solar system and raise more questions about the mystery of the galaxy. </p>
            </div>
        </div>
        <div class="half" data-sr="enter right move 100px over 0.5s wait 0.6s">
            <div class="text">
                <p>Back on Earth there is no question about the capabilities of successful women with the Russian Business Woman of The Year Awards, highlighting the huge economic contribution that outstanding female individuals are making to our society.</p>
            </div>
        </div>
    </div>
</div>