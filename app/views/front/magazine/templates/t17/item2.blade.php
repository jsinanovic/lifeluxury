<div class="s s-2 clearfix content-data-bg" data-key="img-2">

    <div class="line line-2" data-sr="enter right move 300px"><div class="circle"></div></div>

    <div class="for-img for-img-b" data-sr="enter bottom move 300px  wait 0.3s">
        <img src="http://lifeandluxury.com/uploads/articles/content/dc2e429cdec9b9eea58881dde3a7d56b.png" data-img="img-3" alt="">
    </div>

    <div class="text text-b" data-sr="enter right 300px wait 0.6s">
        <p>Founded in 2006 by Janice Ruffle, the award is the only one of its kind in Cyprus with a mission to recognise achievement and applaud female business success. The awards help to recognise, support and promote inspirational women who have overcome obstacles in setting up a business and progressed positively and pro-actively operating as a business owner, top professional or community leader.</p>
    </div>
    <img src="http://lifeandluxury.com/uploads/articles/content/43187189499f0befdafc52735f1649f0.jpg">
</div>