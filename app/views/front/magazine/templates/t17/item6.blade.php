<div class="s s-6 clearfix">

    <div class="line line-4" data-sr="enter right move 300px"><div class="circle"></div></div>

    <div class="for-img" data-sr="enter left 300px wait 0.6s">
        <img src="http://lifeandluxury.com/uploads/articles/content/8ff5d0aa49681322c80c5ab255a0ec64.jpg" data-img="img-8" alt="">
    </div>

    <div class="text" data-sr="enter bottom 300px wait 0.3s">
        <p>The spotlight of the evening was on Olesya Nikolskaya as she won the Top Professional category award and also brought home the Russian Business Woman of the Year Award Title. Successful category winners were Valeria Michael as 'Entrepreneur of the Year' and Mila Levinskaya in 'Community and Culture'. Of course the six finalists do not go home empty handed and are each presented with a branded prizes as well as recognition from the most prestigious event in the Cyprus business calendar. The event continues to gain widespread media coverage and helps to propel female-led business into the spotlight. Inspirational influential and another step forward for feminism and equality. The Russian Business Woman Awards is sure to gain even more success in the coming years.</p><p> 
        </p>
    </div>
</div>