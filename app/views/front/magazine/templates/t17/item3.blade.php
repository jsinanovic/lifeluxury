<div class="s s-3 clearfix content-data-bg" data-key="img-4">

    <div class="line line-2" data-sr="enter right move 300px"><div class="circle"></div></div>

    <div class="for-img for-img-b" data-sr="enter bottom 300px wait 0.3s">
        <img src="http://lifeandluxury.com/uploads/articles/content/257ed03a6df19aa2a5ce1ed3278b38c5.jpg" data-img="img-5" alt="">
    </div>

    <div class="text text-b" data-sr="enter right 300px wait 0.6s">
        <p>Nominations require approval from the nominee or a direct nomination from a female entrepreneur. From those nominations six finalists are elected and forwarded to the judging panel for a personal interview. The judging panel is elected from candidates who have served the Cyprus commercial community and often includes previous winners.</p>
    </div>
    <img src="http://lifeandluxury.com/uploads/articles/content/2175617c276f2bfd29503f29f6acd1fa.jpg">
</div>