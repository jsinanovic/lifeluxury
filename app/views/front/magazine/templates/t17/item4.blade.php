<div class="s s-4 clearfix">
    <div class="for-img">
        <img src="http://lifeandluxury.com/uploads/articles/content/bcb9e71d7d8bcdbaa287ef8d80e96392.jpg" data-img="img-6" alt="" data-sr="enter top 300px wait 0.3s">
    </div>

    <div class="text" data-sr="enter right 300px wait 0.6s">
        <p>This year the awards ceremony was hosted at the Hilton Hotel in Nicosia and presided over by three judges; Olga Balakleets is this year’s main judge and ambassador, she is CEO of Ensemble Productions and won last years Russian Business Woman of the Year Award; second judge Anna Homenko is Managing Director of Fiduciana Trust Limited; the final judge is Svetlana Kalogera who is Vice President and Head of Retail and Corporate Business Development at RCB Bank Ltd.</p><p> </p>
    </div>

    <div class="line line-3" data-sr="enter left move 300px"><div class="circle"></div></div>
</div>