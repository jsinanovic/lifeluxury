<div class="s row row-4">
    <div class="col-md-6">
        <div class="img-holder">
            <img src="http://lifeandluxury.com/uploads/articles/content/462038a4f9b75b28f7ac71e23a4d410b.jpg" data-img="img-4" alt="">
        </div>
    </div>
    <div class="col-md-6">
        <div class="text" data-sr="wait 0.5s, ease-in-out 100px, scale down 25%">
            <h2>SKI DOWN WITH STYLE</h2>
            <h3>ST MORITZ</h3>
            <i class="define-line"></i>
            <p>
                For winter sport aficionados, St. Moritz is the place 
                to be. Famous for its world-class elegance, exclusivity
                and cosmopolitan ambiance, St. Moritz has much more to 
                offer than its wondrous ski slopes and perfect powder snow.
            </p>
        </div>
    </div>
</div>