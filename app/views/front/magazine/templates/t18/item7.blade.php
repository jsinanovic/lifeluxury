<div class="s row row-9">
    <div class="col-md-5 col-md-offset-1">
        <div class="text" data-sr="wait 0.5s, ease-in-out 100px, scale down 25%">
            <p>
                Don’t forget to keep yourself warm with hot cocoa from
                Bosco Bar while strolling down the elegant fashion paradise
                of Russian Street. Whether it is December or January, winter
                is always beautiful enough to be celebrated.   
            </p>
        </div>
    </div>
    <div class="col-md-6">
        <div class="img-holder" data-sr="flip -20deg, spin -20deg, hustle 40px">
            <img src="http://lifeandluxury.com/uploads/articles/content/134bb133f8a4027ecb8f946166f036ae.jpg" data-img="img-9" alt="">
        </div>
    </div>
</div>