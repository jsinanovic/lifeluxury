<div class="s row row-3">
    <div class="col-md-5 col-md-offset-1">
        <div class="text" data-sr="wait 0.5s, ease-in-out 100px, scale down 25%">
            <h2>TRACING TRADITIONS</h2>
            <h3>DRESDEN</h3>
            <i class="define-line"></i>
            <p>
                Standing in front of the dreamy Elbe River and the Frauenkirche
                is one of Germany’s greatest and no doubt oldest Christmas Market.
                That is, Striezelmarkt embodies a great history of this festive 
                season with wooden ornaments, Christmas pyramids, nutcrackers, the
                smoking man, mulled wine and the famous Dresden Stollen. 
            </p>
            <p>
                After having greeted Father Christmas, celebrate the event by
                treating yourself to a gastronomically enriching dinner at either
                the charming Caroussel, the characterful bean &amp; beluga, or the
                contemporary Elements, all of which represent the one-star-Michelin
                pride of the city. Childhood traditions mixed with sensible enjoyment,
                now that what Christmas is all about.
            </p>
        </div>
    </div>
    <div class="col-md-6">
        <div class="img-holder" data-sr="flip -20deg, spin -20deg, hustle 40px">
            <img src="http://lifeandluxury.com/uploads/articles/content/319a4688a23e0a99202a8ee7479fc275.jpg" data-img="img-3" alt="">
        </div>
    </div>
</div>