<div class="s row">
    <div class="col-md-5 col-md-offset-1">
        <div class="text" data-sr="wait 0.5s, ease-in-out 100px, scale down 25%">
            <h2>MERRY SECOND CHRISTMAS</h2>
            <h3>MOSCOW</h3>
            <i class="define-line"></i>
            <p>
                Celebrations never seize if you know where and when 
                to toast. In this beautiful Orthodox capital, Ded Moroz – 
                Grandfather Frost – and Snegurochka – Snow Maiden – will visit on 7th January.  
            </p>
        </div>
    </div>
    <div class="col-md-6">
        <div class="img-holder" data-sr="flip -20deg, spin -20deg, hustle 40px">
            <img src="http://lifeandluxury.com/uploads/articles/content/b997b6de7fb9689d30df2c34d0950f4f.jpg" data-img="img-7" alt="">
        </div>
    </div>
</div>