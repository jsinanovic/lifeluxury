<div class="s">
    <div class="row">
        <div class="col-md-6">
            <div class="text" data-sr="wait 0.5s, ease-in-out 100px, scale down 25%">
                <p>
                    For winter sport aficionados, St. Moritz is the place 
                    to be. Famous for its world-class elegance, exclusivity
                    and cosmopolitan ambiance, St. Moritz has much more to 
                    offer than its wondrous ski slopes and perfect powder snow.
                </p>
            </div>
        </div>
        <div class="col-md-6">
            <div class="img-holder" data-sr="flip -20deg, spin -20deg, hustle 40px">
                <img src="http://lifeandluxury.com/uploads/articles/content/e05cd24bf8816e19604eae559c0b644c.jpg" data-img="img-5" alt="">
            </div>
        </div>
    </div>



    <div class="s row row-6">
        <div class="col-md-6">
            <div class="img-holder" data-sr="flip -20deg, spin -20deg, hustle 40px">
                <img src="http://lifeandluxury.com/uploads/articles/content/585be1e18dfabdb0b74ca9a3956cc2b6.jpg" data-img="img-6" alt="">
            </div>
        </div>
        <div class="col-md-6">
            <div class="text" data-sr="wait 0.5s, ease-in-out 100px, scale down 25%">
                <h2>A “HYGGELIGHT” HOLIDAY</h2>
                <h3>COPENHAGEN</h3>
                <i class="define-line"></i>
                <p>
                    In Danish, “hygge” stands for the creation of a warm 
                    atmosphere and the enjoyment of good things in life 
                    with good people. And that is exactly what you can do
                    in this far north capital. 
                </p>
                <p>
                    From the delightful Christmas in Tivoli Gardens and 
                    the two-star-Michelin trios Restaurant AOC, noma and 
                    Restaurant Genarium to Copenhagen’s Best Wine Bar 2015 
                    Ravnsborg Vinbar and the stylish Royal Copenhagen Christmas 
                    Tables exhibition where ideas for your Christmas table 
                    setting assembled by various artists and celebrities are 
                    found, there are chances aplenty for a truly “hyggeligt” 
                    vacation filled with life’s wonders and shared with your dearest.
                </p>
            </div>
        </div>
    </div>

</div>