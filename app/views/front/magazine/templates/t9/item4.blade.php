<div data-sr>
    <div class="item item-success content-data-bg" data-key="img-4">
        <div class="block-table">
            <div class="block-cell" data-sr="wait 0.5s enter left">
                <div class="item-success-text">
                    <h5>A SUCCESSFUL PROJECT?</h5>
                </div>
            </div>
            <div class="block-cell" data-sr="wait 0.5s enter right">
                <div class="item-success-text">
                    <p>Very much so, it would appear. VTT Vasiliko has received all Health Safety and Environmental, and Environmental Impact Assessment certificates required for an oil storage facility of this kind. It is a state of the art facility, which has used modern technology in the construction of its storage tanks, to meet the needs of its potential customers. The tanks are made from a mix of mild steel and, are mild steel coated as well as mild steel insulated; this makes them suitable for holding gasoline, jet fuel, kerosene, naphtha, gasoil, diesel, fuel oil and petrochemicals.</p>
                </div>
            </div>
        </div>
        <img src="http://lifeandluxury.com/uploads/articles/content/4ca406262fc2c657db24e237764ad8d9.jpg" alt="" />
    </div>
</div>