<section class="{{isset($type) && $type == 'static'? 'section-contents-static' : ''}}"
         style="">

    <section class="" id="advertisment-area" style="position:relative;">
        <div class="advertisement-img-container" style="background-color: #ffffff;">
        </div>
        <div class="container">
            <div class="row" style="position: relative; margin-top: 20px; margin-bottom: 100px;">
                <div class="">
                    <div class="col-md-12 article-related-title">
                        Related
                    </div>
                    @foreach($relatedArticles as $article)
                        <div class="col-md-4 test">
                            <div class="category-article-container">
                                <div class="row">
                                    <div class="col-md-12 article-related-item-container">
                                        <img src="{{$article->getImage('minithumb')}}" alt="{{$article->name}} thumb" width="100%" height="auto"/>
                                        <div class="article-related-item-desc">
                                            <div class="article-related-item-content">
                                                <div class="row" style="width: 100%;">
                                                    <div class="col-md-12 article-related-item-title">{{$article->name}}</div>
                                                    <div class="col-md-12 article-related-item-discover">DISCOVER NOW<i class="fa fa-angle-right" style="margin-left: 10px;" aria-hidden="true"></i></div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                </div>
                            </div>
                        </div>
                    @endforeach
                </div>
            </div>
        </div>
    </section>

    <!-- ADVERTISEMENT AREA -->

    @include('front.partials.advertisment-area')

   <!-- NEWSLETTER -->

    <div style="position:relative;">
        <div class="newsletter-img-container" style="height: 427px;">
            <img  src="/front/img/newsletter/welcome-newsletter-background.png">
        </div>
        <div id="newsletter-content" class="container newsletter-content">
            <div class="row">
                <div class="col-md-12">
                    <div class="newsletter-content-title">
                        Be part of our Universe!
                    </div>
                    <div class="newsletter-content-text">
                        Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula <br> eget dolor. Aenean massa. Cum sociis natoque
                    </div>
                    <div id="join-newsletter-btn" class="newsletter-join-btn">
                        <span>Join Our Newsletter <i class="fa fa-angle-right" style="margin-left: 20px;" aria-hidden="true"></i></span>
                    </div>
                </div>
            </div>
        </div>

        <div id="newsletter-popup-container">
            <div class="newsletter-popup-container">
                <div class="container">
                    <div class="col-md-8 col-md-offset-2">
                        <div class="row">
                            <div class="col-md-6 col-md-offset-3">
                                <div class="row newsletter-form">
                                    {{Form::open(array('url' => 'subscribe', 'method' => 'POST', 'role' => 'form', 'class' => 'subscribe-form'))}}
                                    <div class="col-md-6 form-group newsletter-popup-name-group">
                                        <input  type="text" name="name" id="subscribe-name" class="form-control nl-form-control"  placeholder="First Name:"  required="required"/>
                                    </div>
                                    <div class="col-md-6 form-group newsletter-popup-surname-group">
                                        <input  type="text" name="surname" id="subscribe-surname" class="form-control nl-form-control"  placeholder="Surname:"  required="required" />
                                    </div>
                                    <div class="col-md-12 form-group">
                                        <input type="email" name="email" id="subscribe-email" class="form-control nl-form-control"  placeholder="E-mail:"  required="required" />
                                    </div>
                                    <div class="col-md-12">
                                        {{Form::select('country', Setting::values('countries'), null, array('id' => 'subscribe-country', 'class' => 'form-control','placeholder' => 'Country', 'required' => 'required'))}}
                                    </div>
                                    <div class="newsletter-popup-join-btn">
                                        <span>Join <i class="fa fa-angle-right" style="margin-left: 20px;" aria-hidden="true"></i></span>
                                    </div>
                                    {{Form::close()}}
                                </div>
                            </div>
                        </div>
                        <div id="newsletter-close-btn">x</div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- FOOTER -->

    <div class="container-fluid footer-container">
        <div class="container">
            <div class="row">
                <div class="col-md-6">
                    <div class="row">
                        <div class="col-md-4">
                            <span class="footer-item">THE COLLECITON</span>
                            <span class="footer-item">PRESS KIT</span>
                            <span class="footer-item">NEWSLETTER</span>
                        </div>
                        <div class="col-md-4">
                            <span class="footer-item">SERVICE PROMISE</span>
                            <span class="footer-item">PRIVACY POLICY</span>
                            <span class="footer-item">TERMS & CONDITIONS</span>
                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="row">
                        <div class="col-md-6 col-md-offset-6">
                        <span class="footer-item footer-item-right">
                                    <a href="https://www.facebook.com/lifeandluxurycollection"><i class="fa fa-facebook" aria-hidden="true"></i></a> &nbsp
                                    <a href="https://twitter.com/LifenLuxury"><i class="fa fa-twitter" aria-hidden="true"></i></a> &nbsp
                                    <a href="https://www.youtube.com/channel/UCePsdWq9xg-EN9E7DGaAuHg"><i class="fa fa-youtube" aria-hidden="true"></i></a> &nbsp
                                    <a href="https://www.instagram.com/your_finest_collection"><i class="fa fa-instagram" aria-hidden="true"></i></a>
                                </span>
                            <span class="footer-item footer-item-right">LIFE & LUXURY COLLECTION</span>
                            <span class="footer-item footer-item-right">ALL RIGHTS RESERVED</span>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

@section('js-plugins')
    @parent
    {{HTML::script('front/plugins/owl-carousel/owl.carousel.min.js')}}
@stop

@section('css-plugins')
    @parent
    {{HTML::style('front/plugins/owl-carousel/owl.carousel.css')}}
    {{HTML::style('front/plugins/owl-carousel/owl.theme.css')}}
@stop
