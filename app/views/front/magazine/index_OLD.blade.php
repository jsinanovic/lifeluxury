@extends('front.layouts.main')

@section('content')
<section class="section-full section-magazine-general">
    
    @include('front.magazine.navbar')

    <div class="slider-magazine">

        <div id="slider-magazine" class="carousel slide" data-ride="carousel" data-interval="false">
            <div class="carousel-inner" role="listbox">
                
                @foreach($articles as $key => $article)
                    @if($article->image)
                        <div class="item {{$key == 0? 'active' : ''}}" style="background-image: url({{$article->getImage()}})">
                            <div class="carousel-caption">
                                <h1>{{{$article->name}}}</h1>
                                <div class="magazine-issues">
                                    {{$article->season()}} issue {{$article->year()}}
                                </div>
                            </div>
                            
                            <a href="{{URL::to('magazine/article/' . $article->slug)}}" class="article-discover">
                                Discover <br><i class="fa line-1">|</i><br><i class="fa line-2">|</i><br><i class="fa line-3">&darr;</i>
                            </a>
                        </div>
                    @endif
                @endforeach
            </div>

            <a class="left carousel-control" href="#slider-magazine" role="button" data-slide="prev"><i class="fa fa-angle-left"></i></a>
            <a class="right carousel-control" href="#slider-magazine" role="button" data-slide="next"><i class="fa fa-angle-right"></i></a>
        </div>

    </div>
</section>

@include('front.magazine.contents')

@stop

@section('css-plugins')
{{HTML::style('css/mcustomscrollbar/jquery.mCustomScrollbar.css', array('async'))}}
@stop

@section('js-plugins')
    {{HTML::script('js/plugins/mcustomscrollbar/jquery.mCustomScrollbar.min.js')}}
@stop

@section('js')
    @parent
    {{HTML::script('front/js/magazine.js')}}
@stop
