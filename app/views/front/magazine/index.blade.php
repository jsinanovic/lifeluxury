@extends('front.layouts.main')


@section('header')
@include('front.partials.header')
@stop

@section('content')
        <!-- END SITE NAVIGATION -->
@include('front.partials.sidebar-left')
        <!-- START POPUP -->
@include('front.partials.sidebar-right')
        <!-- END POPUP -->

<section class="section-full section-top" id="home">
    <div class="container vertical-center">
        <div class="row">
            <div class="">
                <div class="col-md-12 welcome-main-title text-center">
                    {{$category->name}}
                </div>
                <div class="col-md-3 category-articles-back" onclick="history.back()">
                    <i class="fa fa-angle-left" style="margin-left: 5px;" aria-hidden="true"></i>
                </div>
                <div class="col-md-6 welcome-main-desc text-center">
                    LIFE & LUXURY OFFERS THE FINEST COLLECTION OF SERVICES WITH REPRESENTATIVES
                    IN CYPRUS, RUSSIAN FEDERATION AND SWITZERLAND
                </div>
                <div class="col-md-3 category-articles-next">
                        <span>
                            <a href="/randvuaz/category/{{$nextCategory->slug}}" style="font: inherit;">
                                <span>{{$nextCategory->name}}</span>
                                <span><i class="fa fa-angle-right" style="margin-left: 5px;" aria-hidden="true"></i></span>
                            </a>
                        </span>

                </div>
            </div>
            <a href="javascript:void(0);" class="learn-more">
                DISCOVER MORE
                <div class="round-button"></div>
            </a>
        </div>
    </div>
</section>

<section class="" id="category-articles" style="padding-bottom: 40px; background-color:#eeede8;">

    <div id="exTab1" class="container-fluid" style="padding-left: 0px; padding-right: 0px;">
        <ul  class="nav nav-pills" style="text-align: center; padding-top: 10px; padding-bottom: 10px; background-color: #ffffff;">
            <li class="active">
                <a  href="#1a" data-toggle="tab">HOTELS & RESORTS</a>
            </li>
            <li>
                <a class="tab-slash" href="" style="cursor: default;">/</a>
            </li>
            <li><a href="#2a" data-toggle="tab">LOUNGES</a>
            </li>
            <li>
                <a class="tab-slash" href="" style="cursor: default;">/</a>
            </li>
            <li><a href="#3a" data-toggle="tab">CLUBS</a>
            </li>
            <li>
                <a class="tab-slash" href="" style="cursor: default;">/</a>
            </li>
            <li><a href="#4a" data-toggle="tab">RESTAURANTS</a>
            </li>
        </ul>

        <div class="container tab-content" style="padding-top: 80px;">
            {{--<div class="tab-pane active" id="1a">--}}
            {{--<h3>Content's background color is the same for the tab</h3>--}}
            {{--</div>--}}
            {{--<div class="tab-pane" id="2a">--}}
            {{--<h3>We use the class nav-pills instead of nav-tabs which automatically creates a background color for the tab</h3>--}}
            {{--</div>--}}
            {{--<div class="tab-pane" id="3a">--}}
            {{--<h3>We applied clearfix to the tab-content to rid of the gap between the tab and the content</h3>--}}
            {{--</div>--}}
            {{--<div class="tab-pane" id="4a">--}}
            {{--<h3>We use css to change the background color of the content to be equal to the tab</h3>--}}
            {{--</div>--}}
            {{--</div>--}}


            <div class="tab-pane active" id="1a" style="clear: both;">
                <div class="row masonry1-container">
                    @foreach($articles as $key => $article)
                        <div class="col-md-4 item1">
                            <div class="category-article-container">
                                <div class="row">
                                    <div class="col-md-12">
                                        <img src="{{$article->getImage('minithumb')}}" alt="{{$article->name}} thumb" width="100%" height="auto"/>
                                    </div>
                                    <div class="col-md-12">
                                        <div class="category-article-content-container">
                                            <div class="row">
                                                <div class="col-md-12">
                                                    <div class="category-article-category-title">
                                                        {{$category->name}}
                                                    </div>
                                                </div>
                                                <div class="col-md-12">
                                                    <div class="category-article-article-title">
                                                        {{$article->name}}
                                                    </div>
                                                </div>
                                                <div class="col-md-12">
                                                    <div class="category-article-article-desc">
                                                        Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.
                                                    </div>
                                                </div>
                                                <div class="col-md-12">
                                                    <div class="category-article-read-more">
                                                        <a href="/randvuaz/article/{{$article->slug}}">READ MORE <i class="fa fa-angle-right" style="margin-left: 5px;" aria-hidden="true"></i></a>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    @endforeach
                </div>
            </div>

            <div class="tab-pane" id="2a" style="clear: both;">
                <div class="row masonry2-container">
                    @foreach($articles as $key => $article)
                        <div class="col-md-4 item2">
                            <div class="category-article-container">
                                <div class="row">
                                    <div class="col-md-12">
                                        <img src="{{$article->getImage('minithumb')}}" alt="{{$article->name}} thumb" width="100%" height="auto"/>
                                    </div>
                                    <div class="col-md-12">
                                        <div class="category-article-content-container">
                                            <div class="row">
                                                <div class="col-md-12">
                                                    <div class="category-article-category-title">
                                                        {{$category->name}}
                                                    </div>
                                                </div>
                                                <div class="col-md-12">
                                                    <div class="category-article-article-title">
                                                        {{$article->name}}
                                                    </div>
                                                </div>
                                                <div class="col-md-12">
                                                    <div class="category-article-article-desc">
                                                        Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.
                                                    </div>
                                                </div>
                                                <div class="col-md-12">
                                                    <div class="category-article-read-more">
                                                        <a href="/randvuaz/article/{{$article->slug}}">READ MORE <i class="fa fa-angle-right" style="margin-left: 5px;" aria-hidden="true"></i></a>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    @endforeach
                </div>
            </div>

            <div class="tab-pane" id="3a" style="clear: both;">
                <div class="row masonry3-container">
                    @foreach($articles as $key => $article)
                        <div class="col-md-4 item3">
                            <div class="category-article-container">
                                <div class="row">
                                    <div class="col-md-12">
                                        <img src="{{$article->getImage('minithumb')}}" alt="{{$article->name}} thumb" width="100%" height="auto"/>
                                    </div>
                                    <div class="col-md-12">
                                        <div class="category-article-content-container">
                                            <div class="row">
                                                <div class="col-md-12">
                                                    <div class="category-article-category-title">
                                                        {{$category->name}}
                                                    </div>
                                                </div>
                                                <div class="col-md-12">
                                                    <div class="category-article-article-title">
                                                        {{$article->name}}
                                                    </div>
                                                </div>
                                                <div class="col-md-12">
                                                    <div class="category-article-article-desc">
                                                        Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.
                                                    </div>
                                                </div>
                                                <div class="col-md-12">
                                                    <div class="category-article-read-more">
                                                        <a href="/randvuaz/article/{{$article->slug}}">READ MORE <i class="fa fa-angle-right" style="margin-left: 5px;" aria-hidden="true"></i></a>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    @endforeach
                </div>
            </div>

            <div class="tab-pane" id="4a" style="clear: both;">
                <div class="row masonry4-container">
                    @foreach($articles as $key => $article)
                        <div class="col-md-4 item4">
                            <div class="category-article-container">
                                <div class="row">
                                    <div class="col-md-12">
                                        <img src="{{$article->getImage('minithumb')}}" alt="{{$article->name}} thumb" width="100%" height="auto"/>
                                    </div>
                                    <div class="col-md-12">
                                        <div class="category-article-content-container">
                                            <div class="row">
                                                <div class="col-md-12">
                                                    <div class="category-article-category-title">
                                                        {{$category->name}}
                                                    </div>
                                                </div>
                                                <div class="col-md-12">
                                                    <div class="category-article-article-title">
                                                        {{$article->name}}
                                                    </div>
                                                </div>
                                                <div class="col-md-12">
                                                    <div class="category-article-article-desc">
                                                        Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.
                                                    </div>
                                                </div>
                                                <div class="col-md-12">
                                                    <div class="category-article-read-more">
                                                        <a href="/randvuaz/article/{{$article->slug}}">READ MORE <i class="fa fa-angle-right" style="margin-left: 5px;" aria-hidden="true"></i></a>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    @endforeach
                </div>
            </div>

        </div>
    </div>
</section>


<section id="welcome-5-footer" style=" position:relative;">

    @include('front.partials.footer')

</section>

@stop

@section('js-plugins')
    {{HTML::script('js/plugins/masonry/masonry.pkgd.min.js')}}
    {{--<script src="https://unpkg.com/masonry-layout@4/dist/masonry.pkgd.min.js"></script>--}}
@stop
@section('js')

    <script>
//        $(window).load(function() {
//            var $container1 = $('.masonry1-container');
//            $container1.masonry({
//                columnWidth: '.item1',
//                itemSelector: '.item1'
//            });
//            var $container2 = $('.masonry2-container');
//            $container2.masonry({
//                columnWidth: '.item2',
//                itemSelector: '.item2'
//            });
//            var $container3 = $('.masonry3-container');
//            $container3.masonry({
//                columnWidth: '.item3',
//                itemSelector: '.item3'
//            });
//            var $container4 = $('.masonry4-container');
//            $container4.masonry({
//                columnWidth: '.item4',
//                itemSelector: '.item4'
//            });
//        });
    </script>

@stop
