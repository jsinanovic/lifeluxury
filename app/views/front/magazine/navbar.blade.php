<nav class="mn">

    <div class="mn-control">
        <div class="mn-btn mn-toggle">
            <i class="fa fa-navicon"></i>
        </div>
        <div class="mn-share">

            <div class="toggle-content">
                @foreach($settings['social-links'] as $key => $link)
                <a href="{{{$link}}}" target="_blank" class="mn-btn"><i class="fa fa-{{ isset($socialOrder[$key])? $socialOrder[$key] : 'question'}}"></i></a>
                @endforeach
            </div>

            <div class="mn-btn toggle-btn">
                <i class="fa fa-share-alt"></i>
            </div>

        </div>
    </div>

    <div class="mn-items">
        <div class="mn-item-categories">
            <a href="javascript:void(0);" class="mn-item contents-open">All</a>
            @foreach($categories as $category)
            <div class="mn-panel">
                <a href="javascript:void(0);" class="mn-item">{{{$category->name}}}</a>
                <div class="mn-panel-content">
                    <div class="part">
                        <div class="close">x</div>
                        <h3>{{{$category->name}}}</h3>
                        <hr />
                        <p>{{{$category->annotation}}}</p>
                        <a href="{{URL::to('magazine/category/' . $category->slug)}}" class="btn btn-primary btn-lg">EXPLORE ALL ARTICLES</a>
                    </div>
                    @for($k = 0; $k < 2; ++$k)
                        @if(isset($category->articles[$k]))
                            <?php $article = $category->articles[$k]; ?>
                            <div class="part">
                                <a href="{{URL::to('magazine/article/' . $article->slug)}}" class="preview preview-full">
                                    <div class="name">
                                        <div class="heading">{{{$article->name}}}</div>
                                        <div class="subheading">{{{$article->annotation}}}</div>
                                    </div>
                                    <div class="image" style="background-image: url({{$article->getImage('thumb')}})"></div>
                                </a>
                            </div>
                        @endif
                    @endfor
                </div>
            </div>
            <!--<a href="{{URL::to('magazine/category/' . $category->slug)}}" class="mn-item"></a>-->
            @endforeach
        </div>

        <div class="nav-issues mn-item-accordion">
            <div class="mn-item mn-item-toggle">
                Sort by issue <i class="fa fa-chevron-down"></i>
            </div>
            <div class="mn-item-panel">
                @foreach($issues as $year => $seasons)
                <div class="issue-year">
                    <div class="mn-item issue-year-toggle">{{$year}}</div>
                    <div class="issue-seasons">
                        @foreach($seasons as $season)
                        <a href="{{URL::to('magazine/issue/' . $year . '/' . $season)}}" class="mn-item">{{$season}}</a>
                        @endforeach
                    </div>
                </div>
                @endforeach
            </div>
        </div>

        <div class="mn-panel">
            <a href="javascript:void(0);" class="mn-item">About life & luxury magazine</a>
            <div class="mn-panel-content">
                <div class="part">
                    <div class="close">x</div>
                    <?php $navText = Setting::getByKey('menu-text'); ?>
                    <h3>{{{$navText->name}}}</h3>
                    <hr />
                    <p>{{$navText->content}}</p>
                </div>

                <div class="part part-menu">
                    <div class="links">
                        <a href="javascript:void(0);">Impressum</a>
                        <a href="javascript:void(0);">Media Kit</a>
                        <a href="javascript:void(0);">Advertising</a>
                        <a href="javascript:void(0);">Join our universe</a>
                        <a href="javascript:void(0);">Contact us</a>
                    </div>
                    <a href="javascript:void(0);" onclick="alert('Cooming soon')">LIFE & LUXURY COLLECTION</a>
                </div>

                <div class="part part-ad">
                    @if(count($articles) > 0)
                        <?php $randArticle = $articles[rand(0, count($articles) - 1)]; ?>
                        <a href="{{URL::to('magazine/article/' . $randArticle->slug)}}" class="preview" style="background-image: url({{$randArticle->getImage('minithumb')}})">
                            <div class="dark-effect"></div>
                            <div class="heading">{{{$randArticle->name}}}</div>
                            <div class="subheading">{{{$randArticle->annotation}}}</div>
                        </a>
                    @endif

                    <div class="part-submenu">
                        <div class="impressium">
                            <div class="block-table">
                                <div class="block-cell">
                                    {{$settings['menu-authors']}}
                                </div>
                            </div>
                        </div>

                        <div class="media-kit">
                            <div class="block-table">
                                <div class="block-cell">
                                    <button class="btn btn-primary btn-lg" onclick="alert('Cooming Soon!')">View online</button>
                                    <button class="btn btn-primary btn-lg" onclick="alert('Cooming Soon!')">Download</button>
                                </div>
                            </div>
                        </div>

                        <div class="ad">
                            <div class="block-table">
                                <div class="block-cell">
                                    We are preparing something special<br>
                                    Be sure to check shortly!
                                </div>
                            </div>
                        </div>

                        
                        <div class="signup subscribe">
                            @include('front.layouts.subscribeform')
                        </div>
                        

                        <div class="signup contacts">
                            {{Form::open(array('url' => 'contacts', 'method' => 'post', 'role' => 'form', 'class' => 'contact-form', 'data-type' => 'contacts'))}}
                                <div class="form-group">
                                    <label for="">First name</label>
                                    <input type="text" name="user_name" class="form-control" />
                                </div>

                                <div class="form-group">
                                    <label for="">Surname</label>
                                    <input type="text" name="user_surname" class="form-control" />
                                </div>

                                <div class="form-group">
                                    <label for="">Email</label>
                                    <input type="email" name="user_email" class="form-control" />
                                </div>

                                <div class="form-group">
                                    <label for="">Collection</label>
                                    {{Form::select('subject', array('' => 'Please Select Desired Collection') + Setting::keyValues('collections'), null, array('class' => 'form-control'))}}
                                    <!--<input type="text" class="form-control" />-->
                                </div>

                                <div class="form-group">
                                    <label for="">Message</label>
                                    <textarea name="message" class="form-control"></textarea>
                                </div>
                            
                            <div class="alert alert-success" role="alert" style="display: none;">
                                Success! You message has been sent
                            </div>
                            <div class="alert alert-danger" role="alert" style="display: none;">
                                <div class="alert-text"></div>
                            </div>
                                <input type="submit" class="btn btn-primary" value="SEND INQUIRY">
                            {{Form::close()}}
                        </div>

                    </div>
                </div>

            </div>
        </div>
    </div>

</nav>
<div class="mn-overlay mn-toggle"></div>