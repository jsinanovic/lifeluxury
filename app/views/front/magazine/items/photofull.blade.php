<div  data-sr="scale down 50% over 1s">
    <div class="item item-photo-full content-data-bg" data-key="{{$mediaKey}}">
        <div class="block-table">
            <div class="block-cell">
                {{$content}}
            </div>
        </div>
    </div>
</div>