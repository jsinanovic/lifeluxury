<section class="section-full section-contents {{isset($type) && $type == 'static'? 'section-contents-static' : ''}}" 
         style="{{isset($background)? 'background-image: url(' . $background . ');' : ''}}">

    <div class="title">Contents</div>
    <div class="section-contents-close contents-close"><i class="fa fa-times"></i></div>

    <div class="contents-categories container">
        
        @foreach($categories as $category)
        <div class="contents-category row" {{isset($type) && $type == 'static'? 'data-sr' : ''}}>

            <div class="col-sm-2">
                <a href="{{URL::to('magazine/category/' . $category->slug)}}" class="category-name">{{{$category->name}}}</a>
            </div>

            <div class="col-sm-10">
                <div class="owl">
                    @foreach($category->articles()->active()->orderBy('created_at', 'DESC')->get() as $article)
                    <a href="{{URL::to('magazine/article/' . $article->slug)}}" class="item article-preview">
                        <img src="{{$article->getImage('minithumb')}}" alt="{{{$article->name}}} thumb" width="189" height="113"/>
                        <div class="article-preview-date">{{$article->created_at}}</div>
                        <div class="article-preview-title">{{{$article->name}}}</div>
                    </a>
                    @endforeach
                </div>
            </div>


        </div>
        @endforeach
    </div>
</section>


@section('js-plugins')
    @parent
    {{HTML::script('front/plugins/owl-carousel/owl.carousel.min.js')}}
@stop

@section('css-plugins')
    @parent
{{HTML::style('front/plugins/owl-carousel/owl.carousel.css')}}
{{HTML::style('front/plugins/owl-carousel/owl.theme.css')}}
@stop
