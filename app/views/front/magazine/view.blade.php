@extends('front.layouts.main')

@section('meta')
    <meta property="fb:app_id" content="{{Config::get('laravel-social::providers')['facebook']['client_id']}}" />
    <meta property="og:title" content="{{{$article->name}}}" />
    <meta property="og:site_name" content="Life&Luxury"/>
    <meta property="og:url" content="{{URL::to($localeUri . 'magazine/article/' . $article->slug)}}" />
    @if($article->image)
        <meta property="og:image" content="{{$article->getImage('original')}}" />
    @endif
    <meta property="og:description" content="{{{$article->annotation}}}" />
    <meta property="og:type" content="article" />
    <meta property="og:locale" content="en_US" />

    <meta property="article:author" content="https://www.facebook.com/lifeandluxurycollection" />
    <meta property="article:publisher" content="https://www.facebook.com/lifeandluxurycollection" />
@stop

@section('content')
    <main class="article-view article-view-t{{$article->template}}">
        {{--<section class="section-full section-image" style="background-image: url({{$article->getImage()}});">--}}
        {{--<h1>{{$article->name}}</h1>--}}
        {{--@yield('article-top')--}}
        {{--</section>--}}

        <section class="section-top-general" id="home">
            <div class="container vertical-center-general">
                <div class="row" style="width: 100%;">
                    <div class="">
                        <div class="col-md-12 welcome-main-title text-center">
                            {{$article->name}}
                        </div>
                    </div>
                </div>
                <div class="">
                    <div>
                        <a href="javascript:void(0);" class="learn-more">
                            DISCOVER MORE
                            <div class="round-button"></div>
                        </a>
                    </div>
                </div>
            </div>
        </section>

        <section class="content">

            {{$article->content}}

        </section>


        {{--@include('front.magazine.authors')--}}

        @include('front.magazine.contents', array('type' => 'static', 'background' => $article->getImage('original', 'contents_bg'), 'relatedArticles' => $relatedArticles))

    </main>

    @if(isset($admin) && $admin)
        <div class="article-control-panel">
            <div class="btn btn-success btn-content-save">Save Content</div>
            <div title="Add Section" class="btn btn-info btn-content-add" data-toggle="modal" data-target="#templateModal"><i class="fa fa-plus"></i></div>
            <div title="Sections Editing Mode" class="btn btn-info btn-content-sort"><i class="fa fa-edit"></i></div>
            <a title="Preview (content must be saved)" href="{{URL::to('admin/articles/edit-content/' . $article->slug . '/preview')}}" target="_blank" class="btn btn-info"><i class="fa fa-eye"></i></a>
        </div>


        <!-- Modal -->
        <div class="modal fade" id="templateModal" tabindex="-1" role="dialog">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title" id="myModalLabel">Choose template</h4>
                    </div>
                    <div class="modal-body">
                        @foreach($templates as $num => $template)
                            <div class="template-item" style="background-image: url(/front/img/previews/p{{$article->template . '_' . ($num+1)}}.jpg)">
                                <div class="template-item-content">
                                    {{$template}}
                                </div>
                            </div>
                        @endforeach
                    </div>
                </div>
            </div>
        </div>

    @endif
@stop


        @section('css-plugins')
                <!--{{HTML::style('css/mcustomscrollbar/jquery.mCustomScrollbar.css', array('async'))}}-->
        @stop

        @section('js-plugins')
        @parent
        @if(!(isset($admin) && $admin))
        {{HTML::script('front/js/scrollReveal.js')}}

        @if($article->template == 12)
        {{HTML::script('/front/plugins/parallax/jquery.parallax.min.js')}}
        @endif

        @else
        {{HTML::script('js/plugins/ckeditor/ckeditor.js')}}
        {{HTML::script('js/plugins/ckeditor/adapters/jquery.js')}}
        @endif
                <!--{{HTML::script('js/plugins/mcustomscrollbar/jquery.mCustomScrollbar.min.js')}}-->
@stop

@section('css')
    @parent
    @if($article->template == 12)
        {{HTML::style('front/css/t12.css')}}
    @endif
    @if($article->template >= 14)
        {{HTML::style('front/css/t' . $article->template . '.css')}}
    @endif

    @if(isset($admin) && $admin)
        {{HTML::style('css/article.css')}}
    @endif
@stop

@section('js')
    @parent
        @if(isset($admin) && $admin)
            <script>
                var articleId = {{$article->id}};
                var articleTemplate = {{$article->template}};
            </script>
            {{HTML::script('js/article.js')}}
        @else
            {{HTML::script('front/js/article.js')}}
            @if($article->template == 12)
                <script>
                    $(document).ready(function () {
                        setTimeout(function () {$('.for-parallax').parallax();}, 1);
                    });
                </script>
            @endif
        @endif
    <script>
        $('#join-newsletter-btn').click(function() {
            displayNewsletterPopup();
        });

        $('#newsletter-close-btn').click(function() {
            hideNewsletterPopup();
        });

        function hideNewsletterPopup() {
            $('#newsletter-popup-container').hide();
            $('#newsletter-content').show();
        }
        function displayNewsletterPopup() {
            $('#newsletter-content').hide();
            $('#newsletter-popup-container').show();
        }
    </script>
@stop