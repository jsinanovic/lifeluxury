<div class="modal fade" id="contactModal" tabindex="-1" role="dialog" aria-labelledby="contactModal">
    <div class="modal-dialog" role="document">
        {{Form::open(array('url' => 'contacts', 'method' => 'post', 'role' => 'form', 'class' => 'contact-form', 'data-type' => 'contacts'))}}
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h3 class="modal-title" id="contactModal">global yet local approach<em>life and luxury russian federation</em></h3>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="form-group col-sm-6">
                        <label for="">First name</label>
                        <input type="text" name="user_name" class="form-control" />
                    </div>

                    <div class="form-group col-sm-6">
                        <label for="">Surname</label>
                        <input type="text" name="user_surname" class="form-control" />
                    </div>
                </div>

                <div class="row">
                    <div class="form-group col-sm-6">
                        <label for="">Email</label>
                        <input type="email" name="user_email" class="form-control" />
                    </div>

                    <div class="form-group col-sm-6">
                        <label for="">Collection</label>
                        {{Form::select('subject', array('' => 'Please Select Desired Collection') + Setting::keyValues('collections'), null, array('class' => 'form-control'))}}
                        <!--<input type="text" class="form-control" />-->
                    </div>
                </div>

                <div class="row">
                    <div class="form-group col-sm-12">
                        <label for="">Message</label>
                        <textarea name="message" class="form-control"></textarea>
                    </div>
                </div>

                <div class="row">
                    <div class="col-sm-12">  
                        <div class="alert alert-success" role="alert" style="display: none;">
                            Success! You message has been sent
                        </div>
                        <div class="alert alert-danger" role="alert" style="display: none;">
                            <div class="alert-text"></div>
                        </div>
                    </div>
                </div>

            </div>
            <div class="modal-footer">
                <input type="submit" class="btn btn-primary" value="SEND INQUIRY">
            </div>
        </div>
        {{Form::close()}}
    </div>
</div>

<div class="modal fade modal-light" id="callBackModal" tabindex="-1" role="dialog" aria-labelledby="callBackModal">
    <div class="modal-dialog" role="document">
        {{Form::open(array('url' => 'contacts', 'method' => 'post', 'role' => 'form', 'class' => 'callback-form', 'data-type' => 'callback'))}}
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h3 class="modal-title" id="callBackModal">call back</h3>
                <div class="modal-description">One morning, when Gregor Samsa woke from troubled dreams,
                    he found himself transformed in his bed into a horrible vermin. He lay on his armour-like 
                    back, and if he lifted his head a little he could see his brown belly, slightly domed and 
                    divided by arches into stiff sections. The bedding was hardly able to cover it and seemed 
                    ready to slide off any moment. His many legs, pitifully thin compared with the size of the 
                    rest of him, waved about helplessly as he looked. 
                </div>
            </div>
            <div class="modal-body"> 
                <div class="modal-section">
                    <h4>personal details</h4>

                    <div class="row">
                        <div class="form-group col-sm-6">
                            <label for="">First name</label>
                            <input type="text" name="user_name" class="form-control" />
                        </div>

                        <div class="form-group col-sm-6">
                            <label for="">Surname</label>
                            <input type="text" name="user_surname" class="form-control" />
                        </div>
                    </div>

                    <div class="row">
                        <div class="form-group col-sm-6">
                            <label for="">Email</label>
                            <input type="email" name="user_email" class="form-control" />
                        </div>

                        <div class="form-group col-sm-6">
                            <label for="">Phone number</label>
                            <input type="text" name="user_phone" class="form-control" />
                        </div>
                    </div>

                    <div class="row">
                        <div class="form-group col-sm-12">
                            <label for="">Collection</label>
                            {{Form::select('subject', array('' => 'Please Select Desired Collection') + Setting::keyValues('collections'), null, array('class' => 'form-control'))}}
                        </div>
                    </div>
                </div>
                
                <div class="modal-section">
                    <h4>best time to call you</h4>

                    <div class="row">
                        <div class="form-group col-sm-12">
                            <div class="checkbox">
                                <label>
                                    <input type="checkbox" value="1" name="call_time">
                                    morning
                                </label>
                            </div>
                            <div class="checkbox">
                                <label>
                                    <input type="checkbox" value="2" name="call_time">
                                    afternoon
                                </label>
                            </div>
                        </div>
                    </div>
                </div>
                
                <div class="modal-section">
                    <div class="row">
                        <div class="col-sm-12">  
                            <div class="alert alert-success" role="alert" style="display: none;">
                                Success! You message has been sent
                            </div>
                            <div class="alert alert-danger" role="alert" style="display: none;">
                                <div class="alert-text"></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>    
                
            <div class="modal-footer">
                <button type="submit" class="btn btn-dark btn-callback">SEND MESSAGE</button>
            </div>
            
        </div>
        {{Form::close()}}
    </div>
</div>

<div class="modal fade modal-light" id="quoteModal" tabindex="-1" role="dialog" aria-labelledby="quoteModal">
    <div class="modal-dialog" role="document">
        {{Form::open(array('url' => 'contacts', 'method' => 'post', 'role' => 'form', 'class' => 'quote-form', 'data-type' => 'quote'))}}
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h3 class="modal-title" id="quoteModal">quote form</h3>
                <div class="modal-description">One morning, when Gregor Samsa woke from troubled dreams,
                    he found himself transformed in his bed into a horrible vermin. He lay on his armour-like 
                    back, and if he lifted his head a little he could see his brown belly, slightly domed and 
                    divided by arches into stiff sections. The bedding was hardly able to cover it and seemed 
                    ready to slide off any moment. His many legs, pitifully thin compared with the size of the 
                    rest of him, waved about helplessly as he looked. 
                </div>
            </div>
            <div class="modal-body">
                
                <div class="modal-section">
                    <h4>personal details</h4>

                    <div class="row">
                        <div class="form-group col-sm-6">
                            <label for="">First name</label>
                            <input type="text" name="user_name" class="form-control" />
                        </div>

                        <div class="form-group col-sm-6">
                            <label for="">Surname</label>
                            <input type="text" name="user_surname" class="form-control" />
                        </div>
                    </div>

                    <div class="row">
                        <div class="form-group col-sm-6">
                            <label for="">Email</label>
                            <input type="email" name="user_email" class="form-control" />
                        </div>

                        <div class="form-group col-sm-6">
                            <label for="">Website</label>
                            <input type="text" name="user_website" class="form-control" />
                        </div>
                    </div>
                </div>
                
                <div class="modal-section">
                    <h4>LEASE SELECT DESIRED SERVICE</h4>

                    <div class="row">
                        <div class="form-group col-sm-6 col-md-4">
                            <div class="checkbox">
                                <label>
                                    <input type="checkbox" value="Advertising" name="service">
                                    ADVERTISING
                                </label>
                            </div>
                            <div class="checkbox">
                                <label>
                                    <input type="checkbox" value="Brand Identity" name="service">
                                    BRAND IDENTITY
                                </label>
                            </div>
                            <div class="checkbox">
                                <label>
                                    <input type="checkbox" value="Graphics Design" name="service">
                                    GRAPHICS DESIGN
                                </label>
                            </div>
                        </div>
                        <div class="form-group col-sm-6 col-md-8">
                            <div class="checkbox">
                                <label>
                                    <input type="checkbox" value="Web Design & Development" name="service">
                                    WEB DESIGN & DEVELOPMENT
                                </label>
                            </div>
                            <div class="checkbox">
                                <label>
                                    <input type="checkbox" value="Video Production" name="service">
                                    VIDEO PRODUCTION
                                </label>
                            </div>
                        </div>
                    </div>
                </div>
                
                <div class="modal-section modal-section-service">
                    <div class="row">
                        <div class="form-group col-sm-12">
                            <label for="">PLEASE PROVIDE US WITH A BRIEF PROJECT DESCRIPTION</label>
                            <textarea name="description" class="form-control"></textarea>
                        </div>
                    </div>

                    <div class="row">
                        <div class="form-group col-sm-6">
                            <label for="">PLEASE PROVIDE US WITH AN ESTIMATED BUDGET</label>
                            <input type="text" name="budget" class="form-control" />
                        </div>
                    </div>
                </div>
                
                <div class="modal-section">
                    <div class="row">
                        <div class="col-sm-12">  
                            <div class="alert alert-success" role="alert" style="display: none;">
                                Success! You message has been sent
                            </div>
                            <div class="alert alert-danger" role="alert" style="display: none;">
                                <div class="alert-text"></div>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
            
            <div class="modal-footer">
                <button type="submit" class="btn btn-dark">SEND MESSAGE</button>
            </div>
        </div>
        {{Form::close()}}
    </div>
</div>