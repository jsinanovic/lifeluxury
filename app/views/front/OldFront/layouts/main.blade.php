<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <title>{{{$metaTitle}}}</title>
        <meta name="keywords" content="{{{$metaKeywords}}}"/>
        <meta name="description" content="{{{$metaDescription}}}"/>
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        @yield('meta')

        <link rel="shortcut icon" href="{{asset('/favicon.ico')}}" type="image/x-icon">
        <link rel="icon" href="{{asset('/favicon.ico')}}" type="image/x-icon">

        {{HTML::style('front/plugins/bootstrap-3.3.2-dist/css/bootstrap.min.css', array('async'))}}
        {{HTML::style('front/plugins/bootstrap-3.3.2-dist/css/bootstrap-theme.min.css', array('async'))}}
        {{HTML::style('front/plugins/font-awesome/css/font-awesome.min.css', array('async'))}}
        {{HTML::style('front/plugins/jqueryformstyler-master/jquery.formstyler.css', array('async'))}}
        
        <link href='http://fonts.googleapis.com/css?family=Open+Sans:300,400,800&subset=latin,cyrillic-ext' rel='stylesheet' type='text/css'>
        <link href="http://fonts.googleapis.com/css?family=Lato:300,400,600,800,900" rel="stylesheet" type="text/css">
        <link href="http://fonts.googleapis.com/css?family=Arapey" rel="stylesheet" type="text/css">
        
        @yield('css-plugins')
        
        {{HTML::style('front/css/styles.css', array('async'))}}
        
        @yield('css')

        <!--[if lt IE 9]>
            <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
            <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
        <![endif]-->

        @if(isset($settings['services-connection']))
        {{$settings['services-connection']}}
        @endif
    </head>
    <body class="mz-loading locale-{{App::getLocale()}}">
        
        <header class="navbar navbar-fixed-top">
            <div class="container-fluid">
                <div class="navbar-header">
                    <button class="navbar-toggle collapsed" type="button" data-toggle="collapse" data-target="#bs-navbar" aria-controls="bs-navbar" aria-expanded="false">
                        <span class="sr-only">Toggle navigation</span><span class="icon-bar"></span><span class="icon-bar"></span><span class="icon-bar"></span>
                    </button>
                    
                    <a href="/" class="navbar-brand">
                        <img src="/front/img/logo.png" alt="Life & Luxury Logo" width="247" height="79">
                    </a>
                </div>
                
                <nav id="bs-navbar" class="collapse navbar-collapse" role="navigation">
                    <ul class="nav navbar-nav navbar-right">
                        @foreach($navMenu->categories as $item)
                        <li><a href="{{{$item->url}}}">{{{$item->name}}}</a></li>
                        @endforeach
                    </ul>
                </nav>
            </div>
        </header>
        
        @yield('content')
        
        @include('front.magazine.loader')
        
        <script>
            var baseUrl = '{{URL::to("/")}}';
            var subscribeFormRules = {{$subscribeFormRules}};
            var contactFormRules = {{$contactFormRules}};
            var isHomePage = {{$pageType == 'home'? 1 : 0}};
        </script>
        
        {{HTML::script('front/plugins/jquery/jquery-1.11.2.min.js')}}
        {{HTML::script('front/plugins/bootstrap-3.3.2-dist/js/bootstrap.min.js')}}
        {{HTML::script('front/plugins/jqueryformstyler-master/jquery.formstyler.min.js')}}
        {{HTML::script('js/plugins/jquery-validation/jquery.validate.js')}}
        @yield('js-plugins')
        {{HTML::script('front/js/scripts.js')}}
        @yield('js')

    </body>
</html>