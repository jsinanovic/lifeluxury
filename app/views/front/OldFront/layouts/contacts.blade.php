<section class="section-contact" id="contact">
    <div class="container">
        <h3>Global yet local approach<em>Life and Luxury International</em></h3>

        <div class="row row-contact">
            <div class="col-sm-5  col-sm-offset-1">
                <div class="title">
                    GENERAL INFORMATION<br>
                    <b>GET CONNECTED</b>
                </div>
                <p>
                    Skype: {{{$settings['skype']}}}<br>
                    {{{$settings['contact-email']}}}
                </p>
                <button class="btn btn-default" role="button" data-toggle="modal" data-target="#contactModal">Contact us</button>

                <div class="subtitle">Customer care</div>
                <p>{{{$settings['customercare-email']}}}</p>
                <button class="btn btn-default btn-customcare" role="button" data-toggle="modal" data-target="#contactModal">Contact us</button>

                <a href="javascript:void(0);" class="open-chat">Chat now/Live support <i class="fa fa-comments-o"></i></a>

            </div>
            <div class="col-sm-4 col-sm-offset-1 clearfix">
                <div class="title">
                    SIGN UP TO OUR<br>
                    <b>NEWSLETTER</b>
                </div>

                @include('front.layouts.subscribeform')

            </div>
        </div>

    </div>

    @include('front.layouts.contactform')
    
</section>

