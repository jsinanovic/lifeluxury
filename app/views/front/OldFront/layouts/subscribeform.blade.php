{{Form::open(array('url' => 'subscribe', 'method' => 'POST', 'role' => 'form', 'class' => 'subscribe-form'))}}
<div class="form-group">
    <label for="subscribe-name">First name</label>
    <input type="text" name="name" id="subscribe-name" class="form-control" required="required"/>
</div>

<div class="form-group">
    <label for="subscribe-surname">Surname</label>
    <input type="text" name="surname" id="subscribe-surname" class="form-control" required="required" />
</div>

<div class="form-group">
    <label for="subscribe-email">Email</label>
    <input type="email" name="email" id="subscribe-email" class="form-control" required="required" />
</div>

<div class="form-group">
    <label for="subscribe-country">Country</label>
    {{Form::select('country', Setting::values('countries'), null, array('id' => 'subscribe-country', 'class' => 'form-control', 'required' => 'required'))}}
</div>

<input type="submit" value="Signup" class="btn btn-success pull-right">
{{Form::close()}}