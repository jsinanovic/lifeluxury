@extends('front.layouts.main')

@section('content')
<section class="section-full section-top" id="home">

    <div class="video-container">
        <div class="bordered-video">
            <iframe width="100%" height="100%" src="https://www.youtube.com/embed/D6G5ByfaKqs" frameborder="0" allowfullscreen></iframe>
            <ul class="parallax">
                <li class="layer border"  data-depth="0.50"><i class="fa fa-play-circle-o"></i></li>
            </ul>
        </div>
        <h1 class="under-info">Welcome to the world of Life&Luxury <br> your finest collection of services</h1>
    </div>

    <a href="javascript:void(0);" class="learn-more">
        Learn more
        <div class="round-button"></div>
    </a>
    <div class="fullscreen"><a href="">Fullscreen <i class="fa fa-expand"></i></a></div>

    <div class="effects">
        <ul class="parallax">
            <li class="layer" data-depth="0.20" data-limit-y="50" data-scalar-y="2" style="z-index: 100"> 
                <img id="welcome-text" src="/front/img/welcome.png" alt="Welcome to Life&amp;Luxury"> 
            </li>
            <li class="layer" data-depth="0.25" data-limit-y="50" data-scalar-y="2" style="z-index: 110"> 
                <img id="luxury-text"  src="/front/img/lifeandluxury.png" alt="Life&amp;Luxury"> 
            </li>
            <li class="layer" data-depth="0.2" data-calibrate-x="false" data-limit-x="false" style="z-index: 120">
                <img id="champagne" src="/front/img/champagne.png" alt="Champagne Luxury"> 
            </li>
            <li class="layer" data-depth="0.3" style="z-index: 130">
                <img id="gentleman" src="/front/img/gentleman.png" alt="Gentleman">
            </li>
            <li class="layer" data-depth="0.4" style="z-index: 140">
                <img id="braid" src="/front/img/braid.png" alt="Braid">
            </li>
        </ul>
    </div>
    
    <div class="bullets">
        <div class="luxury-bullet luxury-one" data-content="Creative Ideas Filled With Passion &amp; Love"><span></span></div>
        <div class="luxury-bullet luxury-two" data-content="Fashions fade, style is eternal, Yves Saint-Laurent"><span></span></div>
        <div class="luxury-bullet luxury-three" data-content="The only real elegance is in the mind itself"><span></span></div>
        <div class="luxury-bullet luxury-four" data-content="Elegance is the only beauty that never fades, Audrey Hepburn"><span></span></div>
        <div class="luxury-bullet luxury-five" data-content="Unique and different is the next generation of beautiful, Taylor Swift"><span></span></div>
    </div>

</section>

<div class="section-separator">
    <div class="section-separator-cell">
        <div class="content">
            LIFE & LUXURY OFFERS THE <b>FINEST</b> COLLECTION OF SERVICES WITH REPRESENTATIVES IN CYPRUS, RUSSIAN FEDERATION AND SWITZERLAND
        </div>
    </div>
</div>

<section class="section-full section-about" id="about">

    <div class="section-content about">
        <div class="container">
            <div class="about-container">
                <h2>About us</h2>
                <div class="row about-info">
                    <div class="col-md-8">
                        At Life & Luxury our commitment to the notion that each project is unique is unwavering.
                    </div>
                    <div class="col-md-4">
                        At Life & Luxury<br>
                        Our Service is a Promise
                    </div>
                </div>

                
                
                <div class="row about-description">
                    <div class="col-md-4">
                        <div class="for-img">
                            <img src="/front/img/service.jpg" title="Unique Services">
                            <ul class="for-parallax parallax-img-1" data-invert-x="true" data-invert-y="false">
                                <li class="layer border"  data-depth="0.20"></li>
                            </ul>
                        </div>
                        <h4>UNIQUE SERVICES</h4>
                        <p>We have created a unique collection of services which are 
                            just as unique as our customers. All services are carefully 
                            laid out to fulfil the most demanding desires.
                        </p>
                    </div>
                    <div class="col-md-4">
                        <div class="for-img">
                            <img src="/front/img/deal.jpg" title="Attention to Detail">
                            <ul class="for-parallax parallax-img-2"  data-invert-x="false" data-invert-y="true">
                                <li class="layer border"  data-depth="0.30"></li>
                            </ul>
                        </div>
                        <h4>ATTENTION TO DETAIL</h4>
                        <p>Our work is never complete until it reaches perfection. Swiss Quality combined with our Award Winning Service makes the whole process an Unforgettable Experience for each of our customers whether first time or returning.</p>
                    </div>
                    <div class="col-md-4">
                        <div class="for-img">
                            <img src="/front/img/newlogo.jpg" title="Why Life &amp; Luxury?">
                            <ul class="for-parallax parallax-img-3" data-invert-x="true" data-invert-y="false">
                                <li class="layer border"  data-depth="0.20"></li>
                            </ul>
                        </div>
                        <h4>WHY LIFE & LUXURY?</h4>
                        <p>Always Innovating, Always Challenging and Always Pushing our Boundaries. All customers are just as unique as their dreams and most important of all, our knowledge is available to anyone - no matter how small the project.</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<div class="section-separator">
    <div class="section-separator-cell">
        <div class="content">
            OUR BRAND IS A <b>PROMISE</b><br>
            OUR MULTILINGUAL PERSONNEL, A FINEST <b>QUALITY</b> SERVICE AND A <b>PASSION</b> FOR OUR WORK,<br>
            WILL MAKE YOUR REQUEST TRULY <b>EXCLUSIVE!</b>
        </div>
    </div>
</div>

<section class="section-full section-creative" id="creative">


    <div class="section-content">

        <div class="effects">
            <ul class="parallax">
                <li class="layer" data-depth="0.30">
                    <img id="glasses" src="/front/img/glasses.png" alt="Life &amp; Luxury Logo">
                </li>
            </ul>
        </div>

        <div class="container">
            <h2>
                CREATIVE
                <em>Ideas filled with passion and love</em>
            </h2>

            <div class="row">
                <div class="col-md-7 creative-video">
                    <div class="bordered-video">
                        <iframe width="100%" height="100%" src="https://www.youtube.com/embed/eF3W2JH8arE" frameborder="0" allowfullscreen></iframe>
                        <ul class="parallax">
                            <li class="layer border"  data-depth="0.10" data-origin-x="0.00" data-origin-y="0"><i class="fa fa-play-circle-o"></i></li>
                        </ul>
                    </div>
                </div> 
                <div class="col-md-5 creative-links clearfix">
                    @foreach($services as $item)
                    <div class="col-xs-6"><a href="{{URL::to('creative/' . $item->slug)}}"><p>{{{$item->name}}}</p></a></div>
                    @endforeach
                    <!--<div class="col-xs-6"><a href=""><p>Brand Identity</p></a></div>
                    <div class="col-xs-6"><a href=""><p>Graphics Design</p></a></div>
                    <div class="col-xs-6"><a href=""><p>Web Design & Development</p></a></div>
                    <div class="col-xs-6"><a href=""><p>Video Production</p></a></div>-->
                    <div class="col-xs-6"><a href="javascript:void(0);" class="active" role="button" data-toggle="modal" data-target="#quoteModal"><p>Get a Free Quote</p></a></div>
                </div>
            </div>

            <div class="row">
                <div class="col-md-12">
                    <div class="under-info">
                        Unique Design Creations – A bold statement filled with passion and love for each of our projects at Life&Luxury Creative. 
                        Every concept detail is handcrafted to perfection in order fulfill the needs even for our most imaginative customers. Our Creative Department offers you a wide variety of solutions tailored to your brand. A wide array of services: advertising, corporate identity solutions, graphic design, website design & development as well as video animations.  
                    </div>
                </div>
            </div>
        </div>

    </div>

</section>


<div class="section-separator">
    <div class="section-separator-cell">
        <div class="content">
            <div class="title">Find out more about what life & luxury can do for you</div>
            <div class="buttons">
                <div class="btn btn-default" role="button" data-toggle="modal" data-target="#callBackModal"><i class="fa fa-phone"></i> Request call back</div>
                <div class="btn btn-default open-chat"><i class="fa fa-comments-o"></i> Let's chat</div>
                <div class="btn btn-default" role="button" data-toggle="modal" data-target="#contactModal"><i class="fa fa-envelope-o"></i> Contact us</div>
            </div>
        </div>
    </div>
</div>

<section class="section-full section-link" id="rendezvous">
    <a href="{{$settings['lifestyle-link']}}" class="btn btn-primary btn-discover" target="_blank">Discover</a>
</section>


<div class="section-separator">
    <div class="section-separator-cell">
        <div class="content">
            OUR <b>UNIQUE</b> GUEST RELATION SERVICE IS AVAILABLE 24 HOURS A DAY, 365 DAYS A YEAR 
            BY SHARING SPECIALIST <b>KNOWLEDGE & EXPERTISE</b>
        </div>
    </div>
</div>

@if($article)
<section class="section-full section-magazine" id="magazine" style="background-image: url({{$article->getImage()}})">
    <div class="container">
        <div class="section-content">
            <div class="top-title">Life&Luxury magazine</div>

            <div class="magazine-issue">
                <div class="num">
                    1
                    <em>issue</em>
                    <div class="magazine-issues">
                        {{$article->season()}} {{$article->year()}}
                    </div>
                </div>
                <h2>{{$article->name}}</h2>
                <a href="{{URL::to('magazine')}}" class="btn btn-primary btn-discover">Discover</a>
            </div>
        </div>
    </div>
</section>
@endif

<div class="section-separator">
    <div class="section-separator-cell">
        <div class="content">
            With our coverage in the <b>Central</b> as well as <b>Eastern Europe</b>, representatives speaking over 6 languages, 
            Life & Luxury <b>connects</b> global and local, providing each of our clients with an <b>unparalleled</b> service 
            whatever the location.
        </div>
    </div>
</div>

<div class="section-full custom-contacts">
@include('front.layouts.contacts')
@include('front.layouts.footer')
</div>

@stop

@section('js-plugins')
{{HTML::script('/front/plugins/parallax/jquery.parallax.min.js')}}
<script>
    $(document).ready(function () {
        setTimeout(function(){
            $('.parallax').parallax();
            $('.parallax-img-1').parallax({
                invertX: true,
		invertY: false
            });
            $('.parallax-img-2').parallax({
                invertX: false,
		invertY: true
            });
            $('.parallax-img-3').parallax({
                invertX: true,
		invertY: false
            });
        }, 1);
    });
</script>
@stop