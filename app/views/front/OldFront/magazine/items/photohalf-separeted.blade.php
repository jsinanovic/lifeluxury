<div class="item item-t7 {{$isImageLeft? 'item-t7-img-left' : 'item-t7-img-right'}}">
    <div class="block-table" data-sr="enter left">
        
        @if($isImageLeft)
        <div class="block-cell img-block content-data-all" data-key="{{$mediaKey}}"></div>
        <div class="block-cell block-cell-separator"></div>
        @endif
        
        <div class="block-cell" style="{{isset($bgColor)? 'background-color: ' . $bgColor : ''}}" data-sr="hustle 100px enter {{$isImageLeft? 'right' : 'left'}}">
            <div class="cell-content" data-sr="wait 0.3s enter top opacity 0.2 scale down 20%"><hr>{{$content}}</div>
        </div>
        
        @if(!$isImageLeft)
        <div class="block-cell block-cell-separator"></div>
        <div class="block-cell img-block content-data-all" data-key="{{$mediaKey}}"></div>
        @endif
        
    </div>
</div>