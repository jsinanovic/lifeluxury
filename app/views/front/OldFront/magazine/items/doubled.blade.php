<div class="item item-doubled" data-sr="vfactor 0.5 enter {{$isImageLeft? 'right': 'left'}}">
    
    <div class="container">
        <div class="item-num {{$isImageLeft? 'item-num-left': ''}}" data-sr="wait 0.3s enter top">
            <span>{{$itemNum}}</span>
            
            @if(isset($isCategory) && $isCategory)
            <div class="article-view-category">
                {{{$article->category->name}}}  |  <span>Life & Luxury Magazine</span>
            </div>
            @endif
            
        </div>
        
        <div class="block-table">
            <div class="block-cell"  @if(!$isImageLeft)data-sr="wait 0.5s opacity 0.5 spin 90deg"@endif>
                @if($isImageLeft)
                <div class="special-img content-data-bg" data-key="{{$mediaKey}}"></div>
                @endif
                {{$content}}
            </div>
            <div class="block-cell" @if($isImageLeft)data-sr="wait 0.5s opacity 0.5 spin -90deg"@endif>
                @if(!$isImageLeft)
                <div class="special-img content-data-bg" data-key="{{$mediaKey}}"></div>
                @endif
                {{$content2}}
            </div>
        </div>
    </div>
</div>