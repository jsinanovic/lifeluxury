<div class="item item-photo-half" data-sr="scale down 50% over 1s">
    <div class="block-table">
        
        @if($isImageLeft)
        <div class="block-cell img-block content-data-all" data-key="{{$mediaKey}}"></div>
        @endif
        
        <div class="block-cell">
            <div class="cell-content">
                @if(isset($isCategory) && $isCategory)
                <div class="article-view-category">
                    {{{$article->category->name}}}  |  <span>Life & Luxury Magazine</span>
                </div>
                @endif
                
                {{$content}}
            </div>
        </div>
        
        @if(!$isImageLeft)
        <div class="block-cell img-block content-data-all" data-key="{{$mediaKey}}"></div>
        @endif
        
    </div>
</div>