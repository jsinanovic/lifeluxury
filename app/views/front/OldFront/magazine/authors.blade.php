<section class="section-full section-articles" style="background-image: url({{$article->getImage('original', 'articles_bg')}})">
    <div class="articles-control" data-sr>
        <a style="{{$prevArticle? '' : 'visibility:hidden;'}}" href="{{$prevArticle? URL::to('magazine/article/' . $prevArticle->slug) : 'javascript:void(0);'}}" class="article-prev"><i class="fa fa-chevron-left"></i> Prev article</a>
        <a style="{{$nextArticle? '' : 'visibility:hidden;'}}" href="{{$nextArticle? URL::to('magazine/article/' . $nextArticle->slug) : 'javascript:void(0);'}}" class="article-next">Next article <i class="fa fa-chevron-right"></i></a>
        <a href="javascript:void(0);" class="article-repeat"><i class="repeat-icon"></i><div>Repeat</div></a>
    </div>

    @if(!empty($article->authors))
    <div class="article-authors">
        @foreach($article->authors as $key => $author)
        <div class="author"  data-sr="wait 0.{{($key+1) * 2}}s">
            <div class="author-role">{{{$author['name']}}}</div>
            <div class="author-name">{{$author['content']}}</div>
        </div>
        @endforeach
    </div>
    @endif
</section>