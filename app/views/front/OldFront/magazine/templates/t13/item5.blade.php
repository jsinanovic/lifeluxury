<div class="item item-5">
    <img src="http://lifeandluxury.com/uploads/articles/content/31d74d364a5be51ec6162c324d728796.png" data-img="img-6" alt="" width="100%">
    <div class="block-content">
        <div class="text bordered" data-sr="scale up 25% move 500px enter top">
            <p>Add a touch of glamour to that thick heavy coat or drab jumper with a sprinkling of faux fur. First introduced in 1929, the use of faux/fake fur has increased in popularity due to the promotion of animal rights and is supported by fashion design labels such as Ralph Lauren and Chanel. Available in a range of styles and trends, have fun with a set of furry ear muffs or go classic with an elegant stole or glamorous gilet. Warm, soft and oh so cosy, make a statement like Catherine Deneuve and fake it ‘till you make it.</p>
        </div>
    </div>
</div>