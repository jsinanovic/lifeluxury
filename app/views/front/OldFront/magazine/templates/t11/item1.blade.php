<div class="bg-extra-space item">
    <div class="container-special">

        <div class="space-line space-line-center space-line-padded block-1" data-sr="enter right, move 100px, wait 0.2s">
            <div class="text-default text-1">
                <p>With the launch of its New Frontier Program, America’s National Aeronautics and Space Administration (NASA) continues to push the boundaries of space exploration, seeking answers from the stars to the questions of the world’s being and the galaxies beyond. </p>
            </div>

            <img class="img-1" src="http://lifeandluxury.com/uploads/articles/content/e3c7c7be705514e7856c46ca736f8349.jpg" data-img="img-1" data-sr="spin 145deg, flip 145deg, wait 0.5s">

            <div class="clearfix"></div>
            <div class="space-circle space-circle-1"></div>
        </div>

        <div class="space-line space-line-right" data-sr="enter left, move 100px, wait 0.2s">
            <div class="block-2 space-line space-line-special">
                <img class="img-2" src="http://lifeandluxury.com/uploads/articles/content/cf9dcf86b6659f3daf14d4a627578327.jpg" data-img="img-2" alt="" data-sr="spin 145deg, flip 145deg, wait 0.5s">

                <div class="text-default text-2">
                    <p>Part of the New Frontiers Program is the exciting New Horizons Mission led by S.Alan.Stern, an interplanetary space probe initiated to study the Pluto system and the Kuiper Belt. Clocking the fastest speed ever recorded at 36,000 miles per hour when it launched on January 19th, 2006 from Cape Canaveral, the mission has become the highest priority for solar system exploration.</p>
                </div>
                <div class="clearfix"></div>
                <div class="space-circle space-circle-2"></div>
            </div>

            <div class="block-3" data-sr="enter right, move 100px, wait 0.2s">
                <div class="text-default text-3">
                    <p>In general, the program seeks to understand where Pluto and its moons “fit in” with the other objects in the solar system, such as the inner rocky planets (Earth, Mars, Venus and Mercury) and the outer gas giants (Jupiter, Saturn, Uranus and Neptune).</p>
                </div>
                <img class="img-3" src="http://lifeandluxury.com/uploads/articles/content/7b2b2a057b7a81eaf76d7b45781debc9.jpg" alt="" data-img="img-3" data-sr="spin 145deg, flip 145deg, wait 0.5s">
                <div class="clearfix"></div>
                <div class="space-circle space-circle-3"></div>
            </div>

            <img class="img-4" src="http://lifeandluxury.com/uploads/articles/content/967fc1dc47cabc5d1edb7c75a291aea6.jpg" alt="" data-img="img-4" data-sr="spin 145deg, flip 145deg, wait 0.5s">
            <div class="space-line space-line-short" data-sr="enter right, move 100px, wait 0.2s">
                <div class="text-default text-4">
                    <p>Originally intended to observe Pluto and its largest moon, Charon, the mission had to adapt tonew discoveries before it had even got off the ground. Seven months before the launch astronomers discovered two more moons, Nix and Hydra, followed by another two, Styx and Kerberos, once the mission was under way, thus the course of the probe had to be adjusted accordingly to avoid collisions.</p>
                </div>
                <div class="space-circle space-circle-4"></div>
            </div>
            <div class="clearfix"></div>
        </div>

        <div class="space-line space-line-left" data-sr="enter left, move 100px, wait 0.2s">
            <div class="clearfix">
                <img class="img-5" src="http://lifeandluxury.com/uploads/articles/content/26402b5e1a4b39f28f3976dd1533c628.jpg" alt="" data-img="img-5" data-sr="spin 145deg, flip 145deg, wait 0.5s">
                <div class="clearfix"></div>
                <div class="space-line space-line-short space-line-short-right">
                    <div class="text-default text-5">
                        <p>The spacecraft passed the Moon in just nine hours and after a brief encounter with an asteroid, New Horizons proceeded to Jupiter the following year where it returned data about the planet’s atmosphere, moons and magnetosphere. The flyby provided a gravity assist that increased the speed of the probe, although much of its post-Jupiter voyage was spent in hibernation to preserve on-board systems. </p>
                    </div>
                    <div class="space-circle space-circle-5"></div>
                </div>
            </div>
        </div>


        <div class="space-line space-line-center" data-sr="enter right, move 100px, wait 0.2s">

            <div class="block-center">

                <img class="img-6" src="http://lifeandluxury.com/uploads/articles/content/65f61fd0864dd062e00ad09579bd228a.jpg" alt="" data-img="img-6" data-sr="spin 145deg, flip 145deg, wait 0.5s">
                <div class="text-default text-6">
                    <p>The spacecraft passed the Moon in just nine hours and after a brief encounter with an asteroid, New Horizons proceeded to Jupiter the following year where it returned data about the planet’s atmosphere, moons and magnetosphere. The flyby provided a gravity assist that increased the speed of the probe, although much of its post-Jupiter voyage was spent in hibernation to preserve on-board systems. </p>
                </div>

                <img class="img-7" src="http://lifeandluxury.com/uploads/articles/content/2acd21d1a1b5e0a198c43ae4049f66df.jpg" alt="" data-img="img-7" data-sr="spin 145deg, flip 145deg, wait 0.5s">
            </div>

            <div class="block-right" data-sr="enter left, move 100px, wait 0.2s">

                <div class="space-line space-line-up-left">
                    <div class="clearfix">
                        <div class="text-default text-7">
                            <p>The spacecraft itself is as ambitious as the mission it has undertaken. Comparable in size and general shape to a grand piano, with a body forming a triangle almost 2.5ft thick, it carries a RTG and dish on a box-in-box structure and is formed mainly by an aluminium alloy tube with a titanium fuel tank. The structure is larger than necessary having been designed to act as shielding to reduce possible electronic errors caused by radiation. With 16 thrusters, two computer systems, three optical instruments, two plasma instruments, a dust sensor and a radio science radiometer, the New Horizons is a significant achievement in scientific and space engineering. </p>
                        </div>

                        <img class="img-8" src="http://lifeandluxury.com/uploads/articles/content/10a7760bd04ac66a333c92bdf4057268.jpg" alt="" data-img="img-8" data-sr="spin 145deg, flip 145deg, wait 0.5s">
                    </div>
                    <div class="space-circle space-circle-6"></div>
                </div>

                <div class="space-line space-line-up-right" data-sr="enter right, move 100px, wait 0.2s">
                    <div class="clearfix">
                        <div class="text-default text-8">
                            <p>The probe carries more than just scientific equipment. Fixed to the inside upper deck is a small container carrying the ashes of Clyde Tombaugh, who discovered Pluto in 1930 and requested that his remains be sent to space upon his death in 1997. With such a presence of genius on board, the mission will no doubt leave not only a rich scientific legacy but also provide inspiration which could help spur on future ambitious exploration efforts throughout the solar system. </p>
                        </div>
                        <img class="img-9" src="http://lifeandluxury.com/uploads/articles/content/8ae585c27b83451dbd4efd132199e5c2.jpg" alt="" data-img="img-9" data-sr="spin 145deg, flip 145deg, wait 0.5s">
                    </div>
                    <div class="space-circle space-circle-7"></div>
                </div>

            </div>

        </div>

    </div>
</div>