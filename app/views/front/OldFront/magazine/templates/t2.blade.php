<div class="item" data-sr="enter left">
    <div class="item-img-right content-data">
        <img src="http://lifeandluxury.com/uploads/articles/content/a1595cd69725bc611056fb740b95ff61.jpg">
    </div>
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-offset-1 col-md-4 text-col">
                <div class="article-view-category" data-sr="wait 0.3s enter top move">
                    Rendez Vouz  |  <span>Life &amp; Luxury Magazine</span>
                </div>

                <div data-sr="wait 0.5s enter right">
                    <h2>SIGHTS SET ON THE MONACO YACHT SHOW</h2>

                    <p class="letter">
                        Home to some of the richest and most influential players on the world 
                        stage, the tiny principality of Monaco on the French Riviera has long served as a haven of opulence for the cream of society. The heady mix of dazzling sunshine, delicious cuisine and decadent lifestyle, make Grace Kelly’s adopted home the perfect site for a luxury rendezvous of the international elite. 
                    </p>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="item" style="margin-top: 40px;" data-sr="enter bottom">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-offset-1 col-md-5" data-sr="wait 0.5s scale up 80%">
                <div class="for-img">
                    <img src="http://lifeandluxury.com/uploads/articles/content/d98253e5d2fe2ae6a116bf50ff549f0b.png">
                </div>
            </div>
            <div class="col-md-offset-1 col-md-4 text-col" data-sr="wait 0.3s enter right">
                <p>Each September since 1991, the stunning Port Hercules hosts one of the yachting  industry’s most glamorous and exclusive events: The Monaco Yacht Show (MYS). For four days the port showcases 500 leading luxury yachting companies including superyacht designers, luxury manufacturers and nautical suppliers, attracting 34,000 visitors who flock to the show for both business and pleasure. </p>
                <p>An outstanding fleet of 115 custom-built superyachts ranging from 25 to 100 metres grace the waters of Monte Carlo’s natural bay, with around 40 yachts making their grand debut each year. Some of the past and present yachting names, business and brands exhibiting at the show include Admiral, Burgess, Edmiston &amp; Company, Imperial Yachts, Azimuth Benetti Group, Fraser Yachts, Camper &amp; Nicholsons International, Merle Wood &amp; Associates, Ocean Independence and Princess Yachts and many more. </p>
            </div>
        </div>
    </div>
</div>

<div class="item" data-sr="enter bottom">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-offset-1 col-md-4 text-col" data-sr="wait 0.3s enter left">

                <p class="letter">
                    The 2015 will see the premiere of prestigious motor yacht company Amels sensa
                    tional 60-metre superyacht Madame Kate. The brainchild of multi-award winning designer Tim Heywood, Madame Kate is beautifully finished with a unique pearl effect and boasts a touch-and-go helipad, a Turkish steam bath and an 8-metre limo-tender. 
                </p>
                <p>The Monaco Yacht Show will also see the launch of the 63-metre Sunrise Mega Yacht IRIMARI designed by acclaimed architect Espen Oenino. Featuring a hi-tensile steel hull and capable of cruising beyond 5,000 miles, the nearly 1500 GT luxury yacht is sure to rival many of the superyachts on show. Alongside the exhibition of the latest high technology nautical products and luxury yachting services, the port enjoys a glittering social scene hosting daily events including galas, charity functions and cocktail parties. On board the spectacular megayachts and hidden in the elegant palaces of Monte Carlo, guests mix business with pleasure at award ceremonies, press conferences and a dazzling array of parties. </p>

            </div>
            <div class="col-md-offset-2 col-md-4" style="text-align: right;" data-sr="wait 0.5s scale up 80%">
                <div class="for-img">
                    <img src="http://lifeandluxury.com/uploads/articles/content/308c2b6290c857df750bfbf1cd508818.png">
                </div>
            </div>
        </div>
    </div>
</div>

<div class="item" data-sr="enter bottom">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-offset-1 col-md-5" data-sr="wait 0.5s scale up 80%">
                <div class="for-img">
                    <img src="http://lifeandluxury.com/uploads/articles/content/c204921693e141c95a42095efb09af40.png">
                </div>
            </div>
            <div class="col-md-offset-1 col-md-4 text-col" data-sr="wait 0.3s enter right">
                <p>This year the show will celebrate its 25th edition with an inaugural cocktail party, where the MYS award ttceremony will be held to honour the outstanding achievements of the superyachting industry, recognising greatness in a range of fields from design to the finest newcomers to the market. The accolades include: The Monaco Award; The Interior Design Award; The Exterior Design Award; The finest new yacht of the 2015 MYS; The GreenPlus RINA Award. </p>
                <p>The inaugural party is followed by countless events on board some of the megayachts, port side hotels or during the exhibitions, with a wide range of interests catered for across the entire festival.Away from the bustling port, the MYS C&amp;C Lounge offers a privileged and exclusive living area for the superyacht captains and crew. Food, entertainment, enology workshops, games, music and even a relaxation area complete with wellness massages are provided for the duration of the show, creating a perfect oasis of peace and calm for members to retreat to. </p>
            </div>
        </div>
    </div>
</div>

<div data-sr="hustle 50px spin 90deg">
    <div class="item item-bg-right content-data content-data-bg" data-key="img-5">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-offset-1 col-md-4 text-col" data-sr="wait 0.5s enter top spin 90deg">
                    <p class="letter">
                        A long with the Formula One Monaco Grand Prix, the Monaco Yacht Show is the  
                        main  worldwide-scale event of the country, and as such it has established itself as a major economic and media player for the Principality, constituting a significant contribution to the overall economy. More importantly, at the heart of the show is a real sense of philanthropy, with support for charities such as the Association Monegasque contre les Myopathies since 2001 and the Prince Albert II of Monaco Foundation since 2010. The show also supports environmental and medical actions with organizations such as the Wood Forever Pact, established to tackle deforestation by convincing the entire yachting community to only use wood certified as originating from sustainably-managed forests. The Wood Forever Pact is the flagship project of the ‘Monaco makes a commitment against deforestation’ initiative, led by the Prince Albert II of Monaco Foundation. 
                    </p>
                </div>
            </div>
        </div>
        <img src="http://lifeandluxury.com/uploads/articles/content/7f36a4781cbee636a5db30bd69009638.png">
    </div>
</div>

<div data-sr="hustle 50px spin 90deg">
    <div class="item item-bg-left content-data content-data-bg" data-key="img-6">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-offset-8 col-md-3 text-col" data-sr="wait 0.5s enter bottom spin 90deg">
                    <p>Ooozing with glamour and brimming with philanthropic goodness,the Monaco Yacht Show is the epitome of celebrity chic, seducing the rich and famous all over the world with its intoxicating mix of nautical genius and luxury service all set in the iconic setting of the Grimaldi playground. A visit to the show is a must for yachting enthusiasts and business people alike and of course for any of those who believe size does matter.  The 25th anniversary of The Monaco Yacht Show takes place from 23rd to 26th September 2015 at Port Hercules, Monte Carlo, Monaco.</p>
                </div>
            </div>
        </div>
        <img src="http://lifeandluxury.com/uploads/articles/content/7c2eefe1140b77b8d5776a425458ae39.png">
    </div>
</div>

