


<div class="item item-t7 item-t7-img-left">
    <div class="block-table" data-sr="enter left">
        <div class="block-cell img-block content-data-all">
            <img src="http://lifeandluxury.com/uploads/articles/content/b65fa8d2048154644132899b31e906e6.jpg">
        </div>
        <div class="block-cell block-cell-separator"></div>

        <div class="block-cell" style="" data-sr="hustle 100px enter right">
            <div class="cell-content" data-sr="wait 0.3s enter top opacity 0.2 scale down 20%"><hr><h2>The Autumn Years</h2>
                <div class="article-view-category">
                    Cover Editorial  |  <span>Life &amp; Luxury Magazine</span>
                </div>
                <p class="letter">
                    The golden age of life is upon us. Here in the 21st century, life and luxury go hand in hand with more successful business and leisure enterprises at our disposal than ever before. Despite the financial crisis that threatens to cripple the European Union, our nations face the challenge with pride and a steely determination that has seen us survive through the decades. 
                </p></div>
        </div>
    </div>
</div>

<div class="item item-t7 item-t7-img-right">
    <div class="block-table" data-sr="enter left">
        <div class="block-cell" style="" data-sr="hustle 100px enter left">
            <div class="cell-content" data-sr="wait 0.3s enter top opacity 0.2 scale down 20%"><hr><p class="letter">
                    Technology is at the forefront of our new age with marketing and design companies operating on a global scale at the click of a button. Whilst the Mediterranean transforms itself into a new energy hub, more and more businesses are working to calculate, reduce and offset their carbon footprint though sustainable luxury; re-designing consumerism by using behavioural economics and transforming the luxury sector into a powerful advocate for sustainable production and consumption. Charitable causes strike through the heart of the glittering party scenes that frequent the likes of the Monaco Yacht Show, the Frankfut Autoshow and the Singapore Grand Prix, attracting the international elite with their combination of travel, fashion and opulent displays of luxury lifestyles. 
                </p>
            </div>
        </div>
        <div class="block-cell block-cell-separator"></div>
        <div class="block-cell img-block content-data-all" data-key="img-2"><img src="http://lifeandluxury.com/uploads/articles/content/251362d7135bd97e47dcfe721ea613d4.jpg"></div>
    </div>
</div>

<div class="item item-t7 item-t7-img-left">
    <div class="block-table" data-sr="enter left">
        <div class="block-cell img-block content-data-all" data-key="img-3"><img src="http://lifeandluxury.com/uploads/articles/content/83050e6500088c34f41f37ace1982967.jpg"></div>
        <div class="block-cell block-cell-separator"></div>
        <div class="block-cell" style="background-color: #e27a3f" data-sr="hustle 100px enter right">
            <div class="cell-content" data-sr="wait 0.3s enter top opacity 0.2 scale down 20%"><hr><p class="letter">Underneath all the glamour the frivolities of life have been stripped away leaving only a steel core burning through the toxic layers to reveal a strip of pure, unadulterated light. No longer living just for ourselves but fighting on with a sense of renewal and pride, life has adopted a new meaning with the wisdom that nothing should be taken for granted and that luxury is golden.   
                </p>
            </div>
        </div>
    </div>
</div>

