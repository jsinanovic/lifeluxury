<div class="item item-photo-half" data-sr="scale down 50% over 1s">
    <div class="block-table">
        <div class="block-cell">
            <div class="cell-content">
                <p><span style="font-size: 20px;">Ah, Paris again! One would have heard of this name perhaps too many times. Yet, for a seemingly over-cliche destination, sometimes, the question is not “What”, but rather “When”.&nbsp;</span></p>
                <p>Not until the last flocks of tourists have boarded the planes towards home after an overheated and overcrowded summer can Paris’ true romantic charm be experienced with ease and elegance. Fresh autumn breeze and nature’s ever-changing palate of colours aside, even simple beauties such as the fragrance of N°5 at La Fayette or the crunchy sound of a freshly baked baguette at Le Grenier ? Pain are more pleasurable.</p>
                <p>More importantly, this is the perfect season to enjoy the outdoors. As the climate cools down and hasn’t yet reached its bone-chilling winter temperature, a cup of Charles Chocolatier’s take-away hot chocolate accompanied by Jean-Paul Hevin macaroons are enough to keep you cosy. Simply dress your best with the latest seasonal trends and casually stroll along les Champs-Elysees, in le Jardin des Tuileries, Parc Monceau, Place des Vosges, Jardin du Luxembourg, or visit le Chateau de Versailles. Uneventful as it sounds, amusement would certainly not be missed as the spirit of The Paris Autumn Festival is present in every corner of France’s capital. Save autumn for Paris, and the definitions of luxury and romance will be unveiled in their most classic tones.</p>
            </div>
        </div>

        <div class="block-cell img-block content-data-all" data-key="img-2"><img src="http://lifeandluxury.com/uploads/articles/content/38e74d2131e04a85254cf8f6381af2e8.jpg"></div>
    </div>
</div>