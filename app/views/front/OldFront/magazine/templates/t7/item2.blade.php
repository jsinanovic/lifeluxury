<div class="item item-t7 item-t7-img-right">
    <div class="block-table" data-sr="enter left">
        <div class="block-cell" style="" data-sr="hustle 100px enter left">
            <div class="cell-content" data-sr="wait 0.3s enter top opacity 0.2 scale down 20%"><hr><p class="letter">
                    Technology is at the forefront of our new age with marketing and design companies operating on a global scale at the click of a button. Whilst the Mediterranean transforms itself into a new energy hub, more and more businesses are working to calculate, reduce and offset their carbon footprint though sustainable luxury; re-designing consumerism by using behavioural economics and transforming the luxury sector into a powerful advocate for sustainable production and consumption. Charitable causes strike through the heart of the glittering party scenes that frequent the likes of the Monaco Yacht Show, the Frankfut Autoshow and the Singapore Grand Prix, attracting the international elite with their combination of travel, fashion and opulent displays of luxury lifestyles. 
                </p>
            </div>
        </div>
        <div class="block-cell block-cell-separator"></div>
        <div class="block-cell img-block content-data-all" data-key="img-2"><img src="http://lifeandluxury.com/uploads/articles/content/251362d7135bd97e47dcfe721ea613d4.jpg"></div>
    </div>
</div>