


<div class="s s-1">

    <div class="for-img" data-sr="move 100px roll -30deg hustle 200px over 0.5s wait 0.2s">
        <img src="http://lifeandluxury.com/uploads/articles/content/c5c174068b41a02355fa3392d1381a00.png" data-img="img-1" alt="">
    </div>

    <div class="container">
        <div class="text text-center" data-sr="move 100px roll 30deg hustle 200px over 0.5s wait 0.2s">
            <p>Before the season of goodwill descends upon the masses, the social calendar is already bursting with the record-breaking aerospace event of the year the Dubai Airshow and the tantalisingly guilty pleasure that is The Victoria’s Secret Show. Spectacular, sensual and certainly stimulating, the two shows </p>
        </div>
    </div>
</div>

<div class="s s-2 clearfix">
    <div class="for-img" data-sr="move 100px roll -30deg hustle 200px over 0.5s wait 0.2s">
        <img src="http://lifeandluxury.com/uploads/articles/content/7699af091dc95a0f61fe323d47b10afe.jpg" data-img="img-2" alt="">
    </div>
    <div class="text" data-sr="move 100px roll 30deg hustle 200px over 0.5s wait 0.2s">
        <img src="/front/img/t16/3.png" alt="">
        <quote>in an age where breaking the mould is tantamount to success.</quote>
    </div>
</div>

<div class="s s-3">

    <div class="for-img" data-sr="enter top move 100px over 0.5s wait 0.2s">
        <img src="http://lifeandluxury.com/uploads/articles/content/07b65528a17425474b6fe41732e32fb6.jpg" data-img="img-3" width="100%" alt="">
    </div>

    <div class="cols">
        <div class="half">
            <div class="text" data-sr="enter left move 100px over 0.5s wait 0.4s">
                <p>what we once thought man was capable of, NASA continue to push the boundaries of scientific experimentation with the New Horizons mission to Pluto. The voyage of discovery is sure to unlock further secrets about our solar system and raise more questions about the mystery of the galaxy. </p>
            </div>
        </div>
        <div class="half" data-sr="enter right move 100px over 0.5s wait 0.6s">
            <div class="text">
                <p>Back on Earth there is no question about the capabilities of successful women with the Russian Business Woman of The Year Awards, highlighting the huge economic contribution that outstanding female individuals are making to our society.</p>
            </div>
        </div>
    </div>
</div>

<div class="s s-4">
    <div class="for-img" data-sr="move 100px roll -30deg hustle 200px over 0.5s wait 0.2s">
        <img src="http://lifeandluxury.com/uploads/articles/content/cd7759415391d81feda50f423b5e252a.png" data-img="img-4" alt="">
    </div>
</div>

<div class="s s-5">
    <div class="for-img" data-sr="move 100px roll 30deg hustle 200px over 0.5s wait 0.2s">
        <img src="http://lifeandluxury.com/uploads/articles/content/75c08147345d418dfbbc73f52e0f5624.jpg" data-img="img-5" alt="">
    </div>

    <div class="text" data-sr="move 100px roll -30deg hustle 200px over 0.5s wait 0.2s">
        <p>No longer grey and gloomy, the festive months are bursting with colour and the sweet smell of new opportunity mingles with the aromas of roaring log fires and intoxicating mulled wine. <br>Whether pushing the boundaries of science, excelling in the work place, or entertaining high society, let Life &amp; Luxury be your guide this winter and live a life less ordinary by going </p>
        <img src="/front/img/t16/7.jpg" alt="">
    </div>
</div>


