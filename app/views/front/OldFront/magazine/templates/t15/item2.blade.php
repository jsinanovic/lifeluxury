<div class="s s-1" data-sr="enter bottom move 100px spin 90deg roll 45deg over 1s wait 0.2s">
    <div class="block-table">

        <div class="block-cell img-cell"><div class="for-img"><img src="http://lifeandluxury.com/uploads/articles/content/9cbf08905cdbd2aff3607e454b08d888.jpg" data-img="img-2" alt=""></div></div>

        <div class="block-cell text-cell">
            <div class="text" data-sr="enter top move 100px flip 180deg over 0.5s wait 0.4s">
                <p style="text-transform: uppercase;font-family:'brandon_bld'">
                    <b>You’ve been known for being<br> the most outstanding host<br> among and beyond your<br> circle of acquaintances.<br>Everyone loves your</b>
                </p>

                <p style="padding: 0 5% 30px;"><img src="http://lifeandluxury.com/uploads/articles/content/496801428c26abfe5593e4366f057853.png" style="width: 699px;"><br></p>
                <p>
                    <span class="special">There’s always so much fun and joy, </span><br>
                    <span class="special">and of course, good words that</span><br>
                    <span class="special">keep echoing even after the party</span><br>
                    <span class="special">has ended weeks ago.</span><br>
                </p>
            </div>
        </div>

    </div>
</div>