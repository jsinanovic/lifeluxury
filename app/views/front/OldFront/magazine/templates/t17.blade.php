<div class="s s-1 clearfix">

    <div class="line" data-sr="enter left move 300px"><div class="circle"></div></div>

    <div class="for-img for-img-b" data-sr="enter bottom move 300px  wait 0.3s">
        <img src="http://lifeandluxury.com/uploads/articles/content/125d75b43df6b5e9a7b34d63bc09fd3e.jpg" data-img="img-1" alt="">
    </div>

    <div class="text" data-sr="enter right 300px wait 0.6s">
        <p>Aphrodite isn’t the only high powered woman making waves from her home in Cyprus. The island boasts an impressively high number of female entrepreneurs who are creators, innovators and extremely hard-workers, often combining their corporative titles with that of wife and mother. </p>
        <p>In what is still perceived as a male dominated society, these women are pushing the boundaries of the professional sectors, determined to reach their goals and objectives and highlight the huge economic contribution that businesswomen are making today. </p>
        <p>Thankfully, their achievements have not gone unnoticed and the Cyprus Awards Scheme has gained prestigious awareness in its objective to celebrate Russian Female Entrepreneurs with an award for The Russian Business Woman of the Year.             </p>
    </div>
</div>


<div class="s s-2 clearfix content-data-bg" data-key="img-2">

    <div class="line line-2" data-sr="enter right move 300px"><div class="circle"></div></div>

    <div class="for-img for-img-b" data-sr="enter bottom move 300px  wait 0.3s">
        <img src="http://lifeandluxury.com/uploads/articles/content/dc2e429cdec9b9eea58881dde3a7d56b.png" data-img="img-3" alt="">
    </div>

    <div class="text text-b" data-sr="enter right 300px wait 0.6s">
        <p>Founded in 2006 by Janice Ruffle, the award is the only one of its kind in Cyprus with a mission to recognise achievement and applaud female business success. The awards help to recognise, support and promote inspirational women who have overcome obstacles in setting up a business and progressed positively and pro-actively operating as a business owner, top professional or community leader.</p>
    </div>
    <img src="http://lifeandluxury.com/uploads/articles/content/43187189499f0befdafc52735f1649f0.jpg"></div>

<div class="s s-3 clearfix content-data-bg" data-key="img-4">

    <div class="line line-2" data-sr="enter right move 300px"><div class="circle"></div></div>

    <div class="for-img for-img-b" data-sr="enter bottom 300px wait 0.3s">
        <img src="http://lifeandluxury.com/uploads/articles/content/257ed03a6df19aa2a5ce1ed3278b38c5.jpg" data-img="img-5" alt="">
    </div>

    <div class="text text-b" data-sr="enter right 300px wait 0.6s">
        <p>Nominations require approval from the nominee or a direct nomination from a female entrepreneur. From those nominations six finalists are elected and forwarded to the judging panel for a personal interview. The judging panel is elected from candidates who have served the Cyprus commercial community and often includes previous winners.</p>
    </div>
    <img src="http://lifeandluxury.com/uploads/articles/content/2175617c276f2bfd29503f29f6acd1fa.jpg">
</div>

<div class="s s-4 clearfix">
    <div class="for-img">
        <img src="http://lifeandluxury.com/uploads/articles/content/bcb9e71d7d8bcdbaa287ef8d80e96392.jpg" data-img="img-6" alt="" data-sr="enter top 300px wait 0.3s">
    </div>

    <div class="text" data-sr="enter right 300px wait 0.6s">
        <p>This year the awards ceremony was hosted at the Hilton Hotel in Nicosia and presided over by three judges; Olga Balakleets is this year’s main judge and ambassador, she is CEO of Ensemble Productions and won last years Russian Business Woman of the Year Award; second judge Anna Homenko is Managing Director of Fiduciana Trust Limited; the final judge is Svetlana Kalogera who is Vice President and Head of Retail and Corporate Business Development at RCB Bank Ltd.</p><p> </p>
    </div>

    <div class="line line-3" data-sr="enter left move 300px"><div class="circle"></div></div>
</div>

<div class="s s-5 clearfix">
    <div class="for-img">
        <img src="http://lifeandluxury.com/uploads/articles/content/f0adebd5e0b6068c31fd3c94a681e497.jpg" data-img="img-7" alt="" data-sr="enter top 300px wait 0.3s">
    </div>

    <div class="text" data-sr="enter right 300px wait 0.6s">
        <p>The judges searched for female entrepreneurs who had generated awareness for the environment, embarked on training and self-development courses and enhanced good community relations. The focal point of recognition will be on the individual's journey to succeed and not the level of success, the determination reflected in her character to overcome all obstacles and face all challenges with a positive and constructive vision, sharing her experiences with the community. </p>
    </div>

    <div class="line line-3" data-sr="enter left move 300px"><div class="circle"></div></div>
</div>

<div class="s s-6 clearfix">

    <div class="line line-4" data-sr="enter right move 300px"><div class="circle"></div></div>

    <div class="for-img" data-sr="enter left 300px wait 0.6s">
        <img src="http://lifeandluxury.com/uploads/articles/content/8ff5d0aa49681322c80c5ab255a0ec64.jpg" data-img="img-8" alt="">
    </div>

    <div class="text" data-sr="enter bottom 300px wait 0.3s">
        <p>The spotlight of the evening was on Olesya Nikolskaya as she won the Top Professional category award and also brought home the Russian Business Woman of the Year Award Title. Successful category winners were Valeria Michael as 'Entrepreneur of the Year' and Mila Levinskaya in 'Community and Culture'. Of course the six finalists do not go home empty handed and are each presented with a branded prizes as well as recognition from the most prestigious event in the Cyprus business calendar. The event continues to gain widespread media coverage and helps to propel female-led business into the spotlight. Inspirational influential and another step forward for feminism and equality. The Russian Business Woman Awards is sure to gain even more success in the coming years.</p><p> 
        </p>
    </div>
</div>

