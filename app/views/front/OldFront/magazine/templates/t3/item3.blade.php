<div class="item">
    <div class="container">
        <div class="row row-same" data-sr="over 1s vFactor 0.4">
            <div class="col-sm-6 bg-center-cover content-data content-data-bg">
                <img src="http://lifeandluxury.com/uploads/articles/content/e7b2925c18f1ba29dfb9fa0f201edaa2.jpg">
            </div>
            <div class="col-sm-6 quote-image">
                <p>I want a yacht and really that's not a lot, sang Marilyn Monroe, arguably the most seductive of all celebrity fashion icons. Like Elizabeth Taylor, Marilyn stole hearts with her boldly coloured, fitted dresses and low cut necklines, but it was her fondness for jewels that made diamonds her best friend. A true Hollywood diva, Marilyn was often a guest on the most luxurious yachts of the golden era and was pictured numerous times in her signature one-piece strapless white bathing suit. </p>
            </div>
        </div>

        <div class="row" data-sr="over 1s vFactor 0.4">
            <div class="col-md-12 fashion-bg">
                <p style="font-size: 24px;">A lover of accessories, Marilyn would add a wide brimmed beach hat or delicate kerchief to her outfit, finishing off the look with large black sunglasses still favoured by many women today. In contrast to the flirtatious hourglass figures of Elizabeth and Marilyn, fashionistaвЂ™s all over the world continue to model their look on the eternally beautiful, poetically petite Audrey Hepburn. With a style defined by grace and simplicity, Audrey preferred basic staples that oozed culture and class and was often photographed in elegant tailored pieces with a simple colour palette.</p>
            </div>
        </div>

        <div class="row row-same" data-sr="over 1s vFactor 0.4">
            <div class="col-sm-6 quote-image">
                <p>Hair up in a kerchief and shades on under impeccably-maintained bangs, images of Audrey playfully posing on a yacht are as ravishing as they are sweet. Teaming high waisted shorts with an effortlessly elegant shirt, Audrey could pull off a dainty tomboyish look whilst still exuding girly glamour, without the need for a revealing bathing suit. Considered the epitome of sophistication, Audrey Hepburn continues to be one of the worldвЂ™s most heralded style icons and a sure style-inspiration for those reclining on the Riviera.</p>
            </div>
            <div class="col-sm-6 bg-center-cover content-data content-data-bg">
                <img src="http://lifeandluxury.com/uploads/articles/content/79de211c3477a6ec690f802ace5f9c35.jpg">
            </div>
        </div>
    </div>
</div>