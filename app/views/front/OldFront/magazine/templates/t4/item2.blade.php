<div data-sr="scale down 50% over 1s">
    <div class="item item-photo-full content-data-bg">
        <div class="block-table">
            <div class="block-cell">
                <p><span style="color: rgb(255, 255, 255); font-family: Slabo13px, 'Myriad Pro', ProximaNovaRegular; font-size: 16.0020008087158px; font-weight: normal; line-height: 24.003002166748px; text-align: center;">After Wall Street imploded in 2008, Greece became the epicentre of Europe’s debt crisis and by the spring of 2010, the nation was veering toward bankruptcy. Greece owed huge sums to European banks who in turn worried that the unpaid loans would threaten the financial sector with a domino effect leading to another credit crunch. The reasons behind the country’s epic financial fall from grace are marred in controversy. Years of widespread tax evasion have bared the brunt of the blame, along with excessive public spending and high public sector wages.</span><br></p>
            </div>
        </div>
        <img src="http://lifeandluxury.com/uploads/articles/content/a007f41194f71a7c8c85864011be0a0a.jpg">
    </div>
</div> 