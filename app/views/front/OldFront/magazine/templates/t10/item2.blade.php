<div class="item item-dark">
        <div class="container">
            <div class="row">
                <div class="col-md-5">
                    <div data-sr="enter left, move 100px, wait 0.5s">
                        <p><span style="color: rgb(255, 255, 255);">Without a doubt, the limelight is still reserved for the main stars of the entire show – the ones that may grant the IAA Frankfurt the title of “2015’s No.1 Automotive Event”: Mercedes-Benz S-Class Cabriolet, Bentley Bentayga, Audi A4 Limousine and Avant, Maserati Levante (SUV), Alfa Romeo Giulia Quadrifoglio, BMW 7 series, Infiniti Q30, Toyota 4th-generation Prius hybrid, Lamboghini Huracan Spyder, Peugeot 308 GTi, Volkswagen Tiguan, Renault Megan, Porsch 911 facelift, and Jaguar F-PACE Performance Crossover.
                            </span></p><p><span style="color: rgb(255, 255, 255);">
                                Thanks to such a stellar line-up waiting for their covers to be pulled, the excitement and expectations projected on to the IAA Frankfurt 2015 cannot soar any higher, and those are not even a full list; notwithstanding, only a few gems of the crown would be mentioned briefly to save the thrill of discovery for the real show.
                            </span></p>
                    </div>
                </div>
                <div class="col-md-7">
                    <div class="for-img content-data-img" data-key="img-2" data-sr=""><img src="http://lifeandluxury.com/uploads/articles/content/d5a812258240aa45388a467f39aca229.png"></div>
                </div>
            </div>
        </div>
    </div>