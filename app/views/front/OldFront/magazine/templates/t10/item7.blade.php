<div class="item item-dark">
    <div class="container">
        <div class="row">
            <div class="col-md-5">
                <div data-sr="enter left, move 100px, wait 0.5s">
                    <p><span style="color: rgb(255, 255, 255);">Meanwhile, Alfa Romeo is bringing its high sense of Italian fashion to the stage with sensuous Giulia Quadrifoglio. Bold and stylish, the all-new Giulia achieves sportiness and sophistication without the typical chromium embellishments and big fender flares. Equipped with new rear-wheel-drive platform, carbon ceramic brakes and a guarantee of perfect 50/50 weight distribution through the use of Carbon Fibre and aluminium, Giulia is the promise of a new dawn for the historic automaker.</span></p><p><img src="http://lifeandluxury.com/uploads/articles/content/b83d17d7c44b325c926dd86d26349266.png" style="width: 501px;"><br></p>
                </div>
            </div>
            <div class="col-md-7">
                <div class="for-img content-data-img" data-key="img-7" data-sr=""><img src="http://lifeandluxury.com/uploads/articles/content/a8665401347a407beaf94f7c1e7b93f5.png"></div>
            </div>
        </div>
    </div>
</div>