<div class="item item-light">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="for-img img-minus content-data-img" data-key="img-5" data-sr=""><img src="http://lifeandluxury.com/uploads/articles/content/bd3c8c7ca8b96ac60786be5977a70863.png"></div>
            </div>
            <div class="col-md-5">
                <div data-sr="enter bottom, move 100px, wait 0.5s">
                    <p>Nevertheless, the German giants shall not steal the entire spotlight of the show. British luxury automaker Bentley is patiently counting down to the IAA Frankfurt 2015, when the Op-Art-inspired camouflage is unveiled to reveal Bentayga – “the world’s most luxurious and fastest SUV” thus far. The company’s masterpiece promises to become the pinnacle of performance and craftsmanship for SUVs. The structural mix of high-strength steel and aluminium, the four round headlamps that edge the matrix grille, and the signature B-shaped wing vent behind the front wheel arch are just a few verses of this pioneering ode to lights, texture and power.</p>
                </div>
            </div>
        </div>
    </div>
</div>