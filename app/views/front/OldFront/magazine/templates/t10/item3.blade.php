<div class="item item-light">
        <div class="container">
            <div class="row">
                <div class="col-md-7">
                    <div class="for-img content-data-img" data-key="img-3" data-sr=""><img src="http://lifeandluxury.com/uploads/articles/content/66a268ee34e286fef0aadb241f4c67ba.png"></div>
                </div>
                <div class="col-md-5">
                    <div data-sr="enter right, move 100px, wait 0.5s">
                        <p>Being the constant apex of luxury and technology among German automakers, BMW continues to bring forth yet another evolutionary design through their new 7 Series: the seamless merge of peripheral chromium-plated window into the Hofmeister kink, the pioneering L-shaped rear light bars, the Executive Lounge featuring an optional massage function for rear-seat passengers, and the efficient dynamics of lightweight construction complex; it is no wonder that all eyes are on the lucky number 7 of BMW.</p><img src="http://lifeandluxury.com/uploads/articles/content/e28d0452249bd4a6cbf40a3d3aea1e1b.png" style="width: 495px;">
                    </div>
                </div>
            </div>
        </div>
    </div>