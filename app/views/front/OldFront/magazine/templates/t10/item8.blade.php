<div class="item item-dark">
    <div class="container">
        <div class="row">
            <div class="col-md-7">
                <div class="for-img content-data-img" data-key="img-8" data-sr=""><img src="http://lifeandluxury.com/uploads/articles/content/303be0a38e52f976e3b87ba602d885fe.png"></div>
            </div>
            <div class="col-md-5">
                <div data-sr="enter right, move 100px, wait 0.5s">
                    <p><span style="color: rgb(255, 255, 255);">Raising a parallel amount of curiosity is the Italian cousin Maserati Levante. As far as leaked sources confirm, the brand’s very first SUV will reportedly adapt large air dams, a great hexagonal gaping grille with eight vertical slaps, and fenders fitted with three vents from the Ghibli and Quattroporte saloons. A touch of inspiration from the exterior design of Maserati Kubang will also be added. What distinguishes Levante from the earlier successful models is a taller, sportier silhouette with sharp, conventional headlights, and a heightened roofline section. Despite the continued mysteries surrounding this five-seat beauty, Levante is unquestionably a strong competitor in the SUV arena.</span></p><p><img src="http://lifeandluxury.com/uploads/articles/content/ff6462b65aff9a10cb3552178d907287.png" style="width: 482px;"><span style="color: rgb(255, 255, 255);"><br></span></p>
                </div>
            </div>
        </div>
    </div>
</div>