<div class="item item-doubled" data-sr="vfactor 0.5 enter right">
    <div class="container">
        <div class="item-num item-num-left" data-sr="wait 0.3s enter top">
            <span>02</span>
        </div>
        <div class="block-table">
            <div class="block-cell">
                <div class="special-img content-data-bg" data-key="img-2"><img src="http://lifeandluxury.com/uploads/articles/content/2dbd59db416f297f605e935f70b77f4e.png"></div>
                <p><span style="color: rgb(255, 255, 255);"> As the word luxury originates from the Latin word for excess, it suggests products which are not only expensive, exclusive and quite possibly, difficult to obtain, but it also suggests products which are in not essential to the needs of everyday lie. As our concept of what constitutes a necessity, rather than a luxury, is constantly evolving in this modern age, many of the things which we once believed to be luxuries are now seen as essential to everyday life.If our idea of what constitutes luxury is constantly evolving, then product design must also evolve to capture that never ending quest for greater comfort, design, craftsmanship and desirability.</span></p>
            </div>
            <div class="block-cell" data-sr="wait 0.5s opacity 0.5 spin -90deg">
                <p> As we now, as consumers, demand ever higher standards form manufacturers, the bar as to what is considered to be luxury is continually rising. It seems that designers and manufacturers of luxury products have been scrutinizing the markets closely and have surreptitiously been asking the question: Is luxury for its own sake as desirable as it was a decade ago?  Consumer research undertaken by many luxury bands have not only seem a shift in thinking towards sustainability, but some are ready to change and influence the current markets themselves, moving towards a climate in which ‘sustainable luxury’ is not only desirable, but a major influencing factor in the future of product design.</p>
            </div>
        </div>
    </div>
</div>