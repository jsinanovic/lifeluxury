<div class="item item-col-3">
    <div class="container-fluid">
        <div class="row-fluid">
            <div class="col-md-4 col-up" data-sr="wait 0.2s vFactor 0.3 enter bottom scale up 40%">
                <h4>GENERATING REVENUE</h4>

                <p style="padding-bottom: 0">As well as creating around 40 full time new jobs in service provision to support terminal and shipping activities, it is estimated that the facility will generate in the region of 10 to 18 million per year via port dues and fees - which is impressive.</p>

                <img class="img-bordered" src="/front/img/t9/event.jpg" alt="">
            </div>
            <div class="col-md-5" data-sr="vFactor 0.3 enter top scale down 30%">
                <div class="for-img content-data-img" data-key="img-3">
                    <img src="http://lifeandluxury.com/uploads/articles/content/3f80650117b3428f66785756d532501a.jpg">
                </div>
            </div>
            <div class="col-md-3" data-sr="wait 0.5s vFactor 0.3 opacity 0 enter right scale down 20%">
                <h4>CREATING BENEFITS</h4>
                <p>Cypriot President Nicos Anastasiades proffered these benefits at the inaugural ceremony, ???There is also the opportunity for storage for Cyprus??™ home fuel markets and the possibility of repatriating stores from outside the island. It is important to say that the cost of storing the national stocks in these facilities will remain at the same level, while, at the same time, an opportunity has been created to repatriate the reserves we kept abroad, securing a strategic advantage in case of a sudden energy crisis.???</p>
                <p>In effect, despite its boost to employment and other local benefits, this new storage opportunity would seem not to automatically translate into a price reduction for domestic users, as many consumers had hoped. Compulsory stocks are currently held abroad in Greece and Malta, as well as locally in Cyprus, at a cost the 17 million per year.</p>
            </div>
        </div>
    </div>
</div>