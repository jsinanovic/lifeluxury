<div class="item item-col-3">
    <div class="container-fluid">
        <div class="row-fluid">
            <div class="col-md-4" data-sr="enter left scale down 20%">
                <h2>VTTV VASILIKO CYPRUS</h2>
                <blockquote>
                    <p>Tanks: 28 </p>
                    <p>Capacity 544,000 m3 </p>
                    <p>Marine Jetty: 1500m </p>
                    <p>Special Features:</p>
                    <p>Road tanker loading facilities.</p>
                </blockquote>
            </div>
            <div class="col-md-4" data-sr="wait 0.5s opacity 0 enter top scale down 20%">
                <div class="article-view-category">
                    Business  |  <span>Life &amp; Luxury Magazine</span>
                </div>
                <p>Attracting major foreign investment into Cyprus in a time of global recession was never going to be easy, so when Netherlands based VTTI B.V., a major player in oil and fuel storage, announced their interest in building a state of the art storage facility with a brand new deep water marine jetty at Vasiliko, many wondered how and when this project would reach fruition. The project itself was not without initial delays and setbacks, but since November 2014 phase1 is now fully operational.</p>
            </div>
            <div class="col-md-4" data-sr="wait 1s  opacity 0 enter right scale down 20%">
                <div class="for-img content-data-img" data-key="img-1">
                    <img src="http://lifeandluxury.com/uploads/articles/content/cbdd0357551a2d10cec2d59e18cdc5b7.jpg">
                </div>
            </div>
        </div>
    </div>
</div>