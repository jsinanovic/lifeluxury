<div class="item item-col-3">
    <div class="container-fluid">
        <div class="row-fluid">
            <div class="col-md-4" data-sr="wait 0.8s enter left scale down 30%">
                <h4>FUTURE DEVELOPMENT</h4>
                <p>It is believed that the new facility will attract more tankers as well as tankers with higher holding capacities. Should recent discoveries of natural gas and hydrocarbon resources come to fruition, there is also the possibility of export. Current discussions taking place about the upgrading of the Suez Canal to allow ships of larger capacities to pass through it, could also affect the landscape of the international energy markets, resulting in new business at Vasiliko. 
                </p><p>
                    Up until recently, 250 trans-shipments per year were taking place in the open sea around Cyprus, but this is expected to change dramatically with the marine jetty now in use. To cope with the projected increased in demand, a phase 2 expansion plan is currently under evaluation, and this would create:
                </p>
                <blockquote>
                    <p>Additional Tanks: 13</p>
                    <p>Additional Capacity: 305,000 m3.</p>
                </blockquote>
            </div>
            <div class="col-md-8" data-sr="wait 0.3s enter top scale up 50%">
                <div class="for-img content-data-img" data-key="img-5">
                    <img src="http://lifeandluxury.com/uploads/articles/content/78305ded184eb9510404ee57ed935cb4.jpg">
                </div>
            </div>
        </div>
    </div>
</div>