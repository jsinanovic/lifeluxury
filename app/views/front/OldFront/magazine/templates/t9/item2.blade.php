<div class="item item-col-3">
    <div class="container-fluid">
        <div class="row-fluid">
            <div class="col-md-4" data-sr="enter left scale down 20%">
                <h3>Cyprus a strategic energy hub</h3>
                <p>Does this description make you smile too? - it is of course referring to Cyprus??™ import/export trans-shipment of oil products to international markets and trade with the Eastern Med, Middle East and European markets. </p>
            </div>
            <div class="col-md-4" data-sr="wait 0.5s opacity 0 enter top scale down 20%">
                <p>It is strategic for VTTI V.B., because, ???Until now, Malta was the main turning circle in the Eastern Mediterranean for oil flows to Lebanon, Egypt and Turkey. In addition, significant quantities were in floating storage on vessels in the open sea in the Mediterranean, which could be a major risk for the environment,??? according to CEO Rob Nijst. It appears that storage in Cyprus is not only a commercial advantage but has some environmental benefits too. </p>
                <p>This obviously would apply if there was a serious accident off the shores of Cyprus, which could prove catastrophic for Cyprus??™ beaches and tourism, but onshore storage could also reduce future potential damage to the environment and wildlife, which is most definitively, positive progress.</p>
            </div>
            <div class="col-md-4" data-sr="wait 1s  opacity 0 enter right scale down 20%">
                <div class="for-img content-data-img" data-key="img-2">
                    <img src="http://lifeandluxury.com/uploads/articles/content/19a2643437bed4ab0ec0dab841347bdd.jpg">
                </div>
            </div>
        </div>
    </div>
</div>