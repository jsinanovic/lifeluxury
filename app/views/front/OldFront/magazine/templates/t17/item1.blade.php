<div class="s s-1 clearfix">

    <div class="line" data-sr="enter left move 300px"><div class="circle"></div></div>

    <div class="for-img for-img-b" data-sr="enter bottom move 300px  wait 0.3s">
        <img src="http://lifeandluxury.com/uploads/articles/content/125d75b43df6b5e9a7b34d63bc09fd3e.jpg" data-img="img-1" alt="">
    </div>

    <div class="text" data-sr="enter right 300px wait 0.6s">
        <p>Aphrodite isn’t the only high powered woman making waves from her home in Cyprus. The island boasts an impressively high number of female entrepreneurs who are creators, innovators and extremely hard-workers, often combining their corporative titles with that of wife and mother. </p>
        <p>In what is still perceived as a male dominated society, these women are pushing the boundaries of the professional sectors, determined to reach their goals and objectives and highlight the huge economic contribution that businesswomen are making today. </p>
        <p>Thankfully, their achievements have not gone unnoticed and the Cyprus Awards Scheme has gained prestigious awareness in its objective to celebrate Russian Female Entrepreneurs with an award for The Russian Business Woman of the Year.             </p>
    </div>
</div>