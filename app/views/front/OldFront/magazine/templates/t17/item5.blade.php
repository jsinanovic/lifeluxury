<div class="s s-5 clearfix">
    <div class="for-img">
        <img src="http://lifeandluxury.com/uploads/articles/content/f0adebd5e0b6068c31fd3c94a681e497.jpg" data-img="img-7" alt="" data-sr="enter top 300px wait 0.3s">
    </div>

    <div class="text" data-sr="enter right 300px wait 0.6s">
        <p>The judges searched for female entrepreneurs who had generated awareness for the environment, embarked on training and self-development courses and enhanced good community relations. The focal point of recognition will be on the individual's journey to succeed and not the level of success, the determination reflected in her character to overcome all obstacles and face all challenges with a positive and constructive vision, sharing her experiences with the community. </p>
    </div>

    <div class="line line-3" data-sr="enter left move 300px"><div class="circle"></div></div>
</div>