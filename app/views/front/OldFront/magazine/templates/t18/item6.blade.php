<div class="s row">
    <div class="col-md-6">
        <div class="img-holder" data-sr="flip -20deg, spin -20deg, hustle 40px">
            <img src="http://lifeandluxury.com/uploads/articles/content/760686de93bb319312f3ef65daf1ea50.jpg" data-img="img-8" alt="">
        </div>
    </div>
    <div class="col-md-5 col-md-offset-1">
        <div class="text" data-sr="wait 0.5s, ease-in-out 100px, scale down 25%">
            <p>
                Throughout this holiday season, expect a snow-coated
                fairyland decorated with fir trees, lavish ornaments,
                the majestic luxury shopping complex GUM, the colourful
                Red Square and its symbolic St. Basil’s Cathedral.   
            </p>
        </div>
    </div>
</div>