<div class="s">
    <div class="row row-1">
        <div class="col-md-4 col-md-offset-1">
            <div class="text" data-sr="wait 0.5s, ease-in-out 100px, scale down 25%">
                <h2>THE OFFICIAL HOMETOWN Of<span><br>SANTA CLAUS</span></h2>
                <i></i>
                <p>Ho ho ho thus comes<br>Santa Claus! </p> 
            </div>
        </div>
        <div class="col-md-7">
            <div class="img-holder" data-sr="flip -20deg, spin -20deg, hustle 40px">
                <img src="http://lifeandluxury.com/uploads/articles/content/f543709277b6e01630801deaa951238d.jpg" data-img="img-1" alt="">
            </div>
        </div>
    </div>
    <div class="row row-2">
        <div class="col-md-5 col-md-offset-1">
            <div class="img-holder" data-sr="flip -20deg, spin -20deg, hustle 40px">
                <img src="http://lifeandluxury.com/uploads/articles/content/5b3331df337903422d358402050dc5ab.jpg" data-img="img-2" alt="">
            </div>
        </div>
        <div class="col-md-5">
            <div class="text" data-sr="wait 0.5s, ease-in-out 100px, scale down 25%">
                <p>
                    Here in Santa’s home-sweet-home, stay at the Santa’s Suite
                    of the Artic Light Hotel, or have Luxury Action customise
                    your high-end holiday with exclusive Nordic-style experience, 
                    such as a VIP cruise on an icebreaker, nights of a lifetime 
                    in an ice or glass igloo underneath the Northern Lights, 
                    delicious cocktails served at the Ice Bar amidst the frozen sea,
                    and just-for-you firework display in the deep winter sky of Finland. 
                </p>
                <i>Christmas could never be more authentic.</i>
            </div>
        </div>
    </div>
</div>