<div data-sr="">
    <div class="item item-bg-center content-data-bg" data-key="img-7">
        <div class="item-cell" data-sr="vFactor 0.8 wait 0.6s enter left scale down 20%">
            <p><span style="color: rgb(255, 255, 255);">A few glasses of sake and plum wines later at the bar in the 
                    same vicinity, your friends call. They already got to Dolce Club and are waiting for your arrival. You left with your partner, head for the venue, look for your VIP table, greet your friends, pop open a champagne bottle, and toast to a wonderful evening basked in lights, upbeat music, cosy sea breeze and a cosmopolitan ambience with the best company ever.  The rest of the evening is history.</span></p>
        </div>
        <div class="item-cell" data-sr="vFactor 0.8 wait 0.3s roll 45deg, spin -45deg, flip 45deg">
            <div class="for-img content-data-img" data-key="img-8"><img src="http://lifeandluxury.com/uploads/articles/content/ac6a322b790e1cba2ab57a0f500c2349.jpg"></div>
        </div>
        <img src="http://lifeandluxury.com/uploads/articles/content/6edc1f167e7cb8b5a5725492ec4ad213.jpg"></div>
</div>