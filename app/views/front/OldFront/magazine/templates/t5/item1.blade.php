<div data-sr="">
    <div class="item item-bg-center content-data-bg" data-key="img-1">
        <div class="item-cell">
            <div class="container">
                <div class="row">
                    <div class="col-md-8 col-md-offset-2" data-sr="wait 0.3s flip 90deg over 1.5s">
                                                <p style="color:#fff;">
                            There are days when we have everything planned out from the first sound of the    
                            alarm clock until the first wave of dreams. Then there are also days when we decide to let loose and roll along as the day unfolds. 
                        </p>
                                            </div>
                </div>
            </div>
        </div>
    <img src="http://lifeandluxury.com/uploads/articles/content/33730a275dcd468c4b12924eb945731d.jpg"></div>
</div>