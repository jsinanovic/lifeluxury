<div>
    <div class="item">
        <img src="http://lifeandluxury.com/uploads/articles/content/cb3a22bdda44d577a669d3e4eb98f336.jpg" data-img="img-2" width="100%" alt="" data-sr="enter left, move 100px">
        <div class="vs-block vs-block-2">
            <div class="vs-block-inner">
                <div class="text" data-sr="rotate 45deg over 0.5s">
                    <p>But there still exists a much more festive recipe: Take the mixture above and sprinkle glamour on top. Add as much creativity as you can, decorate with entertainment, joy and attention, and the final result is one of the U.S.A’s leading annual fashion shows – Victoria’s Secret Fashion Show.<br></p>
                </div>

                <div class="for-parallax" style="transform: translate3d(0px, 0px, 0px); transform-style: preserve-3d; backface-visibility: hidden;">
                    <li class="layer border" data-depth="0.10" style="left: 0px; top: 0px; position: relative; display: block; height: 100%; width: 100%; transform: translate3d(-0.970423%, -1.19476%, 0px); transform-style: preserve-3d; backface-visibility: hidden;"></li>
                </div>

            </div>
        </div>

        <div class="triangle triangle-2"></div>

    </div>
</div>