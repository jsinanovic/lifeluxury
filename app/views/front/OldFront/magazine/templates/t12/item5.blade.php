<div>
    <div class="item">
        <img src="http://lifeandluxury.com/uploads/articles/content/e52ecc5ff578e79d194133569e097ab9.jpg" data-img="img-5" width="100%" alt="" data-sr="enter right, move 100px">
        <div class="vs-block vs-block-5">
            <div class="vs-block-inner">
                <div class="text" data-sr="rotate 45deg over 0.5s">
                    <p>Let us not forget, though, the beautiful Angels who made this an all-star fashion event: Adriana Lima, Lily Aldridge, Candice Swanepoel, Alessandra Ambrosio and Behati Prinsloo, along with the fresh faces of Angels Stella Maxwell, Martha Hunt, Romee Strijd, Jac Jagaciak, Jasmine Tookes, Elsa Hosk, Lais Ribeiro, Sara Sampaio, Kate Grigorieva and Victoria’s Secret PINK Supermodel Rachel Hilbert. It is to no one’s surprise that they are called angels, since if it was not for their exuded sensual charm, those pieces of design would just be another lacklustre sketch.<br></p>
                </div>

                <div class="for-parallax" style="transform: translate3d(0px, 0px, 0px); transform-style: preserve-3d; backface-visibility: hidden;">
                    <li class="layer border" data-depth="0.10" style="left: 0px; top: 0px; position: relative; display: block; height: 100%; width: 100%; transform: translate3d(-0.970423%, -1.19476%, 0px); transform-style: preserve-3d; backface-visibility: hidden;"></li>
                </div>

            </div>
        </div>

        <div class="triangle triangle-5"></div>

    </div>
</div>