<div>
    <div class="item">
        <img src="http://lifeandluxury.com/uploads/articles/content/ceeebd7b578ca269b4b202ce47d2c044.jpg" data-img="img-1" width="100%" alt="" data-sr="enter right, move 100px">
        <div class="vs-block vs-block-1">
            <div class="vs-block-inner">
                <div class="text" data-sr="rotate 45deg over 0.5s">
                    <p>Mix charm with beauty, then blend them together with a great amount of inspiration and entrepreneurial spirit. Finally garnish with a gentleman’s shyness and adoration for his lady, and that is how you obtain Victoria’s Secret – the largest American retailer of women’s lingerie.</p>
                </div>

                <div class="for-parallax" style="transform: translate3d(0px, 0px, 0px); transform-style: preserve-3d; backface-visibility: hidden;">
                    <li class="layer border" data-depth="0.10" style="left: 0px; top: 0px; position: relative; display: block; height: 100%; width: 100%; transform: translate3d(-0.970423%, -1.19476%, 0px); transform-style: preserve-3d; backface-visibility: hidden;"></li>
                </div>

            </div>
        </div>

        <div class="triangle triangle-1"></div>

    </div>
</div>