<div>
    <div class="item">
        <img src="http://lifeandluxury.com/uploads/articles/content/3c6cff3125cd4fcf5d4cf531c07ee63e.jpg" data-img="img-4" width="100%" alt="" data-sr="enter left, move 100px">
        <div class="vs-block vs-block-4">
            <div class="vs-block-inner">
                <div class="text" data-sr="rotate 45deg over 0.5s">
                    <p>Looking back at the themes for previous shows such as Calendar Girls, Angel Ball, British Invasion and Circus, there were certainly times when it was thought that the show could not get any more imaginative than that, and we couldn’t be any more mistaken.&nbsp;<br><br>Exotic Traveller, Silver Screen Angels, and Gilded Angels, it is evident that the sky is not the limit for Victoria’s Secret.<br></p>
                </div>

                <div class="for-parallax" style="transform: translate3d(0px, 0px, 0px); transform-style: preserve-3d; backface-visibility: hidden;">
                    <li class="layer border" data-depth="0.10" style="left: 0px; top: 0px; position: relative; display: block; height: 100%; width: 100%; transform: translate3d(-0.970423%, -1.19476%, 0px); transform-style: preserve-3d; backface-visibility: hidden;"></li>
                </div>

            </div>
        </div>

        <div class="triangle triangle-4"></div>

    </div>
</div>