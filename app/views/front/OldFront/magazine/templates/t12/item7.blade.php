<div>
    <div class="item">
        <img src="http://lifeandluxury.com/uploads/articles/content/59786a9d4b9c37130a9c6bd5a9c64a2e.jpg" data-img="img-7" width="100%" alt="" data-sr="enter right, move 100px">
        <div class="vs-block vs-block-7">
            <div class="vs-block-inner">
                <div class="text" data-sr="rotate 45deg over 0.5s">
                    <p>As 2015 is slowly coming to an end, the revelation of Victoria’s Secret would make a perfect closure to such an eventful and successful year. At the end, Victoria’s Secret is perhaps the best kept secret of all. </p>
                </div>

                <div class="for-parallax" style="transform: translate3d(0px, 0px, 0px); transform-style: preserve-3d; backface-visibility: hidden;">
                    <li class="layer border" data-depth="0.10" style="left: 0px; top: 0px; position: relative; display: block; height: 100%; width: 100%; transform: translate3d(-0.970423%, -1.19476%, 0px); transform-style: preserve-3d; backface-visibility: hidden;"></li>
                </div>

            </div>
        </div>

        <div class="triangle triangle-7"></div>

    </div>
</div>