<div>
    <div class="item">
        <img src="http://lifeandluxury.com/uploads/articles/content/06b70f2907bd4c38626f935f3caaf8b8.jpg" data-img="img-3" width="100%" alt="" data-sr="enter right, move 100px">
        <div class="vs-block vs-block-3">
            <div class="vs-block-inner">
                <div class="text" data-sr="rotate 45deg over 0.5s">
                    <p>Indeed, it was the fantastical transformation of the otherwise-ordinary lingerie and womenswear that have pushed Victoria’s Secret Fashion Show to its high standing today. Every year, the creative force behind this fashion dream line decides on the different themes for the show of that particular year, from which point their imagination would go beyond the stars with elaborate details, bold touch-ups and break-through ideas for costumed lingerie.<br></p>
                </div>

                <div class="for-parallax" style="transform: translate3d(0px, 0px, 0px); transform-style: preserve-3d; backface-visibility: hidden;">
                    <li class="layer border" data-depth="0.10" style="left: 0px; top: 0px; position: relative; display: block; height: 100%; width: 100%; transform: translate3d(-0.970423%, -1.19476%, 0px); transform-style: preserve-3d; backface-visibility: hidden;"></li>
                </div>

            </div>
        </div>

        <div class="triangle triangle-3"></div>

    </div>
</div>