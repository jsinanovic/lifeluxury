<div class="s s-0" data-sr="enter bottom move 100px spin 90deg roll 45deg over 1s wait 0.2s">
    <div class="block-table">
        <div class="block-cell text-cell">
            <div class="text" data-sr="enter top move 100px flip 180deg over 0.5s wait 0.4s">
                <p style="text-transform: uppercase">
                    Wrapping fingers<br>
                    around your cup of morning<br>
                    coffee, you stare nonchalantly at<br> 
                    the calendar. <br>
                </p>

                <p style="padding: 0 5% 30px;">
                    <img src="/front/img/t15/c1.jpg" alt="">
                </p>

                <p>
                    <span class="special">Two months before your</span><br>
                    <span class="special">most favourite holiday of</span><br>
                    <span class="special">the entire year.</span><br>
                </p>
            </div>
        </div>

        <div class="block-cell img-cell"><div class="for-img"><img src="/front/img/t15/1.jpg" alt=""></div></div>
    </div>
</div>

<div class="s s-1" data-sr="enter bottom move 100px spin 90deg roll 45deg over 1s wait 0.2s">
    <div class="block-table">

        <div class="block-cell img-cell"><div class="for-img"><img src="http://lifeandluxury.com/uploads/articles/content/9cbf08905cdbd2aff3607e454b08d888.jpg" data-img="img-2" alt=""></div></div>

        <div class="block-cell text-cell">
            <div class="text" data-sr="enter top move 100px flip 180deg over 0.5s wait 0.4s">
                <p style="text-transform: uppercase;font-family:'brandon_bld'">
                    <b>You’ve been known for being<br> the most outstanding host<br> among and beyond your<br> circle of acquaintances.<br>Everyone loves your</b>
                </p>

                <p style="padding: 0 5% 30px;"><img src="http://lifeandluxury.com/uploads/articles/content/496801428c26abfe5593e4366f057853.png" style="width: 699px;"><br></p>
                <p>
                    <span class="special">There’s always so much fun and joy, </span><br>
                    <span class="special">and of course, good words that</span><br>
                    <span class="special">keep echoing even after the party</span><br>
                    <span class="special">has ended weeks ago.</span><br>
                </p>
            </div>
        </div>

    </div>
</div>



<div class="s s-2" data-sr="enter bottom move 100px spin 90deg roll 45deg over 1s wait 0.2s">
    <div class="block-table">


        <div class="block-cell text-cell">
            <div class="text" data-sr="enter top move 100px flip 180deg over 0.5s wait 0.4s">
                <p style=" font-family: 'brandon_med';">Probably not this year. <br>
                    You sigh and glance quickly<br> 
                    at the flashing light<br> 
                    on your smart phone. Work has<br> 
                    been keeping you preoccpied for<br>
                    the most part of the year.<br> 
                    You’ve been working hard and your<br>
                    dream is almost with in reach. </p>





                <p style="text-transform: uppercase; font-family: 'brandon_blk'; font-size: 47px; line-height: 1;">
                    Nothing can stop you<br><span style="font-size:76px;">now.</span>
                </p>





                <p>
                    <span class="special">Not even your legendary annual</span><br>
                    <span class="special">Christmas event. The phone vibrates</span><br>
                    <span class="special">again, as if to remind you of that fact.</span><br>
                    <span class="special">What a shame.</span><br>
                </p>
            </div>
        </div>

        <div class="block-cell img-cell"><div class="for-img"><img src="/front/img/t15/3.jpg" alt=""></div></div>
    </div>
</div>
<div class="s s-3" data-sr="enter bottom move 100px spin 90deg roll 45deg over 1s wait 0.2s">
    <div class="block-table">

        <div class="block-cell img-cell"><div class="for-img"><img src="http://lifeandluxury.com/uploads/articles/content/0ec1f7abca34346d5410acb55be01e43.jpg" data-img="img-4" alt=""></div></div>

        <div class="block-cell text-cell">
            <div class="text" data-sr="enter top move 100px flip 180deg over 0.5s wait 0.4s">
                <p>As you’re heading out for work, something<br> caught your attention. The name card of that<br> fellow you met the other day at a networking<br> event lay quietly on top of your entry table.</p>

                <p><br></p>

                <p>
                    <span class="special">There’s always so much fun and joy,</span><br>
                    <span class="special">and of course, good words that</span><br>
                    <span class="special">keep echoing even after the party</span><br>
                    <span class="special">has ended weeks ago.</span><br>
                </p>
            </div>
        </div>

    </div>
</div>
<div class="s s-4" data-sr="enter bottom move 100px spin 90deg roll 45deg over 1s wait 0.2s">
    <div class="block-table">


        <div class="block-cell text-cell">
            <div class="text" data-sr="enter top move 100px flip 180deg over 0.5s wait 0.4s">
                <p style="font-size: 42px; font-family: 'brandon_med'">A brilliant idea lights up in your<br>
                    head. You take the card along and dial<br> the number as your driver carries on with<br> his usual routine. If you can’t personally take<br> care of it, why not trust a professional who can<br> make sure that every minute detail is tailored<br> according to your taste? Frankly speaking, you’ve<br> never tried Life &amp; Luxury’s service, but you liked the<br> fellow. He seemed capable and reliable. Why not put<br> their skills to test, right?<br><br>

                    A meeting is immediately arranged. Contract<br> signed. Hands are shaken. Relieved yet<br> nervous, you wonder if it would be a<br> good idea to trust a stranger with<br>such a big task after all.</p>
            </div>
        </div>

        <div class="block-cell img-cell"><div class="for-img"><img src="/front/img/t15/5.jpg" alt=""></div></div>
    </div>
</div>
<div class="s s-5" data-sr="enter bottom move 100px spin 90deg roll 45deg over 1s wait 0.2s">
    <div class="block-table">

        <div class="block-cell img-cell"><div class="for-img"><img src="http://lifeandluxury.com/uploads/articles/content/8cc05bb719c554c3cef23460913f015c.jpg" data-img="img-6" alt=""></div></div>

        <div class="block-cell text-cell">
            <div class="text" data-sr="enter top move 100px flip 180deg over 0.5s wait 0.4s">
                <p style="font-size: 42px; font-family: 'brandon_med'">As time passes and the event draws closer,<br> your worries gradually turn into excitement.<br> The printed invitations have never looked<br> better, and the website dedicated only to this<br> party is just marvellous. Very cool video they<br> made indeed! The company even creates and<br> helps you manage different social media platforms<br> to keep your tech-savvy guests informed<br> and entertained. Entertained! <br>
                    Way before the party! </p>
            </div>
        </div>

    </div>
</div>
<div class="s s-6" data-sr="enter bottom move 100px spin 90deg roll 45deg over 1s wait 0.2s">
    <div class="block-table">


        <div class="block-cell text-cell">
            <div class="text" data-sr="enter top move 100px flip 180deg over 0.5s wait 0.4s">
                <p style="font-size: 42px; font-family: 'brandon_med'">The best part is the logo. These people really make<br> you feel like you are hosting an Oscar. Well, your guest<br> list has always been diverse with a couple of VIPs here and<br>there, but having a logo seems to bring your event to<br> greater heights.</p>
            </div>
        </div>

        <div class="block-cell img-cell"><div class="for-img"><img src="/front/img/t15/7.jpg" alt=""></div></div>
    </div>
</div>
<div class="s s-7" data-sr="enter bottom move 100px spin 90deg roll 45deg over 1s wait 0.2s">
    <div class="block-table">

        <div class="block-cell img-cell"><div class="for-img"><img src="http://lifeandluxury.com/uploads/articles/content/8837eae6e0535e734d79d28a3f7dd120.jpg" data-img="img-8" alt=""></div></div>

        <div class="block-cell text-cell">
            <div class="text" data-sr="enter top move 100px flip 180deg over 0.5s wait 0.4s">
                <p style="font-size: 42px; font-family: 'brandon_med'">Without a doubt, this is the most successful<br> Christmas party you have ever<br> hosted. And your hard work has also<br> reaped rewards with accomplishments<br> and fulfilled plans.</p>
                <p style="font-size: 42px; font-family: 'brandon_med'"><img src="http://lifeandluxury.com/uploads/articles/content/dab997da39d985fcc7fa523a8103de06.png" style="width: 724px;"> </p>
            </div>
        </div>

    </div>
</div>
<div class="s s-8" data-sr="enter bottom move 100px spin 90deg roll 45deg over 1s wait 0.2s">
    <div class="block-table">


        <div class="block-cell text-cell">
            <div class="text" data-sr="enter top move 100px flip 180deg over 0.5s wait 0.4s">
                <p>As you finish your <br>
                    Chardonnay, another idea<br>
                    comes to your head. A new<br>
                    project has already been <br>
                    proposed at your company, and<br>
                    this time the need for strong <br>
                    brand building is vital. You will <br>
                    contact Life &amp; Luxury again tomorrow<br>
                    and ask for their cooperation. </p>

                <p>
                    <span class="special">You</span><br> 
                    <span class="special">smile, knowing that a long-lasting </span><br> 
                    <span class="special">partnership is starting... </span><br> 
                </p>

            </div>
        </div>

        <div class="block-cell img-cell"><div class="for-img"><img src="/front/img/t15/9.jpg" alt=""></div></div>
    </div>
</div>
