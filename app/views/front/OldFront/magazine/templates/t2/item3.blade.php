<div class="item" data-sr="enter bottom">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-offset-1 col-md-4 text-col" data-sr="wait 0.3s enter left">
                <p>
                    The 2015 will see the premiere of prestigious motor yacht company Amels sensa
                    tional 60-metre superyacht Madame Kate. The brainchild of multi-award winning designer Tim Heywood, Madame Kate is beautifully finished with a unique pearl effect and boasts a touch-and-go helipad, a Turkish steam bath and an 8-metre limo-tender. 
                </p>
                <p>The Monaco Yacht Show will also see the launch of the 63-metre Sunrise Mega Yacht IRIMARI designed by acclaimed architect Espen Oenino. Featuring a hi-tensile steel hull and capable of cruising beyond 5,000 miles, the nearly 1500 GT luxury yacht is sure to rival many of the superyachts on show. Alongside the exhibition of the latest high technology nautical products and luxury yachting services, the port enjoys a glittering social scene hosting daily events including galas, charity functions and cocktail parties. On board the spectacular megayachts and hidden in the elegant palaces of Monte Carlo, guests mix business with pleasure at award ceremonies, press conferences and a dazzling array of parties. </p>

            </div>
            <div class="col-md-offset-2 col-md-4" style="text-align: right;" data-sr="wait 0.5s scale up 80%">
                <div class="for-img content-data content-data-img" data-key="img-3"><img src="http://lifeandluxury.com/uploads/articles/content/308c2b6290c857df750bfbf1cd508818.png"></div>
            </div>
        </div>
    </div>
</div>