<div data-sr="hustle 50px spin 90deg">
    <div class="item item-bg-right content-data content-data-bg" data-key="img-5">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-offset-1 col-md-4 text-col" data-sr="wait 0.5s enter top spin 90deg">
                    <p class="letter">
                        A long with the Formula One Monaco Grand Prix, the Monaco Yacht Show is the  
                        main  worldwide-scale event of the country, and as such it has established itself as a major economic and media player for the Principality, constituting a significant contribution to the overall economy. More importantly, at the heart of the show is a real sense of philanthropy, with support for charities such as the Association Monegasque contre les Myopathies since 2001 and the Prince Albert II of Monaco Foundation since 2010. The show also supports environmental and medical actions with organizations such as the Wood Forever Pact, established to tackle deforestation by convincing the entire yachting community to only use wood certified as originating from sustainably-managed forests. The Wood Forever Pact is the flagship project of the вЂMonaco makes a commitment against deforestationвЂ™ initiative, led by the Prince Albert II of Monaco Foundation. 
                    </p>
                </div>
            </div>
        </div>
        <img src="http://lifeandluxury.com/uploads/articles/content/7f36a4781cbee636a5db30bd69009638.png"></div>
</div>