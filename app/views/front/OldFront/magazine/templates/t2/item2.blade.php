<div class="item" style="margin-top: 40px;" data-sr="enter bottom">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-offset-1 col-md-5" data-sr="wait 0.5s scale up 80%">
                <div class="for-img content-data content-data-img">
                    <img src="http://lifeandluxury.com/uploads/articles/content/d98253e5d2fe2ae6a116bf50ff549f0b.png"></div>
            </div>
            <div class="col-md-offset-1 col-md-4 text-col" data-sr="wait 0.3s enter right">
                <p>Each September since 1991, the stunning Port Hercules hosts one of the yachting  industryвЂ™s most glamorous and exclusive events: The Monaco Yacht Show (MYS). For four days the port showcases 500 leading luxury yachting companies including superyacht designers, luxury manufacturers and nautical suppliers, attracting 34,000 visitors who flock to the show for both business and pleasure. </p>
                <p>An outstanding fleet of 115 custom-built superyachts ranging from 25 to 100 metres grace the waters of Monte CarloвЂ™s natural bay, with around 40 yachts making their grand debut each year. Some of the past and present yachting names, business and brands exhibiting at the show include Admiral, Burgess, Edmiston &amp; Company, Imperial Yachts, Azimuth Benetti Group, Fraser Yachts, Camper &amp; Nicholsons International, Merle Wood &amp; Associates, Ocean Independence and Princess Yachts and many more. </p>
            </div>
        </div>
    </div>
</div>