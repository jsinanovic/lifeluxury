<div class="s s-8">
    <div class="block-table">

        <div class="block-cell">
            <div class="text" data-sr="scale down 40%">
                <p>In an age where society is becoming increasingly superlative in its wants and needs, The Dubai Airshow is a stirring alchemy of past feats of <b>aerospace brilliance</b> and ambitious futuristic vision. Now in its 28th year, the 2015 show will be held from 8 – 12 November at the Dubai World Central.   </p>
            </div>
        </div>
        <div class="block-cell">
            <div class="for-img square square-t-r" data-sr="hustle 150px and scale up 20% enter top right"><img src="http://lifeandluxury.com/uploads/articles/content/7552889897c105dd2810d643b3078f53.jpg" data-img="img-7" alt=""></div>
        </div>

    </div>
</div>