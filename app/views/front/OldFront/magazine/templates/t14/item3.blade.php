<div class="s s-3">

    <div class="block-table">
        <div class="block-cell">
            <div class="text text-1" data-sr="scale down 40%">
                <b>While traditional markets are in decline, the focus on the Middle East is on the ascent as it continues to see strong growth across all aerospace sectors.</b>
            </div>
        </div>

        <div class="block-cell">
            <div class="for-img square square-b-l" data-sr="hustle 150px and scale up 20% enter right">
                <img src="http://lifeandluxury.com/uploads/articles/content/bb96a8adf97d7d791177b134414ee7e9.jpg" data-img="img-2" alt="">
            </div>
        </div>
    </div>

    <div class="block-table">    
        <div class="block-cell"></div>
        <div class="block-cell">
            <div class="text text-2" data-sr="scale down 40%">
                <p>The Dubai Airshow demonstrates the success of commercial, business and military aviation with exhibitions including the latest air defence innovations, the UAE Space Agency and for the first time in 2015, the 3D Printshow. In addition, the return of the Gulf Aviation Training Event pavilion and the Futures’ Day event continue to broaden the show’s appeal. Returning exhibitors include industry giants such as Boeing and Airbus as well as Bell Helicopters, Alpha Star, Safat and Tawazan. </p>
            </div>
        </div>

    </div>
</div>