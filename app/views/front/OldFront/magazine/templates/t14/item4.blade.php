<div class="s s-4">
    <div class="block-table">
        <div class="block-cell">
            <div class="text" data-sr="scale down 40%">
                <p>One of the most exciting parts of the show is inevitably the aircraft display. Past displays have included the F-16, V-22 Osprey, F-22 Raptor and the Eurofighter Typhoon. This year, visitors will be treated to the cream of the airspace with a Boeing P-8 military jet, a Qatar Executive G6R0ER and an Emirates Airline Airbus A380 expected on the static display along with aerobatic displays by international teams including the Red Arrows from the UK and Al Fursan from the UAE. </p>
            </div>
        </div>
        <div class="block-cell">
            <div class="for-img square square-t-l" data-sr="hustle 150px and scale up 20% enter top left"><img src="http://lifeandluxury.com/uploads/articles/content/3cd33409096dde7b6b8d51128c3c2c02.jpg" data-img="img-3" alt=""></div>
        </div>
    </div>
</div>
