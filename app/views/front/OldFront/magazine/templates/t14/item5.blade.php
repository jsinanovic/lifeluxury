<div class="s s-5">
    <div class="block-table">
        <div class="block-cell">
            <div class="for-img square square-b-l" data-sr="hustle 150px and scale up 20% bottom left"><img src="http://lifeandluxury.com/uploads/articles/content/67f295bf6688696424178fce0cef4afc.jpg" data-img="img-4" alt=""></div>
        </div>
        <div class="block-cell">
            <div class="text" data-sr="scale down 40%">
                <p>Much debate is expected from the presence of the <b>world’s largest passenger airliner</b>
                    the magnificent Airbus A380, with talks surrounding the development of the elusive A380neo. Publicly lobbied by Emirates, the super-transporter is set to be at the forefront of The Dubai Airshow’s discussions
                </p>
            </div>
        </div>
    </div>
</div>