<div class="s s-7">
    <div class="block-table">
        <div class="block-cell">
            <div class="for-img square square-t-l"><img data-sr="hustle 150px and scale up 20% enter top left" src="http://lifeandluxury.com/uploads/articles/content/38090e0d4e697b15eb498c5003d3e88e.jpg" data-img="img-6" alt=""></div>
        </div>
        <div class="block-cell">
            <div class="text" data-sr="scale down 40%">
                <p>Other events at the show include the Skyview, a dedicated grandstand seating in an area located directly beside the airshow’s venue, as well as the glitzier <b>invite-only events</b> including a gala dinner and exhibitor party. </p>
            </div>
        </div>
    </div>
</div>