<div class="s s-1">
    <div class="text text-center" data-sr="scale down 40%">
        <h2>Dubai embraces the extraordinary</h2>
        <div class="large">Boasting the world’s tallest building, a man-made island shaped like a palm tree and a 7 star hotel oozing with glamour and gold, Dubai is a glittering monument to Arab enterprise and Western capitalism.</div>
        <p>A city of opportunity rising from the desert sands, it is the perfect location for one of the most important events in the aerospace calendar: The Dubai Airshow.  </p>
        <img src="/front/img/t14/1.jpg" alt="">
        <p>Known for luxury shopping, ultramodern architecture and a lively nightlife scene, </p>
    </div>
</div>