<div class="item item-photo-half" data-sr="scale down 50% over 1s">
    <div class="block-table">
        <div class="block-cell img-block content-data">
            <img src="http://lifeandluxury.com/uploads/articles/content/1b44d234291b87e4323f255185feceeb.jpg">
        </div>
        <div class="block-cell">
            <div class="cell-content">
                <div class="article-view-category">
                    Business  |  <span>Life &amp; Luxury Magazine</span>
                </div>

                <p>
                    Greece has given the world much. The founding father of democracy, the country has charmed many a wanderer with its fine food, rich history and enduring traditions. Yet, behind the picture perfect beauty of the cool Mediterranean waters lapping at the sun kissed landscape, an ugly truth lurks in the shadows. In 377 BC, Greece became the first country to default on its debts. Fast-forward 2,392 years and not a lot has changed. 
                </p>
            </div>
        </div>
    </div>
</div>

<div data-sr="scale down 50% over 1s">
    <div class="item item-photo-full content-data-bg">
        <div class="block-table">
            <div class="block-cell">
                <p><span style="color: rgb(255, 255, 255); font-family: Slabo13px, 'Myriad Pro', ProximaNovaRegular; font-size: 16.0020008087158px; font-weight: normal; line-height: 24.003002166748px; text-align: center;">After Wall Street imploded in 2008, Greece became the epicentre of Europe’s debt crisis and by the spring of 2010, the nation was veering toward bankruptcy. Greece owed huge sums to European banks who in turn worried that the unpaid loans would threaten the financial sector with a domino effect leading to another credit crunch. The reasons behind the country’s epic financial fall from grace are marred in controversy. Years of widespread tax evasion have bared the brunt of the blame, along with excessive public spending and high public sector wages.</span><br></p>
            </div>
        </div>
        <img src="http://lifeandluxury.com/uploads/articles/content/a007f41194f71a7c8c85864011be0a0a.jpg">
    </div>
</div>    

<div class="item item-photo-half" data-sr="scale down 50% over 1s">
    <div class="block-table">


        <div class="block-cell">
            <div class="cell-content">

                <p>
                    To avoid financial calamity, in May 2010 the International Monetary Fund, European Union and European Commission, collectively known as Troika, granted the Greek government a 110 billion bailout. A second bailout followed but much of the cash went on Greece’s running costs leaving little if any leftover for an economic recovery. On top of this the bailouts came with conditions and harsh austerity measures, with an increase of 29 percent to the yield of Greek government bonds. Through a programme of strict spending cuts and tax reforms, EU leaders pledged to slash the country’s budget deficit, requiring Greece to overhaul its economy and streamline the government. Unable to repay the monstrous debts of around 356 billion, Greece has had to face the consequences with unemployment above 25 per cent and an economy that has shrunk by a quarter in just five years. 
                </p>
            </div>
        </div>

        <div class="block-cell img-block content-data-all" data-key="img-3"><img src="http://lifeandluxury.com/uploads/articles/content/b78bf26f3d5490176b8dafd019e0b4ae.jpg"></div>

    </div>
</div>    
<div data-sr="scale down 50% over 1s">
    <div class="item item-photo-full content-data-bg" data-key="img-4">
        <div class="block-table">
            <div class="block-cell">
                <p class="text-center" style="color:#fff;">After Wall Street imploded in 2008, Greece became the epicentre of Europe’s debt crisis and by the spring of 2010, the nation was veering toward bankruptcy. Greece owed huge sums to European banks who in turn worried that the unpaid loans would threaten the financial sector with a domino effect leading to another credit crunch. The reasons behind the country’s epic financial fall from grace are marred in controversy. Years of widespread tax evasion have bared the brunt of the blame, along with excessive public spending and high public sector wages.  </p>
            </div>
        </div>
        <img src="http://lifeandluxury.com/uploads/articles/content/48f90ef4104d57d1c39622d265d08d49.jpg"></div>
</div>    
<div class="item item-photo-half" data-sr="scale down 50% over 1s">
    <div class="block-table">

        <div class="block-cell img-block content-data-all" data-key="img-5"><img src="http://lifeandluxury.com/uploads/articles/content/9998660641ccf9d91b5f7bb17300dd96.jpg"></div>

        <div class="block-cell">
            <div class="cell-content">

                <p>
                    Led by Alex Tsipras, the leftist Syriza party stormed to power this year promising to renegotiate the bailout. In a whirl of political strife, the new government called a polarising referendum in July asking the Greek people to vote yes or no to even more austerity measures in return for an extension of bailout funds, demanded mainly by Germany’s chancellor Angela Merkel and the International Monetary Fund. The Greeks responded with overwhelming enthusiasm voting NO by more than 60%. Sadly, the euphoria of the national vote gave way to fatigue ten days later when Tsipras performed an embarrassing yet ultimately inevitable U-turn and signed up to further harsh measures to save the economy. As of yet there are no provisions for departure of the European Union and exiting the single Euro currency would involve a legal minefield that no country has ventured to cross. Despite the political roller coaster, it would seem that Greece do not want to rock the international financial boat. 
                </p>
            </div>
        </div>


    </div>
</div>    
<div data-sr="scale down 50% over 1s">
    <div class="item item-photo-full content-data-bg" data-key="img-6">
        <div class="block-table">
            <div class="block-cell">
                <p class="text-center" style="color:#fff;">Other than demonstrating the admirable pride and strength of the Greek people, the Greek No has been dismissed as a great ethico-political act with no clear prospect of what lies ahead. The Syriza government has been criticised for its contradictive policy, yet Greece now seems strangely resigned to her fate. Indeed, it has been noted that Greece never met the requirements to join the Euro in the first place and that they are simply paying a high price to sustain the Eurocrats’ dream. 
                    With a third bailout in the mix, Greece has taken a step back from the brink of an exit from the euro although the country is likely to suffer a deep recession this year with GDP believed to fall by 2.5 percent due to the impact of capital controls on the economy. Divisions appear to be deepening in the Syriza party with the resignation of finance minister Yanis Varoufakis and squabbling within the Ministerial Council.
                </p>
            </div>
        </div>
        <img src="http://lifeandluxury.com/uploads/articles/content/a4bac39be920e5ac51fd83babcccebc2.jpg"></div>
</div>    
<div class="item item-photo-half" data-sr="scale down 50% over 1s">
    <div class="block-table">
        <div class="block-cell">
            <div class="cell-content">
                <p>
                    As the dismal situation rumbles on, what will happen to this once great nation remains to be seen. Nevertheless, it is clear that the Greeks have put up a heroic fight, making sure that their voices have been heard amidst the booming, oppressive tones of the Troika. The Greek No was an authentic gesture of autonomy and freedom and whilst the Acropolis still stands, the Greek people will rise again. 
                </p>
            </div>
        </div>
        <div class="block-cell img-block content-data-all" data-key="img-7"><img src="http://lifeandluxury.com/uploads/articles/content/fbbc25a25aa7d06e3fd24d23c44b9fa2.jpg"></div>
    </div>
</div>