<div class="item" data-sr="scale down 20%">
    <div class="item-img-center content-data"><img src="{{URL::to('front/img/add.jpg')}}" /></div>
    <div class="container">
        <div class="row">
            <div class="col-md-12 text-col">
                <div data-sr="vFactor 0.1 wait 0.5s and then enter bottom and move 100px and scale up 20%">
                    <p>
                        All geared up and ready to conquer yet again this ever-unforgiving race this autumn are Sebastian Vettel (four-time Formula OneВ® World Champion) and Kimi Raikkonen (current holder of Singapore lap record of 1:45.599) of Scuderia Ferrari. Such impressive achievements have they earned, yet both Vettel and Raikkonen need to keep watchful eyes on the current Formula OneВ® World Champion Lewis Hamilton and his formidable team mate as well as runner-up Nico Rosberg of Mercedes AMG Petronas, for there is currently a big gap between the two teams to overcome. Other remarkable participations include rising stars Daniel Ricciardo and Daniil Kvyat of Infiniti Redbull Racing, Pastor Maldonado and Romain Grosjean of Lotus F1 Team, Carlos Sainz Jr and Max Verstappen of Scuderia Toro Rosso, Felipe Massa and Valtteri Bottas of Williams Martini Racing, Fernando Alonso and Jenson Button of McLaren Honda, Will Stevens and Roberto Merhi of Manor Marussia F1 Team, Nico Hulkenberg and Sergio Perez of Sahara Force India F1 Team, and Felipe Nasr and Marcus Ericsson of Sauber F1 Team. 
                    </p>
                </div>
            </div>
        </div>
    </div>
</div>