<div class="item" data-sr="scale down 20%">
    <div class="item-img-right content-data" style="background-position: center center"><img src="{{URL::to('front/img/add.jpg')}}" /></div>
    <div class="item-img-left item-img-decor" style="background-image: url(/front/img/t1/dragon_bottom.jpg); background-size: 100% auto;     background-position: left bottom;" data-sr></div>

    <div class="container" data-sr="enter bottom move 100px">
        <div class="row">
            <div class="col-md-5 text-col">
                <p>
                    As the sun sets and the skyline lights up, join the exuberant        
                    crowd and cheer for the ambitious F1 racers. When the engines are switched off and the dust settles, enjoy the legendary performances of international artists and celebrate the art of savoury gourmet. If all still come short of your savoir-vivre, head out of the track circuit and soak in the curious wonders of Singapore. After all, there is nothing that the Lion City вЂ“ the one that barely sleeps вЂ“ cannot offer. 
                </p>
            </div>
        </div>
    </div>
</div>