<div class="item" data-sr="scale down 20%">
    <div class="item-img-left item-img-decor" style="background-image: url(/front/img/t1/dragon_left2.jpg); background-size: auto;" data-sr="hustle 40px"></div>
    <div class="item-img-right content-data"><img src="{{URL::to('front/img/add.jpg')}}" /></div>
    <div class="container">
        <div class="row">
            <div class="col-md-5 text-col">
                <div data-sr="vFactor 0.1 wait 0.5s and then enter left and move 100px and scale up 20%">
                    <p>This year, the world’s only Formula One® night race will be witnessed from the 18th to 20th of September at the Marina Bay Street Circuit. </p>

                    <p>
                        As Asia’s first street circuit with a lap length of 5.065 km, this   
                        track promises to provide one of the most adrenaline-pumping experiences of the season with the lighting condition, the hot and humid climate, and the circuit structure itself, making it a real tough challenge for the drivers. 
                    </p>
                    <p>Designed in 2008 by KBR Inc. based on the work of the famous German engineer, racing driver and circuit designer Hermann Tilke, the main heart-throbbing feature lies between Turns 5 and 7 due to their bumpiness. </p>
                    <p>Previously had the notoriously dangerous chicane at Turn 10 – the “Singapore Sling” – been much criticised as “the worst corner in F1”, leading to its removal and replacement by a single-apex left-hand bend in 2013, pushing cars approaching the realigned corner to reach up to 40km/h faster and consequently, complete the lap race in about one second faster. Safer indeed, but definitively not any less rip-roaring.</p>
                    <p><img src="/front/img/t1/singapure.jpg" alt="" /></p>
                </div>
            </div>
        </div>
    </div>
</div>