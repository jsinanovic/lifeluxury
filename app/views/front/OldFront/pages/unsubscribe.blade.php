@extends('front.layouts.main')

@section('content')

<section class="section-error section-full">
    <div class="center-block">
        <div class="top-title">Life & luxury hosting solutions</div>
        <div class="error-name">
            @if($subscriber)
            Success
            @else
            Unsuccess
            @endif
        </div>
        <div class="error-description">
            You unscubscription was processed<br>
            We could take you back to the <a href="{{URL::to('/')}}">Life & Luxury Home Page</a>
        </div>

        <a href="javascript:void(0);" class="learn-more">
            <div class="round-button"></div>
        </a>
    </div>

    <div class="effects">
        <ul class="parallax">
            <li class="layer" data-depth="0.35" data-limit-y="40" data-scalar-y="3">
                <img id="left-cloud" src="/front/img/cloud_left.png" alt="left cloud">
            </li>
            <li class="layer" data-depth="0.45" data-limit-y="50" data-scalar-y="2" data-scalar-x="2"> 
                <img id="right-cloud" src="/front/img/cloud_right.png" alt="right cloud">
            </li>
        </ul>
    </div>
    
</section>

<section class="section-full">
    @include('front.layouts.contacts')
    @include('front.layouts.footer')
</section>

@stop

@section('js-plugins')
{{HTML::script('/front/plugins/parallax/jquery.parallax.min.js')}}
<script>
    $(document).ready(function(){
        $('.parallax').parallax();
    });
</script>
@stop