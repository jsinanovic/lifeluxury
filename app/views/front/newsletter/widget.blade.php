<div class="newsletter {{isset($nsclass)? $nsclass : ''}}">
    <div class="heading">
        <div class="name">{{trans('newsletter.newsletter')}}</div>
    </div>
    <div class="text">{{trans('newsletter.in_touch')}}</div>
    <div class="form-group clearfix">
        {{Form::open(array('url' => URL::to('subscribe'), 'method' => 'POST', 'class' => 'subscribe-form'))}}
            {{Form::text('email', null, array('class' => 'form-control email', 'placeholder' => trans('newsletter.enter_email'), 'required' => 'required'))}}
            <input type="submit" class="btn btn-sm" value="{{trans('newsletter.subscribe')}}">
        {{Form::close()}}
    </div>
</div>
