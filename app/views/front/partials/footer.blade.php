<div class="container-fluid footer-container">
    <div class="container">
        <div class="row">
            <div class="footer-panel panel-left">
                <ul class="footer-list">
                    <li><a href="#" class="link-custom">The Collection</a></li>
                    <li><a href="/press-kit" class="link-custom">Press Kit</a></li>
                    <li><a href="/newsletter" class="link-custom">Newsletter</a></li>
                </ul>

                <ul class="footer-list">
                    <li><a href="/service-promise" class="link-custom">Service Promise</a></li>
                    <li><a href="/privacy-policy" class="link-custom">Privacy Policy</a></li>
                    <li><a href="/terms-and-conditions" class="link-custom">Terms & Conditions</a></li>
                </ul>
            </div>
            <div class="footer-panel panel-right">
                <ul class="footer-social">
                    <li><a href="https://www.facebook.com/lifeandluxurycollection"><i class="fa fa-facebook" aria-hidden="true"></i></a></li>
                    <li><a href="https://twitter.com/LifenLuxury"><i class="fa fa-twitter" aria-hidden="true"></i></a></li>
                    <li><a href="https://www.youtube.com/channel/UCePsdWq9xg-EN9E7DGaAuHg"><i class="fa fa-youtube" aria-hidden="true"></i></a></li>
                    <li><a href="https://www.instagram.com/your_finest_collection"><i class="fa fa-instagram" aria-hidden="true"></i></a></li>
                </ul>

                <ul class="footer-list">
                    <li><a href="#" class="link-custom">Life & Luxury Collection</a></li>
                    <li><a href="#" class="link-custom">All Rights Reserved</a></li>
                </ul>
            </div>
        </div>
    </div>
</div>