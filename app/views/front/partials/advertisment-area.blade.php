<section class="" id="advertisment-area" style="position:relative;">
    <div class="advertisement-img-container" style="height: 201px;">
        <img  src="/img/backgrounds/advertisment_area.png">
    </div>
    <div class="container advertisement-content">
        <div class="row">
            <div class="col-md-12">
                <div class="advertising-area-placeholder">
                    ADVERTISMENT AREA
                </div>
            </div>
        </div>
    </div>
</section>