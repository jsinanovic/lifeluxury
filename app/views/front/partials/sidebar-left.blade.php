<aside class="sidebar">
    <nav class="navigation-bar">
        <ul class="navigation">
            <li>
                <a href="#" class="section-button-move-to-1">
                    <span class="nav-link-number section-button-move-to-1">01</span>
                    <span class="nav-link-line section-button-move-to-1">Home</span>
                </a>
            </li>

            <li>
                <a href="#" class="section-button-move-to-1">
                    <span class="nav-link-number section-button-move-to-1">02</span>
                    <span class="nav-link-line section-button-move-to-1">Welcome</span>
                </a>
            </li>

            <li>
                 <a href="#" class="s-creative section-button-move-to-3">
                    <span class="nav-link-number s-creative section-button-move-to-3">03</span>
                    <span class="nav-link-line s-creative section-button-move-to-3">Creative</span>
                </a>
            </li>

            <li>
                <a href="#" class="section-button-move-to-4">
                    <span class="nav-link-number s-creative section-button-move-to-4">04</span>
                    <span class="nav-link-line s-creative section-button-move-to-4">Rendez Vous</span>
                </a>
            </li>

            <li>
                <a href="#" class="section-button-move-to-5">
                    <span class="nav-link-number s-creative section-button-move-to-5">05</span>
                    <span class="nav-link-line s-creative section-button-move-to-5">Magazine</span>
                </a>
            </li>

            <li>
                <a href="#" class="section-button-move-to-6">
                    <span class="nav-link-number s-creative section-button-move-to-6">06</span>
                    <span class="nav-link-line s-creative section-button-move-to-6">Contact Us</span>
                </a>
            </li>
        </ul>
    </nav>
</aside>