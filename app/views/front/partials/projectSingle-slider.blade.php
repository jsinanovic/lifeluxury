<aside class="popup-projects-single">
    <div class="popup-content-projects-single">
    <div class="popup-carousel-projects-single swiper-container">
        <div class="swiper-wrapper">
            @foreach($services as $item)
            <div class="popup-item-projects-single swiper-slide" style="z-index:100;
            ">
                    {{--background-image: url('uploads/services/icon/{{$item->image}}');">--}}
                <h3 class="popup-item-header-projects-single">{{$item->name}}</h3>

            </div>
            @endforeach
        </div>
    </div>
    </div>
</aside>

<script>
    window.onload = load;

    function load() {
        var $popupslider = $('.popup-projects-single');
        $popupslider.addClass('open');

        if(!!$.fn.swiper) {
            $popupslider.find('.popup-carousel-projects-single').swiper({
                direction:'horizontal',
//                autoplay: 4000,
                autoplayDisableOnInteraction: false,
                speed: 700,
                loop: true,
                grabCursor: true,
                slidesPerView: 1
            });
        }
    }
</script>