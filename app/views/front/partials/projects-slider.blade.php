<aside class="popup-projects">
    <div class="popup-content-projects">
    <div class="popup-carousel-projects swiper-container">
        <div class="swiper-wrapper">

            @foreach($services as $item)
                {{--<li><a href="{{URL::to('creative/' . $item->slug)}}">{{$item->name}}</a></li>--}}

            <div class="popup-item-projects swiper-slide" style="z-index:100;
             {{--background-image: url('front/img/creative/projects1.png');">--}}
                    background-image: url('uploads/services/icon/{{$item->image}}');">
                @if($item->name == 'Advertising' || $item->name == 'Brand Identity' )
                <h3 style="" class="popup-item-header-projects">{{$item->name}}</h3>
                @else
                    <h3 class="popup-item-header-projects">{{$item->name}}</h3>
                @endif
                <a href="{{URL::to('creativProjects/' . $item->slug)}}" class="popup-item-link-projects btn-custom">Discover Now</a>

            </div>


            {{--<div class="popup-item-projects swiper-slide" style="z-index:100; background-image: url('front/img/creative/projects2.png');">--}}
                {{--<h3 class="popup-item-header-projects">Graphics Design</h3>--}}
                {{--<a href="#" class="popup-item-link-projects btn-custom">Discover Now</a>--}}
            {{--</div>--}}

            {{--<div class="popup-item-projects swiper-slide" style="z-index:100; background-image: url('front/img/welcome/welcome_slider1_3.png');">--}}
                {{--<h3 class="popup-item-header-projects">Video Production</h3>--}}
                {{--<a href="#" class="popup-item-link-projects btn-custom">Discover Now</a>--}}
            {{--</div>--}}

            {{--<div class="popup-item-projects swiper-slide" style="z-index:100; background-image: url('front/img/creative/projects2.png');">--}}
                {{--<h3 class="popup-item-header-projects">Brand Identity</h3>--}}
                {{--<a href="#" class="popup-item-link-projects btn-custom">Discover Now</a>--}}
            {{--</div>--}}

            {{--<div class="popup-item-projects swiper-slide" style="z-index:100; background-image: url('front/img/creative/projects1.png');">--}}
                {{--<h3 class="popup-item-header-projects">Web design & development</h3>--}}
                {{--<a href="#" class="popup-item-link-projects btn-custom">Discover Now</a>--}}
            {{--</div>--}}

            {{--<div class="popup-item-projects swiper-slide" style="z-index:100; background-image: url('front/img/welcome/welcome_slider1_3.png');">--}}
                {{--<h3 class="popup-item-header-projects">Get a Free Quote</h3>--}}
                {{--<a href="#" class="popup-item-link-projects btn-custom">Discover Now</a>--}}
            {{--</div>--}}
            @endforeach
        </div>
    </div>
    </div>
</aside>

<script>
    window.onload = load;

    function load() {
        var $popupslider = $('.popup-projects');
        $popupslider.addClass('open');

        if(!!$.fn.swiper) {
            $popupslider.find('.popup-carousel-projects').swiper({
                direction:'horizontal',
                autoplay: 4000,
                autoplayDisableOnInteraction: false,
                speed: 700,
                loop: true,
                grabCursor: true,
                slidesPerView: 5
            });
        }
    }
</script>