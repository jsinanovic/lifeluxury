<aside class="popup">
    <div class="popup-content">
        <div class="popup-carousel swiper-container">
            <div class="swiper-wrapper">
                <div class="popup-item swiper-slide" style="background-image: url('front/img/popup/popup_image_1.jpg');">
                    <h3 class="popup-item-header">Spring Issue 2016</h3>
                    <a href="#" class="popup-item-link btn-custom">Discover Now</a>
                </div>

                <div class="popup-item swiper-slide" style="background-image: url('front/img/popup/popup_image_2.jpg');">
                    <h3 class="popup-item-header">Summer Issue 2016</h3>
                    <a href="#" class="popup-item-link btn-custom">Discover Now</a>
                </div>

                <div class="popup-item swiper-slide" style="background-image: url('front/img/popup/popup_image_3.jpg');">
                    <h3 class="popup-item-header">Spring Issue 2016</h3>
                    <a href="#" class="popup-item-link btn-custom">Discover Now</a>
                </div>

                <div class="popup-item swiper-slide" style="background-image: url('front/img/popup/popup_image_4.jpg');">
                    <h3 class="popup-item-header">Spring Issue 2016</h3>
                    <a href="#" class="popup-item-link btn-custom">Discover Now</a>
                </div>

                <div class="popup-item swiper-slide" style="background-image: url('front/img/popup/popup_image_1.jpg');">
                    <h3 class="popup-item-header">Spring Issue 2016</h3>
                    <a href="#" class="popup-item-link btn-custom">Discover Now</a>
                </div>

                <div class="popup-item swiper-slide" style="background-image: url('front/img/popup/popup_image_2.jpg');">
                    <h3 class="popup-item-header">Summer Issue 2016</h3>
                    <a href="#" class="popup-item-link btn-custom">Discover Now</a>
                </div>

                <div class="popup-item swiper-slide" style="background-image: url('front/img/popup/popup_image_3.jpg');">
                    <h3 class="popup-item-header">Spring Issue 2016</h3>
                    <a href="#" class="popup-item-link btn-custom">Discover Now</a>
                </div>
            </div>
        </div>
    </div>
</aside>