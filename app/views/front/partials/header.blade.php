@section('css')
<style>
    @font-face {
        font-family: 'latin_modern_roman10_italic';
        src: url('/front/fonts/lmroman10-italic-webfont.eot');
        src: url('/front/fonts/lmroman10-italic-webfont.eot?#iefix') format('embedded-opentype'),
        url('/front/fonts/lmroman10-italic-webfont.woff') format('woff'),
        url('/front/fonts/lmroman10-italic-webfont.ttf') format('truetype'),
        url('/front/fonts/lmroman10-italic-webfont.svg#latin_modern_roman10_italic') format('svg');
        font-weight: normal;
        font-style: normal;

    }
    @font-face {
        font-family: 'latin_modern_roman10_italic';
        src: url('/front/fonts/lmroman10-italic-webfont.eot');
        src: url('/front/fonts/lmroman10-italic-webfont.eot?#iefix') format('embedded-opentype'),
        url('/front/fonts/lmroman10-italic-webfont.woff') format('woff'),
        url('/front/fonts/lmroman10-italic-webfont.ttf') format('truetype'),
        url('/front/fonts/lmroman10-italic-webfont.svg#latin_modern_roman10_italic') format('svg');
        font-weight: normal;
        font-style: normal;

    }

    .mz-loading .mz-loader{
        width: 100%;
        height: 100%;
        opacity: 1;
    }
    .mz-loading .mz-loader div{
        display: block;
    }

    .mz-loader {
        position: fixed;
        top: 0;
        left: 0;
        right: 0;
        bottom: 0;
        margin: auto;
        background-color: #fff;
        z-index: 999999999999;
        width: 0;
        height: 0;
        opacity: 0;
        transition: opacity 0.5s ease;
    }
    .mz-loader div {
        position: absolute;
        top: 50%;
        left: 50%;
        -webkit-transform: translate(-50%, -50%);
        -ms-transform: translate(-50%, -50%);
        transform: translate(-50%, -50%);
        text-align: center;
        display: none;
    }
    .mz-loader div p {
        text-transform: uppercase;
        margin-top: 15px;
        font-family: Butler;
        color: #a39c95;
        font-size: 16px;
        font-style: normal;
    }
    .mz-loader div p span {
        font-size: 20px;
    }
    .mz-loader img {
        max-width: 100%;
        height: auto;
    }


    .mz-loading .mz-loader{
        width: 100%;
        height: 100%;
        opacity: 1;
    }
    .mz-loading .mz-loader div{
        display: block;
    }
    .mz-loading .section-image:after{top:50%;}
    .mz-loading .slider-magazine .carousel-inner>.item:after{bottom:50%;}
    .mz-loading .mn, .mz-loading .slider-magazine .carousel-caption , .mz-loading .slider-magazine .carousel-control, .mz-loading .article-discover{top:-200px;}
    .mz-loading .section-image h1{opacity:0;}
    body.mz-loading{overflow:hidden;}

    .mz-loading .article-view-t7 .magazine-issues{opacity:0;}
    .mz-loading .magazine-label{opacity:0;}

    .slider-block.loading{background:#fff url(/front/img/preloader.gif) no-repeat center;}
    .slider-block.loading{background:#fff url(/front/img/preloader.gif) no-repeat center;}
    /*HOME NEW CSS FILE */
    /*  Table of Contents
        - - - - - - - - -
        1. Common
            1.1. General.
            1.2. Header.
            1.3. Sidebar(navigation).
            1.4. Popup(carousel).
            1.5. Footer.
        2. Home
            2.1. Banner.
            2.2. Advertising.
            2.3. Contact us.
        - - - - - - - - -
    */

    /*1. Common.*/

    /*1.1. General.*/
    @font-face {
        font-family: 'Butler';
        src: local('Butler'),
        url('../fonts/Butler/butler_medium-webfont.woff2') format('woff2'),
        url('../fonts/Butler/butler_medium-webfont.woff') format('woff');
        font-weight: 500;
        font-style: normal;
    }

    @font-face {
        font-family: 'Butler';
        src: local('Butler'),
        url('../fonts/Butler/butler_bold-webfont.woff2') format('woff2'),
        url('../fonts/Butler/butler_bold-webfont.woff') format('woff');
        font-weight: 700;
        font-style: normal;
    }

    /*body {*/
        /*font-family: 'Open Sans', sans-serif;*/
        /*color: #fefefe;*/
    /*}*/

    .wrapper {
        position: relative;
        min-height: 100%;
    }

    .container-wide {
        padding: 0 93px 0 89px;
    }

    .panel-left {
        float: left;
    }

    .panel-center {
        display: inline-block;
    }

    .panel-right {
        float: right;
    }

    a:focus {
        outline: none;
    }

    .btn:focus,
    .btn:active:focus {
        outline: none;
    }

    .link-custom {
        position: relative;
    }

    .link-custom:focus,
    .link-custom:hover {
        text-decoration: none;
    }

    .link-custom:before {
        content: '';
        position: absolute;
        bottom: 0;
        left: 0;
        width: 0;
        height: 2px;
        -webkit-transition: .3s width;
        -moz-transition: .3s width;
        -o-transition: .3s width;
        transition: .3s width;
    }

    .link-custom:focus:before,
    .link-custom:hover:before {
        width: 100%;
    }

    /*.btn-custom {*/
        /*position: relative;*/
        /*overflow: hidden;*/
    /*}*/

    .btn-custom:before {
        content: '';
        position: absolute;
        top: 0;
        left: 0;
        z-index: -1;
        width: 150%;
        height: 100%;
        -webkit-transform: rotate3d(0, 0, 1, -45deg) translate3d(0, -3em, 0);
        -moz-transform: rotate3d(0, 0, 1, -45deg) translate3d(0, -3em, 0);
        -o-transform: rotate3d(0, 0, 1, -45deg) translate3d(0, -3em, 0);
        -ms-transform: rotate3d(0, 0, 1, -45deg) translate3d(0, -3em, 0);
        transform: rotate3d(0, 0, 1, -45deg) translate3d(0, -3em, 0);
        -webkit-transform-origin: 0% 100%;
        -moz-transform-origin: 0% 100%;
        -o-transform-origin: 0% 100%;
        -ms-transform-origin: 0% 100%;
        transform-origin: 0% 100%;
        -webkit-transition: -webkit-transform .5s,
        opacity .5s,
        background-color .5s;
        -moz-transition: -moz-transform .5s,
        opacity .5s,
        background-color .5s;
        -o-transition: -o-transform .5s,
        opacity .5s,
        background-color .5s;
        transition: -ms-transform-origin .5s,
        transform .5s,
        opacity .5s,
        background-color .5s;
    }

    .btn-custom:hover:before {
        opacity: 1;
        -webkit-transform: rotate3d(0, 0, 1, 0deg);
        -moz-transform: rotate3d(0, 0, 1, 0deg);
        -o-transform: rotate3d(0, 0, 1, 0deg);
        -ms-transform: rotate3d(0, 0, 1, 0deg);
        transform: rotate3d(0, 0, 1, 0deg);
        -webkit-transition-timing-function: cubic-bezier(0.2, 1, 0.3, 1);
        -moz-transition-timing-function: cubic-bezier(0.2, 1, 0.3, 1);
        -o-transition-timing-function: cubic-bezier(0.2, 1, 0.3, 1);
        transition-timing-function: cubic-bezier(0.2, 1, 0.3, 1);
    }

    .modal {

    }

    .modal-dialog {

    }

    .modal-content {
        height: 582px;
        width: 1036px;
        border-radius: 0;
    }

    /*1.2. Header.*/
    .header {
        z-index: 1000;
        position: fixed;
        top: 35px;
        left: 0;
        width: 100%;
        height: 57px;
        text-align: center;
        background-color: transparent !important;
    }

    .header > .container-wide {
        height: 100%;
    }

    .header-panel {
        height: 100%;
    }

    .header-link {
        display: inline-block;
        width: 18px;
        height: 100%;
        line-height: 57px;
        vertical-align: top;
        -webkit-transition: .3s -webkit-transform;
        -moz-transition: .3s -moz-transform;
        -o-transition: .3s -o-transform;
        transition: .3s transform;
    }

    .header-link:focus,
    .header-link:hover {
        -webkit-transform: scale(1.6);
        -moz-transform: scale(1.6);
        -o-transform: scale(1.6);
        -ms-transform: scale(1.6);
        transform: scale(1.6);
    }

    .header-link:focus path,
    .header-link:hover path {
        fill: #fefefe;
    }

    .header-link-magazine {
        margin-right: 5px;
    }

    .logo-link {
        display: inline-block;
    }

    .sidebar-btn-wrapper {
        line-height: 57px;
    }

    .sidebar-btn-open {
        position: relative;
        width: 35px;
        height: 36px;
        padding: 0;
        border: 1px solid #ced0c3;
        -webkit-border-radius: 4px;
        -moz-border-radius: 4px;
        border-radius: 4px;
        background-color: transparent;
        text-align: center;
        -webkit-transition: .3s border-color,
        .3s -webkit-transform;
        -moz-transition: .3s border-color,
        .3s -moz-transform;
        -o-transition: .3s border-color,
        .3s -o-transform;
        transition: .3s border-color,
        .3s transform;
    }

    .sidebar-btn-open:before {
        content: '';
        position: absolute;
        top: 0;
        left: 0;
        width: 100%;
        height: 100%;
    }

    .sidebar-btn-open:focus,
    .sidebar-btn-open:hover {
        border-color: #fff;
        -webkit-transform: scale(1.1);
        -moz-transform: scale(1.1);
        -o-transform: scale(1.1);
        -ms-transform: scale(1.1);
        transform: scale(1.1);
    }

    .sidebar-btn-strip {
        display: block;
        width: 13px;
        height: 2px;
        margin: 0 auto 2px;
        background-color: #ced0c3;
        -webkit-transition: .3s background-color;
        -moz-transition: .3s background-color;
        -o-transition: .3s background-color;
        transition: .3s background-color;
    }

    .sidebar-btn-strip:last-child {
        margin-bottom: 0;
    }

    .sidebar-btn-open:focus .sidebar-btn-strip,
    .sidebar-btn-open:hover .sidebar-btn-strip{
        background-color: #fff;
    }

    /*1.3. Sidebar(navigation).*/
    .sidebar {
        position: fixed;
        top: 0;
        left: 0;
        z-index: 1002;
        width: 0;
        height: 100%;
        visibility: hidden;
        -webkit-transition: 0s width .5s,
        0s visibility .5s;
        -moz-transition: 0s width .5s,
        0s visibility .5s;
        -o-transition: 0s width .5s,
        0s visibility .5s;
        transition: 0s width .5s,
        0s visibility .5s;
    }

    .sidebar.open {
        width: 323px;
        visibility: visible;
        -webkit-transition: 0s width 0s,
        0s visibility 0s;
        -moz-transition: 0s width 0s,
        0s visibility 0s;
        -o-transition: 0s width 0s,
        0s visibility 0s;
        transition: 0s width 0s,
        0s visibility 0s;
    }

    .navigation-bar {
        position: relative;
        width: 100%;
        height: 100%;
        padding-top: 142px;
        margin-left: -100%;
        background-color: rgba(43, 45, 57, .9);
        -webkit-transition: .5s margin-left cubic-bezier(0.25, 0.1, 0, 1.26);
        -moz-transition: .5s margin-left cubic-bezier(0.25, 0.1, 0, 1.26);
        -o-transition: .5s margin-left cubic-bezier(0.25, 0.1, 0, 1.26);
        transition: .5s margin-left cubic-bezier(0.25, 0.1, 0, 1.26);
    }

    .sidebar.open .navigation-bar {
        margin-left: 0;
    }

    .navigation-bar:before {
        content: '';
        position: absolute;
        top: 0;
        left: 0;
        width: 100px;
        height: 100%;
        background-color: #2b2d39;
    }

    .navigation {
        height: 100%;
        padding-left: 0;
        margin-bottom: 0;
        list-style: none;
    }

    .navigation > li {
        display: table;
        height: 16.33%;
        width: 100%;
    }

    .navigation > li > a {
        display: table-cell;
        width: 100%;
        padding: 0 33px;
        color: #54565b;
        font-weight: 500;
        font-family: 'Butler', serif;
        -webkit-transition: .5s color;
        -moz-transition: .5s color;
        -o-transition: .5s color;
        transition: .5s color;
    }

    /*NAVIGATION ALTERNATIVE*/
    /*.navigation > li > a {
        display: inline-block;
        width: 100%;
        padding: 40.5px 33px;
        color: #54565b;
        font-family: 'Butler', serif;
        -webkit-transition: .5s color;
        -moz-transition: .5s color;
        -o-transition: .5s color;
        transition: .5s color;
    }*/

    .navigation > li > a:focus,
    .navigation > li > a:hover {
        color: #cdcfc1;
        text-decoration: none;
    }

    .nav-link-number {
        display: inline-block;
        font-weight: 700;
        font-size: 34px;
        line-height: 1;
        vertical-align: top;
        -webkit-transform: rotate(270deg);
        -moz-transform: rotate(270deg);
        -o-transform: rotate(270deg);
        -ms-transform: rotate(270deg);
        transform: rotate(270deg);
    }

    .nav-link-line {
        display: inline-block;
        padding-left: 42px;
        font-size: 22px;
        text-transform: uppercase;
        line-height: 40px;
        vertical-align: top;
    }

    /*1.4. Popup(carousel).*/
    .popup {
        position: fixed;
        top: 0;
        right: 0;
        z-index: 1002;
        width: 0;
        height: 100%;
        -webkit-transition: 0s width .5s;
        -moz-transition: 0s width .5s;
        -o-transition: 0s width .5s;
        transition: 0s width .5s;
    }

    .popup.open {
        width: 386px;
        -webkit-transition: 0s width 0s;
        -moz-transition: 0s width 0s;
        -o-transition: 0s width 0s;
        transition: 0s width 0s;
    }

    .popup-content {
        width: 100%;
        height: 100%;
        border-left: 3px solid #cdcfc1;
        margin-left: 100%;
        -webkit-transition: .5s margin-left cubic-bezier(0.25, 0.1, 0, 1.26);
        -moz-transition: .5s margin-left cubic-bezier(0.25, 0.1, 0, 1.26);
        -o-transition: .5s margin-left cubic-bezier(0.25, 0.1, 0, 1.26);
        transition: .5s margin-left cubic-bezier(0.25, 0.1, 0, 1.26);
    }

    .popup.open .popup-content {
        margin-left: 0;
    }

    .popup-carousel {
        height: 100%;
    }

    .popup-item {
        position: relative;
        background: no-repeat center;
        background-size: cover;
        text-align: center;
    }

    .popup-item:before {
        content: '';
        position: absolute;
        top: 0;
        left: 0;
        width: 100%;
        height: 100%;
        background-color: #cdcfc1;
        opacity: 0;
        -webkit-transition: .3s opacity;
        -moz-transition: .3s opacity;
        -o-transition: .3s opacity;
        transition: .3s opacity;
    }

    .popup-item:hover:before {
        opacity: .6;
    }

    .popup-item-header {
        position: absolute;
        top: 50%;
        left: 0;
        overflow: hidden;
        display: inline-block;
        width: 100%;
        height: 28px;
        padding: 0 15px;
        margin: -14px 0 0;
        color: #cdcfc1;
        font-weight: 700;
        font-size: 25px;
        font-family: 'Butler', serif;
        text-align: center;
        text-overflow: ellipsis;
        vertical-align: middle;
        white-space: nowrap;
        visibility: visible;
    }

    .popup-item:hover .popup-item-header {
        visibility: hidden;
    }

    .popup-item-link {
        position: absolute;
        top: 50%;
        left: 50%;
        z-index: 1002;
        display: inline-block;
        padding: 23px 28px;
        border: 1px solid #262730;
        margin-top: -31px;
        margin-left: -104px;
        color: #262730;
        font-weight: 700;
        font-family: 'Montserrat', sans-serif;
        line-height: 1;
        text-align: center;
        text-transform: uppercase;
        vertical-align: middle;
        letter-spacing: 3px;
        visibility: hidden;
        opacity: 0;
        -webkit-transition: .3s background-color,
        .3s color,
        .3s opacity;
        -moz-transition: .3s background-color,
        .3s color,
        .3s opacity;
        -o-transition: .3s background-color,
        .3s color,
        .3s opacity;
        transition: .3s background-color,
        .3s color,
        .3s opacity;
    }

    .popup-item:hover .popup-item-link {
        visibility: visible;
        opacity: 1;
    }

    .popup-item-link:focus,
    .popup-item-link:hover {
        color: #cdcfc1;
        text-decoration: none;
    }

    .popup-item-link:before {
        background-color: #262730;
    }
    /*1.5. Footer.*/
    .footer {
        position: absolute;
        bottom: 38px;
        left: 0;
        width: 100%;
    }

    .footer-panel {
        vertical-align: top;
    }

    .footer-panel.panel-right {
        text-align: right;
    }

    .footer-list {
        display: inline-block;
        padding-left: 0;
        margin-right: 95px;
        margin-bottom: 0;
        list-style: none;
    }

    .footer-list:last-child {
        margin-right: 0;
    }

    .footer-list > li > a {
        color: #fff;
        font-size: 12px;
        text-transform: uppercase;
        letter-spacing: 2.3px;
        line-height: 25px;
    }

    .footer-list > li > a:before {
        height: 1px;
        background-color: #fff;
    }

    .footer-list > li > a:focus,
    .footer-list > li > a:hover {
        text-decoration: none;
    }

    .footer-social {
        padding-left: 0;
        margin-right: 2px;
        margin-bottom: 5px;
        list-style: none;
    }

    .footer-social > li {
        display: inline-block;
        margin-left: 22px;
    }

    .footer-social > li > a {
        display: inline-block;
        color: #fff;
        -webkit-transition: .3s -webkit-transform;
        -moz-transition: .3s -moz-transform;
        -o-transition: .3s -o-transform;
        transition: .3s transform;
    }

    .footer-social > li > a:focus,
    .footer-social > li > a:hover {
        color: #fff;
        -webkit-transform: scale(1.4);
        -moz-transform: scale(1.4);
        -o-transform: scale(1.4);
        -ms-transform: scale(1.4);
        transform: scale(1.4);
    }


    /*--------------------------------------*/
</style>
@stop

<div class="container-wide">
    <div class="header-panel panel-left">
        <div class="sidebar-btn-wrapper">
            <button class="sidebar-btn-open btn">
                <span class="sidebar-btn-strip"></span>
                <span class="sidebar-btn-strip"></span>
                <span class="sidebar-btn-strip"></span>
            </button>
        </div>
    </div>

    <div class="header-panel panel-center">
        <a href="/" class="logo-link">
            <img src="/front/img/common/logo.png" alt="Site logo" class="logo" />
        </a>
    </div>

    <div class="header-panel panel-right">
        <a href="#" class="header-link header-link-magazine"><svg xmlns="http://www.w3.org/2000/svg" width="18" height="15" viewBox="0 0 18 15"><path d="M8.2 0H0.5C0.2 0 0 0.3 0 0.6v5.8C0 6.7 0.2 7 0.5 7H8.2c0.3 0 0.5-0.3 0.5-0.6V0.6C8.6 0.3 8.4 0 8.2 0zM8.2 6.4c0 0 0 0 0 0H0.5c0 0 0 0 0 0V0.6c0 0 0 0 0 0H8.2c0 0 0 0 0 0V6.4L8.2 6.4zM17.6 0H9.9C9.6 0 9.4 0.3 9.4 0.6v5.8c0 0.3 0.2 0.6 0.5 0.6h7.7c0.3 0 0.5-0.3 0.5-0.6V0.6C18 0.3 17.8 0 17.6 0zM17.6 6.4c0 0 0 0 0 0H9.9c0 0 0 0 0 0V0.6c0 0 0 0 0 0h7.7c0 0 0 0 0 0V6.4L17.6 6.4zM8.2 8H0.5C0.2 8 0 8.3 0 8.6v5.8c0 0.3 0.2 0.6 0.5 0.6H8.2c0.3 0 0.5-0.3 0.5-0.6V8.6C8.6 8.3 8.4 8 8.2 8zM8.2 14.4c0 0 0 0 0 0H0.5c0 0 0 0 0 0V8.6c0 0 0 0 0 0H8.2c0 0 0 0 0 0V14.4L8.2 14.4zM17.6 8H9.9c-0.2 0-0.5 0.3-0.5 0.6v5.8c0 0.3 0.2 0.6 0.5 0.6h7.7c0.3 0 0.5-0.3 0.5-0.6V8.6C18 8.3 17.8 8 17.6 8zM17.6 14.4c0 0 0 0 0 0H9.9c0 0 0 0 0 0V8.6c0 0 0 0 0 0h7.7c0 0 0 0 0 0V14.4L17.6 14.4z" fill="#CCC"/></svg></a>
        <a class="header-link header-link-search" data-toggle="modal" data-target="">
            <svg xmlns="http://www.w3.org/2000/svg" width="14" height="14" viewBox="0 0 14 14">
                <path d="M13.7 12.3L10.8 9.4c1.7-2.3 1.5-5.6-0.6-7.7 -2.3-2.3-6.1-2.3-8.5 0 -2.3 2.3-2.3 6.1 0 8.4 2.1 2.1 5.3 2.3 7.7 0.6l2.9 2.9c0.4 0.4 1 0.4 1.4 0C14.1 13.3 14.1 12.7 13.7 12.3zM3.2 8.8c-1.6-1.6-1.6-4.1 0-5.6 1.6-1.6 4.1-1.6 5.6 0 1.6 1.6 1.6 4.1 0 5.6C7.2 10.3 4.7 10.3 3.2 8.8z" fill="#CCC"/></svg></a>
    </div>
</div>