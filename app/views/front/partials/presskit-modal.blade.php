
<section class="" id="welcome-4" style="margin-top: 100px; margin-bottom: 100px;">
    <div class="container">
        <div class="row">
            <div class="col-md-12 welcome-4-main-title">
                Discover the collection
            </div>
            <div class="col-md-12 welcome-4-text">
                We have created a unique collection of services
            </div>
            <div class="row">
                <div class="col-md-4">
                    <div class="welcome-4-img">
                        <img class="" src="/front/img/welcome/welcome-4-rendez-vous.png">
                    </div>
                    <a href="/press-kit">
                        <div class="welcome-4-title">
                            PRESS KIT
                        </div>
                    </a>
                </div>
                <div class="col-md-4">
                    <div class="welcome-4-img">
                        <img class="" src="/front/img/welcome/welcome-4-life-and-luxury-creative.png">
                    </div>
                    <a href="/media-kit">
                        <div class="welcome-4-title">
                           MEDIA KIT
                        </div>
                    </a>
                </div>
                <div class="col-md-4">
                    <div class="welcome-4-img">
                        <img class="" src="/front/img/welcome/welcome-4-life-and-luxury-magazine.png">
                    </div>
                    <a href="/brand-identity">
                        <div class="welcome-4-title">
                            BRAND IDENTITY
                        </div>
                    </a>
                </div>
            </div>
        </div>
    </div>

</section>



@section('js')

    <script>
        $(document).ready(function (ev) {
            $('#custom_carousel').on('slide.bs.carousel', function (evt) {
                $('#custom_carousel .controls li.active').removeClass('active');
                $('#custom_carousel .controls li:eq(' + $(evt.relatedTarget).index() + ')').addClass('active');
            });

            $('#join-newsletter-btn').click(function() {
                displayNewsletterPopup();
            });

            $('#newsletter-close-btn').click(function() {
                hideNewsletterPopup();
            });

            function hideNewsletterPopup() {
                $('#newsletter-popup-container').hide();
                $('#newsletter-content').show();
            }
            function displayNewsletterPopup() {
                $('#newsletter-content').hide();
                $('#newsletter-popup-container').show();
            }
        });
    </script>

@stop