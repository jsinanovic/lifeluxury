<footer class="footer">
    <div class="container-wide">
        <div class="footer-panel panel-left">
            <ul class="footer-list">
                <li><a href="#" class="section-link-footer  link-custom" id="link-custom-collection-btn">The Collection</a></li>
                <li><a href="#" class="section-link-footer link-custom" id="link-custom-press-btn">Press Kit</a></li>
                <li><a  href="#" class="section-link-footer link-custom" id="link-custom-newsletter-btn">Newsletter</a></li>
            </ul>

            <ul class="footer-list">
                <li><a href="/service-promise" class="section-link-footer link-custom">Service Promise</a></li>
                <li><a href="/privacy-policy" class="section-link-footer link-custom">Privacy Policy</a></li>
                <li><a href="/terms-and-conditions" class="section-link-footer link-custom">Terms & Conditions</a></li>
            </ul>
        </div>

        <div class="footer-panel panel-right">
            <ul class="footer-social">
                <li><a href="https://www.facebook.com/lifeandluxurycollection"><i class="fa fa-facebook" aria-hidden="true"></i></a></li>
                <li><a href="https://twitter.com/LifenLuxury"><i class="fa fa-twitter" aria-hidden="true"></i></a></li>
                <li><a href="https://www.youtube.com/channel/UCePsdWq9xg-EN9E7DGaAuHg"><i class="fa fa-youtube" aria-hidden="true"></i></a></li>
                <li><a href="https://www.instagram.com/your_finest_collection"><i class="fa fa-instagram" aria-hidden="true"></i></a></li>
            </ul>

            <ul class="footer-list">
                <li><a class="link-customnoanimatioon">Life & Luxury Collection</a></li>
                <li><a  class="link-custom-noanimation">All Rights Reserved</a></li>
            </ul>
        </div>
    </div>
</footer>
