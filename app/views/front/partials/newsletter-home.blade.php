<div class="newsletter-img-container" style="height: 600px;">
    <img  src="/front/img/newsletter/photo1.png">
</div>
<div id="newsletter-content" class="container newsletter-content">
    <div  class="row">
        <div  style="padding-left: 480px !important;" class="col-md-12">
            <div style="text-align: left;font: 500 46px Butler;" class="newsletter-content-title1">
                Be a part of our Universe
            </div>
            <div style="text-align: left; font-size: 22px " class="newsletter-content-text">
              Newsletter Signup
            </div>
            <div style="text-align: left; font-size: 19px " class="newsletter-content-text">
               Subscribe to our newsletter to get the latest information right in your inbox.
            </div>
            <div style="text-align: left; display: block !important;" id="join-newsletter-btn" class="newsletter-join-btn">
                <span>Join Our Newsletter <i class="fa fa-angle-right" style="margin-left: 20px;" aria-hidden="true"></i></span>
            </div>
        </div>
    </div>
</div>

<div id="newsletter-popup-container">
    <div class="newsletter-popup-container">
        <div class="container">
            <div class="col-md-8 col-md-offset-2">
                <div class="row">
                    <div class="col-md-6 col-md-offset-3" style="margin-left: 45% !important;">
                        <div class="row newsletter-form">
                            {{--<a class="boxclose" id="boxclose"></a>--}}
                            {{Form::open(array('url' => 'subscribe', 'method' => 'POST', 'role' => 'form', 'class' => 'subscribe-form'))}}
                        <div class="col-md-6 form-group newsletter-popup-name-group" >
                                <input  type="text" name="name" id="subscribe-name" class="form-control nl-form-control"  placeholder="First Name:"  required="required"/>
                            </div>
                            <div class="col-md-6 form-group newsletter-popup-surname-group">
                                <input  type="text" name="surname" id="subscribe-surname" class="form-control nl-form-control"  placeholder="Surname:"  required="required" />
                            </div>
                            <div class="col-md-12 form-group">
                                <input type="email" name="email" id="subscribe-email" class="form-control nl-form-control"  placeholder="E-mail:"  required="required" />
                            </div>
                            <div class="col-md-12" style="border-radius: 0px !important;">
                                {{Form::select('country', Setting::values('countries'), null, array('id' => 'subscribe-country', 'class' => 'form-control','placeholder' => 'Country', 'required' => 'required'))}}
                            </div>
                            <div class="newsletter-popup-join-btn" id="nnewsletter-popup-join-btn" style="margin-right: 65%;">
                                <span>Join <i class="fa fa-angle-right" style="margin-left: 20px;" aria-hidden="true"></i></span>
                                </div>
                            {{Form::close()}}
                        </div>
                    </div>
                </div>
                {{--<div id="newsletter-close-btn"><i class="fa fa-close" style=""></i></div>--}}
            </div>
        </div>
    </div>
</div>


{{--@section('js')--}}

    {{--<script>--}}
        {{--$(document).ready(function (ev) {--}}
            {{--$('#custom_carousel').on('slide.bs.carousel', function (evt) {--}}
                {{--$('#custom_carousel .controls li.active').removeClass('active');--}}
                {{--$('#custom_carousel .controls li:eq(' + $(evt.relatedTarget).index() + ')').addClass('active');--}}
            {{--});--}}

            {{--$('#join-newsletter-btn').click(function() {--}}
                {{--displayNewsletterPopup();--}}

            {{--});--}}

            {{--$('#newsletter-close-btn').click(function() {--}}
                {{--hideNewsletterPopup();--}}

            {{--});--}}



            {{--function hideNewsletterPopup() {--}}
                {{--$('#newsletter-popup-container').hide();--}}
                {{--$('#newsletter-content').show();--}}

            {{--}--}}
            {{--function displayNewsletterPopup() {--}}
                {{--$('#newsletter-content').hide();--}}
                {{--$('#newsletter-popup-container').show();--}}
            {{--}--}}
        {{--});--}}
    {{--</script>--}}

{{--@stop--}}