<div class="newsletter-img-container" style="height: 427px;">
    <img  src="/front/img/newsletter/welcome-newsletter-background.png">
</div>
<div id="newsletter-content" class="container newsletter-content">
    <div  class="row">
        <div class="col-md-12">
            <div  class="newsletter-content-title">
                Be part of our Universe
            </div>
            <div class="newsletter-content-text">
              Newsletter Signup!
            </div>
            <div class="newsletter-content-text">
               Subscribe to our newsletter to get the latest information right in your inbox.
            </div>
            <div  id="join-newsletter-btn" class="newsletter-join-btn">
                <span>Join Our Newsletter <i class="fa fa-angle-right" style="margin-left: 20px;" aria-hidden="true"></i></span>
            </div>
        </div>
    </div>
</div>

<div id="newsletter-popup-container">
    <div class="newsletter-popup-container">
        <div class="container">
            <div class="col-md-8 col-md-offset-2">
                <div class="row">
                    <div class="col-md-6 col-md-offset-3">
                        <div class="row newsletter-form">
                            {{--<a class="boxclose" id="boxclose"></a>--}}
                            {{Form::open(array('url' => 'subscribe', 'method' => 'POST', 'role' => 'form', 'class' => 'subscribe-form'))}}
                            <div class="col-md-6 form-group newsletter-popup-name-group">
                                <input  type="text" name="name" id="subscribe-name" class="form-control nl-form-control"  placeholder="First Name:"  required="required"/>
                            </div>
                            <div class="col-md-6 form-group newsletter-popup-surname-group">
                                <input  type="text" name="surname" id="subscribe-surname" class="form-control nl-form-control"  placeholder="Surname:"  required="required" />
                            </div>
                            <div class="col-md-12 form-group">
                                <input type="email" name="email" id="subscribe-email" class="form-control nl-form-control"  placeholder="E-mail:"  required="required" />
                            </div>
                            <div class="col-md-12">
                                {{Form::select('country', Setting::values('countries'), null, array('id' => 'subscribe-country', 'class' => 'form-control','placeholder' => 'Country', 'required' => 'required'))}}
                            </div>
                            <div class="newsletter-popup-join-btn">
                                <span>Join <i class="fa fa-angle-right" style="margin-left: 20px;" aria-hidden="true"></i></span>
                                {{--<button style="outline: 0px solid #cdcdc1;" type="submit" class="btn-cs-join btn btn-primary">Join<i class="fa fa-angle-right cs-angle-icon" aria-hidden="true"></i></button>--}}
                            </div>
                            {{Form::close()}}
                        </div>
                    </div>
                </div>
                <div id="newsletter-close-btn"><i class="fa fa-close" style=""></i></div>
            </div>
        </div>
    </div>
</div>
