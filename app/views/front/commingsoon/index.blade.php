<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title> Life & Luxury</title>

    {{--{{HTML::style('front/plugins/bootstrap-3.3.2-dist/css/bootstrap.min.css', array('async'))}}--}}
    {{--{{HTML::script('front/plugins/bootstrap-3.3.2-dist/js/bootstrap.min.js')}}--}}

    <link href="/front/plugins/bootstrap-3.3.2-dist/css/bootstrap.min.css" rel="stylesheet">
    <link href="/front/plugins/font-awesome/css/font-awesome.min.css" rel="stylesheet">
    <link href='http://fonts.googleapis.com/css?family=Open+Sans:300,400,800&subset=latin,cyrillic-ext' rel='stylesheet' type='text/css'>

    <script type="text/javascript" src="/front/plugins/jquery/jquery-1.11.2.min.js"></script>
    <script type="text/javascript" src="/front/plugins/bootstrap-3.3.2-dist/js/bootstrap.min.js"></script>

    <style>

        @font-face {
            font-family: Butler;
            src: url('/front/fonts/Butler_Regular.otf');
        }

        body {
            overflow: hidden;
            background-image: url("/img/commingsoon/maingimg_rectangle.png");
        }
        .bg-container {
            height: 100vh;
            width: 100%;
            position: relative;
            overflow: hidden;
            display: table;
        }
        .background-overlay {
            image-rendering:al aliased;
            position: absolute;
            top: 0;
            left: 0;
            width: 100%;
            height: 100vh;
            background-color: transparent;
            z-index: -1;

        }
        /* Women image */
        .bg-container img.bg-image {
            position: absolute;
            /*left: 0;*/
            /*top: 0;*/
            opacity: 0.2;
            /* Web */
            left: -1000%;
            right: -1000%;
            top: -1000%;
            bottom: -1000%;
            margin: auto;
            min-height: 100%;
            min-width: 100%;
        }
        /* Middle - circle */
        .bg-container .main-container{
            position: relative;
            z-index: 4;
            display: table-cell;
            vertical-align: middle;
            text-align: center;
        }
        .main-container .heading {
            top: 30px;
            width: 100%;
            position: absolute;
        }
        .main-container .heading img {
            height: 50px;
        }
        .circle-container {
            /* Set width via bootstrap */
            width: 70%;
            display: inline-block;
            text-align: center;
        }
        .outer-circle {
            background: url("/img/commingsoon/circles.png");
            background-size: 200%;
            background-repeat: no-repeat;
            background-position: center;
            display: inline-block;
            /* Web */
            padding: 50px;
        }
        .circle-text {
            /* Web */
            padding: 100px;
            height: 100%;
            text-align: center;
        }
        .circle-heading {
            font-size: 40pt;
            line-height: 45pt;
            color: #cdcfc1;
            font-family: Butler;
        }
        .circle-msg {
            color: #cdcfc1;
            text-transform: uppercase;
            font-size: 10pt;
            line-height: 20pt;
        }
        .circle-join-btn {
            font-family: 'Open Sans', sans-serif;
            margin-top: 20px;
            display: inline-block;
            background: #cdcfc1;
            color: #292b33;
            height: 35px;
            line-height: 35px;
            width: 170px;
            cursor: pointer;
        }
        .circle-join-btn:hover {
            background-color: #2b2d39;
            color: #cdcfc1;
            border: 1px solid #cdcfc1 !important;
        }

        .nl-container {
            display: none;
            height: 300px;
            width: 500px;
            position: absolute;
            margin-top: -35%;
            background-color: #292b33;
            border: 10px solid #fff;
            padding: 40px;
            z-index: 20;
        }
        .nl-form-control {
            border-radius: 0 !important;
            height: 40px !important;
            outline: none !important;
        }
        .nl-form-group {
            display: inline-block !important;
        }
        .form-control-nl-country {
            border-radius: 0 !important;
            height: 40px !important;
        }

        #subscribe-country{
            outline: 1px solid #cdcdc1 !important;
            border:1px solid #cdcdc1 !important;
        }

        .nl-form-group-name {
            width: 48.5%;
            float: left;
        }
        .nl-form-group-surname {
            width: 48.5%;
            float: right;
        }
        .nl-form-group-btn {
            margin-top: 20px;
            cursor: pointer;
        }

        .nl-container input:focus{
            outline: 3px solid #cdcdc1;
            /*outline: none !important;*/
            border:3px solid #cdcdc1;

            /*outline:none;*/
            /*box-shadow:none;*/
            /*color: #cdcdc1 !important;*/
        }

        *:focus {
            border:3px solid #cdcdc1;
            outline: 3px solid #cdcdc1;

        }


        .btn-cs-join:hover {
            background-color: #cdcfc1 !important;
            color: #292b33 !important;
        }

        .btn-primary {
            background-color: #292b33 !important;
        }

        .btn-primary:hover {
            background-color: #cdcfc1 !important;
            color: #292b33 !important;
        }
        .btn-cs-join {
            /*background-color: #292b33 !important;*/
            border: 1px solid #888 !important;
            color: #888 !important;
            border-radius: 0 !important;
            padding: 10px 20px;
            font: 400 12px 'Open Sans', sans-serif;
        }
        *::-webkit-input-placeholder {
            color: #555 !important;
            font-family:'Open Sans', sans-serif;
        }
        *:-moz-placeholder {
            /* FF 4-18 */
            color: #555 !important;
            font-family:'Open Sans', sans-serif;
        }
        *::-moz-placeholder {
            /* FF 19+ */
            color: #555 !important;
            font-family:'Open Sans', sans-serif;
        }
        *:-ms-input-placeholder {
            /* IE 10+ */
            color: #555 !important;
            font-family: 'Open Sans', sans-serif;
        }
        .cs-angle-icon {
            margin-left: 10px;
        }

        .footer {
            width: 100%;
            position: absolute;
            color: #cdcfc1;
            font-size: 12px;
            text-transform: uppercase;
            font-family: 'Open Sans', sans-serif;
            /* Web */
            bottom: 20px;
            right: 80px;
            text-align: right;
        }
        .icons {
            line-height: 20pt;
            /*margin-right: -5px;*/
        }
        .icons a i:hover{
            color: #eeede8;
        }
        .fa {
            color: #888;
        }

        a.boxclose{
            float:right;
            margin-top:-68px;
            margin-right:-68px;
            cursor:pointer;
            color: #fff;
            /*border: 1px solid #AEAEAE;*/
            border-radius: 30px;
            /*background: #605F61;*/
            font-size: 31px;
            font-weight: bold;
            display: inline-block;
            line-height: 0px;
            padding: 11px 3px;
        }
        a.boxclose:hover{
            text-decoration: none;
        }

        .boxclose:before {
            content: "×";
        }


        @media (max-width: 768px) {
            .circle-container {
                width: 70%;
            }
            .circle-heading {
                text-align: center;
                padding-top: 2%;
                font-size: 1.9em;
                line-height: 1.2em;
            }
            .circle-msg {
                font-size: 0.9em;
                line-height: 0.6em;
                padding-top: 10px;
                padding-bottom: 10px;
            }
            .circle-join-btn {
                font-size: 0.9em;
                line-height: 0.6em;
                display: inline-block;
                padding: 7px 8px;
                margin-top: 10px;
                width: auto;
                height: auto;
            }
            .bg-container img.bg-image {
                height: 100%;
                width: auto;
                min-height: 100%;
                min-width: 100%;
                left: -1000%;
                right: -1000%;
                top: -1000%;
                bottom: -1000%;
                margin: auto;
            }
            .circle-text {
                padding: 25%;
            }
            .outer-circle {
                display: block;
            }

            .newcursor {
                cursor: url("/img/commingsoon/close.png");
                width: 100px;
                height: 100px;
            }


        }

        @media (max-width: 480px) {
            .circle-container {
                width: 90%;
            }
            .circle-text {
                padding: 15%;
            }
            .circle-heading {
                text-align: center;
                padding-top: 2%;
                font-size: 1.6em;
                line-height: 1.2em;
            }
            .circle-msg {
                font-size: 0.8em;
                line-height: 0.6em;
                padding-top: 10px;
                padding-bottom: 10px;
            }
            .circle-join-btn {
                font-size: 0.8em;
                line-height: 0.6em;
                display: inline-block;
                padding: 10px 10px;
                margin-top: 10px;
                width: auto;
                height: auto;
            }
            .bg-container img.bg-image {
                height: 100%;
                width: auto;
                min-height: 100%;
                min-width: 100%;
            }
        }

        @media (max-width: 1170px) {
            .footer {
                display: none;
            }
        }
    </style>
</head>
<body>

<div class="bg-container">
    <!-- Circle -->

    <div class="main-container">
        <div class="heading"><img src="/img/commingsoon/logo.png"></div>

        <div class="circle-container">
            <div class="outer-circle">
                <div class="circle-text">
                    <div class="circle-heading"> Everyday <br> Transformed </div>
                    <div class="circle-msg"> Discover it soon! </div>
                    <div class="circle-join-btn" id="circle-join-btn">Join Our Universe</div>
                </div>
                <div id="background-overlay" class="background-overlay"></div>
                <div class="nl-container">
                    {{--<a class="boxclose" id="boxclose"></a>--}}
                    {{Form::open(array('url' => 'subscribe', 'method' => 'POST', 'role' => 'form', 'class' => 'subscribe-form'))}}
                    <div class="form-group nl-form-group nl-form-group-name">
                        <input  type="text" name="name" id="subscribe-name" class="form-control nl-form-control"  placeholder="First Name:"  required="required"/>
                    </div>
                    <div class="form-group nl-form-group nl-form-group-surname">
                        <input  type="text" name="surname" id="subscribe-surname" class="form-control nl-form-control"  placeholder="Last Name:"  required="required" />
                    </div>
                    <div class="form-group">
                        <input type="email" name="email" id="subscribe-email" class="form-control nl-form-control"  placeholder="E-mail:"  required="required" />
                    </div>
                    {{Form::select('country', Setting::values('countries'), null, array('id' => 'subscribe-country', 'class' => 'form-control form-control-nl-country','placeholder' => 'Country', 'required' => 'required'))}}
                    <div>
                        <div class="nl-form-group-btn">
                            <button style="outline: 0px solid #cdcdc1;" type="submit" class="btn-cs-join btn btn-primary">Join<i class="fa fa-angle-right cs-angle-icon" aria-hidden="true"></i></button>
                        </div>
                    </div>
                    {{Form::close()}}
                </div>

            </div>
        </div>

        <div class="footer">
                    <span class="icons">
                        <a href="https://www.facebook.com/lifeandluxurycollection"><i class="fa fa-facebook" aria-hidden="true"></i></a> &nbsp
                        <a href="https://twitter.com/LifenLuxury"><i class="fa fa-twitter" aria-hidden="true"></i></a> &nbsp
                        <a href="https://www.youtube.com/channel/UCePsdWq9xg-EN9E7DGaAuHg"><i class="fa fa-youtube" aria-hidden="true"></i></a> &nbsp
                        <a href="https://www.instagram.com/your_finest_collection"><i class="fa fa-instagram" aria-hidden="true"></i></a>
                    </span>
            <br>Life & Luxury collection<br>
            All rights reserved
        </div>
    </div>
    <!-- Main bg image -->
    <img class="bg-image" src="/img/commingsoon/maingimg.png">
</div>

<script>

    $(document).ready(function(){

        var scrollTop     = $(window).scrollTop(),
                elementOffset = $('.circle-text').offset().top,
                distance      = (elementOffset - scrollTop);

        var screenSize = $(window).width();
        var isPopupActive = false;

        $('#circle-join-btn').click( function() {
            activateNewsletterPopup();
        });

        $('#boxclose').click(function(){
            $('.nl-container').animate({'top':'-500px'},500,function(){
                $('.nl-container').fadeOut('fast');
                deactivateNewsletterPopup();
            });
        });

        $('#background-overlay').click( function() {
            deactivateNewsletterPopup();
        });

        $(document).keyup(function(e) {
            if (isPopupActive)
                if (e.keyCode === 27) deactivateNewsletterPopup();   // esc
        });

        function activateNewsletterPopup() {

            if (screenSize < 750) {
                $('.nl-container').css({'width':'auto', 'height':'auto', 'margin-top':'-60%', 'padding':'10px'});
                $('.outer-circle').css({'padding':'0'});
                $('.circle-container').css({'display':'block'});
                $('.footer').css({'display':'none'});
            }
            else if (screenSize > 1170)
            {
                $('.nl-container').css({'margin-top':'0', 'top':distance+55});

                var wrpWidth = $('.circle-text').outerWidth(true);
                $('.nl-container').css({'width':wrpWidth});

            }
            else {
                var newWidth = $('.outer-circle').width();
                $('.nl-container').css({'height':'auto', 'width':newWidth, 'margin-top':'-40%', 'padding':'10px'});
            }
            $('.bg-image').css({'opacity':'0.1'});
            $('#background-overlay').css({'background-color':'transparent' , 'z-index' : '5',
                'cursor' : 'url("/img/commingsoon/close.png"), auto',
                'cursor': '-webkit-image-set(url("/img/commingsoon/close.png") 1x, url("/img/commingsoon/close.png") 2x ), auto'
            });

            $('.nl-container').css({'display':'block'});

            isPopupActive = true;
        }

        function deactivateNewsletterPopup() {

            if (screenSize < 750) {
                $('.nl-container').css({'width':'auto', 'height':'auto', 'margin-top':'-35%', 'padding':'10px'});
                $('.outer-circle').css({'padding':'50px'});
                $('.circle-container').css({'display':'inline-block'});
            }
            $('.bg-image').css({'opacity':'0.2'});
            $('.nl-container').css({'display':'none'});
            $('#background-overlay').css({'background-color':'transparent' , 'z-index' : '-1', 'cursor' : 'auto'});

            isPopupActive = false;
        }
    });

</script>

</body>

</html>