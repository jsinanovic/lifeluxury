<?php

class ClientRequestsController extends BaseAdminController {

    protected static $moduleName = 'client-requests';
    protected static $modelName = 'ClientRequest';

    public static function getIndex($type = false) {
        $data = array(
            'pageName' => 'All ' . str_plural(static::$modelName),
            'type' => $type,
        );
        $data = static::init($data, 'index');
        return View::make(static::getViewPath() . 'index', $data);
    }

    public static function getView($id) {
        $request = ClientRequest::find($id);
        if ($request) {
            if ($request->status == 0) {
                $request->status = 1;
                $request->save();
                self::onEntryChanged($request, 'status');
            }
            if ($request->type == 'requests') {
                $request->message = json_decode($request->message, TRUE);
            }

            $data = array(
                'entry' => $request
            );
            $data = static::init($data, 'view');
            return View::make(static::getViewPath() . 'view', $data);
        } else {
            App::abort(404);
        }
    }

    protected static function onEntryChanged($entry, $event) {
        parent::onEntryChanged($entry, $event);
        self::onEntriesChanged($event);
        //$request
    }

    public static function postChangeStatus() {
        $result = array('status' => 0);
        $id = (int) Input::get('id', 0);
        if ($id) {
            $modelClass = static::$modelName;
            $entry = $modelClass::find($id);
            $entry->status = ($entry->status == 2) ? 1 : 2;
            $result['status'] = $entry->save();
            static::onEntryChanged($entry, 'status');
            static::onEntriesChanged('status');
        }
        return Response::json($result);
    }

    public static function onAfterEntrySave($result, $entry, $type = 'create') {
        $result = parent::onAfterEntrySave($result, $entry, $type);
        if ($entry->type == 'requests') {
            $entry->message = json_decode($entry->message, TRUE);
        }
        
        $s = Subscriber::where('email', '=', $entry->user_email)->first();
        if(!$s) {
            $s = Subscriber::create(array(
                'name' => $entry->user_name,
                'surname' => $entry->user_surname,
                'email' => $entry->user_email,
                'status' => 1,
                'token' => Hash::make($entry->user_name . time() . $entry->user_email . rand(0, 10000)),
            ));
        }
        
        Mail::send('emails.requests.contact', array('recipient' => $s), function($message) use($entry) {
            $siteName = Setting::value('site-name');
            $message->from('noreply@lifeandluxury.com', $siteName)
                    ->to($entry->user_email)
                    ->subject("THANK YOU FOR YOUR REQUEST SOMEONE WILL REVERT TO YOU SHORTLY");
        });


        Mail::send('emails.requests.clientrequest', array('request' => $entry), function($message) use ($entry) {
            
            $adminEmail = ($entry->type == 'customercare')? Setting::value('contact-email') : Setting::value('customercare-email');
            $siteName = Setting::value('site-name');
            $message->from($entry->user_email, $entry->user_name)
                    ->to($adminEmail)
                    ->subject("New client's request on " . $siteName);
        });

        return $result;
    }
    
    public static function postCreate()
    {
        $type = Input::get('type', 'contacts');
        switch ($type) {
            case 'contacts': 
                ClientRequest::$rules['user_phone'] = '';
                ClientRequest::$rules['user_website'] = '';
                ClientRequest::$rules['description'] = '';
                ClientRequest::$rules['budget'] = '';
                ClientRequest::$rules['subject'] = '';
                break;
            case 'callback':
                ClientRequest::$rules['message'] = '';
                ClientRequest::$rules['user_website'] = '';
                ClientRequest::$rules['description'] = '';
                ClientRequest::$rules['budget'] = '';
                break;
            case 'quote':
                ClientRequest::$rules['message'] = '';
                ClientRequest::$rules['subject'] = '';
                ClientRequest::$rules['user_phone'] = '';
                break;
            case 'customercare':
                ClientRequest::$rules['user_phone'] = '';
                ClientRequest::$rules['user_website'] = '';
                ClientRequest::$rules['description'] = '';
                ClientRequest::$rules['budget'] = '';
                ClientRequest::$rules['subject'] = '';
                break;
        }
        
        return parent::postCreate();
    }
    

}
