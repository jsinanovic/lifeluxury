<?php

class SubscribersController extends BaseAdminController {

    protected static $moduleName = 'subscribers';
    protected static $modelName = 'Subscriber';
    protected static $formFields = array(
        'name' => array(
            'label' => 'Name',
            'type'  => 'text',
        ),
        'surname' => array(
            'label' => 'Surname',
            'type'  => 'text',
        ),
        'country' => array(
            'label' => 'Country',
            'type'  => 'select',
            'values' => array(),
        ),
        'email' => array(
            'label' => 'E-mail',
            'type'  => 'text',
        ),
        'status' => array(
            'label' => 'Active',
            'type'  => 'checkbox',
            'help'  => 'Is subscription active?',
            'value' => 1,
            'default' => true
        ),
    );
    
    
    protected static function getFormFields()
    {
        static::$formFields['country']['values'] = Setting::values('countries');   
        return static::$formFields;
    }
    
    public static function postCreate()
    {
        $modelClass = static::$modelName;
        $data = Input::all();
        $result = $modelClass::validate($data);
        $result['status'] = $result['valid'];
        if ($result['valid']) {
            $s = Subscriber::where('email', '=', $data['email'])->first();
            if(!$s) {
                $s = Subscriber::create(array(
                    'email' => $data['email'],
                    'name' => $data['name'],
                    'surname' => $data['surname'],
                    'country' => $data['country'],
                    'status' => isset($data['status'])? $data['status'] : 1,
                    'token' => sha1($data['name'] . time() . $data['email'] . rand(0, 10000)),
                ));
                static::onEntriesChanged('create');
                
                if(!isset($data['status'])) {
                    Mail::send('emails.requests.subscribe', array('recipient' => $s), function($message) use($s) {
                        $siteName = Setting::value('site-name');
                        $message->from('noreply@lifeandluxury.com', $siteName)
                                ->to($s->email)
                                ->subject("THANK YOU FOR SIGNING UP EMAIL");
                    });
                }
                
            }
        } else {
            $result['errors'] = $result['validator']->messages()->all();
        }
        
        if (Request::ajax()) {
            return Response::json($result);
        } else {
            if($result['valid']) {
//                return Redirect::to(static::getModuleUri() . 'edit/' . $s->id)->withSuccess(true);
            } else {
//                return Redirect::to(static::getModuleUri() . 'create')->withErrors($result['validator'])->withInput($data);
            }
        }

        return Redirect::to('/');
    }
    
    public static function getIndex()
    {
        DB::table('subscribers')->update(array('is_new' => 0));
        $data = parent::getIndex();
        return $data;
    }
    
    public static function getMessage()
    {
        $modelClass = static::$modelName;
        $obj = new $modelClass();
        $data = array(
            'formFields' => array(
                'title' => array(
                    'label' => 'Subject',
                    'type' => 'text',
                ),
                'message' => array(
                    'label' => 'Message',
                    'type' => 'html',
                ),
                'users' => array(
                    'label' => 'Recipients: Active Subscribers',
                    'type' => ''
                ),
            ),
            'formAction' => static::getModuleUri() . 'message',
            'formRules' => $modelClass::getJsonValidationRules(),
            'title' => 'Add a new <strong>' . static::$modelName . '</strong>',
            'entry' => false,
            'translatedAttributes' => $obj->translatedAttributes,
        );
        
        $data = static::init($data, 'create');
        $view = static::$autoForm? 'admin.layouts.autoform' : static::getViewPath() . 'form';
        return View::make($view, $data);
    }
    
    public static function postMessage()
    {
        $subject = Input::get('title', '');
        $content = Input::get('message', false);
        if($content) {
            $recipients = Subscriber::active()->get();

            foreach($recipients as $recipient) {
                Mail::queue('emails.requests.message', 
                        array('content' => $content, 'recipient' => $recipient), 
                        function($message) use ($recipient, $subject) 
                {
                    $siteName = Setting::value('site-name');
                    $message->from('noreply@lifeandluxury.com', $siteName)
                            ->to($recipient->email)
                            ->subject($subject);
                });
            }
            
            return Redirect::to('/admin/subscribers')->withSuccess(true);
        }
        
        return Redirect::to('/admin/subscribers')->withErrors(true);

    }
    
}
