<?php

class ProductsController extends BaseAdminController {

    private static $menuKey = 'shop';
    protected static $moduleName = 'products';
    protected static $modelName = 'Product';
    protected static $formFields = array(
        'name' => array(
            'label' => 'Name',
            'type'  => 'text',
            'help'  => 'Article name',
        ),
        'slug' => array(
            'label' => 'Slug',
            'type'  => 'text',
            'help'  => "Page's URI for better SEO optimization. Left it empty for autogeneration",
        ),
        'image' => array(
            'label' => 'Image',
            'type'  => 'file',
            'help'  => 'Max size is 5MB. Allowed .jpg and .png extensions',
        ),
        'gallery' => array(
            'label' => 'Screenshots',
            'type' => 'gallery',
        ),
        'price' => array(
            'label' => 'Price',
            'type'  => 'money',
            'value' => 0,
        ),
        'url' => array(
            'label' => 'URL',
            'type'  => 'text',
            'help'  => 'URL on the product payment page',
        ),
        'demo_url' => array(
            'label' => 'Demo URL',
            'type'  => 'text',
            'help'  => 'URL on the product demo version',
        ),
        'categories' => array(
            'label' => 'Categories',
            'type'  => 'multiselect',
            'values' => array(),
        ),
        'annotation' => array(
            'label' => 'Annotation',
            'type'  => 'textarea',
            'help'  => "Short content preview",
        ),
        'content' => array(
            'label' => 'Content',
            'type'  => 'html',
        ),
        'features' => array(
            'label' => 'Details',
            'type' => 'features',
        ),
        'prices' => array(
            'label' => 'Prices',
            'type' => 'custom',
            'template' => 'pricesfield',
            'values' => array('personal' => 'Personal License', 'extended' => 'Extended License')
        ),
        'title' => array(
            'label' => 'META Title',
            'type'  => 'text',
            'help'  => 'Title of the page (it is visible in a browser tab)',
        ),
        'keywords' => array(
            'label' => 'META Keywords',
            'type'  => 'textarea',
            'help'  => 'META data for SEO optimization',
        ),
        'description' => array(
            'label' => 'META Description',
            'type'  => 'textarea',
            'help'  => 'META data for SEO optimization',
        ),
        'status' => array(
            'label' => 'Active',
            'type'  => 'checkbox',
            'help'  => 'Do you want to make this page active?',
            'value' => 1,
            'default' => false
        )
    );
    
    
    protected static function getFormFields()
    {
        $categories = array();
        $menu = Menu::getMenu(self::$menuKey);
        foreach ($menu->categories as $category)
            $categories[$category->id] = $category->name;
        self::$formFields['categories']['values'] = $categories;
        return self::$formFields;
    }
    
    
    public static function init($data = array(), $pageType = 'index')
    {
        $data = parent::init($data, $pageType);
        if($pageType == 'edit') {
            $data['entry']->categories = $data['entry']->categories->lists('id');
        }
        return $data;
    }
}
