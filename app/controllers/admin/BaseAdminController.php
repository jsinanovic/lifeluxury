<?php

class BaseAdminController extends BaseController {
    
    protected static $baseName = 'admin';
    protected static $moduleName = '';
    protected static $modelName = '';
    protected static $formFields = array();
    protected static $autoForm = true;
    
    //--------------------------- Default Routes -------------------------------
    //--------------------------------------------------------------------------
    
    public static function getIndex()
    {
        $data = static::init();
        $data['pageName'] = 'All ' . str_plural(static::$modelName);
        return View::make(static::getViewPath() . 'index', $data);
    }
    
    protected static function getFormFields()
    {
        return static::$formFields;
    }
    
    public static function getCreate()
    {
        $modelClass = static::$modelName;
        $obj = new $modelClass();
        $data = array(
            'formFields' => static::getFormFields(),
            'formAction' => static::getModuleUri() . 'create',
            'formRules' => $modelClass::getJsonValidationRules(),
            'title' => 'Add a new <strong>' . static::$modelName . '</strong>',
            'entry' => false,
            'translatedAttributes' => $obj->translatedAttributes,
        );
        
        $data = static::init($data, 'create');
        $view = static::$autoForm? 'admin.layouts.autoform' : static::getViewPath() . 'form';
        return View::make($view, $data);
    }

    public static function getEdit($entry)
    {
        $modelClass = static::$modelName;
        $data = array(
            'formFields' => static::getFormFields(),
            'formAction' => static::getModuleUri() . 'edit/' . $entry->id,
            'formRules' => $modelClass::getJsonValidationRules(),
            'title' => 'Edit "<strong>' . $entry->{$modelClass::$nameField} . '</strong>"',
            'translatedAttributes' => $entry->translatedAttributes,
            'entry' => $entry,
        );
            
        $data = static::init($data, 'edit');
        $view = static::$autoForm? 'admin.layouts.autoform' : static::getViewPath() . 'form';
        return View::make($view, $data);
    }
    
    public static function onSuccessRedirect($entry)
    {
        return Redirect::to(static::getModuleUri())->withSuccess(true);
    }
    
    public static function postCreate()
    {
        $modelClass = static::$modelName;
        $data = Input::all();
        $result = $modelClass::validate($data);
        $result['status'] = 0;
        if ($result['valid']) {
            $entry = new $modelClass($result['modifiedData']);
            $entry = static::onBeforeEntrySave($entry);
            $result['status'] = $entry->save();
            $result['id'] = $entry->id;
            $result = static::onAfterEntrySave($result, $entry);
            // CALLBACK
            static::onEntriesChanged('create');
            
            if (Request::ajax())
                return Response::json($result);
            if (isset($data['continue']))
                return Redirect::to(static::getModuleUri() . 'edit/' . $entry->id)->withSuccess(true);
            else
                return static::onSuccessRedirect ($entry);
        } else {
            if (Request::ajax()) {
                $result['errors'] = $result['validator']->messages()->all();
                return Response::json($result);
            } else {
                if($modelClass::$imageField && isset($data[$modelClass::$imageField]))
                    unset($data[$modelClass::$imageField]);
                return Redirect::to(static::getModuleUri() . 'create')->withErrors($result['validator'])->withInput($data);
            }
        }
    }
    
    public static function postEdit($entry)
    {
        $modelClass = static::$modelName;
        $modelClass::$isSort = false;
        $data = Input::all();

        // if we edit entry and already have slug for it, we should make it unique
        // except current entry
        if ($modelClass::$isSlug) {
            $modelClass::$rules['slug'] .= ',slug,' . $entry->id;
            if (isset($data['slug']) && $entry->slug == $data['slug'])
                $modelClass::$isSlug = false;
        }

        $result = $modelClass::validate($data);
        $result['status'] = 0;
        if ($result['valid']) {
            $entry->fill($result['modifiedData']);
            $entry = static::onBeforeEntrySave($entry, 'edit');
            $result['status'] = $entry->save();
            $result['id'] = $entry->id;
            $result = static::onAfterEntrySave($result, $entry, 'edit');
            // CALLBACK
            static::onEntryChanged($entry, 'edit');
            static::onEntriesChanged('edit');
            
            
            if (Request::ajax())
                return Response::json($result);
            if (isset($data['continue']))
                return Redirect::to(static::getModuleUri() . 'edit/' . $entry->id)->withSuccess(true);
            else
                return static::onSuccessRedirect ($entry);
        } else {
            if (Request::ajax())
                return Response::json($result);
            else {
                if(isset($data['assigned']))
                    $data['assigned'] = '';
                return Redirect::to(static::getModuleUri() . 'edit/' . $entry->id)->withErrors($result['validator'])->withInput($data);
            }
        }
    }
    
    
    //--------------------------- DATA functions -------------------------------
    //--------------------------------------------------------------------------
    
    public static function getViewPath()
    {
        return static::$baseName . '.' . static::$moduleName . '.';
    }
    
    public static function getModuleUri()
    {
        return static::$baseName . '/' . static::$moduleName . '/';
    }
    
    public static function init($data = array(), $pageType = 'index') 
    {
        $data['pageType'] = $pageType;
        $data['moduleUri'] = static::getModuleUri();
        $data['moduleName'] = static::$moduleName;
        $data['baseName'] = static::$baseName;
        $data['modelName'] = static::$modelName;
        $data['menus'] = array();
        $data['settings'] = Setting::getAllSettings();
        $data['locales'] = ['en']; // should be array locales from the dimsav/laravel-translatable package
        $data['newSubscribers'] = Subscriber::where('is_new', 1)->count();
        $menus = Menu::getAllCached();
        foreach ($menus as $menu)
            $data['menus'][$menu->key] = $menu;
        
        $data['requestsCounter'] = ClientRequest::getCounters();
        
        return $data;
    }
    
    public static function getAll()
    {
        $modelClass = static::$modelName;
        $params = Input::all();
        return $modelClass::getAllCached($params);
    }
    
    public static function onBeforeEntrySave($entry, $type = 'create')
    {
        $modelClass = static::$modelName;
        if($modelClass::$imageField) {
            $imageName = Input::get($modelClass::$imageField . '_name', '');
            if($type == 'edit' && $entry->getOriginal($modelClass::$imageField) && !$imageName) {
                $entry->removeImage($modelClass::$imageField);
            }
            $entry->{$modelClass::$imageField} = $imageName;
            foreach ($modelClass::$imageFieldsExtra as $imageField) {
                $imageName = Input::get($imageField . '_name', '');
                if($type == 'edit' && $entry->getOriginal($imageField) && !$imageName) {
                    $entry->removeImage($imageName);
                }
                $entry->{$imageField} = $imageName;
            }
        }
        return $entry;
    }
    
    public static function onAfterEntrySave($result, $entry, $type = 'create')
    {
        $modelClass = static::$modelName;
        $entry->bindAssignedImages(Input::get('assigned', array()), array('content'));
        if($modelClass::$imageField && isset($result['modifiedData'][$modelClass::$imageField])) {
            
            $filename = $modelClass::uploadImage($result['modifiedData'][$modelClass::$imageField], $modelClass::$imageSizes, $entry->id);
            if($filename) {
                $entry->{$modelClass::$imageField} = $filename;
                $entry->save();
            }
            //$modelClass::processImage($result['modifiedData'][$modelClass::$imageField], $entry);
        }
        
        foreach ($modelClass::$imageFieldsExtra as $imageField) {
            if(isset($result['modifiedData'][$imageField])) {
                $filename = $modelClass::uploadImage($result['modifiedData'][$imageField], $modelClass::$imageSizes, $entry->id);
                if($filename) {
                    $entry->{$imageField} = $filename;
                    $entry->save();
                }
            }
        }
        
        
        if(isset($result['translation']))
            static::processTranslation($entry, $result['translation']);
        static::processEntryCategories($entry);
        static::processEntrySubcategories($entry);
        static::processEntryTags($entry);
        static::processEntrySimilar($entry);
        return $result;
    }
    
    protected static function processTranslation($entry, $translation)
    {
        if($translation) {
            foreach ($translation as $locale => $translatedData)
                foreach ($translatedData as $key => $value) {
                    $entry->translateOrNew($locale)->{$key} = $value;
                }

            $entry->save();
        }
    }
    
    public static function processEntrySimilar($entry)
    {
        $modelClass = static::$modelName;
        if($modelClass::$isSimilar) {
            $ids = Input::get('similar', array());
            $entry->similar()->sync($ids);
        }
    }
    
    public static function processEntryCategories($entry)
    {
        $modelClass = static::$modelName;
        if($modelClass::$isCategories) {
            $ids = Input::get('categories', array());
            if(!is_array($ids))
                $ids = array($ids);
            $entry->categories()->sync($ids);
        }
    }

    public static function processEntrySubcategories($entry)
    {
        $modelClass = static::$modelName;
        if($modelClass::$isSubcategories) {
            $ids = Input::get('subcategories', array());
            if(!is_array($ids))
                $ids = array($ids);
            $entry->subcategories()->sync($ids);
        }
    }
    
    public static function processEntryTags($entry)
    {
        $modelClass = static::$modelName;
        if($modelClass::$tagType) {
            $tagString = Input::get('tags', '');
            $tagsNames = explode(',', $tagString);
            $entryTagsNames = array();
            foreach ($entry->tags as $tag)
                $entryTagsNames[] = trim($tag->name);
            
            $ids = array();
            foreach ($tagsNames as $tagName) {
                $tagName = trim($tagName);
                $index = array_search($tagName, $entryTagsNames);
                if($tagName) {
                    $tag = Tag::add($tagName, $modelClass::$tagType);
                    if($index === FALSE) {
                        $tag->increment('count');
                    }
                    $ids[] = $tag->id;
                }
            }
            $entry->tags()->sync($ids);
        }
    }
    
    
    //--------------------------- JSON functions -------------------------------
    //--------------------------------------------------------------------------
    
    public static function getAllJson()
    {
        $result = array();
        $data = static::getAll()->toArray();
        $result = array_map(function($row){
            return array_values($row);
        }, $data);
        
        return Response::json(array('data' => $result));
    }
    
    public static function postChangeStatus()
    {
        $result = array('status' => 0);
        $id = (int)Input::get('id');
        if($id) {
            $modelClass = static::$modelName;
            $entry = $modelClass::find($id);
            $entry->status = !$entry->status;
            $result['active'] = $entry->status;
            $result['status'] = $entry->save();
            static::onEntryChanged($entry, 'status');
            static::onEntriesChanged('status');
        }
        return Response::json($result);
    }
    
    public static function postSort()
    {
        $result = array('status' => 0, 'error' => '');
        $id = (int) Input::get('id', 0);
        $prevId = (int) Input::get('prev', 0);
        $nextId = (int) Input::get('next', 0);
        if($id > 0) {
            $modelClass = static::$modelName;
            $entry = $modelClass::find($id);
            if($prevId > 0) {
                $prevEntry = $modelClass::find($prevId);
                $entry->sort = $prevEntry->sort + 1;
            } else {
                $nextEntry = $modelClass::find($nextId);
                $entry->sort = $nextEntry->sort;
            }
            
            $modelClass::where('sort', '>=', $entry->sort)->orderBy('sort', 'DESC')->increment('sort', 1);
            $entry->save();
            $result['status'] = 1;
            
            static::onEntryChanged($entry, 'sort');
            static::onEntriesChanged('sort');
        } else {
            $result['status'] = -1;
            $result['error'] = 'Wrong ID';
        }
        
        return Response::json($result);
    }
    
    public static function postRemove()
    {
        $modelClass = static::$modelName;
        $result = array('status' => 0);
        $id = (int)Input::get('id', 0);
        $entry = $modelClass::find($id);
        if($entry) {
            // if entry has image field, we should remove image from folder
            if($modelClass::$imageField) {
                $entry->removeImage();
            }
            // remove entry's media content from folder and library
            $entry->removeAssign();
            static::onEntryChanged($entry, 'remove');
            $result['status'] = $modelClass::destroy($id);
            static::onEntriesChanged('remove');
        }
        return Response::json($result);
    }
    
    public static function getEntry($id)
    {
        $modelClass = static::$modelName;
        $entry = $modelClass::find($id);
        if (Request::ajax()) {
            $entry = $entry->prepareAjaxEntryTranslate();
            return Response::json(array('status' => $entry? 1 : 0,'entry' => $entry));
        }
        
        return $entry;
    }
    
    public static function translit($string)
    {
        $t = \Transliterator::create('latin; NFKD; [^\u0000-\u007E] Remove; NFC');
        $t = $transliterator->transliterate($string);
        $t = preg_replace('/[^a-zA-Z]/', '_', $t);
        $t = trim($t, '_');
        return $t;
    }
    
    public static function postAssignImage($entryId = 0)
    {
        $result = array('url' => '', 'status' => 0, 'msg' => '');
        $file = Input::file('file');
        if($file->isValid()) {
            $modelName = static::$modelName;
            $assign = $modelName::assignImage($file, $modelName, $entryId);
            if($assign) {
                $result['url'] = $modelName::getImageFolder('content') . $assign->name;
                $result['id'] = $assign->id;
                $result['status'] = 1;
            } else {
                $result['msg'] = 'This module doesn\'t accept assignments';
            }
        } else {
            $result['msg'] = 'Uploaded file is not valid';
        }
        
        return Response::json($result);
    }
    
    public static function postUploadGalleryItem($idEntry)
    {
        $modelClass = static::$modelName;
        $result = array('url' => '', 'status' => 0, 'msg' => '');
        $data = Input::all();
        $file = $data['file'];
        $entry = $modelClass::find($idEntry);
        $filename = false;
        if(!$entry) {
            $result['status'] = -1;
            $result['msg'] = 'Entry is not found';
        } elseif($file) {
            $filename = $modelClass::uploadImage($file, $modelClass::$gallerySizes);
        }
        
        if ($filename) {
            $size = $file->getSize();
            if($size > 0)
                $size /= 1024;
            $photo = new Photo(array(
                'name' => $filename,
                'size' => $size
            ));
            $entry->photos()->save($photo);
            $result['thumb'] = URL::to('uploads/products/gallery_thumb/' . $filename);
            $result['id'] = $photo->id;
            $result['status'] = 1;
        } else {
            $result['status'] = -2;
            $result['msg'] = 'File is not valid';
        }
        
        return Response::json($result);
    }
    
    public static function postRemoveGalleryItem($idItem)
    {
        $photo = Photo::find($idItem);
        $photo->remove();
        $photo->delete();
    }
    
    /**
     * Function Callback. Calls when list of the entries in module has been changed
     * Usually it is helpful for cache code
     */
    protected static function onEntriesChanged($event)
    {
        $modelName = static::$modelName;
        $modelName::clearGeneralCache($event);
    }
    
    /**
     * Function Callback. Calls when one of the entries in module has been changed
     * Usually it is helpful for cache code
     * @param type $entry
     */
    protected static function onEntryChanged($entry, $event)
    {
        $entry->clearEntryCache($event);
    }
    
}
