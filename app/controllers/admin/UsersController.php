<?php

class UsersController extends BaseAdminController {

    protected static $moduleName = 'users';
    protected static $modelName = 'User';
    protected static $formFields = array(
        'name' => array(
            'label' => 'Name',
            'type'  => 'text',
            'help' => '',
        ),
        'status' => array(
            'label' => 'Active',
            'type'  => 'checkbox',
            'help'  => 'Do you want to make this UsersController active?',
            'value' => 1,
            'default' => false
        )
    );
	
}