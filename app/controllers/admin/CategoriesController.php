<?php

class CategoriesController extends BaseAdminController {

    protected static $moduleName = 'categories';
    protected static $modelName = 'Category';
    protected static $formFields = array(
        'name' => array(
            'label' => 'Name',
            'type'  => 'text',
        ),
        'slug' => array(
            'label' => 'Slug',
            'type'  => 'text',
            'help'  => "Leave a blank for autogenerating",
        ),
        'url' => array(
            'label' => 'URL',
            'type'  => 'text',
            'help'  => 'URL to another page (it overrides default link)',
        ),
        'page_id' => array(
            'label' => 'Page',
            'type'  => 'select',
            'values' => array(),
            'help'  => "You can connect static page to menu element",
            'class' => 'page-select'
        ),
        'annotation' => array(
            'label' => 'Annotation',
            'type'  => 'textarea',
            'help'  => "Short content preview",
            'pagehide' => true,
        ),
        'content' => array(
            'label' => 'Content',
            'type'  => 'html',
            'pagehide' => true,
        ),
        'title' => array(
            'label' => 'META Title',
            'type'  => 'text',
            'help'  => 'Title of the page (it is visible in a browser tab)',
            'pagehide' => true,
        ),
        'keywords' => array(
            'label' => 'META Keywords',
            'type'  => 'textarea',
            'help'  => 'META data for SEO optimization',
            'pagehide' => true,
        ),
        'description' => array(
            'label' => 'META Description',
            'type'  => 'textarea',
            'help'  => 'META data for SEO optimization',
            'pagehide' => true,
        ),
        'status' => array(
            'label' => 'Active',
            'type'  => 'checkbox',
            'help'  => 'Do you want to make this category active?',
            'value' => 1,
            'default' => false
        )
    );
    
    
    public static function getFormFields()
    {
        self::$formFields['page_id']['values'] = array(0 => "Page is not connected"); 
        $pages = Page::active()->orderBy('name', 'ASC')->get(array('id', 'name'));
        foreach ($pages as $page)
            self::$formFields['page_id']['values'][$page->id] = $page->name;
        
        return self::$formFields;
    }
    
    public static function postSort()
    {
        $result = array('status' => 0);
        $operations = Input::get('operations', array());
        $result['operations'] = $operations;
        $menuId = Input::get('menu_id', 0);
        if(!empty($operations) && $menuId > 0) {
            DB::transaction(function() use($operations, $menuId){
                foreach ($operations as $key => $value) {
                    $parent_id = 0;
                    $category = Category::find($key);
                    if(isset($value['parent_id'])) {
                        $category->parent_id = $value['parent_id'];
                        $parent_id = $value['parent_id'];
                    }
                    if(isset($value['sort'])) {
                        $category->sort = $value['sort'];
                        if(isset($value['move'])) {
                            DB::table('categories')
                                    ->where('menu_id', '=', $menuId)
                                    ->where('parent_id', '=', $parent_id)
                                    ->where('sort', '>=', $value['sort'])
                                    ->orderBy('sort', 'DESC')
                                    ->increment('sort');
                        }
                    }
                    $category->save();
                }
            });
            $result['status'] = 1;
        }
        
        return Response::json($result);    
    }
    
    public static function onBeforeEntrySave($entry, $type = 'create')
    {
        if(!isset($entry->page_id) || $entry->page_id <= 0)
                $entry->page_id = null;
        return $entry;
    }
    
    public static function onAfterEntrySave($result, $entry, $type = 'create')
    {
        parent::onAfterEntrySave($result, $entry, $type);
        $result['item'] = View::make('admin.menus.item', array('categories' => array($entry)))->render();
        return $result;
    }
    
    protected static function onEntryChanged($entry, $event)
    {
        parent::onEntryChanged($entry, $event);
        $entry->menu->clearMenuCache();
    }
    
}
