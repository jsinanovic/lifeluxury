<?php

class PricesController extends BaseAdminController {

    protected static $moduleName = 'prices';
    protected static $modelName = 'Price';
    protected static $formFields = array(
        'name' => array(
            'label' => 'Name',
            'type'  => 'text',
            'help'  => 'Price name',
        ),
        'category_id' => array(
            'label' => 'Category',
            'type'  => 'select',
            'values' => array(),
        ),
        'price' => array(
            'label' => 'Price',
            'type'  => 'money',
            'value' => 0,
        ),
        'percent' => array(
            'label' => 'Percent',
            'type'  => 'percent',
            'help' => 'You can set up percent to this section instead of price. Price should be empty or zero',
        ),
        'minimal_time' => array(
            'label' => 'Minimal time in days',
            'type'  => 'number',
            'value' => 1,
        ),
        'maximal_time' => array(
            'label' => 'Maximal time in days',
            'type'  => 'number',
            'value' => 1,
        ),
        /*'annotation' => array(
            'label' => 'Annotation',
            'type'  => 'textarea',
            'help'  => 'Short description',
        ),
        'content' => array(
            'label' => 'Content',
            'type'  => 'html',
        ),*/
    );
    
    
    protected static function getFormFields($idCategory = false)
    {
        $fields = self::$formFields;
        $fields['category_id']['values'] = array('' => 'Select category') 
                + Menu::ofKey('prices')->first()
                ->categories()->active()->orderBy('name', 'ASC')->lists('name', 'id');
        if($idCategory > 0) {
            $fields['category_id']['value'] = $idCategory;
        }
        return $fields;
    }
    
    public static function onBeforeEntrySave($entry, $type = 'create')
    {
        $entry = parent::onBeforeEntrySave($entry, $type);
        //dd($entry->price, number_format($entry->price, 2));
        //$entry->price = number_format($entry->price, 2);
        return $entry;
    }
    
    public static function init($data = array(), $pageType = 'index')
    {
        $data = parent::init($data, $pageType);
        $data['categories'] = Menu::mainCategories('prices')->with(array('categories', 'prices'))->get();
        $data['menuLink'] = URL::to('admin/menus/view/' . Menu::ofKey('prices')->pluck('id'));
        return $data;
    }
    
    public static function getCreate($idCategory = false)
    {
        $modelClass = static::$modelName;
        $obj = new $modelClass();
        $data = array(
            'formFields' => static::getFormFields($idCategory),
            'formAction' => static::getModuleUri() . 'create',
            'formRules' => $modelClass::getJsonValidationRules(),
            'title' => 'Add a new <strong>' . static::$modelName . '</strong>',
            'entry' => false,
            'translatedAttributes' => $obj->translatedAttributes,
        );
        
        $data = static::init($data, 'create');
        $view = static::$autoForm? 'admin.layouts.autoform' : static::getViewPath() . 'form';
        return View::make($view, $data);
    }
    
}
