<?php

class AdminController extends BaseAdminController {
    
    protected static $moduleName = 'admin';

    public static function getIndex()
    {
        $data = static::init();
        return View::make('admin.dashboard', $data);
    }

    public static function getLogin()
    {
        return View::make('admin.login');
    }

    public static function getCommingSoon()
    {
        Artisan::call('down');
        return Redirect::to('admin');
    }

    public static function getCommingSoonEnable()
    {

 Artisan::call('up');
        return Redirect::to('admin');
    }

    public static function postLogin()
    {
        if (Auth::attempt(array('email' => Input::get('email'), 'password' => Input::get('password')))) {
            return Redirect::intended('admin');
        } else {
            return Redirect::to('admin/login')
                            ->with('message', 'Your username/password combination was incorrect')
                            ->withInput();
        }
    }

    public static function getLogout()
    {
        Auth::logout();
        return Redirect::to('admin/login');
    }

}
