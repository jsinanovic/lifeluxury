<?php

class ArticlesController extends BaseAdminController {

    private static $menuKey = 'magazine';
    protected static $moduleName = 'articles';
    protected static $modelName = 'Article';
    protected static $formFields = array(
        'name' => array(
            'label' => 'Name',
            'type'  => 'text',
            'help'  => 'Article name',
        ),
        'slug' => array(
            'label' => 'Slug',
            'type'  => 'text',
            'help'  => "Page's URI for better SEO optimization. Left it empty for autogeneration",
        ),
        'created_at' => array(
            'label' => 'Created At',
            'type'  => 'date',
            'help'  => "Article's issue depends on the article's date. Date format: yyyy-mm-dd",
        ),
        'image' => array(
            'label' => 'Cover Image',
            'type'  => 'file',
            'help'  => 'Max size is 10MB. Allowed .jpg and .png extensions',
        ),
        'articles_bg' => array(
            'label' => 'Authors Background',
            'type'  => 'file',
            'help'  => 'Max size is 10MB. Allowed .jpg and .png extensions',
        ),
        'contents_bg' => array(
            'label' => 'Contents Background',
            'type'  => 'file',
            'help'  => 'Max size is 10MB. Allowed .jpg and .png extensions',
        ),
        'categories' => array(
            'label' => 'Categories',
            'type'  => 'select',
            'values' => array(),
        ),
        'subcategories' => array(
            'label' => 'Subcategories',
            'type'  => 'select',
            'values' => array(),
        ),
        'annotation' => array(
            'label' => 'Annotation',
            'type'  => 'textarea',
            'help'  => "Short content preview",
        ),
        'template' => array(
            'label' => 'Template',
            'type'  => 'select',
            'class' => 'template-select',
            'values' => array(
                1 => 'Singapure Grand Prix', 
                2 => 'The Golden Sails', 
                3 => 'Fashion on the French Riviera',
                4 => 'The Greek Crisis',
                5 => 'Another Day Another Way',
                6 => 'Sustainable Luxury',
                7 => 'The Golden Age of Life',
                8 => 'Voyage',
                9 => 'Mediterraneans New Energy Hub',
                10 => 'Frankfurt Autumn Issue',
                11 => 'New horizont',
                12 => 'Fashion week',
                13 => 'Winter fashion',
                14 => 'Dubai air show',
                15 => 'Towards the party of lifetime',
                16 => 'Beyound the expected',
                17 => 'Rule the world',
                18 => 'A winter tale',
            ),
        ),
        /*'content' => array(
            'label' => 'Content',
            //'type'  => 'features',
            'type'  => 'advancedHtml',
            'class' => 'template-control',
        ),*/
        
        'content' => array(
            'label' => 'Content',
            'type'  => 'custom',
            'template' => 'contentfield',
        ),
        
        'authors' => array(
            'label' => 'Authors',
            'type'  => 'features',
        ),
        /*'tags' => array(
            'label' => 'Tags',
            'type'  => 'tags',
            'help'  => 'Enter tags separeted by comma',
        ),*/
        /*'similar' => array(
            'label' => 'Similar articles',
            'type'  => 'multiselect',
            'values' => array(),
        ),*/
        'title' => array(
            'label' => 'META Title',
            'type'  => 'text',
            'help'  => 'Title of the page (it is visible in a browser tab)',
        ),
        'keywords' => array(
            'label' => 'META Keywords',
            'type'  => 'textarea',
            'help'  => 'META data for SEO optimization',
        ),
        'description' => array(
            'label' => 'META Description',
            'type'  => 'textarea',
            'help'  => 'META data for SEO optimization',
        ),
        'is_cover' => array(
            'label' => 'Issue cover',
            'type'  => 'checkbox',
            'help'  => 'Do you want to show this article on the issue\'s top?',
            'value' => 1,
            'default' => 0
        ),
        'status' => array(
            'label' => 'Active',
            'type'  => 'checkbox',
            'help'  => 'Do you want to make this article active?',
            'value' => 1,
            'default' => false
        ),
        
    );
    
    public static function getEditContent($slug, $preview = false)
    {
        $entry = Article::slug($slug)->first();
        if($preview == 'preview') {
            return MagazineController::getArticle($slug);
        } else {
            $templates = $entry->getTemplates();
            //echo($entry->content);exit;
            return MagazineController::getArticle($slug)->with(array(
                'admin'=> true,
                'templates' => $templates
            ));
        }
    }
    
    public static function postUpload($articleId)
    {
        $url = '';
        $error = '';
        $entry = Article::find($articleId);
        
        if($entry) {
            $data = Input::all();
            $assign = Article::assignImage($data['upload'], 'Article', $entry->id);

            if($assign) {
                $url = URL::to('uploads/articles/content/' . $assign->name);
            } else {
                $error = 'Upload failed!';
            }
            
        } else {
            $error = 'Entry not found!';
        }
        
        $funcNum = Input::get('CKEditorFuncNum', 0);
        
        return '<script type="text/javascript">
            window.parent.CKEDITOR.tools.callFunction("' . $funcNum . '", "' . $url . '", "'. $error .'");
        </script>';
    }
    
    public static function postContent($articleId)
    {
        $entry = Article::find($articleId);
        if($entry) {
            $content = Input::get('content', '');
            $entry->content = $content;
            $entry->save();
            return 1;
        } 
        
        return 0;
    }
    
    
    public static function getFormFields()
    {
        $menu = Menu::getMenu(self::$menuKey);
        $categories = $menu->categories()->active()
                ->orderBy('name', 'ASC')->get(array('name', 'id'));
        foreach ($categories as $category) {
            self::$formFields['categories']['values'][$category->id] = $category->name;
        }

        $subcategories = DB::table('subcategories')->where('status', 1)->orderBy('name', 'ASC')->get(array('name','id'));
        foreach ($subcategories as $subcategory) {
            self::$formFields['subcategories']['values'][$subcategory->id] = $subcategory->name;
        }

        //self::$formFields['categories']['values'] = $menu->categories()->active()
        //        ->orderBy('name', 'ASC')->lists('name', 'id');
        /*self::$formFields['similar']['values'] = Article::active()
                ->orderBy('name', 'ASC')->lists('name', 'id');*/
        return self::$formFields;
    }
    
    
    public static function init($data = array(), $pageType = 'index')
    {
        $data = parent::init($data, $pageType);
        if($pageType == 'edit') {
            $tags = array();
            foreach ($data['entry']->tags as $tag) {
                $tags[] = $tag->name;
            }
            $data['entry']->tags = implode(',', $tags);
            $data['entry']->categories = $data['entry']->categories->lists('id');
//            dd($data['entry']['attributes']['id']);
            $articleId = $data['entry']['attributes']['id'];
//            dd($articleId);
            $data['entry']->subcategories = DB::table('article_subcategory')->where('article_id', $articleId)->lists('subcategory_id');

            //$data['entry']->similar = $data['entry']->similar->lists('id');
        }
        return $data;
    }
    
    public static function onAfterEntrySave($result, $entry, $type = 'create')
    {
        if($entry->is_cover) {
            // set is_cover to 0 for all other articles in this issue
            Article::issue($entry->year(), $entry->season())
                    ->where('id', '!=', $entry->id)->update(array('is_cover' => 0));
        }
        return parent::onAfterEntrySave($result, $entry, $type);
    }
}
