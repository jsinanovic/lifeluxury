<?php

class SettingsController extends BaseAdminController {

    protected static $moduleName = 'settings';
    protected static $modelName = 'Setting';
    protected static $autoForm = true;
    protected static $formFields = array(
        'name' => array(
            'label' => 'Name',
            'type'  => 'text',
            'help'  => 'Name of setting',
        ),
        'slug' => array(
            'label' => 'Unique key',
            'type'  => 'text',
            'help'  => "Left it empty for autogeneration by name",
        ),
        'description' => array(
            'label' => 'Setting Description',
            'type'  => 'textarea',
        ),
        'module' => array(
            'label' => 'Attach option to Module',
            'type'  => 'select',
            'values' => array(
                'articles' => 'Articles',
                'menus' => 'Menus',
                'pages' => 'Pages',
                'settings' => 'Settings'
            ),
        ),
        
        'type' => array(
            'label' => 'Setting type',
            'type'  => 'select',
            'values' => array(
                'text' => 'text',
                'textarea' => 'textarea',
                'select' => 'select',
            )
        ),
        
        'content' => array(
            'label' => 'Value',
            'type'  => 'textarea',
        ),
        
        'values' => array(
            'label' => 'Possible values',
            'type' => 'textarea',
            'help' => 'If you chose "select" setting type, you should enter possible values. Each value should begin from the new line'
        )
        
    );
    
    
}
