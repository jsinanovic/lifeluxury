<?php

class PagesController extends BaseAdminController {

    protected static $moduleName = 'pages';
    protected static $modelName = 'Page';
    protected static $formFields = array(
        'name' => array(
            'label' => 'Name',
            'type'  => 'text',
            'help'  => 'Name of the page',
        ),
        'slug' => array(
            'label' => 'Slug',
            'type'  => 'text',
            'help'  => "Page's URI for better SEO optimization. Left it empty for autogeneration",
        ),
        'key' => array(
            'label' => 'System key',
            'type'  => 'select',
            'help'  => 'System field for pages binding. Page that binded with system key can have totally different behavior',
            'values' => array(),
            'value' => 'default'
        ),
        'subtitle' => array(
            'label' => 'Subtitle',
            'type'  => 'textarea',
            'help'  => 'Some pages use subtitle',
        ),
        'content' => array(
            'label' => 'Content',
            'type'  => 'html',
        ),
        'title' => array(
            'label' => 'META Title',
            'type'  => 'text',
            'help'  => 'Title of the page (it is visible in a browser tab)',
        ),
        'keywords' => array(
            'label' => 'META Keywords',
            'type'  => 'textarea',
            'help'  => 'META data for SEO optimization',
        ),
        'description' => array(
            'label' => 'META Description',
            'type'  => 'textarea',
            'help'  => 'META data for SEO optimization',
        ),
        'status' => array(
            'label' => 'Active',
            'type'  => 'checkbox',
            'help'  => 'Do you want to make this page active?',
            'value' => 1,
            'default' => false
        )
    );
    
    
    protected static function getFormFields()
    {
        $fields = self::$formFields;
        $fields['key']['values'] = Page::getPagesKeys();
        return $fields;
    }
    
    public static function init($data = array(), $pageType = 'index') {
        $data = parent::init($data, $pageType);
        if($pageType == 'edit') {
            $data['formFields']['key']['value'] = $data['entry']->key;
        }
        return $data;
    }
}
