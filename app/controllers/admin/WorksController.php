<?php

class ServicesController extends BaseAdminController {

    protected static $moduleName = 'works';
    protected static $modelName = 'Work';
    protected static $formFields = array(
        'name' => array(
            'label' => 'Name',
            'type'  => 'text',
            'help'  => 'Work name',
        ),
        'slug' => array(
            'label' => 'Slug',
            'type'  => 'text',
            'help'  => "Page's URI for better SEO optimization. Left it empty for autogeneration",
        ),
        'image' => array(
            'label' => 'Image',
            'type'  => 'file',
            'help'  => 'Max size is 5MB. Allowed .jpg and .png extensions',
        ),
        'annotation' => array(
            'label' => 'Annotation',
            'type'  => 'textarea',
            'help'  => "Short content preview",
        ),
        'content' => array(
            'label' => 'Content',
            'type'  => 'html',
        ),
        'features' => array(
            'label' => 'Features',
            'type' => 'features',
        ),
        'title' => array(
            'label' => 'META Title',
            'type'  => 'text',
            'help'  => 'Title of the page (it is visible in a browser tab)',
        ),
        'keywords' => array(
            'label' => 'META Keywords',
            'type'  => 'textarea',
            'help'  => 'META data for SEO optimization',
        ),
        'description' => array(
            'label' => 'META Description',
            'type'  => 'textarea',
            'help'  => 'META data for SEO optimization',
        ),
        'status' => array(
            'label' => 'Active',
            'type'  => 'checkbox',
            'help'  => 'Do you want to make this page active?',
            'value' => 1,
            'default' => false
        )
    );
    
    
    
}
