<?php

class ModulesController extends BaseAdminController {

    protected static $moduleName = 'modules';
    protected static $modelName = 'Module';
    
    protected static $formFields = array(
        'name' => array(
            'label' => 'Name',
            'type'  => 'text',
            'help'  => 'Module name',
        ),
        'model' => array(
            'label' => 'Model',
            'type'  => 'text',
            'help'  => 'Model name (singular form of module name)',
        ),
        'uri' => array(
            'label' => 'Uri',
            'type'  => 'text',
            'help'  => 'Module URI',
        ),
        'access_level' => array(
            'label' => 'Acces Level',
            'type'  => 'select',
            'values' => array(1 => 'Admin', 2 => 'SuperAdmin'),
            'help'  => 'Who can work with this module?',
        ),
        'migration' => array(
            'label' => 'Migration String for generator',
            'type'  => 'textarea',
            'help'  => 'Something like this: "name:string(255):unique, content:text:default(\'Test\'):nullable"',
            'default' => 'name:string, slug:string:unique, annotation:string(500), content:text, status:tinyInteger:default(\'0\')'
        ),
        'status' => array(
            'label' => 'Active',
            'type'  => 'checkbox',
            'help'  => 'Do you want to make this module active?',
            'value' => 1,
            'default' => false
        ),
        
        'generate[migration]' => array(
            'label' => 'Migration',
            'help' => 'Do you want to generate migration?',
            'type'  => 'checkbox',
            'value' => 1,
            'default' => false
        ),
        
        'generate[migrate]' => array(
            'label' => 'Apply migration',
            'help' => 'Do you want to apply the new migrations?',
            'type'  => 'checkbox',
            'value' => 1,
            'default' => false
        ),
        
        'generate[model]' => array(
            'label' => 'Model',
            'help' => 'Do you want to generate model?',
            'type'  => 'checkbox',
            'value' => 1,
            'default' => false
        ),
        
        'generate[controller]' => array(
            'label' => 'Controller',
            'help' => 'Do you want to generate controller?',
            'type'  => 'checkbox',
            'value' => 1,
            'default' => false
        ),
        
        'generate[view]' => array(
            'label' => 'View',
            'help' => 'Do you want to generate admin.index view?',
            'type'  => 'checkbox',
            'value' => 1,
            'default' => false
        ),
        
        
    );
    
    public static function onAfterEntrySave($result, $entry, $type = 'create') {
        $result = parent::onAfterEntrySave($result, $entry, $type);
        $generators = Input::get('generate', array());
        
        $commands = array();
        if(isset($generators['migration']) && $generators['migration']) {
            $command = array(
                'command' => 'generate:migration',
                'options' => array(
                    'migrationName' => 'create_' . Str::lower($entry->name) . '_table'
                ),
            );
            
            if($result['modifiedData']['migration']) {
                $command['options']['--fields'] = $result['modifiedData']['migration'];
            }
            $commands[] = $command;
        }
        
        if(isset($generators['migrate']) && $generators['migrate']) {
            $commands[] = array(
                'command' => 'migrate',
                'options' => array()
            );
        }
        
        if(isset($generators['model']) && $generators['model']) {
            $commands[] = array(
                'command' => 'generate:model',
                'options' => array(
                    'modelName' => $entry->model
                ),
            );
        }
        
        if(isset($generators['controller']) && $generators['controller']) {
            $commands[] = array(
                'command' => 'generate:controller',
                'options' => array(
                    'controllerName' => $entry->name . 'Controller'
                ),
            );
            $commands[] = array('command' => 'dump-autoload', 'options' => array());
        }
        
        if(isset($generators['view']) && $generators['view']) {
            $commands[] = array(
                'command' => 'generate:view',
                'options' => array(
                    'viewName' => 'admin.' . $entry->uri . '.index'
                ),
            );
        }
        
        try {
            foreach($commands as $command) {
                Artisan::call($command['command'], $command['options']);
            }
        } catch(Exception $e) {
            dd($e->getMessage(), $command);
        }
        
        return $result;
    }

}