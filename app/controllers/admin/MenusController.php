<?php

class MenusController extends BaseAdminController {

    protected static $moduleName = 'menus';
    protected static $modelName = 'Menu';
    protected static $formFields = array(
        'name' => array(
            'label' => 'Name',
            'type'  => 'text',
            'help'  => 'Menu Name',
        ),
        'key' => array(
            'label' => 'Key',
            'type'  => 'text',
            'help'  => "The menu key will be used in code to determine the menu",
        ),
        'depth' => array(
            'label' => 'Depth',
            'type'  => 'select',
            'values' => array(1 => 1, 2 => 2, 3 => 3, 4 => 4, 5 => 5, 99 => 99 ),
            'help'  => "The menu depth defines max level of depth in the menu",
        ),
    );
    
    public static function getView($entry)
    {
        $obj = new Category();
        $data = array(
            'entry' => $entry,
            'title' => '<strong>' . $entry->name . ' ' . static::$modelName . '</strong> View',
            'categories' => $entry->categories()->where('parent_id', '=', 0)->orderBy('sort', 'ASC')->get(),
            'pages' => array(0 => "Page is not connected") + Page::active()->orderBy('name', 'ASC')->lists('name', 'id'),
            'formRules' => Category::getJsonValidationRules(),
            'categoryFields' => App::make('CategoriesController')->getFormFields(),
            'translatedAttributes' => $obj->translatedAttributes,
        );
        $data = static::init($data, 'view');
        return View::make(static::getViewPath() . 'view', $data);
    }
    
    public static function getMenuView($entry)
    {
        $result = array('status' => 0, 'menu' => '');
        if($entry) {
            $result['status'] = 1;
            $categories = $entry->categories()
                    ->where('parent_id', '=', 0)
                    ->orderBy('sort', 'ASC')
                    ->get();
            $result['menu'] = View::make('admin.menus.item', array(
                'categories' => $categories,
                'isAddButtons' => true,
            ))->render();
        }
        
        return Response::json($result);
    }
    
    public static function postEdit($entry)
    {
        $modelClass = static::$modelName;
        $data = Input::all();

        $modelClass::$rules['key'] = $modelClass::$rules['key'] . ',' . $entry->id;

        $result = $modelClass::validate($data);
        if ($result['valid']) {
            $entry->fill($result['modifiedData'])->save();
            //$entry->clearCache();
            static::onEntryChanged($entry, 'edit');
            static::onEntriesChanged('edit');
            if (isset($data['continue']))
                return Redirect::to(static::getModuleUri() . 'edit/' . $entry->id)->withSuccess(true);
            else
                return Redirect::to(static::getModuleUri() . 'view/' . $entry->id)->withSuccess(true);
        } else {
            return Redirect::to(static::getModuleUri() . 'create')->withErrors($result['validator'])->withInput($data);
        }
    }
    
    public static function postCreate()
    {
        $modelClass = static::$modelName;
        $data = Input::all();
        $result = $modelClass::validate($data);
        $result['status'] = 0;
        if ($result['valid']) {
            $entry = new $modelClass($result['modifiedData']);
            $result['status'] = $entry->save();
            $result['id'] = $entry->id;
            
            static::onEntriesChanged('create');
            
            if (isset($data['continue']))
                return Redirect::to(static::getModuleUri() . 'edit/' . $entry->id)->withSuccess(true);
            else
                return Redirect::to(static::getModuleUri() . 'view/' . $entry->id)->withSuccess(true);
        } else {
            
            return Redirect::to(static::getModuleUri() . 'create')->withErrors($result['validator'])->withInput($data);
        }
    }
    
    protected static function onEntryChanged($entry, $event)
    {
        parent::onEntryChanged($entry, $event);
        $entry->clearMenuCache();
    }
    
}
