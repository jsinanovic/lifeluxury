<?php

class MagazineController extends BaseFrontController {
    
    protected static $modelName = 'Article';
    protected static $moduleName = 'magazine';
    
    public static function getArticle($slug)
    {
        $article = Article::slug($slug)->active()->first();
        if(Auth::check() && !$article) {
            $article = Article::slug($slug)->first();
        }
        if (!$article)
            return App::abort('404');
        
        $article->authors = json_decode($article->authors, TRUE);
        $article->category = $article->categories()->first();

        /*$content = json_decode($article->content, TRUE);
        $result = array();
        if(is_array($content))
            foreach ($content as $item) {
                $result[$item['name']] = $item['content'];
            }
        $article->content = $result;*/

        $data = self::init([], 'view');
        $data['article'] = $article;

        // NEXT AND PREV ARTICLES
        $data['nextArticle'] = null;
        $data['prevArticle'] = null;
        $articles = Article::issue($article->year(), $article->season())
                ->active()->orderBy('is_cover', 'DESC')
                ->orderBy('created_at', 'DESC')->get();

        $data['relatedArticles'] = $articles->take(3);

        foreach($articles as $key => $value) {
            if($value->id == $article->id) {
                $data['nextArticle'] = isset($articles[$key + 1])? $articles[$key + 1] : null;
                $data['prevArticle'] = isset($articles[$key - 1])? $articles[$key - 1] : null;
                break;
            }
        }
        
        self::setMetaData($data, $article);
        return View::make('front.magazine.view', $data);
        //return View::make('front.magazine.templates.t' . $article->template, $data);
    }

    public static function getIndex()
    {
        $data = static::init([], 'magazine');
        $season = Article::getCurrentSeason();
        $year = date('Y');
        
        for($k = 0; $k < 10; ++$k) {
            $articles = Article::issue($year, $season)->active()->orderBy('is_cover', 'DESC')
                ->orderBy('created_at', 'DESC')->get();
            if(count($articles) > 0) {
                break;
            } 
            
            $season = Article::getPrevSeason($season);
            if($season == 'autumn') {
                --$year;
            }
        }
        
        $data['currentYear'] = $year;
        $data['currentSeason'] = $season;
        $data['articles'] = $articles;
        $data['issues'] = Article::getIssues();
        return View::make('front.magazine.index', $data);
    }
    
    public static function getIssue($year, $season) 
    {
        $data = static::init([], 'magazine');
        $articles = Article::issue($year, $season)->active()->orderBy('is_cover', 'DESC')
                ->orderBy('created_at', 'DESC')->get();
        $data['currentYear'] = $year;
        $data['currentSeason'] = $season;
        $data['articles'] = $articles;
        $data['issues'] = Article::getIssues();
        return View::make('front.magazine.index', $data);
    }

    public static function getCategory($slug)
    {
        $data = static::init([], 'magazine');
        $articles = array();
        foreach ($data['categories'] as $key => $category) {
            if($category->slug == $slug) {
                $data['category'] = $category;
                $articles = $category->articles()->active()->orderBy('created_at', 'DESC')->get();
                $data['nextCategory'] = isset($data['categories'][$key + 1])? $data['categories'][$key + 1] : $data['categories'][0];
            }
        }

        $data['articles'] = $articles;
        $data['issues'] = Article::getIssues();
        return View::make('front.magazine.index', $data);
    }
    
    public static function getUnsubscribe($token) 
    {
        $token = urldecode($token);
        $s = Subscriber::where('token', $token)->first();
        if($s) {
            $s->status = 0;
            $s->token = sha1($s->id . time() . $s->email . rand(0, 10000));
            $s->save();
            Subscriber::clearGeneralCache('update');
        } else {
            return App::abort('404');
        }
        $data = static::init([], 'main');
        $data['subscriber'] = $s;
        return View::make('front.pages.unsubscribe', $data);
    }
}
