<?php

class PortfolioController extends BaseFrontController {

    private static $perPage = 12;
    protected static $modelName = 'Work';
    protected static $moduleName = 'works';
    
    public static function getIndex()
    {
        $data = array(
            'works' => Work::active()
                ->orderBy('created_at', 'DESC')
                ->take(self::$perPage)
                ->get(),
            'perPage' => self::$perPage
        );
        $data = self::init($data, 'works');
        return View::make('front.works.index', $data);
    }
    
    public static function getMore($start)
    {
        $result = array('status' => 0);
        $works = Work::active()
                ->orderBy('id', 'DESC')
                ->skip($start)
                ->take(self::$perPage)
                ->get();
        
        if(!$works || count($works) <=0 )
            $result['status'] = -1;
        else {
            $result['status'] = 1;
            $result['html'] = View::make('front.works.list', array('works' => $works))->render();
            if(count($works) < self::$perPage)
                $result['status'] = 2;
        }
        
        return Response::json($result);
    }
    
    
    public static function getView($slug)
    {
        $work = Work::slug($slug)->active()->first();
        if(!$work) {
            return App::abort('404');
        }
        $work->increment('count_views');
        $work->features = json_decode($work->features, TRUE);
        $data = array(
            'work' => $work,
            'prev' => Work::where('id', '<', $work->id)->orderBy('id', 'DESC')->first(),
            'next' => Work::where('id', '>', $work->id)->orderBy('id', 'ASC')->first(),
        );
        $data = self::init($data, 'works');
        $data = self::setMetaData($data, $work);
        return View::make('front.works.view', $data);
    }

}
