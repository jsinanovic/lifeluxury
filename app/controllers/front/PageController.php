<?php

class PageController extends BaseFrontController {


    /**
     * Home Page View
     */
    public static function getIndex()
    {
        $data = static::init(array(), 'home');
        //$data['creativeMenu'] = Menu::getMenu('creative', 1);
        $data['services'] = Service::active()->take(5)->get(array('id', 'name', 'slug'));
        $data['article'] = Article::cover()->active()->orderBy('created_at', 'DESC')->first();

        if(Config::get('app.debug'))
            return View::make('front.commingsoon.index', $data);
        else
            return View::make('front.home.index', $data);
        //return View::make('front.commingsoon.index', $data);
    }
    /**
     * Welcome View
     */
    public function getWelcome()
    {
        $data = static::init(array(), 'welcome');
        return View::make('front.welcome.index', $data);
    }

    public function getTermsAndConditions()
    {
        $data = static::init(array(), 'terms-and-conditions');
        return View::make('front.pages.terms&conditions', $data);
    }

    public function getPrivacyPolicy()
    {
        $data = static::init(array(), 'privacy-policy');
        return View::make('front.pages.privacypolicy', $data);
    }

    public function getServicePromise()
    {
        $data = static::init(array(), 'service-promise');
        return View::make('front.pages.servicepromise', $data);
    }
    public function getGetAQuote()
    {
        $data = static::init(array(), 'get-a-quote');
        return View::make('front.pages.getaquote', $data);
    }

    public function getBrandIdentity()
    {
        return View::make('front.pages.brandidentity');
    }

    public function getPressKit()
    {
        return View::make('front.pages.presskit');
    }

    public function getMediaKit()
    {
        return View::make('front.pages.mediaKit');
    }

    public function getArticleSinglePage()
    {
        return View::make('front.pages.article-single-page');
    }

    public function getEventsSinglePage()
    {
        return View::make('front.pages.events-single-page');
    }


    /**
     * Home Page View
     */
    public static function getCommingSoon()
    {
      return View::make('front.commingsoon.index', $data);
    }

    public static function getNewsLetter()
    {
        $data = static::init(array(), 'page');
        return View::make('front.newsletter.index', $data);
    }

    public static function getPage($slug)
    {
        $page = Page::active()->slug($slug)->first();
        if(!$page) {
            return App::abort (404);
        }
        
        $data = static::init(array(), 'page');
        $data['page'] = $page;
        self::setMetaData($data, $page);
        return View::make('front.pages.static', $data);
    }
    
    
    public static function getContacts()
    {
        $data = static::init(array(), 'contacts');
        $data['entry'] = array();
        $data['subjects'] = array(
            trans('pages.general_questions') => trans('pages.general_questions'),
            trans('pages.site_development') => trans('pages.site_development'),
            trans('pages.support') => trans('pages.support'),
            trans('pages.maintenance') => trans('pages.maintenance'),
        );
        $data['formRules'] = ClientRequest::getJsonValidationRules();
//        return View::make('front.pages.contacts', $data);
        return View::make('front.pages.contact', $data);
    }
    
    
    public static function getAbout()
    {
        $data = array(
            'page' => Page::getActivePageByKey('about')//Page::where('key', '=', 'about')->first(),
        );
        $data = static::init($data, 'about');
        return View::make('front.pages.about', $data);
    }
    
    
    public static function postContacts()
    {
        $result = array();
        
        return Response::json($result);
    }

}
