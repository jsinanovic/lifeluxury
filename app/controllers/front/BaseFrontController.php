<?php

class BaseFrontController extends BaseController {
    
    protected static $baseName = 'front';
    protected static $modelName = '';
    protected static $moduleName = '';
    
    public static function getModuleUri()
    {
        return static::$moduleName . '/';
    }
    
    public static function init($data = array(), $pageType = 'index') {
        $data = static::setMetaData($data, static::getMetaEntry($pageType));
        $data['pageType'] = $pageType;
        $data['baseName'] = static::$baseName;
        $data['moduleName'] = static::$moduleName;
        $data['moduleUri'] = static::getModuleUri();
        $data['navMenu'] = Menu::getMenu('nav', 1);
        $data['categories'] = Menu::getMenu('magazine', 1)->categoriesList();
        $data['footerMenu'] = Menu::getMenu('footer', 1);

        //$data['recentPosts'] = Article::getRecentPosts(4);
        //$data['popularTags'] = Tag::popular(12);
        //$data['topProducts'] = Product::popular(2);
        //$data['subscribersCount'] = Setting::value('subscribers-count');
        
        //$data['blogCategories'] = Category::has('articles')->active()->get(array('id', 'name', 'slug'));
        //$data['popularPosts'] = Article::popular(4);
        
        $data['subscribeFormRules'] = Subscriber::getJsonValidationRules();
        $data['contactFormRules'] = ClientRequest::getJsonValidationRules();

        //$data['collections'] = Setting::
        $data['settings'] = Setting::getAllSettings();
        $data['localeUri'] = (App::getLocale() == 'en')? '' : 'ru/';
        $data['switchLanguageLink'] = '';
        $data['socialOrder'] = array('facebook', 'twitter', 'youtube', 'instagram');
        
        $currentRoute = Route::getCurrentRoute();
        $prefix = $currentRoute? $currentRoute->getPrefix() : '';

        
        if($prefix == 'ru') {
            $data['switchLanguageLink'] .= str_replace('/ru', '', URL::current()) ;
        } else {
            $segments = explode('/', URL::current());
            array_splice($segments, 3, 0, "ru");
            $data['switchLanguageLink'] = implode('/', $segments);
        }
        
        return $data;
    }
    
    protected static function getMetaEntry($pageType)
    {
        $page = Page::getActivePageByKey($pageType);//Page::where('key', '=', $pageType)->active()->first();
        if(!$page)
            $page = Page::slug($pageType)->active()->first();
        if(!$page)
            $page = Page::getActivePageByKey('home');
        return $page;
    }
    
    protected static function setMetaData(&$data, $metaEntry)
    {
        if($metaEntry) {
            $data['metaTitle'] = $metaEntry->title;
            $data['metaKeywords'] = $metaEntry->keywords;
            $data['metaDescription'] = $metaEntry->description;
            $data['metaEntry'] = $metaEntry;
        }
        
        return $data;
    }
    
    public static function postAddShare($idEntry, $network)
    {
        $result = array('status' => 0, 'msg' => '');
        if(array_search($network, Share::$networks) !== false) {
            $modelClass = static::$modelName;
            $entry = $modelClass::find($idEntry);
            if($entry) {
                $entry->addShare($network);
                $result['status'] = 1;
                $result['msg'] =  'Done';
            } else {
                $result['status'] = -1;
                $result['msg'] =  'Wrong Entry';
            }
        } else {
            $result['status'] = -2;
            $result['msg'] =  'Wrong network';
        }
        
        return Response::json($result);
    }
    
    public static function postAddComment($idEntry) {
        $result = array('status' => 0, 'msg' => '');
        
        if(Auth::check()) {
        
            $modelClass = static::$modelName;
            $entry = $modelClass::find($idEntry);
            if ($modelClass::$hasComments && $entry) {
                $content = Input::get('content', false);
                $parentId = Input::get('parent_id', 0);
                if(!empty($content)) {
                    
                    $comment = $entry->addComment($content, $parentId, Auth::id(), 1);
                    $result['status'] = 1;
                    $result['msg'] = 'Done';
                    $result['comment'] = View::make('front.layouts.comment', array('comment' => $comment, 'subcomments' => array()))
                            ->render();
                } else {
                    $result['status'] = -1;
                    $result['msg'] = 'The comment is empty!';
                }
            } else {
                $result['status'] = -1;
                $result['msg'] = 'Wrong Entry';
            }
            
        } else {
            $result['status'] = -3;
            $result['msg'] = 'Auth is wrong';
        }
        return Response::json($result);
    }
    
    public static function postAddSubscribe()
    {
        if(Request::ajax()) {
            Setting::ofKey('subscribers-count')->increment('content');
        }
    }

}
