<?php

class CustomPresenter extends Illuminate\Pagination\Presenter {

    public function getPrevious($text = '&laquo;')
    {
        $prev = $this->paginator->getUrl($this->currentPage - 1);
        $first = $this->paginator->getUrl(1);
        if ($this->currentPage <= 1)
            $first = $prev = 'javascript:void(0)';

        return '<li class="control first"><a href="'
                . $first . '"></a></li><li class="control prev"><a href="'
                . $prev . '"></a></li>';
    }

    public function getNext($text = '&laquo;')
    {
        $next = $this->paginator->getUrl($this->currentPage + 1);
        $last = $this->paginator->getUrl($this->lastPage);
        if ($this->currentPage >= $this->lastPage)
            $last = $next = 'javascript:void(0)';

        return '<li class="control next"><a href="'
                . $next . '"></a></li><li class="control last"><a href="'
                . $last . '"></a></li>';
    }

    public function getActivePageWrapper($text)
    {
        $pagiantor = $this->getPaginator();
        $perPage = $pagiantor->getPerPage();
        $start = ((int) $text - 1) * $perPage + 1;
        return '<li class="pages active"><a href="javascrip:void(0);">' . $start . ' - ' . ($start + $perPage - 1) . '</a></li>';
    }

    public function getDisabledTextWrapper($text)
    {
        return '<li class="pages unavailable"><a href="javascrip:void(0);">' . $text . '</a></li>';
    }
    
    public function getDots()
    {
        return '';
    }

    public function getPageLinkWrapper($url, $page, $rel = null)
    {
        $pagiantor = $this->getPaginator();
        $perPage = $pagiantor->getPerPage();
        $start = ($page - 1) * $perPage + 1;
        return '<li class="pages"><a href="' . $url . '">' . $start . ' - ' . ($start + $perPage - 1) . '</a></li>';
    }

    protected function getPaginator()
    {
        return $this->paginator;
    }

    protected function getPageSlider()
    {
        return $this->getAdjacentRange();
    }

    public function render()
    {
        // The hard-coded thirteen represents the minimum number of pages we need to
        // be able to create a sliding page window. If we have less than that, we
        // will just render a simple range of page links insteadof the sliding.
        if ($this->lastPage < 5) {
            $content = $this->getPageRange(1, $this->lastPage);
        } else {
            $content = $this->getPageSlider();
        }

        return $this->getPrevious() . $content . $this->getNext();
    }

    public function getAdjacentRange()
    {
        $left = 2;
        $right = 2;
        if($this->currentPage - $left <= 0) {
            $left = $this->currentPage - 1;
            $right = 4 - $left;
        } elseif($this->lastPage - $this->currentPage < 2) {
            $right = $this->lastPage - $this->currentPage;
            $left = 4 - $right;
        }
            
        return $this->getPageRange($this->currentPage - $left, $this->currentPage + $right);
    }

}
