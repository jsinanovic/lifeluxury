<?php

class CreativeController extends BaseFrontController {

    private static $perPage = 12;
    protected static $modelName = 'Service';
    protected static $moduleName = 'services';

    public static function getIndex($slug = false)
    {
        $data = self::init(array(), 'services');
        if ($slug ) {
            $service = Service::slug($slug)->active()->first();
            if (!$service) {
                return App::abort(404);
            }
            // RANDOM ARTICLES FROM CREATIVE CATEGORY
            $data['randomArticles'] = array();
            foreach ($data['categories'] as $category) {
                if ($category->slug == 'creative') {
                    $data['randomArticles'] = count($category->articles) > 3 ?
                            array_rand($category->articles, 3) : $category->articles;
                }
            }


            $data['service'] = $service;
            $data['services'] = Service::active()->get();
            static::setMetaData($data, $service);
        } else {
            // All services variant
        }


        return View::make('front.creative.index', $data);
    }


    public static function getProjects($slug = false)
    {
        // RANDOM ARTICLES FROM CREATIVE CATEGORY
//        $data['randomArticles'] = array();
//        foreach ($data['categories'] as $category) {
//            if ($category->slug == 'creative') {
//                $data['randomArticles'] = count($category->articles) > 3 ?
//                    array_rand($category->articles, 3) : $category->articles;
//            }
//        }

        $data = self::init(array(), 'services');
        $data['services'] = Service::active()->get();


        return View::make('front.creative.projects', $data);
    }

    public static function getProjectSingle($slug = false)
    {
      $data = self::init(array(), 'services');
      $data['services'] = Service::active()->get();


        foreach ($data['services'] as $key => $category) {
            if($category->slug == $slug) {
                $data['service'] = $category;

                $data['nextCategory'] = isset($data['services'][$key + 1])? $data['services'][$key + 1] : $data['services'][0];
            }
        }


        return View::make('front.creative.creativeprojects', $data);
    }


}
