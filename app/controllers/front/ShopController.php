<?php

class ShopController extends BaseFrontController {
    
    // model name uses for shares now
    protected static $modelName = 'Product';
    protected static $moduleName = 'shop';

    public static function getIndex()
    {
        $products = Product::active()->orderBy('sort', 'ASC')->paginate(6);
        $data = array(
            'currentCategory' => false,
            'menu' => Menu::mainCategories('shop')->has('products')->get(array('id', 'name', 'slug')),
            'products' => $products
        );
        $data = self::init($data, 'shop');
        return View::make('front.products.index', $data);
    }

    public static function getCategory($slug)
    {
        $category = Category::slug($slug)->active()->first();
        if (!$category)
            return App::abort('404');
        $products = $category->products()->active()->orderBy('sort', 'ASC')->paginate(6);
        $data = array(
            'currentCategory' => $category,
            'menu' => Menu::mainCategories('shop')->has('products')->get(array('id', 'name', 'slug')),
            'products' => $products
        );
        $data = self::init($data, 'shop');
        return View::make('front.products.index', $data);
    }

    public static function getProduct($slug)
    {
        $product = Product::slug($slug)->active()->first();
        if (!$product)
            return App::abort('404');

        $product->features = json_decode($product->features, true);
        
        $prices = json_decode($product->prices, true);
        $rp = array();
        if($prices)
            foreach($prices as $price)
                $rp[$price['license']][] = $price;
        $product->prices = $rp;
        $data = array(
            'product' => $product,
        );
        $data = self::init($data, 'view');
        $data = self::setMetaData($data, $product);
        return View::make('front.products.view', $data);
    }

    protected static function getMetaEntry($pageType)
    {
        if ($pageType == 'view')
            return new Page();
        return parent::getMetaEntry($pageType);
    }
    
    

}
