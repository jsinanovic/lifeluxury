<?php

class ArticleTableSeeder extends Seeder {

    public function run()
    {
        DB::table('articles')->delete();
        
        $tags = Tag::ofType(Article::$tagType)->get()->all();
        $categories = Menu::ofKey('blog')->first()->categories;
        
        for($k = 0; $k < 3; ++$k) {
            $article = Article::create(Article::modifyData(array(
                'name' => ($k+1) . ' Rules for Creating Gorgeous UI',
                'annotation' => 'In a responsive theme you will want some parts of your website to look the same 
whether you see it on a large screen device like a laptop or smaller device like 
mobile phones or tablets. But some parts of your website should behave and feel 
very different based on screen size as users interact differently. The top or the side 
menu is one such element where you should strictly...',
                'content' => 'In a responsive theme you will want some parts of your website to look the same 
whether you see it on a large screen device like a laptop or smaller device like 
mobile phones or tablets. But some parts of your website should behave and feel 
very different based on screen size as users interact differently. The top or the side 
menu is one such element where you should strictly.',
                'status' => 1,
                'title' => 'Test'
            )));
            $article->categories()->save($categories[rand(0, count($categories) - 1)]);
            $article->tags()->save($tags[rand(0, count($tags) - 1)]);
        }
    }

}