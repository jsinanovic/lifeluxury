<?php

class DatabaseSeeder extends Seeder {

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Eloquent::unguard();
        $this->call('ModuleTableSeeder');
        $this->call('UserTableSeeder');
        $this->call('PageTableSeeder');
        $this->call('MenuTableSeeder');
        $this->call('CategoryTableSeeder');
        $this->call('TagTableSeeder');
        $this->call('ArticleTableSeeder');
        $this->call('SettingsTableSeeder');
        $this->call('ProductTableSeeder');
        $this->call('WorkTableSeeder');
        $this->call('PriceTableSeeder');
        Cache::flush();
    }

}
