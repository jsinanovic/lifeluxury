<?php

class MenuTableSeeder extends Seeder {

    public function run()
    {
        DB::table('menus')->delete();
        Menu::create(array(
            'name' => 'Navigation',
            'key' => 'nav',
            'depth' => 1
        ));
        Menu::create(array(
            'name' => 'Blog Categories',
            'key' => 'blog',
            'depth' => 1
        ));
        Menu::create(array(
            'name' => 'Shop Categories',
            'key' => 'shop',
            'depth' => 1
        ));
        Menu::create(array(
            'name' => 'Prices Menu',
            'key' => 'prices',
            'depth' => 2
        ));
    }

}