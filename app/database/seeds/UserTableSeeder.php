<?php

class UserTableSeeder extends Seeder {

    public function run()
    {
        DB::table('users')->delete();

        $user = User::create(array(
            'first_name' => 'Aleksey',
            'last_name' => 'Zemliakov',
            'email' => 'aleksz_07@mail.ru',
            'password' => Hash::make('Introduction11'),
            'membership_id' => 1,
        ));
        
        $user = User::create(array(
            'first_name' => 'Ron',
            'last_name' => 'Design',
            'email' => 'aleksey@guru-team.com',
            'password' => Hash::make('test'),
            'membership_id' => 1,
        ));
    }

}
