<?php

class TagTableSeeder extends Seeder {

    public function run()
    {
        DB::table('tags')->delete();
        
        $articlesTags = array('Design', 'Background', 'Rules', 'Gallery',
            'Vimeo', 'Video', 'Link', 'Twitter', 'Image', 'Post');
        
        foreach ($articlesTags as $tag) {
            Tag::create(Tag::modifyData(
                array(
                    'name' => $tag,
                    'type' => 'articles',
                    'count' => 10
                )
            ));
        }
    }

}