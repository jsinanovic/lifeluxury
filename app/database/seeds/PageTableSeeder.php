<?php

class PageTableSeeder extends Seeder {

    public function run()
    {
        DB::table('pages')->delete();
        Page::create(Page::modifyData(array(
                    'name' => 'Home',
                    'title' => 'Home',
                    'key' => 'home',
                    'content' => 'Content is empty now',
                    'status' => 1
        )));

        Page::create(Page::modifyData(array(
                    'name' => 'Prices',
                    'title' => 'Prices',
                    'key' => 'prices',
                    'content' => 'Content is empty now',
                    'status' => 1
        )));

        Page::create(Page::modifyData(array(
                    'name' => 'Our themes',
                    'title' => 'Shop',
                    'subtitle' => 'Expertly made themes for your business',
                    'key' => 'shop',
                    'content' => 'Content is empty now',
                    'status' => 1
        )));

        Page::create(Page::modifyData(array(
                    'name' => 'Works',
                    'title' => 'Works',
                    'key' => 'works',
                    'content' => 'Content is empty now',
                    'status' => 1
        )));

        Page::create(Page::modifyData(array(
                    'name' => 'Blog',
                    'title' => 'Blog',
                    'key' => 'blog',
                    'content' => 'Content is empty now',
                    'status' => 1
        )));

        Page::create(Page::modifyData(array(
                    'name' => 'About',
                    'title' => 'About',
                    'key' => 'about',
                    'content' => '<div class="title">RON DESIGN STORY</div>
            <p>Студия работает с 2010 года. За это время мы успели поработать с большим количеством проектом.</p>
            <p>Недавно появилась необходимость продавать свои работы и поэтому в старый сайт был переделанпод новые нужды.</p>
            <p>Теперь Ron Design создает и интегрирует дизайн, а также продает свои продукты. Смотрите наше портфолио на 
                <a href="">Dribbble</a> и <a href="">Behance</a>.
            </p>',
                    'status' => 1
        )));

        Page::create(Page::modifyData(array(
                    'name' => 'Contacts',
                    'title' => 'Contacts',
                    'key' => 'contacts',
                    'content' => 'Content is empty now',
                    'status' => 1
        )));

        Page::create(Page::modifyData(array(
                    'name' => 'Error 404',
                    'title' => 'Error 404',
                    'key' => 'error404',
                    'content' => 'Content is empty now',
                    'status' => 1
        )));
    }

}
