<?php

class SettingsTableSeeder extends Seeder {

    public function run()
    {
        DB::table('settings')->delete();
        
        Setting::create(Setting::modifyData(array(
            'name' => 'Site Name',
            'description' => 'The site name',
            'module' => 'settings',
            'type' => 'text',
            'content' => 'Ron Design'
        )));
        
        Setting::create(Setting::modifyData(array(
            'name' => 'Map Code',
            'description' => 'Embed map code for the map on contacts page',
            'module' => 'settings',
            'type' => 'text',
            'content' => '<iframe src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d635.1530520367228!2d30.5187544!3d50.4483235!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x40d4cf4ee15a4505%3A0x764931d2170146fe!2sKyiv%2C+Ukraine!5e0!3m2!1sen!2sru!4v1426660878020" width="100%" height="533" frameborder="0" style="border:0"></iframe>'
        )));
        
        Setting::create(Setting::modifyData(array(
            'name' => 'Contact Email',
            'description' => 'Email shows on the contacts page and uses for contact form',
            'module' => 'settings',
            'type' => 'text',
            'content' => 'aleksz_07@mail.ru'
        )));
        
        Setting::create(Setting::modifyData(array(
            'name' => 'Contact Phone',
            'description' => 'Phone shows on the contacts page',
            'module' => 'settings',
            'type' => 'text',
            'content' => '+380950948376'
        )));
        
        Setting::create(Setting::modifyData(array(
            'name' => 'Contact Skype',
            'description' => 'Skype shows on the contacts page',
            'module' => 'settings',
            'type' => 'text',
            'content' => 'ron_evgeniy1'
        )));
        
        Setting::create(Setting::modifyData(array(
            'name' => 'Contact Subjects',
            'description' => 'Subjects for the form on contact page',
            'module' => 'settings',
            'type' => 'textarea',
            'content' => "Site development\nSupport\nMaintance"
        )));
        
        Setting::create(Setting::modifyData(array(
            'name' => 'Pages Keys',
            'description' => 'Pages Keys for system pages binding',
            'module' => 'pages',
            'type' => 'select',
            'content' => "default",
            'values' => "default\nhome\nprices\nshop\nworks\nabout\ncontacts\nblog\nerror404"
        )));
        
        Setting::create(Setting::modifyData(array(
            'name' => 'Dribble Link',
            'description' => 'URL on the Dribble profile',
            'module' => 'settings',
            'type' => 'text',
            'content' => 'https://dribbble.com/'
        )));
        
        Setting::create(Setting::modifyData(array(
            'name' => 'Behance Link',
            'description' => 'URL on the Behance profile',
            'module' => 'settings',
            'type' => 'text',
            'content' => 'http://behance.net/'
        )));
        
        Setting::create(Setting::modifyData(array(
            'name' => 'Facebook Link',
            'description' => 'URL on the Facebook profile',
            'module' => 'settings',
            'type' => 'text',
            'content' => 'https://www.facebook.com/'
        )));
        
        Setting::create(Setting::modifyData(array(
            'name' => 'Twitter Link',
            'description' => 'URL on the Twitter profile',
            'module' => 'settings',
            'type' => 'text',
            'content' => 'https://twitter.com/'
        )));
        
        Setting::create(Setting::modifyData(array(
            'name' => 'LinkedIn Link',
            'description' => 'URL on the LinkedIn profile',
            'module' => 'settings',
            'type' => 'text',
            'content' => 'https://www.linkedin.com/'
        )));
        
        Setting::create(Setting::modifyData(array(
            'name' => 'Pinterest Link',
            'description' => 'URL on the Pinterest profile',
            'module' => 'settings',
            'type' => 'text',
            'content' => 'https://www.pinterest.com/'
        )));
        
    }

}