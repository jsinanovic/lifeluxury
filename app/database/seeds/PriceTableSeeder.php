<?php

class PriceTableSeeder extends Seeder {

    public function run()
    {
        DB::table('prices')->delete();
        
        $data = array(
            array('name' => 'Логотип', 'cat' => 0, 'price' => 500, 'minimal_time' => 7, 'maximal_time' => 7),
            
            array('name' => 'Первая страница', 'cat' => 0, 'sub' => 0, 'price' => 1000, 'minimal_time' => 7, 'maximal_time' => 7),
            array('name' => 'Следующая страница', 'cat' => 0,'sub' => 0, 'price' => 300, 'minimal_time' => 1, 'maximal_time' => 3),
            array('name' => 'Адаптивные страницы, за страницу', 'cat' => 0,'sub' => 0, 'price' => 0, 'percent' => 50, 'minimal_time' => 1, 'maximal_time' => 1),
            array('name' => 'Верстка траниц, за страницу', 'cat' => 0,'sub' => 0, 'price' => 0, 'percent' => 50, 'minimal_time' => 1, 'maximal_time' => 1),
            
            array('name' => 'Дизайн первого экрана', 'cat' => 0,'sub' => 1, 'price' => 1000, 'minimal_time' => 3, 'maximal_time' => 3),
            array('name' => 'Следующий экран', 'cat' => 0,'sub' => 1, 'price' => 300, 'minimal_time' => 1, 'maximal_time' => 2),
            array('name' => 'Адаптация под планшет, за экран', 'cat' => 0,'sub' => 1, 'price' => 0, 'percent' => 75, 'minimal_time' => 1, 'maximal_time' => 1),
            array('name' => 'Адаптация под 2 платформы, за экран', 'cat' => 0,'sub' => 1, 'price' => 0, 'percent' => 50, 'minimal_time' => 1, 'maximal_time' => 1),
            
            array('name' => 'Сайт визитка', 'cat' => 1,'sub' => 0, 'price' => 1000, 'percent' => 0, 'minimal_time' => 7, 'maximal_time' => 14),
        );
        
        $categories = Menu::ofKey('prices')->first()->categories;
        foreach($data as $row) {
            $item = $row;
            unset($item['cat']);
            unset($item['sub']);
            $price = Price::create(Price::modifyData($item));
            if(isset($row['cat']) && isset($row['sub'])) {
                $categories[$row['cat']]->categories[$row['sub']]->prices()->save($price);
                //$price->category()->associate($categories[$row['cat']]->categories[$row['sub']]);
            } elseif(isset($row['cat'])) {
                $categories[$row['cat']]->prices()->save($price);
                //$price->category()->associate($categories[$row['cat']]);
            }
        }
        
    }

}