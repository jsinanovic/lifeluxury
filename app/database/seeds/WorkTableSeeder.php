<?php

class WorkTableSeeder extends Seeder {

    public function run()
    {
        DB::table('works')->delete();
        for($k = 0; $k < 3; ++$k) {
            $work = Work::create(Work::modifyData(array(
                'name' => 'Concept adress book in iOS8',
                'content' => 'This is just a personal preference on how I want to experience the site, I want it to be fun, 
    modern, less busy and clean compared to their current website IMO. Definitely Linkedin 
    has their own reason with their current look. Please note that content are not all accurate 
    based on Bill profile.',
                'count_likes' => rand(1, 5000),
                'count_views' => rand(1, 5000),
                'status' => 1,
                'title' => 'Concept adress book in iOS8',
                'features' => '[{"name" : "Site", "content" : "<a href=\'Rondesign.ru\'>Rondesign.ru</a>"}, {"name" : "Role in project", "content" : "Cofounder"}]'
            )));
        }
    }

}