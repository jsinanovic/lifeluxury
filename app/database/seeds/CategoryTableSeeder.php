<?php

class CategoryTableSeeder extends Seeder {

    public function run()
    {
        DB::table('categories')->delete();


        // NAVIGATION MENU
        $navMenu = Menu::ofKey('nav')->first();
        $categories = array(
            array('name' => 'Home', 'url' => '/'),
            array('name' => 'Prices', 'url' => '/prices'),
            array('name' => 'Shop', 'url' => '/shop'),
            array('name' => 'Works', 'url' => '/works'),
            array('name' => 'Blog', 'url' => '/blog'),
            array('name' => 'About', 'url' => '/about'),
            array('name' => 'Contacts', 'url' => '/contacts'),
        );
        foreach ($categories as $category) {
            $category = new Category(Category::modifyData(
                            array(
                                'name' => $category['name'],
                                'url' => $category['url'],
                                'status' => 1
                            )
            ));
            $navMenu->categories()->save($category);
            $page = Page::where('name', '=', $category->name)->first();
            if ($page)
                $page->category()->save($category);
        }

        // BLOG MENU
        $blogMenu = Menu::ofKey('blog')->first();
        $categories = array('Design', 'Freebies', 'Inspiration', 'Templates', 'Deals');
        foreach ($categories as $category) {
            $category = new Category(Category::modifyData(array('name' => $category, 'status' => 1)));
            $blogMenu->categories()->save($category);
        }
        
        // SHOP MENU
        $blogMenu = Menu::ofKey('shop')->first();
        $categories = array('Wordpress', 'HTML', 'Ecommerce', 'PSD', 'Free');
        foreach ($categories as $category) {
            $category = new Category(Category::modifyData(array('name' => $category, 'status' => 1)));
            $blogMenu->categories()->save($category);
        }

        // PRICES MENU
        $pricesMenu = Menu::ofKey('prices')->first();
        $categories = array(
            array(
                'name' => 'Дизайн', 
                'content' => 'В данном разделе вы можете узнать примерные расценки по дизайну, если вы хотите узнать более точные расценки, то напишите нам о вашем проекте на странице <a href="/contacts">контактов</a>',
                'categories' => array(
                    array('name' => 'Дизайн сайтов и интерфейсов'),
                    array('name' => 'Дизайн мобильных приложений'),
                    //array('name' => 'Want a discount')
                )),
            array('name' => 'Разработка', 'content' => '', 'categories' => array(
                    array('name' => 'Веб сайт'),
                    array('name' => 'Android приложение'),
                    //array('name' => 'Want a discount')
                )),
            array('name' => 'СЕО оптимизация', 'content' => ''),
        );
        foreach ($categories as $category) {
            $obj = new Category(Category::modifyData(
                            array(
                                'name' => $category['name'],
                                'content' => $category['content'],
                                'status' => 1
                            )
            ));
            $pricesMenu->categories()->save($obj);
            if (isset($category['categories'])) {
                foreach ($category['categories'] as $subcategory) {
                    $subcategory = Category::create(Category::modifyData(
                                    array(
                                        'name' => $subcategory['name'],
                                        'parent_id' => $obj->id,
                                        'menu_id' => $pricesMenu->id,
                                        'status' => 1
                                    )
                    ));
                }
            }
        }
    }

}
