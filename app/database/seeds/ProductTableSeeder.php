<?php

class ProductTableSeeder extends Seeder {

    public function run()
    {
        DB::table('products')->delete();
        $categories = Menu::ofKey('shop')->first()->categories;
        
        for($k = 0; $k < 2; ++$k) {
            $product = Product::create(Product::modifyData(array(
                'name' => 'Z-DAY (Part '.$k.')',
                'annotation' => 'Тема продается в нескольких вариантах, PSD, HTML шаблон, Drupal шаблон, а также wordpress шаблон. ',
                'content' => '<h2>Theme Features</h2>
<div class="features-list row">
<div class="col-sm-6">
                <div class="media">
                    <div class="media-left">
                        <div class="media-object">
                            <i class="fa fa-search"></i>
                        </div>
                    </div>
                    <div class="media-body">
                        <h3 class="media-heading">Slide show in posts</h3>
                        <p>Impress your readers by adding 
                            beautiful image galleries to your posts 
                            with our built-in slideshow generator.
                        </p>
                    </div>
                </div>
            </div>
			<div class="col-sm-6">
                <div class="media">
                    <div class="media-left">
                        <div class="media-object">
                            <i class="fa fa-search"></i>
                        </div>
                    </div>
                    <div class="media-body">
                        <h3 class="media-heading">Slide show in posts</h3>
                        <p>Impress your readers by adding 
                            beautiful image galleries to your posts 
                            with our built-in slideshow generator.
                        </p>
                    </div>
                </div>
            </div>
			<div class="col-sm-6">
                <div class="media">
                    <div class="media-left">
                        <div class="media-object">
                            <i class="fa fa-search"></i>
                        </div>
                    </div>
                    <div class="media-body">
                        <h3 class="media-heading">Slide show in posts</h3>
                        <p>Impress your readers by adding 
                            beautiful image galleries to your posts 
                            with our built-in slideshow generator.
                        </p>
                    </div>
                </div>
            </div>
			<div class="col-sm-6">
                <div class="media">
                    <div class="media-left">
                        <div class="media-object">
                            <i class="fa fa-search"></i>
                        </div>
                    </div>
                    <div class="media-body">
                        <h3 class="media-heading">Slide show in posts</h3>
                        <p>Impress your readers by adding 
                            beautiful image galleries to your posts 
                            with our built-in slideshow generator.
                        </p>
                    </div>
                </div>
            </div>
			</div>',
                'features' => '[{"name":"Release date","content":"May 8, 2014"},{"name":"Formats","content":"PSD, HTML, CSS, Webfont, SVG"},{"name":"HTML version","content":"Yes <a href=\"http://themeforest.com\">See on themeforest</a>"}]',
                'status' => 1,
                'title' => 'Z-DAY (Part '.$k.')',
                'price' => rand(25, 250),
                'url' => 'http://guru-team.com',
                'demo_url' => 'http://guru-team.com',
                'prices' => '[{"name":"PSD","details":["23 pages","Organized Layers","Documentation","Free fonts"],"value":"8","active":false,"link":"http://themeforest.com","button":"Buy on themeforest","license":"personal"},{"name":"HTML / CSS3","details":["BOOTSTRAP3","LESS / CSS","Documentation ","Free fonts"],"value":"18","active":true,"link":"http://themeforest.com","button":"Buy on themeforest","license":"personal"},{"name":"Drupal","details":["New framework","SEO ptimized","Documentation","Free Fonts"],"value":"48","active":false,"link":"http://themeforest.com","button":"Buy on themeforest","license":"personal"},{"name":"Wordpress","details":["New framework","SEO optimized","Documentation","Free fonts"],"value":"58","active":false,"link":"http://themeforest.com","button":"Buy on themeforest","license":"personal"},{"name":"PSD","details":["23 pages","Organized Layers","Documentation","Free fonts"],"value":"1000","active":true,"link":"http://themeforest.com","button":"Buy on themeforest","license":"extended"}]',
            )));
            $product->categories()->save($categories[rand(0, count($categories) - 1)]);
        }
    }

}