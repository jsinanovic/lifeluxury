<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTagsTable extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tags', function($table){
            $table->engine = 'InnoDB';
            
            $table->increments('id');
            $table->string('name');
            $table->string('slug');
            $table->string('type', 20);
            $table->integer('count')->default(0);
            
            $table->unique(array('slug', 'type'));
            $table->index('type');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('tags');
    }

}
