<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateArticleSubcategoryTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('article_subcategory', function(Blueprint $table) {
			$table->engine = 'InnoDB';

			$table->integer('article_id')->unsigned();
			$table->integer('subcategory_id')->unsigned();
			$table->primary(array('article_id', 'subcategory_id'));
//			$table->foreign('article_id')->references('id')->on('articles')
//				->onDelete('cascade');
//			$table->foreign('subcategory_id')->references('id')->on('subcategories')
//				->onDelete('cascade');

			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('article_subcategory');
	}

}
