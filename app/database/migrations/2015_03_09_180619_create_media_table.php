<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMediaTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
            Schema::create('media', function($table){
                $table->engine = 'InnoDB';

                $table->increments('id');
                $table->string('name', 32);
                $table->string('model', 20);
                $table->integer('entry_id');

                $table->index(array('model', 'entry_id'));
            });
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
            Schema::drop('media');
	}

}
