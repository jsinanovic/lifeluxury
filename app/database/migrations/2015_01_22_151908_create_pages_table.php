<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePagesTable extends Migration {

    public function up()
    {
        Schema::create('pages', function ($table) {
            $table->engine = 'InnoDB';
            
            $table->increments('id');
            $table->string('name', 255);
            $table->string('slug', 255);
            $table->string('key', 50);
            $table->string('subtitle', 500);
            $table->text('content');
            $table->enum('type', array('full', 'short'))->default('full');
            
            $table->integer('sort')->unsigned();
            $table->tinyInteger('status')->default('0');
            
            $table->string('title');
            $table->string('keywords', 500);
            $table->string('description', 500);
            
            $table->timestamps();
            
            $table->unique('key');
            $table->unique('slug');
            $table->index(array('slug', 'status'));
            $table->unique('sort');
        });
        
        Schema::create('page_translations', function ($table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->integer('page_id')->unsigned();
            $table->string('locale')->index();
            
            $table->string('name', 255);
            $table->string('subtitle', 500);
            $table->text('content');
            $table->string('title');
            $table->string('keywords', 500);
            $table->string('description', 500);
            
            $table->unique(['page_id','locale']);
            $table->foreign('page_id')->references('id')->on('pages')->onDelete('cascade');
        });
        
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('page_translations');
        Schema::drop('pages');
    }

}
