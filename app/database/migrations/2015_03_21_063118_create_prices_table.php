<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePricesTable extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('prices', function(Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->string('name', 255);
            $table->integer('category_id')->unsigned();
            $table->decimal('price', 10, 2)->unsigned();
            $table->tinyInteger('percent');
            $table->tinyInteger('minimal_time')->unsigned()->default(1);
            $table->tinyInteger('maximal_time')->unsigned()->default(1);
            $table->string('annotation', 500);
            $table->text('content');
            $table->integer('sort')->unsigned();
            
            $table->unique('sort');
        });
        
        
        Schema::create('price_translations', function ($table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->integer('price_id')->unsigned();
            $table->string('locale')->index();
            
            $table->string('name', 255);
            $table->string('annotation', 500);
            $table->text('content');
            
            $table->unique(['price_id','locale']);
            $table->foreign('price_id')->references('id')->on('prices')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('price_translations');
        Schema::drop('prices');
    }

}
