<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class LaravelSocialCreateProvidersTable extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
        $table_name = Config::get('laravel-social::table', 'user_providers');

        Schema::create($table_name, function ($table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->integer('user_id')->unsigned();
            $table->string('provider');
            $table->string('provider_id');
            $table->text('access_token');
            $table->timestamps();

            $table->unique(array('provider', 'provider_id'));
            $table->index(array('user_id', 'provider'));

            $table->foreign('user_id')->references('id')->on('users')
                    ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        $table_name = Config::get('laravel-social::table', 'user_providers');

        Schema::drop($table_name);
    }

}
