<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
        Schema::create('users', function ($table) {
            $table->increments('id');
            $table->string('first_name', 20);
            $table->string('last_name', 20);
            $table->string('email', 100)->index();
            $table->string('password', 64);
            $table->string('remember_token', 100)->nullable();
            
            $table->enum('gender', array('male', 'female', ''))->default('');
            $table->string('locale', 50);
            $table->tinyInteger('timezone');
            $table->string('image');
            $table->integer('membership_id')->unsigned()->default(0);
            
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        Schema::drop('users');
    }

}
