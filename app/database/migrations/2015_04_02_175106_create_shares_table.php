<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSharesTable extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('shares', function ($table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->enum('network', array('twitter', 'facebook', 'gplus', 'linkedin', 'pinterest'));
            $table->integer('count')->default(0);
            $table->integer('shareable_id')->unsigned();
            $table->string('shareable_type', 50);
            
            $table->unique(array('shareable_type', 'shareable_id', 'network'));
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('shares');
    }

}
