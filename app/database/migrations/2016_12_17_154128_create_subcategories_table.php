<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSubcategoriesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('subcategories', function(Blueprint $table)
		{
			$table->engine = 'InnoDB';

			$table->increments('id');
			$table->string('name', 255);
			$table->integer('category_id')->unsigned()->nullable();

			$table->integer('sort')->unsigned();
			$table->tinyInteger('status')->default('0');

			$table->text('content');
			$table->string('title');
			$table->string('keywords', 500);
			$table->string('description', 500);
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('subcategories');
	}

}
