<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateArticlesTable extends Migration {

    public function up()
    {
        Schema::create('articles', function ($table) {
            $table->engine = 'InnoDB';
            
            $table->increments('id');
            $table->string('name');
            $table->string('slug', 255);
            $table->string('image', 50);
            $table->string('annotation', 500);
            $table->text('content');
            $table->tinyInteger('comments_count')->unsigned()->default('0');
            
            $table->string('title');
            $table->string('keywords', 500);
            $table->string('description', 500);
            
            $table->tinyInteger('status')->default('0');
            
            $table->timestamps();
            
            $table->unique('slug');
        });
        
        Schema::create('article_translations', function ($table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->integer('article_id')->unsigned();
            $table->string('locale')->index();
            
            $table->string('name', 255);
            $table->string('annotation', 500);
            $table->text('content');
            $table->string('title');
            $table->string('keywords', 500);
            $table->string('description', 500);
            
            $table->unique(['article_id','locale']);
            $table->foreign('article_id')->references('id')->on('articles')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('article_translations');
        Schema::drop('articles');
    }

}
