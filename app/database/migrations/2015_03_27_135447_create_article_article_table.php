<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateArticleArticleTable extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('article_article', function($table) {
            $table->engine = 'InnoDB';

            $table->integer('article_id')->unsigned();
            $table->integer('similar_id')->unsigned();

            $table->primary(array('article_id', 'similar_id'));
            $table->foreign('article_id')->references('id')->on('articles')
                    ->onDelete('cascade');
            $table->foreign('similar_id')->references('id')->on('articles')
                    ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('article_article');
    }

}
