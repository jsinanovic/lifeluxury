<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateArticleCategoryTable extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('article_category', function($table){
            $table->engine = 'InnoDB';
            
            $table->integer('article_id')->unsigned();
            $table->integer('category_id')->unsigned();
            $table->primary(array('article_id', 'category_id'));
            $table->foreign('article_id')->references('id')->on('articles')
                    ->onDelete('cascade');
            $table->foreign('category_id')->references('id')->on('categories')
                    ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('article_category');
    }

}
