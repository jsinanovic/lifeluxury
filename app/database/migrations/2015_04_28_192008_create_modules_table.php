<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateModulesTable extends Migration {

    public function up() {
         Schema::create('modules', function ($table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->string('name', 255);
            $table->string('model', 255);
            $table->string('uri', 255);
            $table->tinyInteger('access_level')->default('1');
            
            $table->text('migration');
            $table->tinyInteger('status')->default('0');

            
        });
    }

    public function down() {
        Schema::drop('modules');
    }

}
