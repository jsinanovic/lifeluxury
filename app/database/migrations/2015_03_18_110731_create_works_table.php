<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateWorksTable extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('works', function(Blueprint $table) {
            $table->engine = 'InnoDB';

            $table->increments('id');
            $table->string('name', 255);
            $table->string('slug', 255);
            $table->string('image', 50);
            $table->string('annotation', 500);
            $table->text('content');
            $table->text('features');

            $table->integer('count_likes')->default(0);
            $table->integer('count_views')->default(0);
            
            $table->tinyInteger('status')->default('0');

            $table->string('title');
            $table->string('keywords', 500);
            $table->string('description', 500);

            $table->timestamps();

            $table->unique('slug');
        });
        
        Schema::create('work_translations', function ($table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->integer('work_id')->unsigned();
            $table->string('locale')->index();
            
            $table->string('name', 255);
            $table->string('annotation', 500);
            $table->text('content');
            $table->text('features');
            
            $table->string('title');
            $table->string('keywords', 500);
            $table->string('description', 500);
            
            $table->unique(['work_id','locale']);
            $table->foreign('work_id')->references('id')->on('works')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('work_translations');
        Schema::drop('works');
    }

}
