<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCategorySubcategoryTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('category_subcategory', function(Blueprint $table)
		{
			$table->engine = 'InnoDB';

			$table->integer('category_id')->unsigned();
			$table->integer('subcategory_id')->unsigned();
			$table->primary(array('category_id', 'subcategory_id'));
//			$table->foreign('category_id')->references('id')->on('categories')
//				->onDelete('cascade');

			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('category_subcategory');
	}

}
