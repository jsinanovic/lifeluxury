<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCategoriesTable extends Migration {

    public function up()
    {
        Schema::create('categories', function ($table) {
            $table->engine = 'InnoDB';

            $table->increments('id');
            $table->string('name', 255);
            $table->string('slug', 255);
            $table->string('url', 255);
            $table->integer('parent_id')->unsigned()->default('0');
            $table->integer('menu_id')->unsigned();
            $table->integer('page_id')->unsigned()->nullable();

            $table->integer('sort')->unsigned();
            $table->tinyInteger('status')->default('0');
            
            $table->string('annotation', 500);
            $table->text('content');
            
            $table->string('title');
            $table->string('keywords', 500);
            $table->string('description', 500);

            $table->unique(array('slug', 'menu_id'));
            //$table->index('menu_id');
            //$table->index(array('slug', 'status'));
            $table->unique(array('menu_id', 'parent_id', 'sort'));
            
            $table->foreign('menu_id')->references('id')->on('menus')->onDelete('cascade');
            //$table->foreign('parent_id')->references('id')->on('categories')->onDelete('cascade');
            $table->foreign('page_id')->references('id')->on('pages')->onDelete('set null');
        });
        
        Schema::create('category_translations', function ($table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->integer('category_id')->unsigned();
            $table->string('locale')->index();
            
            $table->string('name', 255);
            $table->string('annotation', 500);
            $table->text('content');
            
            $table->string('title');
            $table->string('keywords', 500);
            $table->string('description', 500);
            
            $table->unique(['category_id','locale']);
            $table->foreign('category_id')->references('id')->on('categories')->onDelete('cascade');
        });
    }
    
    public function down()
    {
        Schema::drop('category_translations');
        Schema::drop('categories');
    }

}
