<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProductsTable extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('products', function(Blueprint $table) {
            $table->engine = 'InnoDB';
            
            $table->increments('id');
            $table->string('name', 255);
            $table->string('slug', 255);
            $table->string('image', 50);
            $table->decimal('price', 10, 2)->unsigned();
            $table->string('url');
            $table->string('demo_url');
            
            $table->string('annotation', 500);
            $table->text('content');
            $table->text('features');
            $table->text('prices');
            
            $table->tinyInteger('status')->default('0');
            $table->integer('sort')->unsigned();
            
            $table->string('title');
            $table->string('keywords', 500);
            $table->string('description', 500);
            
            $table->timestamps();
            
            $table->unique('slug');
            $table->unique('sort');
        });
        
        
        Schema::create('product_translations', function ($table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->integer('product_id')->unsigned();
            $table->string('locale')->index();
            
            $table->string('name', 255);
            $table->string('annotation', 500);
            $table->text('content');
            $table->text('features');
            $table->text('prices');
            
            $table->string('title');
            $table->string('keywords', 500);
            $table->string('description', 500);
            
            $table->unique(['product_id','locale']);
            $table->foreign('product_id')->references('id')->on('products')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('product_translations');
        Schema::drop('products');
    }

}
