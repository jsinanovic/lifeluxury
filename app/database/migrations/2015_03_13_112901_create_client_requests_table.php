<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateClientRequestsTable extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('client_requests', function(Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->string('type', 50);
            $table->string('user_name', 255);
            $table->string('user_email', 255);
            $table->string('subject', 255);
            $table->text('message');
            $table->text('details');
            $table->tinyInteger('status')->default('0');
            
            $table->timestamps();
            $table->index(array('type'));
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('client_requests');
    }

}
