<?php

// switch locales
$locales = array('en', 'ru');
$locale = Request::segment(1);
in_array($locale, $locales)? App::setLocale($locale) : $locale = null;
// ------------------------------ Front Routes ---------------------------------
// -----------------------------------------------------------------------------
Route::group(array('prefix' => $locale, 'before' => 'locale'), function() {

    Route::get('admin/login', 'AdminController@getLogin');
    Route::get('remind', 'RemindersController@getRemind');
    Route::get('password/reset/{token}', 'RemindersController@getReset');

    Route::get('/', 'PageController@getIndex');
    Route::get('welcome', 'PageController@getWelcome');

    Route::get('terms-and-conditions', 'PageController@getTermsAndConditions');
    Route::get('privacy-policy', 'PageController@getPrivacyPolicy');
    Route::get('service-promise', 'PageController@getServicePromise');
    Route::get('get-a-quote', 'PageController@getGetAQuote');

    Route::get('brand-identity', 'PageController@getBrandIdentity');
    Route::get('press-kit', 'PageController@getPressKit');
    Route::get('media-kit', 'PageController@getMediaKit');
    Route::get('newsletter', 'PageController@getNewsLetter');

    Route::get('article-single-page', 'PageController@getArticleSinglePage');
    Route::get('events-single-page', 'PageController@getEventsSinglePage');

    Route::get('unsubscribe/{token}', 'MagazineController@getUnsubscribe');
    Route::controller('randvuaz', 'MagazineController');
    Route::get('page/{slug}', 'PageController@getPage');
    Route::controller('page', 'PageController');
    Route::get('contacts', 'PageController@getContacts');
    Route::get('prices', 'PageController@getPrices');
    Route::get('about', 'PageController@getAbout');
    Route::get('creative/{slug?}', 'CreativeController@getIndex');
    Route::get('creativ', 'CreativeController@getProjects');
    Route::get('creativProjects/{slug}', 'CreativeController@getProjectSingle');

    Route::get('creativ-single', 'CreativeController@getProjectSingle');
});

Route::group(array('before' => 'csrf'), function() {
    Route::post('contacts', 'ClientRequestsController@postCreate');
    Route::post('subscribe', 'SubscribersController@postCreate');
    Route::post('admin/login', 'AdminController@postLogin');
    Route::post('remind', 'RemindersController@postRemind');
    Route::post('password/reset', 'RemindersController@postReset');
});

Route::post('add-subscribe', 'BaseFrontController@postAddSubscribe');


// ------------------------------ Admin Routes ---------------------------------
// -----------------------------------------------------------------------------
Route::group(array('prefix' => 'admin', 'before' => 'auth.admin'), function() {

    // AUTH ROUTES
    Route::get('/', 'PagesController@getIndex');
    Route::get('logout', 'AdminController@getLogout');

    Route::get('commingsoon', 'AdminController@getCommingSoon');
    Route::get('commingsoonEnable', 'AdminController@getCommingSoonEnable');

    // ADDITIONAL AND CUSTOM ROUTES
    Route::get('client-requests/{type}', 'ClientRequestsController@getIndex')
            ->where('type', ClientRequest::getRouteTypesFilter());

    Route::get('menus/view/{Menu}', 'MenusController@getView');
    Route::get('menus/menu-view/{Menu}', 'MenusController@getMenuView');

    // MODULES ROUTES
    $modules = Module::getAllCached();
    foreach ($modules as $module) {
        if($module->access_level == 2) {
            Route::group(array('before' => 'auth.superadmin'), function() use ($module) {
                $module->getDefaultRoutes();
            });
        } else {
            $module->getDefaultRoutes();
        }
    }
    
    Route::post('cache', function(){
        Cache::flush();
    });
});
