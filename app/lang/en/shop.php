<?php

return array(
    'all_themes' => 'All themes',
    'havent_found' => "If you haven't found anything suitable for you, we design and develop to order",
    'theme' => 'theme',
    'buy' => 'Buy',
    'see_more' => 'See more',
    'top_selling_products' => 'TOP SELLING PRODUCTS',
    'demo' => 'DEMO',
    'buy_now' => 'BUY NOW',
    'features' => 'Features',
    'screenshots' => 'Screenshots',
    'details' => 'Details',
    'purchase' => 'Purchase',
    'choose_license' => 'Choose the license plan that suits your needs best. Save money by buying more licenses.',
    'personal_license' => 'Personal License',
    'extended_license' => 'Extended License',
    'have_questions' => 'Do you have questions about this theme?',
    'glad_to_answer' => 'We are glad to answer them.',
    'to_order' => "If you haven't found anything suitable for you, we design and develop to order",
    '' => '',
    '' => '',
    '' => '',
    '' => '',
    '' => '',
    '' => '',
    '' => '',
    '' => '',
    '' => '',
    
);

