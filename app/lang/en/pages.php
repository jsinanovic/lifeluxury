<?php

return array(
    'ron_evgeniy' => 'RON EVGENIY',
    'founder_designer' => 'Founder & UI/UX Designer',
    'aleksey_zemliakov' => 'ALEKSEY ZEMLIAKOV',
    'developer' => 'Developer',
    'aleksey_morozov' => 'ALEKSEY MOROZOV',
    'marketing_seo' => 'Marketing & SEO optimization',
    'you_have_questions' => 'You have questions or you need help',
    'dont_hesitate' => "don't hesitate, write to us",
    'write_to_us' => 'Write to us',
    'general_questions' => 'General Questions',
    'site_development' => 'Site Development',
    'support' => 'Support',
    'maintenance' => 'Maintance',
);

