<?php

return array(
    'comments' => 'Comments',
    'add_comment_placeholder' => 'Add your comment here',
    'sign_in_facebook' => 'Sign in with Facebook',
    'sign_in_twitter' => 'Sign in with Twitter',
    'characters_left' => 'characters left',
    'add_comment' => 'Add comment',
    'show_more_comments' => 'Show more comments',
);
