<?php

return array(
    'ron_evgeniy' => 'РОНЬ ЕВГЕНИЙ',
    'founder_designer' => 'Основатель и UI/UX Дизайнер',
    'aleksey_zemliakov' => 'АЛЕКСЕЙ ЗЕМЛЯКОВ',
    'developer' => 'Разработчик',
    'aleksey_morozov' => 'АЛЕКСЕЙ МОРОЗОВ',
    'marketing_seo' => 'Маркетинг и SEO оптимизация',
    'you_have_questions' => 'У вас есть вопросы или вам нужна помощь',
    'dont_hesitate' => 'не стесняйтесь, пишите нам',
    'write_to_us' => 'Напишите нам',
    'general_questions' => 'Общие Вопросы',
    'site_development' => 'Разработка сайта',
    'support' => 'Поддержка',
    'maintenance' => 'Обслуживание',
);

