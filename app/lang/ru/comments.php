<?php

return array(
    'comments' => 'Комментарии',
    'add_comment_placeholder' => 'Добавить комментарий',
    'sign_in_facebook' => 'Зайти через Facebook',
    'sign_in_twitter' => 'Зайти через Twitter',
    'characters_left' => 'осталось символов',
    'add_comment' => 'Добавить комментарий',
    'show_more_comments' => 'Показать больше комментариев',
);
