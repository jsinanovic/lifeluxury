<?php

return array(

	/*
	|--------------------------------------------------------------------------
	| Validation Language Lines
	|--------------------------------------------------------------------------
	|
	| The following language lines contain the default error messages used by
	| the validator class. Some of these rules have multiple versions such
	| as the size rules. Feel free to tweak each of these messages here.
	|
	*/

	"accepted"             => ":attribute должно быть принято.",
	"active_url"           => "Ссылка :attribute недействительна.",
	"after"                => ":attribute должно быть датой после :date.",
	"alpha"                => ":attribute может содержать только буквы.",
	"alpha_dash"           => ":attribute может содержать только буквы, цифры и тире.",
	"alpha_num"            => ":attribute может содержать только буквы, цифры.",
	"array"                => ":attribute может быть массивом.",
	"before"               => ":attribute должно быть датой перед :date.",
	"between"              => array(
		"numeric" => ":attribute должно быть между :min и :max.",
		"file"    => ":attribute должно быть от :min и до :max килобайт.",
		"string"  => ":attribute должно содержать между :min и :max знаков.",
		"array"   => ":attribute должно иметь от :min и до :max наименований.",
	),
	"boolean"              => "Введенное :attribute верно или неверно.",
	"confirmed"            => ":attribute не совпадают.",
	"date"                 => ":attribute дата не действительна.",
	"date_format"          => ":attribute не соответствует формату :format.",
	"different"            => ":attribute и :other должны быть разными.",
	"digits"               => ":attribute должно быть :digits цифрами.",
	"digits_between"       => ":attribute должно иметь от :min и до :max цифр.",
	"email"                => ":attribute должен быть указан действительный e-mail адрес.",
	"exists"               => "Указанный :attribute не действителен.",
	"image"                => ":attribute должно быть картинкой.",
	"in"                   => "Выбранный :attribute не действителен.",
	"integer"              => ":attribute должно быть целым числом",
	"ip"                   => ":attribute должно быть существующим IP адресом.",
	"max"                  => array(
		"numeric" => ":attribute не может быть больше :max.",
		"file"    => ":attribute не может быть больше :max килобайт.",
		"string"  => "Th:attribute не может содержать больше :max знаков.",
		"array"   => ":attribute не может содержать больше :max наименований.",
	),
	"mimes"                => ":attribute должен быть файлом формата: :values.",
	"min"                  => array(
		"numeric" => ":attribute должно быть минимум :min.",
		"file"    => ":attribute должно быть минимум :min килобайт.",
		"string"  => ":attribute должно содержать минимум :min знаков.",
		"array"   => ":attribute должно содержать минимум :min наименований.",
	),
	"not_in"               => "Выбранный :attribute не действителен.",
	"numeric"              => ":attribute должен быть числом.",
	"regex"                => ":attribute формат не действителен.",
	"required"             => ":attribute поле обязательно для заполнения.",
	"required_if"          => ":attribute поле обязательно для заполнения,  где :other это :value.",
	"required_with"        => ":attribute поле обязательно для заполнения, где указано :values.",
	"required_with_all"    => ":attribute поле обязательно для заполнения, где указано :values.",
	"required_without"     => ":attribute поле обязательно для заполнения, где не указано :values.",
	"required_without_all" => ":attribute поле обязательно для заполнения, где ни один из параметров не указан.",
	"same"                 => ":attribute и :other должны совпадать.",
	"size"                 => array(
		"numeric" => "Размер файла :attribute должен быть :size.",
		"file"    => ":attribute  должен быть :size килобайт.",
		"string"  => ":attribute должно содержать :size знаков.",
		"array"   => ":attribute должно содержать :size наименований.",
	),
	"unique"               => ":attribute уже занят.",
	"url"                  => ":attribute формат не действителен.",
	"timezone"             => ":attribute должно быть действительной зоной.",

	/*
	|--------------------------------------------------------------------------
	| Custom Validation Language Lines
	|--------------------------------------------------------------------------
	|
	| Here you may specify custom validation messages for attributes using the
	| convention "attribute.rule" to name the lines. This makes it quick to
	| specify a specific custom language line for a given attribute rule.
	|
	*/

	'custom' => array(
		'attribute-name' => array(
			'rule-name' => 'custom-message',
		),
	),

	/*
	|--------------------------------------------------------------------------
	| Custom Validation Attributes
	|--------------------------------------------------------------------------
	|
	| The following language lines are used to swap attribute place-holders
	| with something more reader friendly such as E-Mail Address instead
	| of "email". This simply helps us make messages a little cleaner.
	|
	*/

	'attributes' => array(),

);
