<?php

return array(

	/*
	|--------------------------------------------------------------------------
	| Password Reminder Language Lines
	|--------------------------------------------------------------------------
	|
	| The following language lines are the default lines which match reasons
	| that are given by the password broker for a password update attempt
	| has failed, such as for an invalid token or invalid new password.
	|
	*/

	"password" => "Пароль должен состоять минимум из шести символов, чтобы пройти подтверждение.",

	"user" => "Пользователь с указанным e-mail адресом не найден",

	"token" => "Данная ссылка устарела",

	"sent" => "Напоминание пароля отправлено вам на почту",

	"reset" => "Пароль изменен",

);
