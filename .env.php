<?php
//file: /.env.local.php
// return the configuration for the 'local' environment
return array(
    'MYSQL_DB_HOST' => 'localhost',
    'MYSQL_DB_NAME' => 'lifeandluxury', // specify database name
    'MYSQL_DB_USER' => 'root', // specify database username
    'MYSQL_DB_PASSWORD' => '', // specify database password
);
