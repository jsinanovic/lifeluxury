$(document).ready(function () {
    
    $('.article-repeat').on('click', function(){
        $('html,body').animate({scrollTop: 0}, 'slow');
        return false;
    });
    
    $(".owl").owlCarousel({
        navigation: true,
        navigationText: ['<i class="fa fa-angle-left"></i>', '<i class="fa fa-angle-right"></i>'],
        items: 4,
        pagination: false,
        itemsDesktop: [1199, 3],
        itemsDesktopSmall: [980, 2],
        itemsTablet: [768, 2],
        itemsTabletSmall: false,
        itemsMobile: [479, 1],
    });
    
    
    var $content = $('.content');
    /*var content = $content.data('content');
    $content.removeAttr('data-content');
    var $items = $('.content-data, .content-data-bg, .content-data-img, .content-data-all');
    var $imgs = $('[data-img]');*/
    
    /*$imgs.each(function () {
        var $item = $(this);
        var key = $item.data('img');
        if (!!content[key]) {
            var $img = $('<div>').append(content[key]).find('img').first();
            var src = $img.attr('src');
            var alt = $img.attr('alt');
            $item.attr({
                src: src,
                alt: alt,
            });

        }
    });
    
    setTimeout(function () {
        $items.each(function(){
            var $item = $(this);
            var key = $item.data('key');
            if(!!content[key]) {
                var $temp = $('<div>').append(content[key]);
                var $img = $temp.find('img').removeAttr('style');
                
                
                if($item.hasClass('item-img-left') || $item.hasClass('item-img-right') || $item.hasClass('content-data-all')) {
                    $item.css('background-image', 'url(' + $img.attr('src') + ')');
                    $item.append($img);
                }
                
                if($item.hasClass('item-img-center') || $item.hasClass('content-data-img')) {
                    $item.append($img);
                }
                
                if($item.hasClass('content-data-bg')) {
                    $item.css('background-image', 'url(' + $img.attr('src') + ')');
                }
                
            }
        });
    }, 50);
    */
    
    var timer = false;
    var mouseWheelEvent = (/Firefox/i.test(navigator.userAgent)) ? "DOMMouseScroll" : "mousewheel";
    var $body = $('body');
    
    $body.on(mouseWheelEvent, function (e) {
        if (!timer) {
            timer = setTimeout(function () {
                var evt = window.event || e;
                evt = evt.originalEvent ? evt.originalEvent : evt;
                var delta = evt.detail ? evt.detail * (-40) : evt.wheelDelta;
                
                if (delta > 0) {
                    //scroll up
                    var $link = $('.slider-magazine .item.active .article-discover');
                    $link.trigger('click');
                    if($body.scrollTop() <= 0) {
                        if ($body.hasClass('to-magazine')) {
                            $body.addClass('mz-loading');
                            window.location.href = baseUrl + '/magazine';
                        }
                        $body.addClass('to-magazine');
                    }
                    //window.location.href = '/magazine';
                } else {
                    $body.removeClass('to-magazine');
                }
                timer = false;
            }, 140);
        }

    });
    
    /* GSAP ANIMATIONS SECTION */
    //var $section = $('img');
    //TweenMax.from($section, 1, {opacity: 0});
    var config = {
        reset: true,
        vFactor : 0.2,
        delay : 'always',
        move : '100px',
    }
    window.sr = new scrollReveal(config);
    /* GSAP ANIMATIONS FINISH */
    
    
    var $dataContent = $content.find('.content-data, .content-data-all');
    console.log($dataContent);
    $dataContent.find('> img').each(function(){
        var src = $(this).attr('src');
        $(this).parent().css('background-image', 'url(' + src + ')');
    });
    
    $content.find('.content-data-bg > img').each(function(){
        var src = $(this).attr('src');
        $(this).parent().css('background-image', 'url(' + src + ')');
        $(this).remove();
    });
    
    
});