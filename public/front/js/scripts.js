$(document).ready(function () {
    
    $(window).on('load', function(){
        $('body').removeClass('mz-loading');
        $('.mz-loader').css({'z-index' : '0'});
    });

    var $sameRows = $('.row-same');
    $sameRows.each(function () {$(this).data('cols', $(this).find('> div'));});
    function adjustSameRows()
    {
        $sameRows.each(function () {
            $(this).data('cols').outerHeight('auto');
            $(this).data('cols').outerHeight($(this).height());
        });
    }
    
    if($sameRows.length > 0) {
        adjustSameRows();
        $(window).on('load resize', adjustSameRows);
    }



    $('.open-chat').on('click', function () {
        $_Tawk.toggle();
        return false;
    });

    $('.carousel').carousel().on('slide.bs.carousel', function (e) {
        var nextH = $(e.relatedTarget).height();
        $(this).find('div.active').parent().animate({height: nextH}, 500);
    });

    // CREATIVE PAGE CODE
    var $slider = $('.slider');
    rebuildSlider();
    $(window).resize(function () {
        rebuildSlider();
    });

    function rebuildSlider()
    {
        var isSmall = isTablet();
        var hasClass = $slider.hasClass('tablet');
        if (!isSmall && hasClass) {
            var $slides = $slider.find('.carousel-inner .item');
            $slides.remove();
            $slider.find('.carousel-inner').append($slider.data('desktop'));
            $slider.removeClass('tablet');

            var nextH = $slider.find('.item.active').height();
            $slider.find('div.active').parent().animate({height: nextH}, 500);

            //var nextH = $(e.relatedTarget).height();
            //$(this).find('div.active').parent().animate({ height: nextH }, 500);
        } else if (isSmall && !hasClass) {
            var $blocks = $slider.find('.text-block, .image-block').clone();
            var $slides = $slider.find('.carousel-inner .item');
            $slider.data('desktop', $slides);
            $slider.addClass('tablet');

            $blocks.each(function (index) {
                var $item = $('<div class="item">').append($(this));
                if (index == 0) {
                    $item.addClass('active');
                }
                $slider.find('.carousel-inner').append($item);
            });

            $slides.remove();

            var nextH = $slider.find('.item.active').height();
            $slider.find('div.active').parent().animate({height: nextH}, 500);
        }
    }

    // HOME PAGE CODE
    $('header .nav > li > a').on('click', function (e) {
        var href = $(this).attr('href');
        if (isHomePage) {
            if (href[0] == '#') {
                var $section = $(href);
                $('html,body').animate({scrollTop: $section.offset().top - 70}, 'slow', function () {
                    window.location.hash = href;
                });
                return false;
            } else if (href[href.length - 1] == '/') {
                $('html,body').animate({scrollTop: 0}, 'slow', function () {
                    window.location.hash = '';
                });
                return false;
            }
        } else {
            if (href[0] == '#') {
                window.location.href = '/' + href;
                return false;
            }
        }
    });

    if (window.location.hash[0] == '#') {
        var $section = $(window.location.hash);
        $('html,body').animate({scrollTop: $section.offset().top - 0}, 'slow');
    }

    $('.section-footer, .section-btn-next-down').on('click', function () {
        var $section = $('#welcom2').length > 0 ? $('#welcom2') : $(this).closest('section').next();
        $('html,body').animate({scrollTop: $section.offset().top - 0}, 'slow');
        return false;
    });

    $('.section-footer3 , .section-btn-next-down3').on('click', function () {
        var $section = $('#section_2').length > 0 ? $('#section_2') : $(this).closest('section').next();
        $('html,body').animate({scrollTop: $section.offset().top - 0}, 'slow');
        return false;
    });


    $('.learn-more, .discover').on('click', function () {
        var $section = $('#about').length > 0 ? $('#about') : $(this).closest('section').next();
        $('html,body').animate({scrollTop: $section.offset().top - 0}, 'slow');
        return false;
    });

    $('.learn-more2, .discover').on('click', function () {
        var $section = $('#creative').length > 0 ? $('#creative') : $(this).closest('section').next();
        $('html,body').animate({scrollTop: $section.offset().top - 0}, 'slow');
        return false;
    });

    $('.learn-more3, .discover').on('click', function () {
        var $section = $('#rendezvous').length > 0 ? $('#rendezvous') : $(this).closest('section').next();
        $('html,body').animate({scrollTop: $section.offset().top - 0}, 'slow');
        return false;
    });

    $('.learn-more4, .discover').on('click', function () {
        var $section = $('#magazine').length > 0 ? $('#magazine') : $(this).closest('section').next();
        $('html,body').animate({scrollTop: $section.offset().top - 0}, 'slow');
        return false;
    });

    $('.learn-more5, .discover').on('click', function () {
        var $section = $('#home').length > 0 ? $('#home') : $(this).closest('section').next();
        $('html,body').animate({scrollTop: $section.offset().top - 0}, 'slow');

        return false;
    });


    /* Left Menu Slider */
    $('.nav-link-line').on('click', function () {
        $('html,body').animate({scrollTop: 30}, 'slow', function () {
            window.location.hash = '';
        });
        return false;
    });
    /* End section on left side animation */


    $('.to-top').on('click', function () {
        $('html,body').animate({scrollTop: 0}, 'slow');
        return false;
    });

    $('.fullscreen a').on('click', function () {
        toggleFullScreen();
        return false;
    });

    $('.bordered-video .parallax').on('click', function (e) {
        var player = $(this).closest('.bordered-video').find('iframe');
        /*if (player.length > 0 && !!player[0].contentWindow) {
            player[0].contentWindow.postMessage({method: 'play'}, '*');
        }*/
        
        var videoURL = player.prop('src') + "?autoplay=1";
        player.prop('src', videoURL);
        $(this).parallax('disable');
        $(this).remove();
    });


    // CONTACTS PAGE
    $('.btn-customcare').on('click', function(){
        $('.contact-form').data('type', 'customercare');
    });
    
    $('#contactModal').on('hidden.bs.modal', function(){
        $('.contact-form').data('type', 'contacts');
    });
    
    $('.contact-form').validate({rules: contactFormRules});
    $('.contact-form').submit(function () {
        return sendRequest($(this), $(this).data('type'));
    });

    function showErrors(errors, $block) {
        if (!errors)
            return;
        var $content = $block.find('.alert-text');
        $content.html('');
        for (var k = 0, l = errors.length; k < l; ++k) {
            $content.append((k + 1) + '. ' + errors[k] + '<br>');
        }
        $block.show(500);
    }


    function sendRequest($this, type, extra)
    {
        if (!$this.valid())
            return false;
        var $button = $this.find('[type="submit"]');
        $button.button('loading');
        $this.find('.alert').hide();

        var data = $this.serializeObject();
        if (!!type)
            data.type = type;
        if (!!extra) {
            data.message = JSON.stringify({
                message: data.message,
                extra: extra
            });
        }
        
        // when a user chooses morning and afternoon call times
        if(type == 'callback') {
            if(data.call_time && Array.isArray(data.call_time)) {
                data.call_time = 3;
            }
        }
        
        // when a user chooses all services
        if(type == 'quote' && data.service && Array.isArray(data.service)) {
            data.service = data.service.join(', ');
        }

        $.ajax({
            url: baseUrl + '/contacts',
            data: data,
            type: 'POST',
            dataType: 'JSON',
            success: function (res) {
                if (res.valid) {
                    $this.find('.alert-success').show(500, function () {
                        setTimeout(function () {
                            $this.find('.alert-success').hide();
                            $this.closest('.modal-dialog').find('.close').trigger('click');
                        }, 5000);
                    });
                    $this[0].reset();
                } else {
                    showErrors(res.errors, $this.find('.alert-danger'));
                }
                $button.button('reset');
                $this.find('.jq-checkbox.checked').removeClass('checked');
            },
            error: function () {
                $button.button('reset');
                alert('Server error! Refresh page and try again, please.');
            }
        });

        return false;
    }


    // SUBSCRIBE FORM
    $('.subscribe-form').validate({rules: subscribeFormRules});
    $('.subscribe-form').submit(function () {
        var $form = $(this);
        $form.find('.email').removeClass('success');
        if (!$form.valid())
            return false;
        var $btn = $form.find('[type="submit"]');
        $btn.button('loading');
        var data = $form.serializeArray();
        $.post(baseUrl + '/subscribe', data, function (res) {
            if (res.status) {
                $form.find('[name]').val('');
                alert('You have been successfully subscribed on our updates');
            } else {
                $form.find('.email').addClass('error').removeClass('valid');
            }
            $btn.button('reset');
        }, 'JSON');
        return false;
    });

    //SHARES FUNCTIONALITY
    $('.share-entry').click(function (e) {
        e.preventDefault();
        var network = $(this).data('network');
        var action = $(this).closest('.article-shares').data('action');

        window.open($(this).prop('href'), '', 'width=600, height=350');
        $.post(action + '/' + network, function (res) {
        });
        return false;
    });

    // COMENTS FUNCTIONALITY
    var $comments = $('.comments');
    if ($comments.length > 0) {
        updateComments($comments);

        $comments.on('keyup change', '.comment-input', function () {
            var $charsCounter = $(this).closest('.comment-body').find('.chars-count');
            var max = $charsCounter.data('max');
            $charsCounter.text(max - $(this).val().length);
        });

        $comments.on('submit', '.comment-form', function () {
            var data = {};
            var $form = $(this);
            var $comment = $form.closest('.comment');
            var $button = $form.find('.comment-add-button');
            var $input = $form.find('.comment-input');
            $button.button('loading');
            data.content = $input.val();
            data.parent_id = 0;
            if ($form.closest('.comment:not(.comment-add)').length > 0)
                data.parent_id = $form.closest('.comment:not(.comment-add)').data('id');

            $.post($(this).prop('action'), data, function (res) {
                if (res.status == 1) {
                    $input.val('').trigger('change');
                    if ($comment.hasClass('comment-answer')) {
                        $comment.replaceWith(res.comment);
                    } else {
                        $comment.after(res.comment);
                    }

                    updateComments($comments);
                } else {
                    alert(res.msg);
                }
                $button.button('reset');
            }).error(function () {
                alert('Server error');
                $button.button('reset');
            });
            return false;
        });

        $comments.on('click', '.comment-reply', function () {
            var $comment = $(this).closest('.comment');
            var $form = $comment.find('> .media-body > .comment-add');
            if ($form.length <= 0) {
                $form = $('.comments .comment-add').first().clone();
                $form.addClass('comment-answer').css('display', 'none');
                $comment.find('> .media-body').append($form);
                updateComments($comments);
            }
            var hide = $form.is(':visible');
            $('.comment-answer').hide();
            if (!hide) {
                $form.show(0);
                if (isTwoColumns) {
                    adjustTwoColumns();
                }
                $form.hide(0);
                $form.slideDown(500, function () {
                    if (isTwoColumns) {
                        adjustTwoColumns();
                    }
                });
            }

            $('html,body').animate({
                scrollTop: $comment.offset().top},
            'slow');
        });

        $comments.find('.comment-icon img').on('error', function () {
            this.src = baseUrl + '/front/images/ava.png';
        });
    }

    function updateComments($comments)
    {
        $comments.find('.comment').removeClass('comment-last').last().addClass('comment-last');
        if (isTwoColumns) {
            adjustTwoColumns();
        }
    }


    // SUBSCRIBERS COUNTER IN THE HEADER

    $('.btn-subscribe').on('click', function () {
        var $counter = $('.subscribe-counter span');
        var count = parseInt($counter.text());
        $counter.text(++count);
        $.post(baseUrl + '/add-subscribe')
    });
    
    initForm($('.callback-form'), [{
        fieldName: 'user_phone',
        rule: {
            required: true,
            minlength: 2,
            maxlength: 50
        }
    },{
        fieldName: 'subject',
        rule: {
            required: true
        }
    }]);

    initForm($('.quote-form'), [{
        fieldName: 'user_website',
        rule: {
            required: true,
            minlength: 2,
            maxlength: 255
        }
    },{
        fieldName: 'description',
        rule: {
            required: true,
            minlength: 10,
            maxlength: 2000
        }
    },{
        fieldName: 'budget',
        rule: {
            required: true,
            maxlength: 255,
        }
    }]);
    
    function initForm($form, aditionalRules)
    {
        $form.find('input[type=checkbox]').styler();
        
        var rules = {
            user_name: {
                required: true,
                minlength: 1,
                maxlength: 255
            },
            user_surname: {
                required: true,
                minlength: 1,
                maxlength: 255
            },
            user_email: {
                required: true,
                minlength: 2,
                maxlength: 255,
                email: true
            }
        };
        
        aditionalRules.forEach(function(value){
            rules[value.fieldName] = value.rule;
        });
        
        var validator = $form.validate({
            rules: rules
        });
            
        $form.on('submit', function(){
            if(validator.form())
                return sendRequest($(this), $(this).data('type'));
            
            return false;
        });
    }
});



$.fn.serializeObject = function ()
{
    var o = {};
    var a = this.serializeArray();
    $.each(a, function () {
        if (o[this.name] !== undefined) {
            if (!o[this.name].push) {
                o[this.name] = [o[this.name]];
            }
            o[this.name].push(this.value || '');
        } else {
            o[this.name] = this.value || '';
        }
    });
    return o;
};

function isMobile()
{
    return navigator.userAgent.match(/(iPhone|iPod|iPad|Android|BlackBerry)/) || $(document).width() < 768;
}

function isTablet()
{
    return navigator.userAgent.match(/(iPhone|iPod|Android|BlackBerry)/) || $(document).width() < 992;
}

function toggleFullScreen() {
    if ((document.fullScreenElement && document.fullScreenElement !== null) ||
            (!document.mozFullScreen && !document.webkitIsFullScreen)) {
        if (document.documentElement.requestFullScreen) {
            document.documentElement.requestFullScreen();
        } else if (document.documentElement.mozRequestFullScreen) {
            document.documentElement.mozRequestFullScreen();
        } else if (document.documentElement.webkitRequestFullScreen) {
            document.documentElement.webkitRequestFullScreen(Element.ALLOW_KEYBOARD_INPUT);
        }
    } else {
        if (document.cancelFullScreen) {
            document.cancelFullScreen();
        } else if (document.mozCancelFullScreen) {
            document.mozCancelFullScreen();
        } else if (document.webkitCancelFullScreen) {
            document.webkitCancelFullScreen();
        }
    }
}