$(document).ready(function () {
    var $body = $('body');
    var $share = $('.mn-share');
    var $aboutPanel = $('.mn-panel');

    $aboutPanel.find('.mn-item').on('click', function(){
        var $panel = $(this).closest('.mn-panel');
        $aboutPanel.removeClass('opened');
        $panel.toggleClass('opened');
        $panel.find('.part-menu .links a').first().trigger('click');
    });
    $aboutPanel.find('.part-menu .links a').on('click', function(){
        var $panel = $(this).closest('.mn-panel');
        $panel.find('.part-submenu > div').hide().eq($(this).index()).slideDown();
    });
    
    $aboutPanel.find('.close').on('click', function(){
        var $panel = $(this).closest('.mn-panel');
        $panel.toggleClass('opened');
    });
    
    $aboutPanel.find('.part-submenu .signup').mCustomScrollbar({
        theme : 'light',
    });

    var owl = $(".owl").owlCarousel({
        navigation: true,
        navigationText: ['<i class="fa fa-angle-left"></i>', '<i class="fa fa-angle-right"></i>'],
        items: 4,
        pagination: false,
        itemsDesktop: [1199, 3],
        itemsDesktopSmall: [980, 2],
        itemsTablet: [768, 2],
        itemsTabletSmall: false,
        itemsMobile: [479, 1],
    });

    var $contents = $('.section-contents');
    var mouseWheelEvent = (/Firefox/i.test(navigator.userAgent)) ? "DOMMouseScroll" : "mousewheel";
    var timer = false;

    function contentsOpen()
    {
        $body.removeClass('mn-opened')
        $contents.addClass('opened');
        $contents.scrollTop(0);
    }
    function contentsClose()
    {
        $contents.removeClass('opened');
    }

    $('.contents-open').on('click', contentsOpen);
    $('.contents-close').on('click', contentsClose);

    $('body').on(mouseWheelEvent, function (e) {
        if (!timer && !$body.hasClass('mz-loading') && !$body.hasClass('mn-opened')) {
            timer = setTimeout(function () {
                var evt = window.event || e;
                evt = evt.originalEvent ? evt.originalEvent : evt;
                var delta = evt.detail ? evt.detail * (-40) : evt.wheelDelta;
                var isOpened = $contents.hasClass('opened');
                if (delta > 0) {
                    //scroll up
                    if (!isOpened) {
                        contentsOpen();
                    }
                    $contents.removeClass('scroll-closing');
                } else {
                    //scroll down
                    if (isOpened) {
                        if ($contents.scrollTop() + $contents.innerHeight() >= $contents[0].scrollHeight) {
                            if ($contents.hasClass('scroll-closing'))
                                contentsClose();
                            $contents.addClass('scroll-closing');
                        } else {
                            $contents.removeClass('scroll-closing');
                        }
                    } else {
                        var $link = $('.slider-magazine .item.active .article-discover');
                        $link.trigger('click');
                        window.location.href = $link.attr('href');
                    }
                }
                timer = false;
            }, 200);
        }

    });


    $('.mn-toggle').on('click', function () {
        $body.toggleClass('mn-opened');
    });

    $('.mn-item-toggle').on('click', function () {
        var $accordion = $(this).closest('.mn-item-accordion');
        if ($accordion.hasClass('issue-year-opened')) {
            $accordion.removeClass('issue-year-opened');
            $('.issue-year.active').removeClass('active');
        }
        $accordion.toggleClass('mn-item-opened');
    });

    $('.issue-year-toggle').on('click', function () {
        $(this).closest('.mn-item-accordion').toggleClass('issue-year-opened');
        $(this).closest('.issue-year').toggleClass('active')
    });

    $share.find('.toggle-btn').on('click', function () {
        $(this).find('.fa').toggleClass('fa-share-alt').toggleClass('fa-times');
        $share.toggleClass('opened');
    });


    $('.article-discover').on('click', function () {
        $body.addClass('mz-loading');
    });
});