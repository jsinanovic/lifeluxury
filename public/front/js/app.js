$(function() {
    'use strict';
    
    var SCROLLING_ANIMATION_DELAY = 100;
    
    init();
    
    function init()
    {
        var $header = $('.header');

        initFullpage();
        initSidebar($header.find('.sidebar-btn-open'));
        initPopup($header.find('.header-link-magazine'));
    }
    
    function initFullpage()
    {
        var $main = $('.main');
        
        if(!!$.fn.fullpage) {
            $main.fullpage({
                fitToSectionDelay: SCROLLING_ANIMATION_DELAY
            });

            $main.find('.section-btn-next-down').on('click', function(e) {
                e.preventDefault();
                $.fn.fullpage.moveSectionDown();
            }); 

            $main.find('.section-btn-next-up').on('click', function(e) {
                e.preventDefault();
                $.fn.fullpage.moveTo(1);
            });

            $main.find('.go-to-home').on('click', function(e) {
                e.preventDefault();
                $.fn.fullpage.moveTo(1);
            });
            $main.find('.go-to-collection').on('click', function(e) {
                e.preventDefault();
                $.fn.fullpage.moveTo(2);
            });
            $main.find('.go-to-rendezvous').on('click', function(e) {
                e.preventDefault();
                $.fn.fullpage.moveTo(3);
            });
            $main.find('.go-to-magazine').on('click', function(e) {
                e.preventDefault();
                debugger
                $.fn.fullpage.moveTo(4);
            });
            $main.find('.go-to-contact').on('click', function(e) {
                e.preventDefault();
                $.fn.fullpage.moveTo(5);
            });



            //$main.find('.section-button-move-to-5').on('click', function(e) {
            //    e.preventDefault();
            //    debugger
            //    //var $sidebar = $('.sidebar');
            //
            //    $.fn.fullpage.moveTo(4);
            //});

        }
    }
    
    function initSidebar($sidebarRtnOpen)
    {
        var $sidebar = $('.sidebar');
        
        $sidebarRtnOpen.on('click', function() {
            $sidebar.addClass('open');
        });
        
        $(document).on('click', function(e) {
            debugger
            var $el = $(e.target);
            if(!$el.closest('.sidebar,.sidebar-btn-open').length) {
                $sidebar.removeClass('open');
            }
        });
    }
    
    function initPopup($popupBtnOpen)
    {
        var $popup = $('.popup');

        $popupBtnOpen.on('click', function(e) {
            e.preventDefault();
            $popup.addClass('open');
        });

        $(document).on('click', function(e) {
            var $el = $(e.target);
            if(!$el.closest('.popup,.header-link-magazine').length) {
                $popup.removeClass('open');
            }
        });

        if(!!$.fn.swiper) {
            $popup.find('.popup-carousel').swiper({
                direction:'vertical',
                autoplay: 5000,
                autoplayDisableOnInteraction: false,
                speed: 700,
                loop: true,
                grabCursor: true,
                slidesPerView: 5
            });
        }
    }


    
});