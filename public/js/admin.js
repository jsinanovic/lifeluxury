// GENERAL FUNCTIONS
$.fn.serializeObject = function ()
{
    var o = {};
    var a = this.serializeArray();
    $.each(a, function () {
        if (o[this.name] !== undefined) {
            if (!o[this.name].push) {
                o[this.name] = [o[this.name]];
            }
            o[this.name].push(this.value || '');
        } else {
            o[this.name] = this.value || '';
        }
    });
    return o;
};


$('.clear-cache').on('click', function(){
    $.post('/admin/cache', function(){alert('Success')}).fail(function(){alert('Fail')});
});

var $msg = $('#msg-alert');
function msg(html) {
    $msg.find('.mb-content > p').html(html);
    $msg.addClass('open');
    var sound = $msg.data("sound");
    if (sound === 'alert')
        playAudio('alert');

    if (sound === 'fail')
        playAudio('fail');
}

function changeStatus($switch, id, callback, uri) {
    var $input = $switch.find('input');
    $input.attr('disabled', 'disabled');
    var requestUri = uri || moduleUri;
    $.post(requestUri + '/change-status', {id: id}, function (res) {
        $input.removeAttr('disabled');
        if (res.status) {
            if ($input.is(':checked'))
                $input.prop('checked', false);
            else
                $input.prop('checked', true);
            if (callback)
                callback($switch, id, res);
        } else {
            msg(
                    'Server error. Check your internet connection. Refresh page. Try again later!');
        }
    }, 'JSON').fail(function () {
        $input.removeAttr('disabled');
        msg(
                'Server error. Check your internet connection. Refresh page. Try again later!');
    });

    return false;
}


$(".sortable-table").sortable({
    revert: true,
    helper: "clone",
    appendTo: "body",
    items: 'tr:not(.active)',
    connectWith: ".connectedSortable",
    update: function(event, ui){
        sortEntry($(ui.item));
    }
});

function sortEntry($entry) {
    var data = {};
    data.id = $entry.data('id');
    data.prev = $entry.prev('tr:not(.active)').data('id') || 0;
    data.next = $entry.next('tr:not(.active)').data('id') || 0;
    $.post(moduleUri + '/sort', data, function (res) {
        if (res.status != 1) {
            alert(res.error);
        }
    });
}

function removeEntry($row, id, callback, uri) {
    customConfirm(function(){
        var requestUri = uri || moduleUri;
        $.post(requestUri + '/remove', {id: id}, function (res) {
            if (res.status) {
                $row.fadeOut(500, function () {
                    if(!!callback)
                        callback($row, id, res);
                });
            }
        });
    });
}

function deleteRow($row, callback) {
    customConfirm(function(){
        $row.hide("slow", function () {
            $row.remove();
            if(!!callback)
                callback();
        });
    });
}

function customConfirm(callback)
{
    var box = $('#mb-remove-row');
    if (box.length <= 0) {
        var box = $(
                '<div class="message-box animated fadeIn" data-sound="alert" id="mb-remove-row"><div class="mb-container"><div class="mb-middle"><div class="mb-title"><span class="fa fa-times"></span> Remove <strong>Data</strong> ?</div><div class="mb-content"><p>Are you sure you want to remove this row?</p><p>Press Yes if you sure.</p></div><div class="mb-footer"><div class="pull-right"><button class="btn btn-success btn-lg mb-control-yes">Yes</button> <button class="btn btn-default btn-lg mb-control-close">No</button></div></div></div></div></div>');
        box.appendTo('body');
    }
    box.addClass("open");
    box.find(".mb-control-yes").on("click", function () {
        box.removeClass("open");
        callback();
    });
    box.find(".mb-control-close").on("click", function () {
        box.remove();
    });
}

function initSummernote($selector, type)
{
    type = type || 'default';
    if(type == 'default') {
        $selector.summernote({
            height: 150,
            width: '100%',
            toolbar: [
                ['headline', ['style']],
                ['style', ['bold', 'italic', 'underline', 'superscript',
                        'subscript', 'strikethrough', 'clear']],
                ['fontface', ['fontname']],
                ['textsize', ['fontsize']],
                ['fontclr', ['color']],
                ['alignment', ['ul', 'ol', 'paragraph', 'lineheight']],
                ['height', ['height']],
                ['insert', ['link', 'hr']],
                ['view', ['fullscreen', 'codeview']],
                ['help', ['help']]
            ]
        });
    } else if (type == 'minimal') {
        $selector.summernote({
            height: 150,
            width: '100%',
            toolbar: [
                ['style', ['bold', 'italic', 'underline', 'strikethrough', 'clear']],
                ['textsize', ['fontsize']],
                ['fontclr', ['color']],
                ['alignment', ['ul', 'ol', 'paragraph', 'lineheight']],
                ['height', ['height']],
                ['insert', ['link', 'hr']],
                ['view', ['fullscreen', 'codeview']],
            ]
        });
    } else if (type == 'full') {
        $selector.summernote({
            height: 150,
            width: '100%',
            codemirror: {
                mode: 'text/html',
                htmlMode: true,
                lineNumbers: true,
                theme: 'default',
            }, 
            onImageUpload: function(files, editor, welEditable) {
                console.log(files);
                sendFile(files[0],editor,welEditable);
            },
            toolbar: [
                ['style', ['style']],
                ['font', ['bold', 'italic', 'underline', 'clear']],
                ['fontname', ['fontname']],
                ['textsize', ['fontsize']],
                ['color', ['color']],
                ['para', ['ul', 'ol', 'paragraph']],
                ['height', ['height']],
                ['table', ['table']],
                ['insert', ['link', 'picture', 'video', 'hr']],
                ['view', ['fullscreen', 'codeview']],
                ['help', ['help']]
              ],
        });
    }
}


function sendFile(file, editor, welEditable) {
    var data = new FormData();
    data.append("file", file);
    $.ajax({
        data: data,
        type: "POST",
        dataType: 'JSON',
        url: moduleUri + "/assign-image/" + entryId,
        cache: false,
        contentType: false,
        processData: false,
        success: function (res) {
            if (res.status == 1) {
                addAssigned(res.id);
                editor.insertImage(welEditable, res.url);
            } else
                alert(res.msg);
        }
    });
}

function addAssigned(url)
{
    var $input = $('[name="assigned[]"]').last();
    $input.after($input.clone());
    $input.val(url);
    //var values = [];
}
// END OF GENERAL FUNCTIONS


// FEATURES FUNCTIONALITY
$('.features').each(function () {
    initFeatures($(this));
});

function initFeatures($panel)
{
    var json = $panel.find('input[type="hidden"]').first().val();
    if (!!json) {
        var data = JSON.parse(json);
        var $item = '';
        var $list = $panel.find('.list-group');
        for (var k = 0, n = data.length; k < n; ++k) {
            $item = createFeatureItem(data[k].name, data[k].content);
            $item.appendTo($list);
        }
    }
}

function saveFeatures($panel)
{
    var data = [];
    $panel.find('.list-group-item').each(function () {
        data.push({
            'name': $(this).find('.contacts-title').text(),
            'content': $(this).find('.feature-description').html(),
        });
    });
    $panel.find('input[type="hidden"]').first().val(JSON.stringify(data));
}

function createFeatureItem(name, content)
{
    var $item = $(
            '<div class="list-group-item"><span class="contacts-title"></span><p class="feature-description"></p><div class="list-group-controls"><button class="btn btn-primary btn-rounded feature-edit"><span class="fa fa-pencil"></span></button> <div class="btn btn-danger btn-rounded feature-remove"><span class="fa fa-trash-o"></span></div></div></div>');
    $item.find('.contacts-title').text(name);
    $item.find('.feature-description').html(content);
    return $item;
}

function initFeaturesForm($addForm)
{
    $addForm.find('.feature-cancel').click(function () {
        $addForm.hide();
    });

    $addForm.submit(function () {
        if ($addForm.valid()) {
            var name = $addForm.find('.contacts-title').val();
            var content = $addForm.find('.summernote').code();
            var $item = createFeatureItem(name, content);
            $addForm.replaceWith($item);
            var $panel = $item.closest('.panel');
            saveFeatures($panel);
        }
        return false;
    });

    initSummernote($addForm.find('.summernote'), 'full');
    
    $addForm.validate({
        rules: {
            name: {required: true, minlength: 1, maxlength: 255}
        },
    });
}

$('.features').on('click', '.feature-remove', function () {
    var $row = $(this).closest('.list-group-item');
    var $panel = $row.closest('.panel')
    customConfirm(function(){
        $row.hide("slow", function () {
            $row.remove();
            saveFeatures($panel);
        });
    });
    return false;
});

$('.features').on('click', '.feature-edit', function () {
    var $item = $(this).closest('.list-group-item');
    var $oldItem = $item.clone();
    var name = $item.find('.contacts-title').text();
    var content = $item.find('.feature-description').html();
    var $addForm = $(
            '<form class="feature-form list-group-item clearfix"><div class="form-group"><label style="width: 100%;"> Feature Name: <input class="form-control contacts-title" type="text" name="name"/></label></div><div class="form-group"><label> Feature Content: <textarea class="summernote feature-description" name="content"></textarea></label></div><div class="btn btn-default pull-left feature-cancel">Cancel</div><input type="submit" value="Save feature" class="btn btn-primary pull-right feature-save"></form>');
    $addForm.find('[name="name"]').val(name);
    $addForm.find('[name="content"]').val(content);
    $item.replaceWith($addForm);
    initFeaturesForm($addForm);
    $addForm.find('.feature-cancel').unbind('click').click(function () {
        $addForm.replaceWith($oldItem);
    });
    return false;
});

$('.feature-add').click(function () {
    var $panel = $(this).closest('.panel');
    if ($panel.find('.feature-form').length > 0) {
        $panel.find('.feature-form').show();
        return;
    }

    var $addForm = $(
            '<form class="feature-form list-group-item clearfix"><div class="form-group"><label style="width: 100%;"> Feature Name: <input class="form-control contacts-title" type="text" name="name"/></label></div><div class="form-group"><label> Feature Content: <textarea class="summernote feature-description" name="content"></textarea></label></div><div class="btn btn-default pull-left feature-cancel">Cancel</div><input type="submit" value="Save feature" class="btn btn-primary pull-right feature-save"></form>');
    $panel.find('.list-group').append($addForm);
    initFeaturesForm($addForm);

});

$(".sortable").sortable({
    revert: true,
    helper: "clone",
    appendTo: "body",
    items: '> div',
    stop: function () {
        saveFeatures($(this).closest('.panel'));
    }
});
// END FEATURES FUNCTIONALITY

// TEMPLATE CONTROL
$(document).ready(function () {
    var $templateControl = $('.template-control');
    var $templateSelect = $('select.template-select');
    var templatesSettings = {
            1 : ['img-1', 'text-1', 'img-2', 'text-2', 'img-3', 'text-3', 'img-4', 'text-4', 'img-5', 'text-5', 'img-6', 'text-6'],
            2 : ['img-1', 'text-1', 'img-2', 'text-2', 'img-3', 'text-3', 'img-4', 'text-4', 'img-5', 'text-5', 'img-6', 'text-6'],
            3 : ['img-1', 'text-1', 'img-2', 'text-2', 'text-3', 'text-4', 'text-5', 'img-3', 'img-4', 'text-6', 'img-5',  'text-7'],
            4 : ['img-1', 'text-1', 'img-2', 'text-2', 'img-3', 'text-3', 'img-4', 'text-4', 'img-5', 'text-5', 'img-6', 'text-6', 'img-7', 'text-7'],
            5 : ['img-1', 'text-1', 'img-2', 'text-2', 'img-3', 'text-3', 'img-4', 'text-4', 'img-5', 'img-6', 'text-5', 'img-7', 'img-8', 'text-6', 'img-9', 'text-7'],
            6 : ['img-1', 'text-1', 'img-2', 'text-2', 'text-3', 'img-3', 'text-4', 'img-4', 'text-5', 'img-5', 'text-6', 'text-7', 'img-6', 'text-8'],
            7 : ['img-1', 'text-1', 'img-2', 'text-2', 'img-3', 'text-3'],
            8 : ['img-1', 'text-1', 'img-2', 'text-2', 'img-3', 'text-3', 'img-4', 'text-4', 'img-5', 'text-5',],
            9 : ['text-1', 'text-2', 'img-1', 'text-3', 'text-4', 'img-2', 'text-5', 'img-3', 'text-6', 'img-4', 'text-7', 'text-8', 'text-9', 'img-5'],
            10 : ['img-1', 'text-1', 'img-2', 'text-2', 'img-3', 'text-3', 'img-4', 'text-4', 'img-5', 'text-5', 'img-6', 'text-6', 'img-7', 'text-7', 'img-8', 'text-8', 'img-9', 'text-9'],
            
            11 : ['img-1', 'text-1', 'img-2', 'text-2', 'img-3', 'text-3', 'img-4', 'text-4', 'img-5', 'text-5', 'img-6', 'text-6', 'img-7', 'text-7', 'img-8', 'text-8', 'img-9'],
            12 : ['img-1', 'text-1', 'img-2', 'text-2', 'img-3', 'text-3', 'img-4', 'text-4', 'img-5', 'text-5', 'img-6', 'text-6', 'img-7', 'text-7',],
            13 : ['img-1', 'text-1', 'img-2', 'text-2', 'img-3', 'text-3', 'img-4', 'text-4', 'img-5', 'text-5', 'img-6', 'text-6', 'img-7', 'text-7', 'text-8'],
            14 : ['img-1', 'text-1', 'img-2', 'text-2', 'img-3', 'text-3', 'img-4', 'text-4', 'img-5', 'text-5', 'img-6', 'text-6', 'img-7', 'text-7', 'text-8', 'text-9'],
            15 : ['img-1', 'text-1', 'img-2', 'text-2', 'img-3', 'text-3', 'img-4', 'text-4', 'img-5', 'text-5', 'img-6', 'text-6', 'img-7', 'text-7', 'img-8', 'text-8', 'img-9', 'text-9'],
            16 : ['img-1', 'text-1', 'img-2', 'text-2', 'img-3', 'text-3', 'img-4', 'text-4', 'img-5', 'text-5',],
            17 : ['img-1', 'text-1', 'img-2', 'text-2', 'img-3', 'text-3', 'img-4', 'text-4', 'img-5', 'text-5', 'img-6', 'text-6', 'img-7', 'img-8',],
            18 : ['img-1', 'text-1', 'img-2', 'text-2', 'img-3', 'text-3', 'img-4', 'text-4', 'img-5', 'text-5', 'img-6', 'text-6', 'img-7', 'text-7', 'img-8', 'text-8', 'img-9', 'text-9'],
    };
    //$templateControl.find('.feature-add').remove();
    //$templateControl.find('.feature-remove').remove();
    $templateSelect.on('change', function () {
        var template = $(this).val();
        var templateSettings = templatesSettings[template];
        var json = $templateControl.find('input[type="hidden"]').first().val();
        var data = [];
        if (!!json) {
            data = JSON.parse(json);
        }
        var $item = '';
        var $list = $templateControl.find('.list-group');
        $list.html('');
        for (var k = 0, n = templateSettings.length; k < n; ++k) {
            var content = '';
            for (var p = 0, l = data.length; p < l; ++p) {
                if (data[p].name == templateSettings[k])
                    content = data[p].content;
            }
            $item = createFeatureItem(templateSettings[k], content);
            $item.appendTo($list);
        }
        //$templateControl.find('.feature-remove').remove();
    });
    
    $templateSelect.trigger('change');
});
// END TEMPLATE CONTROL

// CUSTOM PRICES FIELD
$(document).ready(function () {

    //var keys = [];
    $('[data-prices]').each(function () {
        initPricesField($(this), $(this).data('key'))
        //keys.push($(this).data('key'));
    });

    function initPricesField($custom, customkey)
    {
        var $field = $('[name="' + customkey + '"]');
        var initData = $field.val();
        if (!!initData) {
            var prices = JSON.parse(initData);
            for (var k = 0; k < prices.length; ++k) {
                var price = prices[k];
                var $licenseBlock = $('.license-' + price.license);
                var $priceBlock = $licenseBlock.find('.prices-item').not('.inited').first();
                $priceBlock.find('.prices-name').val(price.name);
                $priceBlock.find('.prices-detail').each(function (num) {
                    if (num < price.details.length) {
                        $(this).val(price.details[num]);
                    } else {
                        return false;
                    }
                });
                $priceBlock.find('.prices-value').val(price.value);
                $priceBlock.find('.prices-link').val(price.link);
                $priceBlock.find('.prices-button').val(price.button);
                if (price.active)
                    $priceBlock.find('.prices-active').prop('checked', 'checked');
                $priceBlock.addClass('inited');
            }
        }

        $custom.closest('form').submit(function () {
            var prices = [];
            var price = {};
            $custom.find('.prices-item').each(function () {
                price = {};
                price.name = $(this).find('.prices-name').val();
                if (!price.name)
                    return true;
                price.details = [];
                $(this).find('.prices-detail').each(function () {
                    var detail = $(this).val();
                    if (!!detail)
                        price.details.push(detail);
                });
                price.value = $(this).find('.prices-value').val();
                price.active = $(this).find('.prices-active').is(':checked');
                price.link = $(this).find('.prices-link').val();
                price.button = $(this).find('.prices-button').val();
                price.license = $(this).closest('.prices-license').data('key');
                prices.push(price);
            });

            $field.val(JSON.stringify(prices));
        });
    }
    
    var $moduleForm = $('.form-for-module');
    if($moduleForm.length > 0) {
        var $nameField = $moduleForm.find('[name="name"]');
        var $modelField  = $moduleForm.find('[name="model"]');
        var $uriField  = $moduleForm.find('[name="uri"]');
        
        $nameField.change(function(){
            var name = $nameField.val();
            
            if(!$uriField.val()) {
                $uriField.val(name.toLowerCase());
            }
            
            if(!$modelField.val()) {
                $modelField.val(name.slice(0, -1));
            }
        });
    }
    
});