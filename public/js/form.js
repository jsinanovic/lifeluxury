// DROPZONE FOR GALLERY
Dropzone.autoDiscover = false;
$(function () {
    $('.dropzone').each(function () {
        var url = $(this).data('url');
        var myDropzone = new Dropzone($(this).get(0), {url: url});
        $(this).find('+ .dropzone-files > li').each(function () {
            var obj = {
                'id': $(this).data('id'),
                'name': $(this).text(),
                'size': $(this).data('size') * 1024
            };
            console.log(obj);
            myDropzone.emit("addedfile", obj);
            myDropzone.emit("thumbnail", obj, $(this).data('url'));
            myDropzone.emit("complete", obj);
        });
        myDropzone.on("success", function (file, res) {
            if (res.status) {
                file.id = res.id;
            } else {
                alert('Error: ' + res.msg);
                myDropzone.removeFile(file);
            }
        });
        myDropzone.on("removedfile", function (file) {
            if (file.id) {
                $.post(moduleUri + '/remove-gallery-item/' + file.id);
            }
        });
    });
});
// FILEINPUT FUNCTIONALITY

$('.file').each(function () {
    var initalPreview = [];
    if (previewFolder) {
        var $input = $(this);
        var $control = $(this).closest('.file-control');
        var filename = $control.find('.file-name').val();
        if (!!filename) {
            var img = "<img src='" + previewFolder + '/' + filename + "?" + new Date().getTime() + "' class='file-preview-image'>";
            initalPreview.push(img);
        }
    }

    $input.fileinput({
        showUpload: false,
        showCaption: true,
        browseClass: "btn btn-danger",
        fileType: "any",
        initialPreview: initalPreview,
        allowedFileExtensions: ['jpg', 'png'],
    });
});
$('html').on('click', '.fileinput-remove', function () {
    var name = $(this).closest('.file-control').find('.file').prop('name');
    $('[name="' + name + '_name"]').val('');
});
// FILEINPUT END

// SUMMERNOTE FUNCTIONALITY 
$(".summernote").summernote({height: 250,
    codemirror: {
        mode: 'text/html',
        htmlMode: true,
        lineNumbers: true,
        theme: 'default',
    },
    onImageUpload: function (files, editor, welEditable) {
        sendFile(files[0], editor, welEditable);
    },
    toolbar: [
        ['style', ['style']],
        ['font', ['bold', 'italic', 'underline', 'clear']],
        ['fontname', ['fontname']],
        ['color', ['color']],
        ['para', ['ul', 'ol', 'paragraph']],
        ['height', ['height']],
        ['table', ['table']],
        ['insert', ['link', 'picture', 'video', 'hr']],
        ['view', ['fullscreen', 'codeview']],
        ['help', ['help']]
    ],
});


function sendFile(file, editor, welEditable) {
    data = new FormData();
    data.append("file", file);
    $.ajax({
        data: data,
        type: "POST",
        dataType: 'JSON',
        url: "{{URL::to($moduleUri)}}/assign-image/" + entryId,
        cache: false,
        contentType: false,
        processData: false,
        success: function (res) {
            if (res.status == 1) {
                addAssigned(res.id);
                editor.insertImage(welEditable, res.url);
            } else
                alert(res.msg);
        }
    });
}

function addAssigned(url)
{
    var $input = $('[name="assigned[]"]').last();
    $input.after($input.clone());
    $input.val(url);
    var values = [];
}
// SUMMERNOTE END 

/*var from = $('.slider-default').val();
 $('.slider-default').ionRangeSlider({
 min: 0,
 max: 500,
 postfix: "%",
 from: from
 });*/

CKEDITOR.disableAutoInline = true;
$('.advanced-html').ckeditor({
    
});