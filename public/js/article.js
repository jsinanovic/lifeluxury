;(function(){
    
    var editor;
    var $contentSection = $('section.content');
    var isEditingMode = false;
    enableEditingMode();
    
    $(".owl").owlCarousel({
        navigation: true,
        navigationText: ['<i class="fa fa-angle-left"></i>', '<i class="fa fa-angle-right"></i>'],
        items: 4,
        pagination: false,
        itemsDesktop: [1199, 3],
        itemsDesktopSmall: [980, 2],
        itemsTablet: [768, 2],
        itemsTabletSmall: false,
        itemsMobile: [479, 1],
    });
    
    $('.article-repeat').on('click', function(){
        $('html,body').animate({scrollTop: 0}, 'slow');
        return false;
    });
    
    var btnText = 'Save Content';
    $('.btn-content-save').on('click', function() {
        var $btn = $(this).button('loading');
        var content = '';
        if(isEditingMode) {
            content= editor.getData();
        } else {
            disableSortingMode();
            content = $contentSection.html();
            enableEditingMode();
        }
        
        $.post('/admin/articles/content/' + articleId, {content: content}, function(res) {
            if(+res == 1) {
                $btn.text('Success');
            } else {
                $btn.text('Unsuccess');
            }
            
            setTimeout(function(){
                $btn.button('reset');
                $btn.text(btnText);
            }, 1000);
            
        }).fail(function(){
            alert('Saving error!');
            $btn.button('reset');
        });
    });
    
    $('.template-item').on('click', function(){
        var templateContent = $(this).find('.template-item-content').html();
        if(isEditingMode) {
            editor.setData(editor.getData() + templateContent);
        } else {
            disableSortingMode();
            $contentSection.append(templateContent);
            enableSortingMode();
        }
        $('#templateModal').modal('hide');
        $('html, body').animate({
            scrollTop: $contentSection.find('> *').last().offset().top
        })
    });
    
    $('.btn-content-sort').on('click', function(){
        if(!$(this).hasClass('active')) {
            disableEditingMode();
            enableSortingMode();
        } else {
            disableSortingMode();
            enableEditingMode();
        }
        
    });
    
    function enableEditingMode()
    {
        isEditingMode = true;
        editor = $contentSection.attr('contenteditable', true).ckeditor({
            //filebrowserBrowseUrl: '/browser/browse.php',
            filebrowserUploadUrl: '/admin/articles/upload/' + articleId
        }, function(){
            
        }).editor;
    }
    
    function disableEditingMode()
    {
        isEditingMode = false;
        $contentSection.attr('contenteditable', false);
        editor.destroy();
    }
    
    function enableSortingMode()
    {
        $('.btn-content-sort').addClass('active');
        
        $contentSection.find('.item, .s').each(function(){
            var $control = $('<div class="item-admin-control">');
            var $moveUp = $('<div class="btn btn-primary item-move-up">').append('<i class="fa fa-arrow-up"></i>');
            var $moveDown = $('<div class="btn btn-primary item-move-down">').append('<i class="fa fa-arrow-down"></i>');
            var $delete = $('<div class="btn btn-primary item-remove">').append('<i class="fa fa-trash-o"></i>');
            $control.append($moveUp).append($moveDown).append($delete);
            $(this).addClass('item-admin').append($control);
        });
        
        $contentSection.on('click.admin', '.item-move-up', function(){
            var $item = $(this).closest('.item, .s');
            if($item.prev().length > 0) {
                $item.prev().before($item);
            }
        });
        
        $contentSection.on('click.admin', '.item-move-down', function(){
            var $item = $(this).closest('.item, .s');
            if($item.next().length > 0) {
                $item.next().after($item);
            }
        });
        
        $contentSection.on('click.admin', '.item-remove', function(){
            var $item = $(this).closest('.item, .s');
            if(confirm('Would you like to remove this item?')) {
                $item.remove();
            }
        });
        
        /*$contentSection.sortable({
            //items: '.item',
            placeholder: "ui-state-highlight"
        });*/
    }
    
    function disableSortingMode()
    {
        $('.btn-content-sort').removeClass('active');
        $contentSection.off('click.admin');
        $contentSection.find('.item, .s').removeClass('item-admin').find('.item-admin-control').remove();
        //$contentSection.sortable("destroy");
    }
    
})();